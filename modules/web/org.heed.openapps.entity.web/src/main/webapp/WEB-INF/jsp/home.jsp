<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
	var mapping_processors = {'none':'Not Included','text':'Text/String','number':'Number','relation':'Relationship'};
	var file_formats = {'spreadsheet':'Spreadsheet','oaxml':'OpenAppsXML'}
	
	var service_path = '<c:out value="${openappsUrl}" />';
</script>

	<script type="text/javascript">
    	var isomorphicDir = "/theme/script/sc/";
	</script>
	<script src="/theme/script/sc/modules/ISC_Core.js"></script>
	<script src="/theme/script/sc/modules/ISC_Foundation.js"></script>
	<script src="/theme/script/sc/modules/ISC_Containers.js"></script>
	<script src="/theme/script/sc/modules/ISC_Grids.js"></script>
	<script src="/theme/script/sc/modules/ISC_Forms.js"></script>
	<script src="/theme/script/sc/modules/ISC_RichTextEditor.js"></script>
	<script src="/theme/script/sc/modules/ISC_DataBinding.js"></script>
	<script src="/theme/script/sc/modules/ISC_Calendar.js"></script>
	<script src="/theme/script/sc/modules/ISC_Drawing.js"></script>
    
    <script type="text/javascript" src="/theme/script/sc/skins/Enterprise/load_skin.js?isc_version=9.0.js"></script>
    
<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
<div id="gwt" style="width:100%;margin-bottom:10px;min-height:500px;"></div>
<script type="text/javascript" language="javascript" src="EntityManager/EntityManager.nocache.js"></script>