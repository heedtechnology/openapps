<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%
response.setContentType("text/xml");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("pragma","no-cache");
%>
<response>
	<status><c:out value="${status}" /></status>
	<message></message>
	<data>
		<c:forEach items="${properties}" var="item">
			<node>
				<group><c:out value="${item.group}" /></group>
				<name><c:out value="${item.name}" /></name>
				<value><c:out value="${item.value}" /></value>
			</node>
		</c:forEach>
	</data>
</response>