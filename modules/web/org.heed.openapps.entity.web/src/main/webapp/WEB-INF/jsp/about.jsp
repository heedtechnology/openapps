The Entity Manager controls the data model for your application.  
Administrators can upload and edit the data dictionary controlling how entities are added and updated.