package org.heed.openapps.entity.client.data.cleaning;

import org.heed.openapps.entity.client.data.ProgressTrackingWizardView;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class CleaningWizardPanel extends HLayout {
	private Canvas pane1;
	private Canvas pane2;	
	private Canvas pane3;
	
	private CleaningChoiceWizardView view1;	
	private CleaningSelectionsWizardView view2;
	
	private ProgressTrackingWizardView tracker;
	
	public static final int MODE_CLEAN = 1;
	public static final int MODE_UPLOAD = 2;
		
	
	public CleaningWizardPanel() {
		setWidth100();
		setHeight100();
		setMargin(1);
		setBorder("1px solid #A7ABB4");
		
		HLayout paneLayout = new HLayout();
		paneLayout.setWidth100();
		paneLayout.setHeight100();
		addMember(paneLayout);
		
		pane1 = new Canvas();
		pane1.setWidth(300);
		pane1.setHeight100();
		pane1.setMargin(10);
		pane1.setBorder("1px solid #A7ABB4");
		paneLayout.addMember(pane1);
		
		view1 = new CleaningChoiceWizardView(this);
		pane1.addChild(view1);
		
		pane2 = new Canvas();
		pane2.setWidth(300);
		pane2.setHeight100();
		pane2.setMargin(10);
		pane2.setBorder("1px solid #A7ABB4");
		pane2.hide();
		paneLayout.addMember(pane2);
				
		view2 = new CleaningSelectionsWizardView(this);
		pane2.addChild(view2);
		
		pane3 = new Canvas();
		pane3.setWidth(300);
		pane3.setHeight100();
		pane3.setMargin(10);
		pane3.setBorder("1px solid #A7ABB4");
		pane3.hide();
		paneLayout.addMember(pane3);
		
		tracker = new ProgressTrackingWizardView();
		pane3.addChild(tracker);
		
		ImgButton button = new ImgButton();
		button.setSrc("/theme/images/icons16/deploy_failed.gif");
		button.setPrompt("Close");
		button.setWidth(16);
		button.setHeight(16);
		//button.setMargin(2);
		button.setShowDown(false);
		button.setShowRollOver(false);
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();
			}			
		});
		
		addMember(button);
	}
	
	public void show(int mode) {
		hideAll();
		if(mode == MODE_CLEAN) {
			view1.show();
			pane1.show();
		} else if(mode == MODE_UPLOAD) {
			pane1.show();
		}
		show();
	}
	public void hideAll() {
		pane1.hide();
		pane2.hide();
		pane3.hide();
		view1.hide();
		view2.hide();
		tracker.hide();
	}
	public void updateJob(String uid, String message) {
		tracker.trackJobStatus(uid, message);
	}
	public void addMessage(int status, String message) {
		if(status == -1) tracker.showMessage("error", message);
		else tracker.showMessage("success", message);
	}
	
		
	public Canvas getPane1() {
		return pane1;
	}
	public Canvas getPane2() {
		return pane2;
	}
	public Canvas getPane3() {
		return pane3;
	}
	
	public CleaningChoiceWizardView getView1() {
		return view1;
	}
	public CleaningSelectionsWizardView getView2() {
		return view2;
	}
	
	public ProgressTrackingWizardView getTracker() {
		return tracker;
	}
	
}
