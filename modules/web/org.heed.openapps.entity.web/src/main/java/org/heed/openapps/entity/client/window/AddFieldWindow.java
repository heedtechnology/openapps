package org.heed.openapps.entity.client.window;

import org.heed.openapps.entity.client.form.FieldForm;

import com.smartgwt.client.widgets.Window;


public class AddFieldWindow extends Window {
	private FieldForm form;
	
	
	public AddFieldWindow() {
		setWidth(400);
		setHeight(265);
		setTitle("Add Field");
		setAutoCenter(true);
		setIsModal(true);
		
		form = new FieldForm(true, false);
		form.setWidth(360);
		form.setHeight100();
		form.setMargin(5);
		addItem(form);
	}
	
	public void setNamespace(String namespace) {
		form.setValue("namespace", namespace);
	}
}