package org.heed.openapps.entity.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class ModelRelationDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static ModelRelationDS instance = null;  
	  
    public static ModelRelationDS getInstance() {  
        if (instance == null) {  
            instance = new ModelRelationDS("modelRelationDS");  
        }  
        return instance;  
    }
    
	public ModelRelationDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/dictionary/relation/fetch.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/dictionary/relation/add.json"); 
        setRemoveDataURL(oa.getServiceUrl()+"/service/entity/dictionary/relation/remove.json");
        setUpdateDataURL(oa.getServiceUrl()+"/service/entity/dictionary/relation/update.json");
	}
}
