package org.heed.openapps.entity.client;
import java.util.Map;

import org.heed.openapps.entity.client.component.EntityManagerList;
import org.heed.openapps.entity.client.dictionary.ModelPanel;
import org.heed.openapps.entity.client.dictionary.data.DictionaryListDS;
import org.heed.openapps.entity.client.dictionary.data.ModelDS;
import org.heed.openapps.entity.client.window.AddModelWindow;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DictionaryManager extends HLayout {
	private EntityManagerList list;
	private ModelDS modelDS = ModelDS.getInstance();
	private ModelPanel modelPanel;
	//private SelectItem fieldSelector;
	
	private AddModelWindow addModelWindow;
	
	public DictionaryManager() {
		setWidth100();
		setHeight100();
		setMembersMargin(1);
		
		VLayout leftSide = new VLayout();
		leftSide.setWidth(300);
		leftSide.setHeight100();
		leftSide.setMembersMargin(1);
		leftSide.setShowResizeBar(true);
		addMember(leftSide);
		/*
		DynamicForm form1 = new DynamicForm();
		form1.setWidth100();
		form1.setColWidths(65, "*");
		fieldSelector = new SelectItem("dictionary", "Dictionary");
		fieldSelector.setWidth(225);
		fieldSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				//list.setData(fields.get(event.getValue()));
			}			
		});
		form1.setFields(fieldSelector);
		leftSide.addMember(form1);
		*/
		Toolbar toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		leftSide.addMember(toolbar);
						
		toolbar.addButton("upload_entities", "/theme/images/icons32/database_add.png", "Import Models", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.UPLOAD_MODEL));
	    	}  
	    });
		toolbar.addButton("upload_entities", "/theme/images/icons32/database_go.png", "Export Models", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.UPLOAD_MODEL));
	    	}  
	    });
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add Model", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.CREATE_MODEL));
			}  
		});
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete Model", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_MODEL));
			}  
		});
				
		list = new EntityManagerList();
		list.setWidth100();
		list.setHeight100();
		list.setDataSource(DictionaryListDS.getInstance());
		leftSide.addMember(list);
                
		Canvas panelCanvas = new Canvas();
		panelCanvas.setWidth100();
		panelCanvas.setHeight100();
		//panelCanvas.setBorder("1px solid #A7ABB4");
		panelCanvas.setMargin(2);
		addMember(panelCanvas);
		
		modelPanel = new ModelPanel();
		modelPanel.setVisible(false);
		panelCanvas.addChild(modelPanel);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		select(event.getRecord());
	        	} else if(event.isType(EntityEventTypes.CREATE_MODEL)) {
	        		addModelWindow.show();
	        	} else if(event.isType(EntityEventTypes.ADD_MODEL)) {
	        		list.addData(event.getRecord());
	        		list.sort();
	        		addModelWindow.hide();       	
	        	} else if(event.isType(EntityEventTypes.DELETE_MODEL)) {
	        		list.removeSelectedData();
	        	} else if(event.isType(EntityEventTypes.SAVE_MODEL)) {
	        		list.updateData(modelPanel.getData());
	        	}
	        }
		});		
		addModelWindow = new AddModelWindow();
	}
	
	public void add() {
		addModelWindow.show();
	}
	public void delete() {
		if(list.getSelectedRecord() != null) {
			list.removeSelectedData();
		}
	}
	public void update() {
		Record data = modelPanel.getData();
		list.updateData(data);
	}
	public void fetchData(final DSCallback callback) {
		Criteria criteria = new Criteria();
		list.fetchData(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(callback != null) callback.execute(response, rawData, request);
			}
		});
	}
	protected void select(Record record) {
		final String type = record.getAttribute("type");
		Criteria criteria = new Criteria();
		criteria.addCriteria("id", record.getAttribute("id"));
		modelDS.fetchData(criteria, new DSCallback() {
			@SuppressWarnings("unchecked")
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					if(type.equals("model")) {
						Record rec = response.getData()[0];
						Map<String,Object> qname = rec.getAttributeAsMap("qname");
						if(qname != null) {
							rec.setAttribute("namespace", qname.get("namespace"));
							rec.setAttribute("localName", qname.get("localName"));
						}
						Record[] fields = rec.getAttributeAsRecordArray("fields");
						for(Record field : fields) {
							Map<String,Object> fieldQname = field.getAttributeAsMap("qname");
							if(fieldQname != null) {
								field.setAttribute("namespace", fieldQname.get("namespace"));
								field.setAttribute("localName", fieldQname.get("localName"));
							}
						}
						Record[] relations = rec.getAttributeAsRecordArray("relations");									
						for(Record relation : relations) {
							Map<String,Object> relationQname = relation.getAttributeAsMap("qname");
							if(relationQname != null) {
								relation.setAttribute("namespace", relationQname.get("namespace"));
								relation.setAttribute("localName", relationQname.get("localName"));
							}
							Map<String,Object> startQname = relation.getAttributeAsMap("startName");
							if(startQname != null) {
								relation.setAttribute("startNamespace", startQname.get("namespace"));
								relation.setAttribute("startlocalName", startQname.get("localName"));
							}
							Map<String,Object> endQname = relation.getAttributeAsMap("endName");
							if(endQname != null) {
								relation.setAttribute("endNamespace", endQname.get("namespace"));
								relation.setAttribute("endlocalName", endQname.get("localName"));
							}
						}
						modelPanel.select(rec);
					}
				}
			}	        			
		});
	}
}
