package org.heed.openapps.entity.client.form;
import java.util.Map;

import org.heed.openapps.entity.client.dictionary.data.DictionaryListDS;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ModelFormItem extends StaticTextItem {
	private RepositoryWindow window;
	private String value_id;
	
	private boolean initialized;
	
	
	public ModelFormItem(String name, String title) {
		super(name, title);
		setHeight(16);
		setWrap(false);
		FormItemIcon formItemIcon = new FormItemIcon();
        setIcons(formItemIcon);
       
        addIconClickHandler(new IconClickHandler() {
            @Override
            public void onIconClick(IconClickEvent event) {
                window.show();
            }
        });
        window = new RepositoryWindow();
	}
	public String getValueId() {		
		return value_id;
	}
	public void setValueId(String id) {
		this.value_id = id;
	}
	public void select(Record record) {
		if(!initialized) {
			window.search();
			initialized = true;
		}		
		if(record != null) {			
			try {
				String parent = record.getAttributeAsString("parent");
				if(parent != null) {
					value_id = parent;
					//Map<String,Object> name = parent.getAttributeAsMap("qname");
					//setValueId(value_id);
					//setValue("{"+name.get("namespace")+"}"+name.get("localName"));
				}
			} catch(ClassCastException e) {
				//setValue("");
			}
		} //else setValue("");
	}
		
	public class RepositoryWindow extends Window {
		private Toolbar toolbar;
		private ListGrid grid;
		
		public RepositoryWindow() {
			VLayout mainLayout = new VLayout();
			mainLayout.setWidth100();
			mainLayout.setHeight100();
			addItem(mainLayout);
			
			setWidth(410);
			setHeight(300);
			setTitle("Add Parent Model");
			setAutoCenter(true);
			setIsModal(true);
						
			HLayout bottomLayout = new HLayout();
			bottomLayout.setHeight100();
			bottomLayout.setWidth100();
			bottomLayout.setMargin(2);
			mainLayout.addMember(bottomLayout);
			
			grid = new ListGrid();
			grid.setWidth100();
			grid.setHeight100();
			grid.setMargin(1);
			grid.setShowHeader(false);
			grid.setSortField("name");
			grid.setDataSource(DictionaryListDS.getInstance());
			ListGridField nameField = new ListGridField("title", "Title");
			nameField.setWidth("*");
			grid.setFields(nameField);
			bottomLayout.addMember(grid);
			
			toolbar = new Toolbar(28);
			//toolbar.setLayoutLeftMargin(5);
			toolbar.setBorder("1px solid #a8c298;");
								
	        toolbar.addButton("link", "/theme/images/icons32/link.png", "Link Repository", new ClickHandler() {  
				public void onClick(ClickEvent event) {
					Record parent = grid.getSelectedRecord();
					value_id = parent.getAttribute("id");
					String name = parent.getAttribute("qname");
					setValueId(value_id);
					setValue(name);
					fireChangedEvent();					
					window.hide();
				}  
		    });
	        toolbar.addButton("clear", "/theme/images/icons32/delete.png", "Clear Parent", new ClickHandler() {  
				public void onClick(ClickEvent event) {
					setValueId("");
					setValue("");
					fireChangedEvent();					
					window.hide();
				}  
		    });
			mainLayout.addMember(toolbar);
		}
		public void search() {
			Criteria criteria = new Criteria();
			grid.fetchData(criteria);
		}
		@Override
		public void show() {
			if(!initialized) {
				window.search();
				initialized = true;
			}
			super.show();
		}
		
	}

	private native void fireChangedEvent() /*-{
		var obj = null;
		obj = this.@com.smartgwt.client.core.DataClass::getJsObj()();
		var selfJ = this;
		if(obj.getValue === undefined){
			return;
		}
		var param = {"form" : obj.form, "item" : obj, "value" : obj.getValue()};
		var event = @com.smartgwt.client.widgets.form.fields.events.ChangedEvent::new(Lcom/google/gwt/core/client/JavaScriptObject;)(param);
		selfJ.@com.smartgwt.client.core.DataClass::fireEvent(Lcom/google/gwt/event/shared/GwtEvent;)(event);
	}-*/;
}