package org.heed.openapps.entity.client.node;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class NodeBrowserDetailDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static NodeBrowserDetailDS instance = null;
	
	
	public static NodeBrowserDetailDS getInstance(String id) {  
        if (instance == null) {  
            instance = new NodeBrowserDetailDS(id);  
        }  
        return instance;  
    }
	
	public NodeBrowserDetailDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("name");	        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
                
        DataSourceTextField idField = new DataSourceTextField("id", "ID", 100);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/node/get.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/create.json");
        setRemoveDataURL(oa.getServiceUrl()+"/service/entity/remove.json");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request, Object data) {
		int httpCode = response.getHttpResponseCode();
		if(httpCode == 404) {
			//response.setData("Unable to connect to server, please check your connection.");
		} else {
			if(response != null && response.getData() != null && response.getData().length > 0) {
				for(Record entity : response.getData()) {
					String localName = entity.getAttribute("localName");
					String name = entity.getAttribute("name");
					if(name != null && name.endsWith("/")) entity.setAttribute("name", name.substring(0, name.length()-1));
					if(localName != null && localName.equals("folder")) {
						try {
							Record source_associations = entity.getAttributeAsRecord("source_associations");
							if(source_associations != null) {
								Record files = source_associations.getAttributeAsRecord("files");
								if(files != null) {
									entity.setAttribute("isFolder", false);	
									entity.setAttribute("icon", "/theme/images/tree_icons/manuscript.png");
								}
							}
						} catch(ClassCastException e) {
						
						}
					}
				}
			}
		}
		super.transformResponse(response, request, data);
	}
}