package org.heed.openapps.entity.client;

import org.heed.openapps.gwt.client.EventTypes;

public class EntityEventTypes extends EventTypes {

	public static final int SHOW_SEARCH = 100;
	
	public static final int NODE_BROWSE = 110;
	public static final int NODE_SELECTION = 111;
	
	public static final int ADD_MODEL = 120;
	public static final int SEARCH_MODEL = 121;
	public static final int CREATE_MODEL = 122;
	public static final int DELETE_MODEL = 123;
	public static final int SAVE_MODEL = 124;
	
	public static final int ADD_FIELD = 130;
	public static final int SEARCH_FIELD = 131;
	public static final int CREATE_FIELD = 132;
	public static final int DELETE_FIELD = 133;
	public static final int SAVE_FIELD = 134;
	
	public static final int ADD_RELATION = 140;
	public static final int SEARCH_RELATION = 141;
	public static final int CREATE_RELATION = 142;
	public static final int DELETE_RELATION = 143;
	public static final int SAVE_RELATION = 144;
	
	public static final int FIELD_SELECTION = 150;
	public static final int RELATION_SELECTION = 151;
	
	public static final int CREATE_FIELD_VALUE = 160;
	public static final int DELETE_FIELD_VALUE = 161;
	public static final int ADD_FIELD_VALUE = 162;
	
	public static final int CREATE_RELATION_VALUE = 170;
	public static final int DELETE_RELATION_VALUE = 171;
	public static final int ADD_RELATION_VALUE = 172;
	public static final int CREATE_RELATION_FIELD = 173;
	public static final int DELETE_RELATION_FIELD = 174;
	
	public static final int SEARCH_ENTITY = 200;
	public static final int DELETE_ENTITY = 201;
	
	public static final int MODEL_SELECT = 210;
	
	public static final int SAVE_UPLOADED_DATA = 220;
}
