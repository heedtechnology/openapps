package org.heed.openapps.entity.client.window;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.entity.client.form.ModelFormItem;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;


public class AddModelWindow extends Window {
	public AddModelWindow() {
		setWidth(400);
		setHeight(275);
		setTitle("Add Model");
		setAutoCenter(true);
		setIsModal(true);
		
		DynamicForm form = new DynamicForm();
		form.setWidth(360);
		form.setHeight100();
		form.setMargin(5);
		form.setCellPadding(5);
		form.setColWidths("50","*");
		
		List<FormItem> items = new ArrayList<FormItem>();
		
		final ModelFormItem parent = new ModelFormItem("parent", "Parent");
		parent.setWidth("*");
		parent.setColSpan(2);
		parent.addChangedHandler(new ChangedHandler() {  
            public void onChanged(ChangedEvent event) {  
            	EventBus.fireEvent(new OpenAppsEvent(EventTypes.SAVE));
            }  
        });
		items.add(parent);
		
		final TextItem namespace = new TextItem("namespace", "Namespace");
		namespace.setWidth("*");
		items.add(namespace);
		
		final TextItem localName = new TextItem("localName", "Local Name");
		localName.setWidth("*");
		items.add(localName);
		
		final TextAreaItem description = new TextAreaItem("description", "Description");
		description.setWidth("*");
		items.add(description);
		
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();				
				record.setAttribute("namespace", namespace.getValue());
				record.setAttribute("localName", localName.getValue());
				record.setAttribute("description", description.getValue());
				record.setAttribute("parent", parent.getValueId());
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.ADD_MODEL, record));
			}
		});
		items.add(submitItem);
		
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		form.setItems(fitems);
		addItem(form);
	}
}