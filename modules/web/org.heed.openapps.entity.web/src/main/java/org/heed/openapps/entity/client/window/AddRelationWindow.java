package org.heed.openapps.entity.client.window;

import org.heed.openapps.entity.client.form.RelationForm;

import com.smartgwt.client.widgets.Window;


public class AddRelationWindow extends Window {
	private RelationForm form;
	
	
	public AddRelationWindow() {
		setWidth(500);
		setHeight(255);
		setTitle("Add Relation");
		setAutoCenter(true);
		setIsModal(true);
		
		form = new RelationForm(true, false);
		form.setWidth100();
		form.setHeight100();
		form.setMargin(5);
		addItem(form);
	}
	
	public void setNamespace(String namespace) {
		form.setValue("namespace", namespace);
		form.setValue("startNamespace", namespace);
		form.setValue("endNamespace", namespace);
	}
}