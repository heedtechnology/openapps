package org.heed.openapps.entity.client;
import java.util.LinkedHashMap;

import org.heed.openapps.entity.client.window.ModelUploadWindow;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.QualifiedName;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.VLayout;


public class EntityManagerEntryPoint implements EntryPoint {
	private OpenApps security = new OpenApps();
	private static final int height = Window.getClientHeight() - 30;
	private static final int width = 1200;
	
	private Toolbar toolbar;
	private DynamicForm searchForm;
	private TextItem searchField;
	private DictionaryManager dictionaryManager;
	private NodeManager nodeManager;
	private DataManager dataManager;
	
	private ModelUploadWindow modelUploadWindow;
		
	private int mode = MODE_ENTITY;
	private String sessionKey;
	
	private static final int MODE_ENTITY = 1;
	private static final int MODE_NODE = 2;
	private static final int MODE_DATA = 3;
	
	@Override
	public void onModuleLoad() {
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight(height);  
		mainLayout.setWidth(width);
		mainLayout.setBorder("1px solid #A7ABB4");
		mainLayout.setMembersMargin(2);
		//EntityManagerToolbar toolbar = new EntityManagerToolbar();
		
		toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		toolbar.setMargin(1);
		
		toolbar.addButton("data", "/theme/images/icons32/database.png", "Data Manager", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				dictionaryManager.hide();
				searchForm.hide();
				nodeManager.hide();
				dataManager.show();
				mode = MODE_DATA;
			}  
		});
		toolbar.addButton("dictionary", "/theme/images/icons32/dictionary_manager.png", "Data Dictionary", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				nodeManager.hide();
				searchForm.hide();
				dataManager.hide();
				dictionaryManager.show();
				mode = MODE_ENTITY;
			}  
		});
		toolbar.addButton("node", "/theme/images/icons32/folders_explorer.png", "Node Manager", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				dictionaryManager.hide();
				searchForm.show();
				dataManager.hide();
				nodeManager.show();
				mode = MODE_NODE;
			}  
		});
		
		searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(5);
		searchForm.setCellPadding(2);
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Record record = new Record();
					record.setAttribute("parent", searchField.getValueAsString());
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.NODE_BROWSE, record));
				}				
			}
			
		});
		
		final SelectItem levelItem = new SelectItem("type", "Type");
		levelItem.setWidth(85);
		levelItem.setShowTitle(false);
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("id","ID/UID");
		valueMap3.put("name","Name");
		levelItem.setValueMap(valueMap3);
		
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("parent", searchField.getValueAsString());
				record.setAttribute("mode", levelItem.getValue());
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.NODE_BROWSE, record));
			}
		});
		searchForm.hide();
		searchForm.setFields(searchField, levelItem, searchButton);
		toolbar.addToLeftCanvas(searchForm);
		mainLayout.addMember(toolbar);
		
		Canvas mainCanvas = new Canvas();
		mainCanvas.setWidth100();
		mainCanvas.setHeight100();
		mainLayout.addMember(mainCanvas);
		
		dictionaryManager = new DictionaryManager();
		mainCanvas.addChild(dictionaryManager);
		dictionaryManager.hide();
		
		nodeManager = new NodeManager();
		mainCanvas.addChild(nodeManager);
		nodeManager.hide();
		
		dataManager = new DataManager();
		mainCanvas.addChild(dataManager);
				
		modelUploadWindow = new ModelUploadWindow();
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SEARCH)) {
	        		RestUtility.get(security.getServiceUrl()+"/service/entity/node/get/"+event.getRecord().getAttribute("query")+".xml", new DSCallback() {
    					public void execute(DSResponse response, Object rawData, DSRequest request) {
    						//fieldPanel.hide();
    	            		//entityPanel.hide();
    	            		//nodePanel.select(response.getData()[0]);
    	            		//nodePanel.show();
    	        		}						
    	        	}); 	
	        	} else if(event.isType(EventTypes.UPLOAD_MODEL)) {
	        		modelUploadWindow.show();
	        	} else if(event.isType(EventTypes.INDEX)) {
	        		if(event.getRecord() != null) {
	        			String qname = event.getRecord().getAttributeAsString("qname");
	        			String namespace = null;
						String localName = null;
	        			if(qname != null) {
	        				try {
	        					QualifiedName q = QualifiedName.createQualifiedName(qname);
	        					namespace = q.getNamespace();
	        					localName = q.getLocalName();
	        				} catch(Exception e) {
	        					e.printStackTrace();
	        				}
						}						
						if(namespace != null && localName != null && namespace.length() > 0 && localName.length() > 0) {
							RestUtility.get(security.getServiceUrl()+"/service/entity/index.json?qname={"+namespace+"}"+localName, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									String message = response.getAttribute("message");
			        				String uid = response.getData()[0].getAttribute("uid");
			        				updateJob(uid, message);
								}						
							}); 
						}
					} else SC.say("Please select an entity to index.");
	        	}
	        }
		});
		
		RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
		
		dictionaryManager.fetchData(new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				dataManager.setModels(response.getData());
			}			
		});
		
		mainLayout.setHtmlElement(DOM.getElementById("gwt"));
	    mainLayout.setPosition(Positioning.RELATIVE);
	    mainLayout.draw();
	}
	
	protected void updateJob(String uid, String message) {
		toolbar.trackJobStatus(uid, message);
	}
	
}
