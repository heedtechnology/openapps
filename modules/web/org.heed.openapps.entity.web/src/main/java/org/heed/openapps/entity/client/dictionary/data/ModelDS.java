package org.heed.openapps.entity.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class ModelDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static ModelDS instance = null;  
	  
    public static ModelDS getInstance() {  
        if (instance == null) {  
            instance = new ModelDS("dictionaryDS");  
        }  
        return instance;  
    }
    
	public ModelDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/dictionary/model/fetch.json");
        //setAddDataURL("/service/entity/dictionary/add.json"); 
        //setRemoveDataURL("/service/entity/dictionary/remove.json");
        //setUpdateDataURL("/service/entity/update.json");
	}
}
