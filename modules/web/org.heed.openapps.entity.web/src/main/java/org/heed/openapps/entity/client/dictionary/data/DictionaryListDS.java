package org.heed.openapps.entity.client.dictionary.data;


import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class DictionaryListDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static DictionaryListDS instance = null;  
	  
    public static DictionaryListDS getInstance() {  
        if (instance == null) {  
            instance = new DictionaryListDS("dictionaryListDS");  
        }  
        return instance;  
    }
    
	public DictionaryListDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/dictionary/models/fetch.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/dictionary/model/add.json"); 
        setRemoveDataURL(oa.getServiceUrl()+"/service/entity/dictionary/model/remove.json");
        setUpdateDataURL(oa.getServiceUrl()+"/service/entity/dictionary/model/update.json");
	}
	
}
