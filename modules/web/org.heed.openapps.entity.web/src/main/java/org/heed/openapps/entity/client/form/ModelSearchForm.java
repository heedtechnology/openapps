package org.heed.openapps.entity.client.form;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class ModelSearchForm extends VLayout {
	private DynamicForm form;
	
	
	public ModelSearchForm() {
		setBorder("1px solid #A7ABB4");
				
		form = new DynamicForm();
		form.setNumCols(4);
		form.setColWidths("*", 30, "*", 50);
		form.setWidth100();
		form.setHeight100();
		form.setMargin(2);
		form.setCellPadding(5);
		
		List<FormItem> items = new ArrayList<FormItem>();
		
		SpacerItem rowSpacer = new SpacerItem();
		rowSpacer.setColSpan(4);
		items.add(rowSpacer);
		
		TextItem namespace = new TextItem("query", "Query");
		namespace.setWidth("*");
		namespace.setColSpan(3);
		namespace.setShowTitle(false);
		items.add(namespace);
		
		ButtonItem searchButton = new ButtonItem("search");
		searchButton.setPrompt("Search");
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SEARCH_MODEL, record));
			}
		});
		items.add(searchButton);
		
		CheckboxItem missingName = new CheckboxItem("missingName", "Missing Qualifed Name");  
		missingName.setLabelAsTitle(true);
        items.add(missingName);
        
        CheckboxItem missingParent = new CheckboxItem("missingParent", "Missing Parent");  
        missingParent.setLabelAsTitle(true);
        items.add(missingParent);
        
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		form.setItems(fitems);
		
		addMember(form);
	}
}
