package org.heed.openapps.entity.client;

import org.heed.openapps.entity.client.data.DataListGrid;
import org.heed.openapps.entity.client.data.cleaning.CleaningApplication;
import org.heed.openapps.entity.client.data.upload.DataUploadApplication;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DataManager extends HLayout {
	private OpenApps security = new OpenApps();
	private Toolbar toolbar1;
	
	private DataListGrid dataGrid;
	private CleaningApplication cleaningApplication;
	private DataUploadApplication dataUploadApplication;
	
	private Record selection;
	
	public DataManager() {
		setWidth100();
		setHeight100();
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth(275);
		leftLayout.setHeight100();
		addMember(leftLayout);
		
		toolbar1 = new Toolbar(30);
		toolbar1.setWidth100();
		toolbar1.setMargin(1);
		toolbar1.setBorder("1px solid #A7ABB4");
		toolbar1.addButton("index", "/theme/images/icons32/document_index.png", "Index Data", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.INDEX, dataGrid.getSelectedRecord()));
	    	}  
	    });
		toolbar1.addButton("clean", "/theme/images/icons32/database_gear.png", "Manage Data", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				if(dataGrid.getSelectedRecord() == null) SC.warn("Please select an entity from the list.");
				else {
					cleaningApplication.show();
					dataUploadApplication.hide();
				}
	    	}  
	    });
		toolbar1.addButton("upload_entities", "/theme/images/icons32/database_add.png", "Import Data", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				if(dataGrid.getSelectedRecord() == null) SC.warn("Please select an entity from the list.");
				else {
					cleaningApplication.hide();
					dataUploadApplication.show();
				}
				//else wizard.show(WizardPanel.MODE_UPLOAD);
				//EventBus.fireEvent(new OpenAppsEvent(EventTypes.UPLOAD_ENTITY));
	    	}  
	    });
		toolbar1.addButton("upload_entities", "/theme/images/icons32/database_go.png", "Export Data", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.UPLOAD_ENTITY));
	    	}  
	    });
		leftLayout.addMember(toolbar1);
		
		dataGrid = new DataListGrid();
		leftLayout.addMember(dataGrid);
		
		Canvas rightLayout = new Canvas();
		rightLayout.setHeight100();
		rightLayout.setWidth100();
		rightLayout.setMargin(1);
		addMember(rightLayout);
			
		cleaningApplication = new CleaningApplication();
		rightLayout.addChild(cleaningApplication);
		dataUploadApplication = new DataUploadApplication();
		dataUploadApplication.hide();
		rightLayout.addChild(dataUploadApplication);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler() {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EntityEventTypes.SEARCH_ENTITY)) {
	        		if(dataGrid.getSelectedRecord() == null)
	        			SC.warn("please select a model on the left to search with.");
	        		else {
	        			event.getRecord().setAttribute("qname", dataGrid.getSelectedRecord().getAttribute("qname"));
	        			cleaningApplication.show();	        		
	        			cleaningApplication.search(event.getRecord());
	        		}
	        	} else if(event.isType(EntityEventTypes.MODEL_SELECT)) {
	        		selection = event.getRecord();
	        	} else if(event.isType(EntityEventTypes.DELETE_ENTITY)) {
	        		RestUtility.post(security.getServiceUrl()+"/entity/remove.xml", event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							cleaningApplication.showMessage(null, "Entity successfully deleted");
							cleaningApplication.hideMessage(60*1000);
							Record record = new Record();
							record.setAttribute("qname", dataGrid.getSelectedRecord().getAttribute("qname"));
		        			record.setAttribute("query", cleaningApplication.getQuery());
		        			cleaningApplication.search(record);
						}						
					});
	        	} else if(event.isType(EventTypes.CLEAN)) {
	        		if(dataGrid.getSelectedRecords() != null) {
						String qname = selection.getAttribute("qname");
						if(qname != null && qname.length() > 0) {
							cleaningApplication.clean(qname);
						}
					} else SC.say("Please select an entity to clean.");
	        	}
	        }
		});
	}
	
	public void setModels(Record[] models) {
		dataGrid.setData(models);
	}
}
