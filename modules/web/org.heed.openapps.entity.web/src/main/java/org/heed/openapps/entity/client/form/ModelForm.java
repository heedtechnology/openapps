package org.heed.openapps.entity.client.form;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;


public class ModelForm extends VLayout {
	private DynamicForm form;
	private ModelFormItem parent;
	
	
	public ModelForm(boolean showAddButton, boolean showSaveButton) {
		form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setCellPadding(5);
		List<FormItem> items = new ArrayList<FormItem>();
		
		parent = new ModelFormItem("parent", "Parent");
		parent.setWidth("*");
		parent.setColSpan(2);
		parent.addChangedHandler(new ChangedHandler() {  
            public void onChanged(ChangedEvent event) {  
            	EventBus.fireEvent(new OpenAppsEvent(EventTypes.SAVE));
            }  
        });
		items.add(parent);
				
		TextItem namespace = new TextItem("namespace", "Namespace");
		namespace.setWidth("*");
		items.add(namespace);
		
		TextItem localname = new TextItem("localName", "Local Name");
		localname.setWidth("*");
		items.add(localname);
		
		TextAreaItem description = new TextAreaItem("description", "Description");
		description.setWidth("*");
		description.setHeight(50);
		items.add(description);
		
		if(showAddButton) {
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.ADD_MODEL, record));
				}
			});
			items.add(submitItem);
		}
		
		if(showSaveButton) {
			ButtonItem submitItem = new ButtonItem("save", "Save");
			submitItem.setIcon("/theme/images/icons16/disk.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SAVE_MODEL, record));
				}
			});
			items.add(submitItem);
		}
		
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		form.setItems(fitems);
		addMember(form);
		
	}
	
	public void editRecord(Record record) {
		form.editRecord(record);
		parent.select(record);
	}
	public Record getValuesAsRecord() {
		Record record = form.getValuesAsRecord();
		if(parent.getValueId() != null) record.setAttribute("parent", parent.getValueId());
		return record;
	}
}
