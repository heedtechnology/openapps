package org.heed.openapps.entity.client.data;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.user.client.Timer;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ProgressTrackingWizardView extends VLayout {
	private HLayout messageCanvas;
	private Img progressBar;
	private Label messageLabel;
	private Img errorImg;
	private Img warningImg;
	private Img successImg;
	
	public static final String ERROR_ICON = "/theme/images/icons32/error.png";
	public static final String WARN_ICON = "/theme/images/icons32/warning.png";
	public static final String SUCCESS_ICON = "/theme/images/icons32/success.png";
	
	private String messageStyle = "font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;";
	
	public ProgressTrackingWizardView() {
		setWidth100();
		setHeight100();
		hide();
		
		Label label = new Label("<div style='font-weight:bold;font-size:13px;margin:10px;'>Results</div>");
		label.setWidth100();
		label.setHeight(30);
		addMember(label);
		
		messageCanvas = new HLayout();
		//messageCanvas.setBorder("1px solid #A7ABB4");
		messageCanvas.setWidth100();
		//messageCanvas.setHeight(height);
		messageCanvas.setMargin(2);
		addMember(messageCanvas);
		
		errorImg = new Img("/theme/images/icons32/error.png");
		errorImg.setWidth(24);
		errorImg.setHeight(24);
		errorImg.hide();
		messageCanvas.addMember(errorImg);
		
		warningImg = new Img("/theme/images/icons32/warning.png");
		warningImg.setWidth(24);
		warningImg.setHeight(24);
		warningImg.hide();
		messageCanvas.addMember(warningImg);
		
		successImg = new Img("/theme/images/icons32/success.png");
		successImg.setWidth(24);
		successImg.setHeight(24);
		successImg.hide();
		messageCanvas.addMember(successImg);
		
		progressBar = new Img("/theme/images/loader.gif");
		progressBar.setWidth(180);
		progressBar.setHeight(24);
		progressBar.hide();
		messageCanvas.addMember(progressBar);
		
		messageLabel = new Label();
		messageLabel.setWidth100();
		messageLabel.setHeight(20);
		messageLabel.setWrap(true);
		messageLabel.setMargin(5);
		messageLabel.hide();
		messageCanvas.addMember(messageLabel);
	}
	public void trackJobStatus(String uid, String message) {
		Record criteria = new Record();
		criteria.setAttribute("group", "general");
		criteria.setAttribute("id", id);
		if(id == null) showMessage("error", message);
		else {
			showMessage("progress", message);
			RestUtility.get("/service/scheduling/status.xml", criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData().length > 0) {
						Record data = response.getData()[0];
						final String message = data.getAttribute("lastMessage");
						String running = data.getAttribute("isRunning");
						showMessage(null, message);
						Timer timer = new Timer() {
							@Override
							public void run() {
								trackJobStatus(id, message);
							}        	
						};
						if(running != null && !running.equals("false")) timer.schedule(5000);
						else {
							//hideProgress();
							//hideMessage(60*1000);
						}
					}
				}						
			});
		}
	}
	public void showMessage(String icon, String message) {
		if(message == null) return;
		if(icon == null) {
			messageLabel.setContents("<label style='"+messageStyle+"'>"+message+"</label>");
		} else {
			if(icon.equals("error")) errorImg.show();
			else errorImg.hide();
			if(icon.equals("warning")) warningImg.show();
			else warningImg.hide();
			if(icon.equals("success")) successImg.show();
			else successImg.hide();
			if(icon.equals("progress")) progressBar.show();
			else progressBar.hide();
			if(message != null) {
				if(icon.equals("progress") && message.length() >= (90)) {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message.substring(0, 90)+"...</label>");
				} else if(message.length() >= (135)) {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message.substring(0, 135)+"...</label>");
				} else {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message+"</label>");
				}
			}
		}
		messageLabel.show();
	}
}
