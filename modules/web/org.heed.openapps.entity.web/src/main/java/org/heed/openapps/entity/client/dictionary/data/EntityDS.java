package org.heed.openapps.entity.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
//import com.smartgwt.client.data.fields.DataSourceDateField;

public class EntityDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static EntityDS instance = null;  
	  
    public static EntityDS getInstance() {  
        if (instance == null) {  
            instance = new EntityDS("accessionDS");  
        }  
        return instance;  
    }
    
	public EntityDS(String id) {
		setID(id);
		
		DataSourceBooleanField paidField = new DataSourceBooleanField("paid");  
		DataSourceBooleanField appraisalItem = new DataSourceBooleanField("appraisal");
		DataSourceBooleanField newCollectionField = new DataSourceBooleanField("new_collection");
		DataSourceBooleanField acknowledgedField = new DataSourceBooleanField("acknowledged");
		DataSourceBooleanField existingCollectionField = new DataSourceBooleanField("existing_collection");
		//DataSourceDateField dateField = new DataSourceDateField("date");
		
		setFields(paidField,appraisalItem,newCollectionField,acknowledgedField,existingCollectionField);
		
		//setAddDataURL(oa.getServiceUrl() + "/repository/accession/add.xml");
		setFetchDataURL(oa.getServiceUrl()+"/service/entity/get.xml");
		setRemoveDataURL(oa.getServiceUrl()+"/service/entity/remove.xml");
		setUpdateDataURL(oa.getServiceUrl()+"/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}
