package org.heed.openapps.entity.client;

public class AdministrationEventTypes {
	public static final int MESSAGE = 1;
	public static final int ENTITY_SEARCH = 2;
	public static final int ENTITY_IMPORT = 3;
	public static final int ENTITY_EXPORT = 4;
	public static final int ENTITY_INDEX = 5;
	
	public static final int INDEX_START = 6;
	public static final int INDEX_END = 7;
}
