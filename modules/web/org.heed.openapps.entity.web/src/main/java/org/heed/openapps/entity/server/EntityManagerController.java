package org.heed.openapps.entity.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.property.PropertyService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class EntityManagerController {
	@Autowired private SecurityService securityService;
	@Autowired private PropertyService propertyService;
	
	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
    public ModelAndView manager(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		//parms.put("openappsUrl", propertyService.getProperty("openapps.url").getValue());
		request.setAttribute("openappsUrl", propertyService.getProperty("openapps.url").getValue());
		if(!user.hasRole("administrator") && !user.hasRole("Administrator")) 
			return new ModelAndView(new RedirectView("/"));
    	return new ModelAndView("home").addAllObjects(parms);
	}
	@RequestMapping(value="/dictionary/manager", method = RequestMethod.GET)
    public ModelAndView user(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("administration");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		if(!user.hasRole("Administrator")) return new ModelAndView(new RedirectView("/"));
    	return new ModelAndView("dictionary").addAllObjects(parms);
	}
}
