package org.heed.openapps.entity.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class EntityListDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static EntityListDS instance = null;  
	  
    public static EntityListDS getInstance() {  
        if (instance == null) {  
            instance = new EntityListDS("entityListDS");  
        }  
        return instance;  
    }
    
	public EntityListDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/search.json");
        //setAddDataURL("/service/entity/dictionary/model/add.json"); 
        //setRemoveDataURL("/service/entity/dictionary/model/remove.json");
        //setUpdateDataURL("/service/entity/dictionary/model/update.json");
	}
	
}
