package org.heed.openapps.entity.client.dictionary;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.entity.client.dictionary.data.ModelFieldDS;
import org.heed.openapps.entity.client.window.AddFieldWindow;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.QualifiedName;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;


public class FieldList extends VLayout {
	private ModelFieldDS modelFieldDS = ModelFieldDS.getInstance();
	private ListGrid list;
	
	private SelectItem fieldSelector;
	
	private AddFieldWindow addFieldWindow;
	
	private Record model;
	private Map<String, Record[]> fields = new HashMap<String, Record[]>();
	
	
	public FieldList() {
		setWidth100();
		setHeight100();
		
		Toolbar toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		toolbar.setMargin(1);
		addMember(toolbar);
		
		DynamicForm form1 = new DynamicForm();
		fieldSelector = new SelectItem("fieldSelection");
		fieldSelector.setWidth(135);
		fieldSelector.setShowTitle(false);
		fieldSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				list.setData(fields.get(event.getValue()));
			}			
		});
		form1.setFields(fieldSelector);
		toolbar.addToLeftCanvas(form1);
		
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				//EventBus.fireEvent(new OpenAppsEvent(EventTypes.ADD, new Record()));
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.CREATE_FIELD));
			}  
		});
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_FIELD, list.getSelectedRecord()));
			}  
		});
				
		list = new ListGrid();
		list.setBorder("0 px");
		list.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = event.getRecord();
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.FIELD_SELECTION, record));
			}			
		});
		List<ListGridField> items = new ArrayList<ListGridField>();		
		ListGridField columnA = new ListGridField("localName", "Local Name");
		items.add(columnA);
		ListGridField[] fitems = new ListGridField[items.size()];
		list.setFields(items.toArray(fitems));
		addMember(list);
		
		addFieldWindow = new AddFieldWindow();
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		model = event.getRecord();        		
	        	} else if(event.isType(EntityEventTypes.ADD_FIELD)) {
	        		if(model == null) SC.warn("Please select a model to add this field to.");
	        		Record record = event.getRecord();
	        		record.setAttribute("model", model.getAttribute("id"));
	        		modelFieldDS.addData(event.getRecord(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				list.addData(response.getData()[0]);
	        			}	        			
	        		});
	        		addFieldWindow.hide();
	        	} else if(event.isType(EntityEventTypes.DELETE_FIELD)) {
	        		modelFieldDS.removeData(event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								list.removeData(response.getData()[0]);
							}
						}	        			
	        		});	        	
	        	} else if(event.isType(EntityEventTypes.CREATE_FIELD)) {
	        		if(model == null) SC.warn("Please select a model to add this field to.");
	        		else {
	        			String qnameStr = model.getAttribute("qname");
						if(qnameStr != null) {
							try {
								QualifiedName qname = QualifiedName.createQualifiedName(qnameStr);
								addFieldWindow.setNamespace(qname.getNamespace());
							} catch(Exception e) {
								e.printStackTrace();
							}
						}	        			
	        			addFieldWindow.show();
	        		}
	        	} else if(event.isType(EntityEventTypes.SAVE_FIELD)) {
	        		modelFieldDS.updateData(event.getRecord(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				//fields.addData(response.getData()[0]);
	        			}	        			
	        		});
	        	}
	        }
		});
	}
	
	@SuppressWarnings("unchecked")
	public void select(Record record) {
		this.model = record;
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		Map<String,Object> qname = record.getAttributeAsMap("qname");
		if(qname != null) {
			Record[] fieldArray = record.getAttributeAsRecordArray("fields");
			String key = "{"+qname.get("namespace")+"}"+qname.get("localName");
			fields.put(key, fieldArray);
			valueMap1.put(key, (String)qname.get("localName"));			
			list.setData(fieldArray);
		}
		/*
		Map<String,Object> parent1 = record.getAttributeAsMap("parent");
		if(parent1 != null) {
			Map<String,Object> parentqname1 = (Map<String,Object>)parent1.get("qname");
			if(qname != null) {
				String key1 = "{"+parentqname1.get("namespace")+"}"+parentqname1.get("localName");
				valueMap1.put(key1, (String)parentqname1.get("localName"));
				List<Map<String, Object>> fieldArray1 = (List<Map<String, Object>>)parent1.get("fields");
				fields.put(key1, mapsToRecords(fieldArray1));
			}
			Map<String,Object> parent2 = (Map<String,Object>)parent1.get("parent");
			if(parent2 != null) {
				Map<String,Object> parentqname2 = (Map<String,Object>)parent2.get("qname");
				if(qname != null) valueMap1.put("{"+parentqname2.get("namespace")+"}"+parentqname2.get("localName"), (String)parentqname2.get("localName"));
			}
		}	
		*/	
		fieldSelector.setValueMap(valueMap1);
		fieldSelector.setValue(valueMap1.get(valueMap1.keySet().toArray()[0]));
	}
	
	protected Record[] mapsToRecords(List<Map<String, Object>> list) {
		Record[] records = new Record[list.size()];
		for(int i=0; i < list.size(); i++) {
			records[i] = mapToRecord(list.get(i));
		}
		return records;
	}
	@SuppressWarnings("unchecked")
	protected Record mapToRecord(Map<String, Object> map) {
		ListGridRecord record = new ListGridRecord();
		for(String key : map.keySet()) {
			if(key.equals("qname")) {
				Map<String,Object> qname = (Map<String,Object>)map.get("qname");
				if(qname != null) {
					record.setAttribute("namespace", qname.get("namespace"));
					record.setAttribute("localName", qname.get("localName"));
				}
			} else {
				record.setAttribute(key, map.get(key));
			}
		}
		return record;
	}
}
