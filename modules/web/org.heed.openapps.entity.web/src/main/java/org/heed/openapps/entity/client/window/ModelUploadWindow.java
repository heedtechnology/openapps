package org.heed.openapps.entity.client.window;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.component.UploadListener;

import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;


public class ModelUploadWindow extends Window {
	private DynamicForm uploadForm;
	private UploadListener listener;
	private SelectItem modeItem;
	private UploadItem fileItem;
	
	
	public ModelUploadWindow() {
		setTitle("Model Upload");
		setWidth(300);
		setHeight(140);
		setAutoCenter(true);
		
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		canvas.setHeight100();
		addItem(canvas);
		
		NamedFrame frame = new NamedFrame("uploadTarget");
		frame.setWidth("1");
		frame.setHeight("1");
		frame.setVisible(false);
		canvas.addChild(frame);	
					
		uploadForm = new DynamicForm();
		uploadForm.setWidth("100%");
		uploadForm.setHeight(25);
		uploadForm.setMargin(5);
		uploadForm.setNumCols(2);
		uploadForm.setCellPadding(8);
		uploadForm.setColWidths("50", "*");
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setTarget("uploadTarget");
		uploadForm.setAction("/service/entity/dictionary/upload");

		
		fileItem = new UploadItem("file", "File");
		fileItem.setWidth(200);				
				
		ButtonItem uploadButton = new ButtonItem("upload","Upload");
		uploadButton.setStartRow(false);
		uploadButton.setEndRow(false);
		uploadButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler(){
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent e) {
				//Object obj = fileItem.getDisplayValue();
				//if (obj != null) {
					uploadForm.submitForm();
				//} else
					//SC.say("Please select a file.");
			}
		});
				
		uploadForm.setItems(fileItem,uploadButton);
		canvas.addChild(uploadForm);
	}
	
	public void setAction(String url) {
		uploadForm.setAction(url);
	}
	
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}
	
	public void uploadComplete(String fileName) {
		if (listener != null) listener.uploadComplete(fileName);		
		EventBus.fireEvent(new OpenAppsEvent(EventTypes.MESSAGE, "Model uploaded successfully"));
		hide();
	}
	public void setModel(LinkedHashMap<String,Object> modes) {
		modeItem.setValueMap(modes);
	}
	
	private native void initComplete(ModelUploadWindow upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.heed.openapps.entity.client.window.ModelUploadWindow::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}
