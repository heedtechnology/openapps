package org.heed.openapps.entity.client.form;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class FieldForm extends DynamicForm {

	public FieldForm(boolean showAddButton, boolean showSaveButton) {
		setWidth100();
		setHeight100();
		//form.setMargin(5);
		setCellPadding(5);
		setNumCols(4);
		
		List<FormItem> items = new ArrayList<FormItem>();
		final TextItem namespace = new TextItem("namespace", "Namespace");
		namespace.setColSpan(4);
		namespace.setWidth("*");
		items.add(namespace);
		
		final TextItem localName = new TextItem("localName", "Local Name");
		localName.setColSpan(4);
		localName.setWidth("*");
		items.add(localName);
		
		final SelectItem type = new SelectItem("type", "Type");
		type.setWidth(150);
		type.setDefaultValue("");
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("0","");
		valueMap1.put("9","Aspect");
		valueMap1.put("1","Boolean");
		valueMap1.put("6","Date");
		valueMap1.put("4","Double");
		valueMap1.put("2","Integer");
		valueMap1.put("8","Large Text");
		valueMap1.put("3","Long");
		valueMap1.put("7","Serializable");
		valueMap1.put("5","String");		
		type.setValueMap(valueMap1);
		items.add(type);
		
		final SpinnerItem minimum = new SpinnerItem("minSize", "Minimum");
		minimum.setWidth(75);
		items.add(minimum);
		
		final SelectItem format = new SelectItem("format", "Format");
		format.setDefaultValue("");
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("0","");
		valueMap2.put("2","EMail");
		valueMap2.put("1","Password");
		format.setWidth(150);
		format.setValueMap(valueMap2);
		items.add(format);
		
		final SpinnerItem maximum = new SpinnerItem("maxSize", "Maximum");
		maximum.setWidth(75);
		items.add(maximum);
		
		final SelectItem index = new SelectItem("index", "Index");
		index.setDefaultValue("");
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("0","");
		valueMap3.put("1","Stored/Tokenized");
		valueMap3.put("2","Not Stored/Tokenized");
		valueMap3.put("3","Stored/Not Tokenized");
		valueMap3.put("4","Not Stored/Not Tokenized");
		index.setWidth(150);
		index.setValueMap(valueMap3);
		items.add(index);
		
		final CheckboxItem mandatory = new CheckboxItem("mandatory", "Mandatory");
		//mandatory.setHeight(35);
		//mandatory.setLabelAsTitle(true);
		items.add(mandatory);
		
		final SelectItem sort = new SelectItem("sort", "Sort");
		sort.setDefaultValue("");
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("0","");
		valueMap4.put("1","Alpha");
		valueMap4.put("2","Numeric");
		sort.setWidth(150);
		sort.setValueMap(valueMap4);
		items.add(sort);
				
		final CheckboxItem unique = new CheckboxItem("unique", "Unique");
		//unique.setHeight(35);
		//unique.setLabelAsTitle(true);
		items.add(unique);
		
		if(showAddButton) {
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.ADD_FIELD, record));
				}
			});
			items.add(submitItem);
		}
		
		if(showSaveButton) {
			ButtonItem submitItem = new ButtonItem("save", "Save");
			submitItem.setIcon("/theme/images/icons16/disk.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SAVE_FIELD, record));
				}
			});
			items.add(submitItem);
		}
		
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		setItems(fitems);
	}
}
