package org.heed.openapps.entity.client.dictionary;

import java.util.LinkedHashMap;

import org.heed.openapps.entity.client.dictionary.data.EntityImportDS;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.DataUploadComponent;
import org.heed.openapps.gwt.client.component.UploadListener;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.TreeGrid;


public class ImportPanel extends VLayout {
	//private NativeImportModel model = new NativeImportModel();
	private TreeGrid grid;
	private DataUploadComponent upload;
	private DynamicForm propertyForm;
	private ListGrid propertyGrid;
	
	private Record repository;
	private String sessionKey;
	
	public ImportPanel() {
		//setBorder("1px solid #A7ABB4");
		HLayout topLayout = new HLayout();
		topLayout.setHeight(40);
		topLayout.setWidth100();
		topLayout.setMargin(2);
		
		upload = new DataUploadComponent(new LinkedHashMap<String,Object>(0));
		upload.setAction("/administration/entity/import/upload.xml");
		upload.setHeight(40);
		upload.setWidth(500);
		upload.addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {
				sessionKey = key;
				Criteria criteria = new Criteria();
				criteria.addCriteria("session", sessionKey);
				grid.fetchData(criteria);
			}
		});
		topLayout.addMember(upload);
		
		addMember(topLayout);
		
		HLayout bottomLayout = new HLayout();
		bottomLayout.setHeight100();
		addMember(bottomLayout);
		
		grid = new TreeGrid();
		grid.setDataSource(EntityImportDS.getInstance());
		grid.setWidth(350);
		grid.setHeight100();
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setWrapCells(true);
		grid.setAutoFetchData(false);
		grid.setShowHeader(true);
		grid.setShowResizeBar(true);
		grid.setLeaveScrollbarGap(false);
		grid.setSelectionType(SelectionStyle.SINGLE);
		ListGridField titleField = new ListGridField("title");
		grid.setFields(titleField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				String id = event.getRecord().getAttribute("uid");
				RestUtility.get("/administration/entity/import/select.xml?id="+id+"&session="+sessionKey, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record collection = response.getData()[0];
							if(collection != null) {
								Record[] properties = new Record[collection.getAttributes().length];
								for(int i=0; i < properties.length; i++) {
									String name = collection.getAttributes()[i];
									String value = collection.getAttribute(name);
									properties[i] = new Record();
									properties[i].setAttribute("name", name);
									properties[i].setAttribute("value", value);
								}
								propertyGrid.setData(properties);
							}
						}
					}
				});
			}			
		});
		bottomLayout.addMember(grid);
		bottomLayout.setMargin(2);
		
		TabSet tabs = new TabSet();
		Tab tab1 = new Tab("Properties");
		VLayout propertyLayout = new VLayout();
		tab1.setPane(propertyLayout);
		tabs.addTab(tab1);
		
		propertyForm = new DynamicForm();
		propertyLayout.addMember(propertyForm);
		StaticTextItem nameItem = new StaticTextItem("localName", "Type");
		propertyForm.setFields(nameItem);
		
		propertyGrid = new ListGrid();
		propertyLayout.addMember(propertyGrid);
		ListGridField nameField = new ListGridField("name", "Name");
		ListGridField valueField = new ListGridField("value", "Value");
		propertyGrid.setFields(nameField, valueField);
		
		bottomLayout.addMember(tabs);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.IMPORT_START)) {
	            	//String repo = repository.getAttribute("id");
	        		RestUtility.post("/administration/entity/import/add.xml?session="+sessionKey, new Record(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				if(response.getData() != null && response.getData().length > 0) {
	        					Record collection = response.getData()[0];
	        					if(collection != null) {
	        						propertyForm.editRecord(collection);
	        						propertyGrid.setData(collection.getAttributeAsRecord("properties").getAttributeAsRecordArray("property"));
	        					}
	        				}
	        			}
	        		});
	            }
	        }
	    });
	}
	
	@SuppressWarnings("unchecked")
	public void setImportProcessors(Record[] processors) {
		LinkedHashMap<String,Object> map = new LinkedHashMap<String,Object>();
		for(Record record : processors) {
			map.put(record.getAttribute("id"), record.getAttribute("name"));
		}
		upload.setModel(map);
	}	
	
}
