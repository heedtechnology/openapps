package org.heed.openapps.entity.client.form;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.entity.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class RelationForm extends DynamicForm {

	
	public RelationForm(boolean showAddButton, boolean showSaveButton) {
		setCellPadding(5);
		setNumCols(3);
		setColWidths(150, "*", 100);
		
		List<FormItem> items = new ArrayList<FormItem>();
		TextItem namespace = new TextItem("namespace", "Qualified Name");
		namespace.setWidth("*");
		items.add(namespace);
		
		TextItem localName = new TextItem("localName");
		localName.setShowTitle(false);
		items.add(localName);
		
		TextItem startnamespace = new TextItem("startNamespace", "Start Name");
		startnamespace.setWidth("*");
		items.add(startnamespace);
		
		TextItem startlocalName = new TextItem("startlocalName");
		startlocalName.setShowTitle(false);
		items.add(startlocalName);
		
		TextItem endnamespace = new TextItem("endNamespace", "End Name");
		endnamespace.setWidth("*");
		items.add(endnamespace);
		
		TextItem endlocalName = new TextItem("endlocalName");
		endlocalName.setShowTitle(false);
		items.add(endlocalName);
		
		final SelectItem sort = new SelectItem("direction", "Direction");
		sort.setDefaultValue("0");
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("0","");
		valueMap4.put("1","Outgoing");
		valueMap4.put("2","Incoming");
		sort.setColSpan(3);
		sort.setValueMap(valueMap4);
		items.add(sort);
		
		final CheckboxItem many = new CheckboxItem("many", "Many");
		many.setColSpan(3);
		items.add(many);
		
		final CheckboxItem cascade = new CheckboxItem("cascade", "Cascade");
		cascade.setColSpan(3);
		items.add(cascade);
		
		if(showAddButton) {
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.ADD_RELATION, record));
				}
			});
			items.add(submitItem);
		}
		if(showSaveButton) {
			ButtonItem submitItem = new ButtonItem("save", "Save");
			submitItem.setIcon("/theme/images/icons16/disk.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = getValuesAsRecord();				
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SAVE_RELATION, record));
				}
			});
			items.add(submitItem);
		}
		
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		setItems(fitems);
	}
}
