package org.heed.openapps.gwt.client.form;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.RichTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;


public class WYSIWYGFormItem extends RichTextItem {
	private SourceWindow sourceWindow;
	
	
	public WYSIWYGFormItem(String name, String title) {
		super(name, title);
		
		String [] myControls = {"myButton"};
		JavaScriptObject myButtonProperties = JSOHelper.createObject();
		JSOHelper.setAttribute(myButtonProperties, "title", "Source");
		JSOHelper.setAttribute(myButtonProperties, "width", 50);
		JSOHelper.setAttribute(myButtonProperties, "prompt", "View Source");
		MyButtonHandler myButtonHandler = new MyButtonHandler();
		JSOHelper.setAttribute(myButtonProperties, "click", myButtonHandler);
		this.setAttribute("myButtonProperties", myButtonProperties);
		this.setAttribute("myControls", myControls);
		
		this.setControlGroups("editControls", "styleControls", "myControls");
		
		sourceWindow = new SourceWindow();
	}
	
	private class MyButtonHandler implements Function {	
		@Override
		public void execute() {
			sourceWindow.setValue((String)getValue());
			sourceWindow.show();
		}
	}
	
	public class SourceWindow extends Window {
		private TextAreaItem item;
		
		public SourceWindow() {
			setWidth(700);
			setHeight(500);
			setTitle("Edit Source");
			setAutoCenter(true);
			setIsModal(true);
			
			DynamicForm form = new DynamicForm();
			
			item = new TextAreaItem(getName()+"_source");
			item.setWidth(685);
			item.setHeight(440);
			item.setShowTitle(false);
			
			ButtonItem saveButton = new ButtonItem("Save");
			saveButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					setRichTextValue(item.getValue());
					sourceWindow.hide();
				}        	
	        });
			
			form.setItems(item, saveButton);
						
			addItem(form);
		}
		public void setValue(Object value) {
			item.setValue(value);
		}
	}
	public void setRichTextValue(Object value) {
		this.setValue(value);
	}
}
