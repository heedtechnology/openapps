package org.heed.openapps.gwt.client.component;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.Timer;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.HeaderControls;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.HeaderControl;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.HLayout;


public class Toolbar extends HLayout {
	//private OpenApps security = new OpenApps();
	private Canvas leftCanvas;
	private HLayout messageCanvas;
	private HLayout rightCanvas;
	
	private Img progressBar;
	private Label messageLabel;
	private Img errorImg;
	private Img warningImg;
	private Img successImg;
	private ImgButton messageImgClose;
	private ImgButton infoImgClose;
	
	private MessageWindow messageWindow;
	
	private String message = "test message";
	private String messageStyle = "font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;";
	
	private Map<String, Canvas> buttons = new HashMap<String, Canvas>();
	
	public static final int TYPE_LARGE = 1;
	public static final int TYPE_SMALL = 2;
	
	public static final String ERROR_ICON = "/theme/images/icons32/error.png";
	public static final String WARN_ICON = "/theme/images/icons32/warning.png";
	public static final String SUCCESS_ICON = "/theme/images/icons32/success.png";
	
	public Toolbar(int height) {
		setWidth100();
		setHeight(height);
				
		leftCanvas = new Canvas();
		leftCanvas.setWidth100();
		leftCanvas.setHeight(height);
		addMember(leftCanvas);
		
		messageCanvas = new HLayout();
		//messageCanvas.setBorder("1px solid #A7ABB4");
		messageCanvas.setWidth100();
		messageCanvas.setHeight(height);
		messageCanvas.setMargin(2);
		addMember(messageCanvas);
		
		errorImg = new Img("/theme/images/icons32/error.png");
		errorImg.setWidth(24);
		errorImg.setHeight(24);
		errorImg.hide();
		messageCanvas.addMember(errorImg);
		
		warningImg = new Img("/theme/images/icons32/warning.png");
		warningImg.setWidth(24);
		warningImg.setHeight(24);
		warningImg.hide();
		messageCanvas.addMember(warningImg);
		
		successImg = new Img("/theme/images/icons32/success.png");
		successImg.setWidth(24);
		successImg.setHeight(24);
		successImg.hide();
		messageCanvas.addMember(successImg);
		
		progressBar = new Img("/theme/images/loader.gif");
		progressBar.setWidth(180);
		progressBar.setHeight(24);
		progressBar.hide();
		messageCanvas.addMember(progressBar);
		
		messageLabel = new Label();
		messageLabel.setHeight(20);
		messageLabel.setWrap(false);
		messageLabel.setMargin(5);
		messageLabel.hide();
		messageCanvas.addMember(messageLabel);
		
		Canvas iconLayout = new Canvas();
		iconLayout.setWidth(20);
		iconLayout.setHeight(20);
		iconLayout.setMargin(5);
		messageImgClose = new ImgButton();
		messageImgClose.setSrc("/theme/images/icons16/remove.png");
		messageImgClose.setWidth(16);
		messageImgClose.setHeight(16);
		messageImgClose.setPrompt("close message");
		messageImgClose.setVisible(false);
		messageImgClose.setShowDown(false);
		messageImgClose.setShowRollOver(false);
		messageImgClose.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				
			}
		});
		iconLayout.addChild(messageImgClose);
		
		infoImgClose = new ImgButton();
		infoImgClose.setSrc("/theme/images/icons16/help.png");
		infoImgClose.setWidth(16);
		infoImgClose.setHeight(16);
		infoImgClose.setPrompt("more information");
		infoImgClose.setVisible(false);
		infoImgClose.setShowDown(false);
		infoImgClose.setShowRollOver(false);
		infoImgClose.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				messageWindow.show(message);
			}
		});
		iconLayout.addChild(infoImgClose);
		messageCanvas.addMember(iconLayout);
		
		Canvas messageSpacer = new Canvas();
		messageSpacer.setWidth100();
		messageCanvas.addMember(messageSpacer);
		
		rightCanvas = new HLayout();
		rightCanvas.setHeight(height);
		rightCanvas.setMembersMargin(8);
		rightCanvas.setAlign(Alignment.RIGHT);
		//buttonCanvas.setBorder("1px solid #A7ABB4");
		addMember(rightCanvas);
		
		messageWindow = new MessageWindow();		
	}
	
	public void addButton(String id, String source, String title, com.smartgwt.client.widgets.events.ClickHandler handler) {
		ImgButton button = new ImgButton();
		button.setSrc(source);
		button.setPrompt(title);
		button.setWidth(24);
		button.setHeight(24);
		//button.setMargin(2);
		button.setShowDown(false);
		button.setShowRollOver(false);
		button.addClickHandler(handler);
		rightCanvas.addMember(button);
		buttons.put(id, button);
	}
	public void addButton(String id, String title, int width, com.smartgwt.client.widgets.events.ClickHandler handler) {
		Button button = new Button(title);
		button.setPrompt(title);
		button.setWidth(width);
		button.setHeight(24);
		//button.setMargin(2);
		button.setShowDown(false);
		button.setShowRollOver(false);
		button.addClickHandler(handler);
		rightCanvas.addMember(button);
		buttons.put(id, button);
	}
	public void addToLeftCanvas(Canvas canvas) {
		leftCanvas.addChild(canvas);
	}
	public void addToRightCanvas(Canvas canvas) {
		rightCanvas.addChild(canvas);
	}
	public void hideButton(String id) {
		Canvas button = buttons.get(id);
		if(button != null) button.hide();
	}
	public void showButton(String id) {
		Canvas button = buttons.get(id);
		if(button != null) button.show();
	}
	public void showMessageIcon(String icon) {
		if(icon.equals("error")) errorImg.show();
		else errorImg.hide();
		if(icon.equals("warning")) warningImg.show();
		else warningImg.hide();
		if(icon.equals("success")) successImg.show();
		else successImg.hide();
		if(icon.equals("progress")) progressBar.show();
		else progressBar.hide();
	}
	public void hideMessageIcon() {
		errorImg.hide();
		warningImg.hide();
		successImg.hide();
	}
	public void showMessage(String icon, String message) {
		this.message = message;
		if(message == null) return;
		int diff = buttons.size() * 4;
		if(icon == null) {
			if(message.length() >= 85) {
				messageLabel.setContents("<label style='"+messageStyle+"'>"+message.substring(0, 85)+"...</label>");
				messageImgClose.hide();
				infoImgClose.show();
			} else {
				messageLabel.setContents("<label style='"+messageStyle+"'>"+message+"</label>");
				messageImgClose.hide();
				infoImgClose.hide();
			}
		} else {
			showMessageIcon(icon);
			if(message != null) {
				if(icon.equals("progress") && message.length() >= (90-diff)) {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message.substring(0, (90-diff))+"...</label>");
					messageImgClose.hide();
					infoImgClose.show();
				} else if(message.length() >= (135-diff)) {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message.substring(0, (135-diff))+"...</label>");
					messageImgClose.hide();
					infoImgClose.show();
				} else {
					messageLabel.setContents("<label style='"+messageStyle+"'>"+message+"</label>");
					messageImgClose.hide();
					infoImgClose.hide();
				}
			}
		}
		messageLabel.show();
	}
	public void hideMessage(int wait) {
		Timer clearTimer = new Timer() {
			@Override
			public void run() {
				hideMessage();
			}        	
		};
		clearTimer.schedule(wait);
	}
	public void hideMessage() {
		messageLabel.hide();
	}	
	public void showProgress() {
		progressBar.show();
	}
	public void hideProgress() {
		progressBar.hide();
	}	
	public void trackJobStatus(final String id, String message) {
		Record criteria = new Record();
		criteria.setAttribute("group", "general");
		criteria.setAttribute("id", id);
		if(id == null) showMessage("error", message);
		else {
			showMessage("progress", message);
			/*
			RestUtility.get(security.getServiceUrl() + "/service/scheduling/status.xml", criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData().length > 0) {
						Record data = response.getData()[0];
						final String message = data.getAttribute("lastMessage");
						String running = data.getAttribute("isRunning");
						showMessage(null, message);
						Timer timer = new Timer() {
							@Override
							public void run() {
								trackJobStatus(id, message);
							}        	
						};
						if(running != null && !running.equals("false")) timer.schedule(5000);
						else {
							hideProgress();
							hideMessage(60*1000);
						}
					}
				}						
			});
			*/
		}
	}
	
	public void setMessageStyle(String messageStyle) {
		this.messageStyle = messageStyle;
	}
	
	public class MessageWindow extends Window {
		private HTMLPane contents;
		
		public MessageWindow() {
			setWidth(600);
			setHeight(300);
			setTitle("More Information");
			setAutoCenter(true);
			
			HeaderControl arrowDown = new HeaderControl(HeaderControl.ARROW_DOWN, new com.smartgwt.client.widgets.events.ClickHandler() {
				@Override
				public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
					hideMessage();
					hideMessageIcon();
					hideProgress();
					infoImgClose.hide();
					hide();
				}				
			});
			arrowDown.setPrompt("Clear Message");
			HeaderControl closeButton = new HeaderControl(HeaderControl.CLOSE, new com.smartgwt.client.widgets.events.ClickHandler() {
				
				public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
					hide();
				}				
			});
			closeButton.setPrompt("Close Dialog");
			setHeaderControls(HeaderControls.HEADER_LABEL,  arrowDown, closeButton);
			
			contents = new HTMLPane();
			contents.setWidth100();
			contents.setHeight100();
			contents.setMargin(5);
			addItem(contents);
		}
		
		public void show(String content) {
			contents.setContents(content);
			super.show();
		}
	}
}
