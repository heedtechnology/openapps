package org.heed.openapps.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.event.shared.SimpleEventBus;

public class EventBus {
	private static com.google.gwt.event.shared.EventBus eventBus = GWT.create(SimpleEventBus.class);

	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void addHandler(Type type, EventHandler handler) {
		eventBus.addHandler(type, handler);
	}
	public static void fireEvent(OpenAppsEvent event) {
		eventBus.fireEvent(event);
	}	
	
}
