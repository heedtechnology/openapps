package org.heed.openapps.gwt.client.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class ContentServiceDS {
	private OpenApps settings = new OpenApps();
	private static ContentServiceDS instance = null;
	
	private BrowseFoldersDatasource foldersSource;
	private BrowseFilesDatasource filesSource;
	private GetFolderDatasource getFolderSource;
	
    public static ContentServiceDS getInstance() {  
        if (instance == null) {  
            instance = new ContentServiceDS();  
        }  
        return instance;  
    }
    
    public ContentServiceDS() {
    	foldersSource = new BrowseFoldersDatasource();
    	filesSource = new BrowseFilesDatasource();
    	getFolderSource = new GetFolderDatasource();
	}
    
    public RestDataSource getFolderBrowseDatasource() {
		return foldersSource;
	}
    
    public void browseFolders(Criteria criteria, DSCallback callback) {
    	foldersSource.fetchData(criteria, callback);
    }
    public void browseFiles(Criteria criteria, DSCallback callback) {
    	filesSource.fetchData(criteria, callback);
    }
    public void getFolder(Criteria criteria, DSCallback callback) {
    	getFolderSource.fetchData(criteria, callback);
    }
    public void update(Record criteria, DSCallback callback) {
    	filesSource.updateData(criteria, callback);
    }
    public void removeFile(Record record, DSCallback callback) {
    	filesSource.removeData(record, callback);
	}
    
    private class BrowseFoldersDatasource extends RestDataSource {
		public BrowseFoldersDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("ContentServiceDS1");  
	        setTitleField("Name");	        
	        DataSourceTextField nameField = new DataSourceTextField("name", "Name");
	        DataSourceTextField idField = new DataSourceTextField("uid", "UID",50);  
	        idField.setPrimaryKey(true);  
	        idField.setRequired(true);
	        setFields(idField,nameField);
	        
	        String ticket = settings.getTicket();
			if(ticket != null) {
				setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/datasource/folders/browse.json?api_key="+ticket);
		        setUpdateDataURL(settings.getServiceUrl() + "service/entity/update.json?api_key="+ticket);
		        setAddDataURL(settings.getServiceUrl() + "service/entity/create.json?api_key="+ticket);
		        setRemoveDataURL(settings.getServiceUrl() + "service/entity/remove.json?api_key="+ticket);
			} else {
		        setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/datasource/folders/browse.json");
		        setUpdateDataURL(settings.getServiceUrl() + "service/entity/update.json");
		        setAddDataURL(settings.getServiceUrl() + "service/entity/create.json");
		        setRemoveDataURL(settings.getServiceUrl() + "service/entity/remove.json");
			}
		}
		@Override
		protected void transformResponse(DSResponse response, DSRequest request,Object data) {
			transformResponse(response, request, data);
			super.transformResponse(response, request, data);
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
    }
    private class BrowseFilesDatasource extends RestDataSource {
		public BrowseFilesDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("ContentServiceDS2");		
			String ticket = settings.getTicket();
			if(ticket != null) {
				setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/datasource/files/browse.json?api_key="+ticket);
				setUpdateDataURL(settings.getServiceUrl() + "service/entity/update.json?api_key="+ticket);
				setRemoveDataURL(settings.getServiceUrl() + "service/archivemanager/content/remove.json?api_key="+ticket);
			} else {
				setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/datasource/files/browse.json");
				setUpdateDataURL(settings.getServiceUrl() + "service/entity/update.json");
				setRemoveDataURL(settings.getServiceUrl() + "service/archivemanager/content/remove.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
    private class GetFolderDatasource extends RestDataSource {
		public GetFolderDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("ContentServiceDS2");	
			String ticket = settings.getTicket();
			if(ticket != null) {
				setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/content/get.json?api_key="+ticket);
			} else {
				setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/content/get.json");
			}
		}
	}
    
    protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		try {
			if(response.getData() != null && response.getData().length > 0) {
				for(int i=0; i < response.getData().length; i++) {
					Record entity = response.getData()[i];
					if(entity != null) {
						String type = entity.getAttribute("mimetype");
						if(type != null && !type.equals("files")) {
							entity.setAttribute("isFolder", false);
							entity.setAttribute("icon", "/theme/images/tree_icons/"+getMimetypeExtension(type)+".png");
						}
						String localName = entity.getAttribute("localName");
						String name = entity.getAttribute("name");
						if(name != null && name.endsWith("/")) entity.setAttribute("name", name.substring(0, name.length()-1));
						if(localName != null && localName.equals("folder")) {
							try {
								Record source_associations = entity.getAttributeAsRecord("source_associations");
								if(source_associations != null) {
									Record files = source_associations.getAttributeAsRecord("files");
									if(files != null) {
										entity.setAttribute("isFolder", false);	
										entity.setAttribute("icon", "/theme/images/tree_icons/manuscript.png");
									}
								}
							} catch(ClassCastException e) {
							
							}
						}
					}
				}
			}
		} catch(ClassCastException e) {
				
		}
	}
    protected String getMimetypeExtension(String mimetype) {
		return mimetype.replace("/", "-");
	}
}
