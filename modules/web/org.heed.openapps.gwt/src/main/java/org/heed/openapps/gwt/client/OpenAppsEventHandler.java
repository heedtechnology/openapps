package org.heed.openapps.gwt.client;

import com.google.gwt.event.shared.EventHandler;

public interface OpenAppsEventHandler extends EventHandler {

	void onEvent(OpenAppsEvent event);
	
}
