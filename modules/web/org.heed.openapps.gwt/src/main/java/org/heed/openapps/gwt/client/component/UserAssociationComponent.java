package org.heed.openapps.gwt.client.component;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class UserAssociationComponent extends VLayout {
	private ListGrid grid;
	private SelectAndAssociateGridWindow addNoteWindow;
	
	
	
	public UserAssociationComponent(String width, String height) {
		setWidth(width);
		setHeight(height);
		//setMargin(10);
		setBorder("1px solid #C0C3C7");
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth("100%");
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Users</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth(50);
		ListGridField nameField = new ListGridField("username","Username");
		grid.setFields(typeField, nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//select(event.getRecord());
			}
		});
        addMember(grid);
        /*
        DynamicForm typeForm = new DynamicForm();
		typeForm.setWidth("100%");
		typeForm.setNumCols(8);
		
		final TextItem groupItem = new TextItem("group", "Group");
		groupItem.setWidth(125);
		
		final SpinnerItem orderItem = new SpinnerItem("order", "Order");
		orderItem.setWidth(35);
		typeForm.setFields(groupItem,orderItem);
		*/
		ListGrid userGrid = new ListGrid();
		ListGridField usernameField = new ListGridField("username","Username");
		userGrid.setFields(usernameField);
		
        addNoteWindow = new SelectAndAssociateGridWindow(SelectAndAssociateGridWindow.MODE_STORE, 420, 350, "Add User", null, 
        		"openapps.org_system_1.0_users", "openapps.org_system_1.0_user", null, userGrid, new DSCallback() {
        	public void execute(DSResponse response, Object rawData, DSRequest request) {
        		if(response.getData() != null && response.getData().length > 0) {
					grid.addData(response.getData()[0]);
					addNoteWindow.hide();
				}
			}        	
        });
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void saveData() {
		
	}
	public void select(Record record) {
		addNoteWindow.select(record);
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("users");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	public List<String> getUserIds() {
		List<String> list = new ArrayList<String>();
		for(Record record : grid.getDataAsRecordList().toArray()) {
			list.add(record.getAttribute("id"));
		}
		return list;
	}
}
