package org.heed.openapps.gwt.client.event;

import com.smartgwt.client.data.Record;

public interface RemoveListener {

	void remove(Record record);
	
}
