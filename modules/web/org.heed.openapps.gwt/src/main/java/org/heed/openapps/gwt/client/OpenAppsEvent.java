package org.heed.openapps.gwt.client;

import com.google.gwt.event.shared.GwtEvent;
import com.smartgwt.client.data.Record;

public class OpenAppsEvent extends GwtEvent<OpenAppsEventHandler> {
	private int type;
	private Record record;
	private String source;
	private String message;
	
	public static Type<OpenAppsEventHandler> TYPE = new Type<OpenAppsEventHandler>();

	
	public OpenAppsEvent() {}
	public OpenAppsEvent(int type) {
		this.type = type;
	}
	public OpenAppsEvent(int type, String message) {
		this.type = type;
		this.message = message;
	}
	public OpenAppsEvent(int type, Record record) {
		this.type = type;
		this.record = record;
	}
	public OpenAppsEvent(int type, String message, Record record) {
		this.type = type;
		this.message = message;
		this.record = record;
	}
	
	@Override
	protected void dispatch(OpenAppsEventHandler handler) {
		handler.onEvent(this);
	}
	@Override
	public Type<OpenAppsEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	public boolean isType(int type) {
		return this.type == type;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Record getRecord() {
		return record;
	}
	public void setRecord(Record record) {
		this.record = record;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
			
}