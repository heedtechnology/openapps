package org.heed.openapps.gwt.client.util;

public class FileUtility {

	public static String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		// get extension
		int pos = filename.lastIndexOf('.');
		if (pos == -1 || pos == filename.length() - 1)
			return null;
		String ext = filename.substring(pos + 1, filename.length());
		ext = ext.toLowerCase();
		return ext;
	}
	public static String getMimetype(String name) {
		if(name != null) {
			String fileName = name.toLowerCase();
			if(fileName.endsWith(".gif")) return Mimetypes.MIMETYPE_IMAGE_GIF;
			if(fileName.endsWith(".jpg")) return Mimetypes.MIMETYPE_IMAGE_JPEG;
			if(fileName.endsWith(".pdf")) return Mimetypes.MIMETYPE_PDF;
			if(fileName.endsWith(".tif") || fileName.endsWith(".tiff")) return Mimetypes.MIMETYPE_IMAGE_TIF;
			if(fileName.endsWith(".mp3")) return Mimetypes.MIMETYPE_MP3;
			if(fileName.endsWith(".mp4")) return Mimetypes.MIMETYPE_MP4;
			if(fileName.endsWith(".flv")) return Mimetypes.MIMETYPE_FLV;
			if(fileName.endsWith(".xml")) return Mimetypes.MIMETYPE_XML;
			if(fileName.endsWith(".xls")) return Mimetypes.MIMETYPE_EXCEL;
			if(fileName.endsWith(".ppt")) return Mimetypes.MIMETYPE_PPT;
			if(fileName.endsWith(".doc")) return Mimetypes.MIMETYPE_WORD;
		}
		return "";
	}
}
