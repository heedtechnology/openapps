package org.heed.openapps.gwt.client;

import com.smartgwt.client.data.Record;

public interface Application extends Component {

	void select(Record record);
	void save();
	void add();
	void delete();
	void search(String query);
	void importEntity();
	void exportEntity();
	
}
