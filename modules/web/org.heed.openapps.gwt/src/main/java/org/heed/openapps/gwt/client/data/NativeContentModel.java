package org.heed.openapps.gwt.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeContentModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getDataSourceTypes() { 
		JavaScriptObject obj = getNativeDataSourceTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeDataSourceTypes() /*-{
        return $wnd.datasource_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getContentTypes() { 
		JavaScriptObject obj = getNativeContentTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeContentTypes() /*-{
        return $wnd.item_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getImageRelationships() { 
		JavaScriptObject obj = getNativeImageRelationships();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeImageRelationships() /*-{
        return $wnd.image_relationships;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getWebLinkTypes() { 
		JavaScriptObject obj = getNativeWebLinkTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeWebLinkTypes() /*-{
        return $wnd.weblink_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getWebContentTypes() { 
		JavaScriptObject obj = getNativeWebContentTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeWebContentTypes() /*-{
        return $wnd.webcontent_types;
	}-*/;
}
