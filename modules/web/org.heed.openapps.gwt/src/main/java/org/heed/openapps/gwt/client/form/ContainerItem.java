package org.heed.openapps.gwt.client.form;

import java.util.LinkedHashMap;

import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;

public class ContainerItem extends StaticTextItem {
	private LinkedHashMap<String,Object> valueMap;
	private ContainerUpdateWindow window;
	
	private String actualValue;
	
	public ContainerItem(String name, String title, LinkedHashMap<String,Object> valueMap) {
		super(name, title);
		setHeight(16);
		this.valueMap = valueMap;
		setWrap(false);
		FormItemIcon formItemIcon = new FormItemIcon();
        setIcons(formItemIcon);
        
        addIconClickHandler(new IconClickHandler() {
            
            public void onIconClick(IconClickEvent event) {
                int idx = 1;
                if(actualValue != null) {
                	String[] parts = actualValue.trim().replace("  ", " ").replace(",", "").split(" ");
                	window.getSelect1().setSpinnerValue("");
        			window.getSelect2().setSelectValue("");
        			window.getSelect2().setSpinnerValue("");
        			window.getSelect3().setSelectValue("");
        			window.getSelect3().setSpinnerValue("");
        			window.getSelect4().setSelectValue("");
        			window.getSelect4().setSpinnerValue("");
                	for(String part : parts) {
                		if(part.length() > 0) {
                			if(idx == 1) window.getSelect1().setSelectValue(part);
                			else if(idx == 2) window.getSelect1().setSpinnerValue(part);
                			else if(idx == 3) window.getSelect2().setSelectValue(part);
                			else if(idx == 4) window.getSelect2().setSpinnerValue(part);
                			else if(idx == 5) window.getSelect3().setSelectValue(part);
                			else if(idx == 6) window.getSelect3().setSpinnerValue(part);
                			else if(idx == 7) window.getSelect4().setSelectValue(part);
                			else if(idx == 8) window.getSelect4().setSpinnerValue(part);
                			idx++;
                		}
                	}
                }
                window.show();
            }
        });
        window = new ContainerUpdateWindow();
	}
	@Override
	public void setValue(String value) {
		if(value != null) {
			actualValue = value.trim();
			String prettyValue = prettyPrint(value.trim());
			super.setValue(prettyValue);
		} else {
			actualValue = "";
			super.setValue("");
		}
	}
	@Override
	public Object getValue() {
		return actualValue;
	}
	protected String prettyPrint(String in) {
		for(String key : valueMap.keySet()) {
			in = in.replace(key, (String)valueMap.get(key));
		}
		return in;
	}
	public class ContainerUpdateWindow extends Window {
		private SelectTextItem select1;
		private SelectTextItem select2;
		private SelectTextItem select3;
		private SelectTextItem select4;
		
		public ContainerUpdateWindow() {
			setWidth(325);
			setHeight(225);
			setTitle("Edit Container");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm form = new DynamicForm();
			form.setColWidths(100,"*");
			select1 = new SelectTextItem("type1", "Container 1", 170, valueMap);
			select2 = new SelectTextItem("type2", "Container 2", 170, valueMap);
			select3 = new SelectTextItem("type3", "Container 3", 170, valueMap);
			select4 = new SelectTextItem("type4", "Container 4", 170, valueMap);
			
			ButtonItem submitItem = new ButtonItem("submit", "save");
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String type1 = select1.getSpinnerValue() != null ? select1.getSpinnerValue().toUpperCase() : "";
					String value1 = select1.getSelectValue();
					String type2 = select2.getSpinnerValue() != null ? select2.getSpinnerValue().toUpperCase() : "";
					String value2 = select2.getSelectValue();
					String type3 = select3.getSpinnerValue() != null ? select3.getSpinnerValue().toUpperCase() : "";
					String value3 = select3.getSelectValue();
					String type4 = select4.getSpinnerValue() != null ? select4.getSpinnerValue().toUpperCase() : "";
					String value4 = select4.getSelectValue();
					setValue(value1+" "+type1+" "+" "+value2+" "+type2+" "+value3+" "+type3+" "+value4+" "+type4.trim());
					window.hide();
				}
			});
			
			form.setFields(select1,select2,select3,select4,submitItem);
			addItem(form);		
		}

		public SelectTextItem getSelect1() {
			return select1;
		}

		public SelectTextItem getSelect2() {
			return select2;
		}

		public SelectTextItem getSelect3() {
			return select3;
		}

		public SelectTextItem getSelect4() {
			return select4;
		}
		
	}
}
