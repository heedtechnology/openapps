package org.heed.openapps.gwt.client.component;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class AddEntityAndAssociateWindow extends Window {
	protected Record source;
	protected Record target;
	
	private IButton saveItem;
	private DynamicForm form;
	
	
	public AddEntityAndAssociateWindow(final String title, final String association, final String entity, final DynamicForm form, final DSCallback callback) {
		this.form = form;
		
		setWidth(form.getWidth()+25);
		setHeight(form.getHeight()+65);
		setTitle(title);
		setAutoCenter(true);
		setIsModal(true);
		
		VLayout mainLayout = new VLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		addItem(mainLayout);
		
		mainLayout.addMember(form);
		
		HLayout buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(35);
		buttonLayout.setMargin(5);
		mainLayout.addMember(buttonLayout);
		
		saveItem = new IButton("Save");
		saveItem.setWidth(75);
		saveItem.setIcon("/theme/images/icons16/disk.png");
		saveItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				if(target != null) {
					String id = target.getAttribute("target_id");
					if(id == null) id = target.getAttribute("id");
					for(Object key : form.getValues().keySet()) {
						String value = form.getValueAsString((String)key);
						if(value != null)record.setAttribute((String)key, value);
					}
					record.setAttribute("id", id);
					record.setAttribute("qname", entity);
					EntityServiceDS.getInstance().updateEntity(record, callback);
				} else {
					record.setAttribute("assoc_qname", association);
					record.setAttribute("entity_qname", entity);
					String id = source.getAttribute("target_id");
					if(id == null) id = source.getAttribute("id");
					record.setAttribute("source", id);
					for(Object key : form.getValues().keySet()) {
						String value = form.getValueAsString((String)key);
						record.setAttribute((String)key, value);
					}
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						@Override
						public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
							callback.execute(dsResponse, data, dsRequest);
						}					
					});
				}
			}			
		});
		buttonLayout.addMember(saveItem);
	}
	public void newRecord() {
		this.target = null;
		form.clearValues();
	}
	public void editRecord(Record record) {
		this.target = record;
		form.editRecord(record);
	}
	public void select(Record record) {
		this.source = record;
	}
}
