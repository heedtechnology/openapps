package org.heed.openapps.gwt.client.component;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.form.TimeItem;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ActivitiesComponent extends VLayout {
	private DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
	private AddNoteWindow addNoteWindow;
	private HLayout buttons;
	private ListGrid grid;
	private ImgButton editButton;
	private ImgButton delButton;
	
	private int autoFitMaxHeight = 200;
	
	private LinkedHashMap<String,String> valueMap;
	private Record selection;
	
	public ActivitiesComponent(String title, String width, LinkedHashMap<String,String> valueMap) {
		this.valueMap = valueMap;

		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		//toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Activities</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addNoteWindow.toAddMode();
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		editButton = new ImgButton();
		editButton.setWidth(15);
		editButton.setHeight(15);
		editButton.hide();
		editButton.setSrc("/theme/images/icons16/pencil.png");
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					addNoteWindow.toUpdateMode(record);
					addNoteWindow.show();
				}
			}
		});
		buttons.addMember(editButton);
		
		delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.hide();
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.addSelectionUpdatedHandler(new SelectionUpdatedHandler() {
			public void onSelectionUpdated(SelectionUpdatedEvent event) {
				editButton.show();
				delButton.show();
			}
		});
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setValueMap(valueMap);
		typeField.setWidth(100);
		
		ListGridField contentField = new ListGridField("name","Name");
		
		ListGridField dateField = new ListGridField("date","Date");
		dateField.setWidth(70);
		
		grid.setFields(typeField, contentField, dateField);
		addMember(grid);
		
		addNoteWindow = new AddNoteWindow();
	}
	
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("activities");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("activities");
					}
				}
				setData(notes);
			}
			editButton.hide();
			delButton.hide();
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	
	public class AddNoteWindow extends Window {
		private final DynamicForm addForm;
		private ButtonItem updateItem;
		private ButtonItem addItem;
		
		public AddNoteWindow() {
			setWidth(500);
			setHeight(315);
			setTitle("Add Activity");
			setAutoCenter(true);
			setIsModal(true);
			
			addForm = new DynamicForm() {
				@Override
				public void editRecord(Record record) {
					if(record != null) {
						this.getField("duration").setValue(record.getAttributeAsString("duration"));
					}
					super.editRecord(record);
				}
			};
			addForm.setMargin(5);
			
			final DateItem dateItem = new DateItem("date", "Date");
			//dateItem.setUseTextField(true);
			
			final SelectItem typeItem = new SelectItem("type", "Activity Type");			
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(190);
					
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth(350);
			
			final TextAreaItem contentItem = new TextAreaItem("content","Note");
			contentItem.setHeight(125);
			contentItem.setWidth(350);
			
			final TimeItem timeItem = new TimeItem("duration","Duration");
					
			updateItem = new ButtonItem("update", "Update");
			updateItem.setStartRow(false);
			updateItem.setEndRow(false);
			updateItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String id = addForm.getValueAsString("target_id");
					if(id == null) id = addForm.getValueAsString("id");
					Record record = new Record();
					record.setAttribute("id", id);
					record.setAttribute("qname", "openapps_org_system_1_0_activity");
					record.setAttribute("date", dateFormatter.format(dateItem.getValueAsDate()));
					record.setAttribute("type", typeItem.getValue());
					record.setAttribute("name", nameItem.getValue());
					record.setAttribute("content", contentItem.getValue());
					record.setAttribute("duration", timeItem.getValue());
					
					EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record task = response.getData()[0];
								if(task != null) {
									String act_id = task.getAttribute("id");
									for(Record record : grid.getRecords()) {
										String rec_id = record.getAttribute("id");
										String rec_tgt = record.getAttribute("target_id");
										if(rec_tgt != null && rec_tgt.equals(act_id)) {
											task.setAttribute("id", rec_id);
											task.setAttribute("target_id", rec_tgt);
											grid.removeSelectedData();
											grid.addData(task);
										} else if(rec_id.equals(act_id)){
											grid.removeData(record);
											grid.addData(task);
										}
									}
								}
							}
							addNoteWindow.hide();
						}							
					});
				}			
			});
			
			addItem = new ButtonItem("submit", "Add");
			addItem.setStartRow(false);
			addItem.setEndRow(false);
			addItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_system_1_0_activities");
					record.setAttribute("entity_qname", "openapps_org_system_1_0_activity");
					record.setAttribute("source", selection.getAttribute("id"));
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("name", addForm.getValue("name"));
					record.setAttribute("content", addForm.getValue("content"));
					record.setAttribute("duration", timeItem.getHours()+":"+timeItem.getMinutes());
					
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record record = response.getData()[0];
								select(record);									
								addForm.clearValues();
								addNoteWindow.hide();
							}
						}							
					});
				}			
			});
			
			addForm.setFields(dateItem,typeItem,nameItem,contentItem,timeItem,addItem,updateItem);
			addItem(addForm);						
		}
		public void toUpdateMode(Record activity) {
			addItem.hide();
			updateItem.show();
			addForm.editRecord(activity);
		}
		public void toAddMode() {
			addItem.show();
			updateItem.hide();
			addForm.clearValues();
		}
	}
	public int getAutoFitMaxHeight() {
		return autoFitMaxHeight;
	}
	public void setAutoFitMaxHeight(int autoFitMaxHeight) {
		this.autoFitMaxHeight = autoFitMaxHeight;
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
	}
		
}
