package org.heed.openapps.gwt.client.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.DSDataFormat;

public class EntityServiceDS {
	private OpenApps security = new OpenApps();
	private static EntityServiceDS instance = null; 
	
	private EntityDatasource entitySource;
	private AssociationDatasource assocSource;
	private SearchDatasource searchSource;
	private IndexDatasource indexSource;
	private SecurityDatasource securitySource;
	
	public static EntityServiceDS getInstance() {
		if (instance == null) {  
            instance = new EntityServiceDS();  
        }
		return instance;
	}
	public EntityServiceDS() {
		entitySource = new EntityDatasource();
		assocSource = new AssociationDatasource();
		searchSource = new SearchDatasource(); 
		indexSource = new IndexDatasource(); 
		securitySource = new SecurityDatasource();
	}
	
	public RestDataSource getEntityDatasource() {
		return entitySource;
	}
	public RestDataSource getAssociationDatasource() {
		return entitySource;
	}
	public RestDataSource getSearchDatasource() {
		return searchSource;
	}
	
	public void permissions(Criteria criteria, DSCallback callback) {
		securitySource.fetchData(criteria, callback);
	}
	
	public void getEntity(Criteria criteria, DSCallback callback) {
		entitySource.fetchData(criteria, callback);
	}
	public void search(Criteria criteria, DSCallback callback) {
		searchSource.fetchData(criteria, callback);
	}
	public void index(Criteria criteria, DSCallback callback) {
		indexSource.fetchData(criteria, callback);
	}
	
	public void updateEntity(Record record, DSCallback callback) {
		entitySource.updateData(record, callback);
	}
	public void addEntity(Record record, DSCallback callback) {
		entitySource.addData(record, callback);
	}
	public void removeEntity(Record record, DSCallback callback) {
		entitySource.removeData(record, callback);
	}
	
	public void switchAssociation(Record record, DSCallback callback) {
		assocSource.updateData(record, callback);
	}
	public void addAssociation(Record record, DSCallback callback) {
		assocSource.addData(record, callback);
	}
	public void removeAssociation(Record record, DSCallback callback) {
		assocSource.removeData(record, callback);
	}
	
	private class SecurityDatasource extends RestDataSource {
		public SecurityDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS3");					
			String ticket = security.getTicket();
			if(ticket != null) {
				setFetchDataURL(security.getServiceUrl() + "service/security/search.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/security/search.json");
			}
		}
	}
	private class EntityDatasource extends RestDataSource {
		public EntityDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS1");
			
			String ticket = security.getTicket();
			if(ticket != null) {
				setAddDataURL(security.getServiceUrl() + "service/entity/add.json?api_key="+ticket);
				setFetchDataURL(security.getServiceUrl() + "service/entity/get.json?api_key="+ticket);
				setRemoveDataURL(security.getServiceUrl() + "service/entity/remove.json?api_key="+ticket);
				setUpdateDataURL(security.getServiceUrl() + "service/entity/update.json?api_key="+ticket);
			} else {
				setAddDataURL(security.getServiceUrl() + "service/entity/add.json");
				setFetchDataURL(security.getServiceUrl() + "service/entity/get.json");
				setRemoveDataURL(security.getServiceUrl() + "service/entity/remove.json");
				setUpdateDataURL(security.getServiceUrl() + "service/entity/update.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
	private class AssociationDatasource extends RestDataSource {
		public AssociationDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS2");
			
			String ticket = security.getTicket();
			if(ticket != null) {
				setAddDataURL(security.getServiceUrl() + "service/entity/association/add.json?api_key="+ticket);
				setRemoveDataURL(security.getServiceUrl() + "service/entity/association/remove.json?api_key="+ticket);
				setUpdateDataURL(security.getServiceUrl() + "service/entity/association/switch.json?api_key="+ticket);
			} else {
				setAddDataURL(security.getServiceUrl() + "service/entity/association/add.json");
				setRemoveDataURL(security.getServiceUrl() + "service/entity/association/remove.json");
				setUpdateDataURL(security.getServiceUrl() + "service/entity/association/switch.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
	private class SearchDatasource extends RestDataSource {
		public SearchDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS3");					
			String ticket = security.getTicket();
			if(ticket != null) {
				setFetchDataURL(security.getServiceUrl() + "service/entity/search.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/entity/search.json");
			}
		}
	}
	private class IndexDatasource extends RestDataSource {
		public IndexDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS4");					
			String ticket = security.getTicket();
			if(ticket != null) {
				setFetchDataURL(security.getServiceUrl() + "service/entity/index.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/entity/index.json");
			}
		}
	}
}
