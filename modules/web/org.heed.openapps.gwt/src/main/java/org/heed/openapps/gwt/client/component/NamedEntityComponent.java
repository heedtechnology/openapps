package org.heed.openapps.gwt.client.component;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class NamedEntityComponent extends VLayout {
	private Record selection;
	private HLayout buttons;
	private ListGrid grid;
	private ImgButton delButton;
	
	private LinkedHashMap<String,Object> functionValueMap;
	private LinkedHashMap<String,Object> roleValueMap;
	
	private AddNamedEntityWindow addWindow;
		
	private SelectionListener selectionListener;
	
	private int autoFitMaxHeight = 200;
	
	public NamedEntityComponent(String width, boolean editable, boolean showSource, boolean showFunction, LinkedHashMap<String,Object> functionValueMap, LinkedHashMap<String,Object> roleValueMap) {
		this.functionValueMap = functionValueMap;
		this.roleValueMap = roleValueMap;
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Named Entities</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
				
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Record rec = new Record();
					rec.setAttribute("id", record.getAttribute("id"));
					EntityServiceDS.getInstance().removeAssociation(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}
					});				 
				}
			}
		});
		buttons.addMember(delButton);
			
		addWindow = new AddNamedEntityWindow();
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.addCellClickHandler(new CellClickHandler() {
			@Override
			public void onCellClick(CellClickEvent event) {
				delButton.show();
				selectionListener.select(event.getRecord());
			}			
		});
		
		List<ListGridField> fields = new ArrayList<ListGridField>();
				
		if(showFunction) {
			ListGridField functionField = new ListGridField("_function");
			functionField.setWidth(60);
			fields.add(functionField);
		}
		if(showSource) {
			ListGridField roleField = new ListGridField("role");
			roleField.setWidth(185);
			fields.add(roleField);
		}
		ListGridField nameField = new ListGridField("name");
		fields.add(nameField);
		
		grid.setFields(fields.toArray(new ListGridField[fields.size()]));		
		addMember(grid);
		
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("named_entities");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("named_entities");
					}
				}
				setData(notes);
			}
			delButton.hide();
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	
	public class AddNamedEntityWindow extends Window {
		private TextItem searchField;
		private ListGrid addGrid;
		private SelectItem classificationSelect;
		
		public AddNamedEntityWindow() {
			setWidth(450);
			setHeight(350);
			setTitle("Add Named Entity");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(3);
			searchForm.setCellPadding(2);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(275);
			
			classificationSelect = new SelectItem("classification");
			classificationSelect.setWidth(100);
			LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
			valueMap1.put("openapps_org_classification_1_0_person","Person");
			valueMap1.put("openapps_org_classification_1_0_corporation","Corporate");
			classificationSelect.setValueMap(valueMap1);
			classificationSelect.setDefaultValue("openapps_org_classification_1_0_person");
			classificationSelect.setShowTitle(false);
			
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", classificationSelect.getValue());
					criteria.setAttribute("field", "name");
					//criteria.setAttribute("sort", "openapps_org_system_1_0_name");
					if(searchField.getValue() != null) {
						criteria.setAttribute("query", searchField.getValueAsString());
					} else {
						criteria.setAttribute("_startRow", "0");
						criteria.setAttribute("_endRow", "75");
					}
					EntityServiceDS.getInstance().search(criteria, new DSCallback() {
						@Override
						public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
							addGrid.setData(dsResponse.getData());
						}						
					});
				}
			});
			searchForm.setFields(searchField,classificationSelect,searchButton);
			addItem(searchForm);
			
			addGrid = new ListGrid();
			addGrid.setWidth("100%");
			addGrid.setHeight("100%");
			addGrid.setShowHeader(false);
			ListGridField nameField = new ListGridField("name","Name");
			addGrid.setFields(nameField);
			addItem(addGrid);
			
			final DynamicForm editForm = new DynamicForm();
			editForm.setHeight(25);
			editForm.setNumCols(6);
			editForm.setMargin(2);
			
			final SelectItem functionItem = new SelectItem("_function","Function");
			functionItem.setWidth(100);
			functionItem.setValueMap(functionValueMap);
			
			final SelectItem roleItem = new SelectItem("role","Role");
			roleItem.setWidth(195);
			roleItem.setValueMap(roleValueMap);
			
			ButtonItem linkItem = new ButtonItem("link", "Link");
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if(addGrid.anySelected()) {
						String id = selection.getAttribute("id");
						//String uid = selection.getAttribute("uid");
						final Record record = new Record();
						record.setAttribute("assoc_qname", "openapps_org_classification_1_0_named_entities");
						record.setAttribute("target", addGrid.getSelectedRecord().getAttribute("id"));
						record.setAttribute("role", roleItem.getValue());
						record.setAttribute("_function", functionItem.getValue());
						if(id != null && id.length() > 0) {
							record.setAttribute("source", id);							
							EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
								@Override
								public void execute(DSResponse response, Object data, DSRequest request) {
									Record record = response.getData()[0];
									select(record);									
									editForm.clearValues();
									addWindow.hide();
								}								
							});							
						} 
					}
				}			
			});
			editForm.setFields(functionItem,roleItem,linkItem);
			addItem(editForm);
		}
	}
	
	public void setSelectionListener(SelectionListener selectionListener) {
		this.selectionListener = selectionListener;
	}
}
