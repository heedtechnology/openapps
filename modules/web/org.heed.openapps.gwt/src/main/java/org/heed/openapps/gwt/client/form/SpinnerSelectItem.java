package org.heed.openapps.gwt.client.form;
import java.util.LinkedHashMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class SpinnerSelectItem extends CanvasItem {
	private HLayout layout;
	private SpinnerItem spinnerItem;
	private SelectItem selectItem;
	
	private final String spinnerName;
	private final String selectName;
	
	public SpinnerSelectItem(final String spinnerName, final String selectName, String title, int width, LinkedHashMap<String,String> valueMap) {
		super("",title);
		setWidth("100%");
		setHeight("100%");
		this.spinnerName = spinnerName;
		this.selectName = selectName;
		
		layout = new HLayout();
		layout.setWidth(115);
		layout.setHeight(30);
		layout.setMargin(5);
		
		DynamicForm form = new DynamicForm();
		form.setWidth(150);
		form.setNumCols(5);
		
		spinnerItem = new SpinnerItem(spinnerName);
		spinnerItem.setWidth(50);
		spinnerItem.setShowTitle(false);
		spinnerItem.setAlign(Alignment.CENTER);
		spinnerItem.setMin(0);
		spinnerItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				getForm().setValue(spinnerName, spinnerItem.getValueAsString());
				
			}			
		});
		selectItem = new SelectItem(selectName);
		selectItem.setWidth(width-60);
		selectItem.setShowTitle(false);
		selectItem.setAlign(Alignment.CENTER);
		selectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				getForm().setValue(spinnerName, selectItem.getValueAsString());
			}			
		});
		selectItem.setValueMap(valueMap);
		
		form.setFields(spinnerItem,selectItem);
		layout.addChild(form);
		
		setCanvas(layout);
	}
	public void editRecord(Record record) {
		String spinnerVal = record.getAttribute(spinnerName);
		String selectVal = record.getAttribute(selectName);
		spinnerItem.setValue(spinnerVal);
		selectItem.setValue(selectVal);
	}
	public String getSpinnerValue() {
		if(spinnerItem.getValue() == null) return "";
		return spinnerItem.getValueAsString();
	}
	public void setSpinnerValue(String value) {
		spinnerItem.setValue(value);
	}
	public String getSelectValue() {
		if(selectItem.getValue() == null) return "";
		return selectItem.getValueAsString();
	}
	public void setSelectValue(String value) {
		selectItem.setValue(value);
	}
}
