package org.heed.openapps.gwt.client.component;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.gwt.client.event.SaveListener;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VStack;


public class FileUploadComponent extends Canvas {
	public static enum Mode {SIMPLE, PROCESSED, COMPLEX};
	public static final String TARGET="uploadTarget";

	private Label message;
	private DynamicForm uploadForm;
	private UploadItem fileItem;
	private UploadListener listener;
	private SaveListener saveListener;
	private List<HiddenItem> hiddenItems = new ArrayList<HiddenItem>();
	private SelectItem modeItem;
	
	private String sessionKey;
		
	public FileUploadComponent() {
		this(null,Mode.SIMPLE);
	}
	
	/**
	 * @param args
	 */
	public FileUploadComponent(LinkedHashMap<String,Object> modes, Mode mode) {
		setWidth100();
		setHeight100();
		//setBorder("1px solid #A7ABB4");
		initComplete(this);
		List<FormItem> items = new ArrayList<FormItem>();
		
		ValuesManager vm = new ValuesManager();
		uploadForm = new DynamicForm();
		uploadForm.setWidth100();
		uploadForm.setMargin(5);
		uploadForm.setCellPadding(5);
		uploadForm.setColWidths(75,"*");
		uploadForm.setValuesManager(vm);
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setTarget(TARGET);
		uploadForm.setNumCols(2);
				
		fileItem = new UploadItem("file", "File");
		fileItem.setWidth(400);				
		items.add(fileItem);		
			
		if(mode == Mode.PROCESSED) {	
			modeItem = new SelectItem("mode","Processor");
			modeItem.setWidth(175);
			modeItem.setValueMap(modes);
			items.add(modeItem);
		}
		
		HiddenItem sourceItem = new HiddenItem("source");
		items.add(sourceItem);
		
		VStack stack = new VStack();
		stack.setWidth100();
		stack.setHeight100();
		stack.setMembersMargin(5);

		NamedFrame frame = new NamedFrame(TARGET);
		frame.setWidth("1");
		frame.setHeight("1");
		frame.setVisible(false);

		VStack mainLayout = new VStack();
		mainLayout.setHeight100();
		mainLayout.setWidth100();
		
		if (mode == Mode.COMPLEX) {		
			CheckboxItem unzip = new CheckboxItem("unzip");
			unzip.setDefaultValue(true);
			unzip.setTitle("Unzip .zip file");
			items.add(unzip);
			CheckboxItem overwrite = new CheckboxItem("overwrite");
			overwrite.setDefaultValue(false);
			overwrite.setTitle("Overwrite existing file"); 
			items.add(overwrite);
			CheckboxItem convertpdf = new CheckboxItem("convertpdf");
			convertpdf.setDefaultValue(true);
			convertpdf.setTitle("Convert Word document to PDF"); 
			items.add(convertpdf);
			CheckboxItem streaming = new CheckboxItem("streaming");
			streaming.setDefaultValue(true);
			streaming.setTitle("Convert video file to streaming format(flv)"); 
			items.add(streaming);
			CheckboxItem thumbnail = new CheckboxItem("thumbnail");
			thumbnail.setDefaultValue(true);
			thumbnail.setTitle("Make thumbnail(48x48) from image");
			items.add(thumbnail);
		}
				
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		uploadForm.setItems(fitems);
		stack.addMember(uploadForm);
		
		HLayout buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		stack.addMember(buttonLayout);
		
		Button uploadButton = new Button("Upload");
		uploadButton.setHeight(25);
		uploadButton.setWidth(65);
		uploadButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				uploadForm.submitForm();
			}
		});
		buttonLayout.addMember(uploadButton);
		
		Button saveButton = new Button("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(55);
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = new Record();
				if(modeItem != null) record.setAttribute("mode", modeItem.getValue());
				record.setAttribute("sessionKey", sessionKey);				
				saveListener.save(record);
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
		
		mainLayout.addMember(stack);
		mainLayout.addMember(frame);
		addChild(mainLayout);
	}

	public String getFile() {
		Object obj = fileItem.getValue();
		if (obj == null)
			return null;
		else
			return obj.toString();
	}
	public void setHiddenItem(String name, String value) {
		for (HiddenItem item: hiddenItems)
			if (item.getName().equals(name)) {
				item.setValue(value);
				uploadForm.setValue(name, value);
				return;
			}
	}
	public void setFields(FormItem[] fields) {
		uploadForm.setFields(fields);
	}
	public void setAction(String url) {
		uploadForm.setAction(url);
	}
	public void addSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}	
	public void uploadComplete(String fileName) {
		if (listener != null)
			listener.uploadComplete(fileName);
	}
	public void setModel(LinkedHashMap<String,Object> modes) {
		modeItem.setValueMap(modes);
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}	
	public void setMessage(String msg) {
		message.setContents(msg);
	}
	
	private native void initComplete(FileUploadComponent upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.heed.openapps.gwt.client.component.FileUploadComponent::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}