package org.heed.openapps.gwt.client.util;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;

public class SmartGWTUtility {

	
	public static Criteria recordToCriteria(Record record) {
		Criteria criteria = new Criteria();
		
		for(String key : record.getAttributes()) {
			criteria.setAttribute(key, record.getAttribute(key));
		}
		
		return criteria;
	}
}
