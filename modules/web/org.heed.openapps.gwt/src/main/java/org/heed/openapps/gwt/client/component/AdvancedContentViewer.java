package org.heed.openapps.gwt.client.component;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.Mimetypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;


public class AdvancedContentViewer extends Canvas {
	private OpenApps security = new OpenApps();
	//private Application application;
	private byte[] key = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	private Record selection;
	
	private VLayout documentCanvas;
	private VLayout	videoCanvas;
	private HTMLPane iframeCanvas;
	
	private HLayout pagingLayout;
	private IButton previous;
	private Label pagingText;
	private IButton next;
	private Menu contextMenu;
	
	private Img image;
	private Integer imageWidth;
	private Integer imageHeight;
	private String file_id;
	private int page;
	private int pages;
	
	private static final String rendering_image = "/theme/images/logo/ArchiveManager_rendering.png";
	
	public AdvancedContentViewer(String width, String height) {
		//this.application = application;
		setWidth(width);
		setHeight(height);
		setBorder("1px solid #C0C3C7");
		
		documentCanvas = new VLayout();
		documentCanvas.setWidth100();
		addChild(documentCanvas);
		
		IButton zoomButton = new IButton("");
		zoomButton.setWidth(40);
        zoomButton.setPrompt("Zoom +");
        zoomButton.setIcon("/theme/images/icons32/zoom_in.png");
        zoomButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {
            	if(imageWidth == null) imageWidth = image.getWidth();
            	if(imageHeight == null) imageHeight = image.getHeight();
                image.animateResize(image.getWidth()+100, image.getHeight()+100);
            }  
        });  
        IButton shrinkButton = new IButton("");
        shrinkButton.setWidth(40);
        shrinkButton.setPrompt("Zoom -");
        shrinkButton.setIcon("/theme/images/icons32/zoom_out.png");
        shrinkButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) { 
            	if(!image.getWidth().equals(imageWidth) && !image.getHeight().equals(imageHeight)) 
            		image.animateResize(image.getWidth()-100, image.getHeight()-100);
            }  
        });  
        HLayout hLayout = new HLayout();
        hLayout.setWidth100();
        hLayout.setBorder("1px solid #C0C3C7");
        hLayout.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
        hLayout.setAlign(Alignment.CENTER);
        hLayout.setMembersMargin(10);  
        hLayout.addMember(zoomButton);  
        hLayout.addMember(shrinkButton);  
        documentCanvas.addMember(hLayout);
        
        Canvas canvas = new Canvas();
        canvas.setWidth(width);
        canvas.setHeight(height);
        canvas.setOverflow(Overflow.AUTO);
        image = new Img("/theme/images/logo/ArchiveManager500_550.png");
        image.setWidth100();
        image.setHeight100();
        image.setBorder("1px solid #C0C3C7");
        image.setContextMenu(contextMenu);//new ContentManagerContextMenu());
        canvas.addChild(image);
        documentCanvas.addMember(canvas);
        
        pagingLayout = new HLayout();
        pagingLayout.hide();
        pagingLayout.setBorder("1px solid #C0C3C7");
        pagingLayout.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
        pagingLayout.setAlign(Alignment.CENTER);
        pagingLayout.setMembersMargin(10);  
        documentCanvas.addMember(pagingLayout);
        
        previous = new IButton();
        previous.setIcon("/theme/images/icons16/resultset_previous.png");
        previous.setWidth(24);
        previous.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(EventTypes.PAGING_PREV));
			}        	
        });                
        pagingLayout.addMember(previous);
        
        pagingText = new Label();
        pagingText.setWidth(80);
        pagingText.setMargin(3);
        pagingLayout.addMember(pagingText);
        
        next = new IButton();
        next.setIcon("/theme/images/icons16/resultset_next.png");
        next.setWidth(24);
        next.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(EventTypes.PAGING_NEXT));
			}        	
        });
        pagingLayout.addMember(next);
        
        Canvas bottomCanvas = new Canvas();
        bottomCanvas.setHeight100();
        bottomCanvas.setHeight100();
        documentCanvas.addMember(bottomCanvas);
        
        videoCanvas = new VLayout();
        videoCanvas.setWidth100();
        videoCanvas.setHeight(300);
        videoCanvas.setBorder("1px solid #C0C3C7");
        videoCanvas.hide();
        Canvas videoPlayer = new Canvas();
        videoPlayer.setWidth100();
        videoPlayer.setHeight(300);
        
        videoPlayer.setContents("<div id='vplayer' style='width:485px;height:300px;'>video player</div>");
        
        videoCanvas.addMember(videoPlayer);
        addChild(videoCanvas);
        
        iframeCanvas = new HTMLPane();
        iframeCanvas.setWidth100();
        iframeCanvas.setHeight100();
        iframeCanvas.setContentsType(ContentsType.PAGE);
        iframeCanvas.setShowEdges(true); 
        addChild(iframeCanvas);
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.FILE_SELECTED)) select(event.getRecord());
	            else if(event.isType(EventTypes.WEBLINK_SELECTED)) select(event.getRecord());
	            else if(event.isType(EventTypes.WEBCONTENT_SELECTED)) select(event.getRecord());
	            else if(event.isType(EventTypes.PAGING_PREV)) fetch(file_id, page-1);
	            else if(event.isType(EventTypes.PAGING_NEXT)) fetch(file_id, page+1);
	        }
	    });
	}
	
	public void select(Record record) {
		String id = record.getAttribute("id");
		if(this.selection == null || (id != null && !this.selection.getAttribute("id").equals(id))) {
			this.selection = record;
			String type = record.getAttribute("localName");
			if(type != null) {
				if(type.equals("file")) {
					//image.setSrc("/view/"+record.getAttribute("target_id"));
					file_id = record.getAttribute("target_id");
					page = 1;
					fetch(file_id, page);
				} else if(type.equals("web_link")) {
					String url = record.getAttribute("url");
					documentCanvas.hide();
					videoCanvas.hide();
					iframeCanvas.show();
					iframeCanvas.setContentsURL(url);
				} else if(type.equals("web_content")) {
					String content = record.getAttribute("content");
					documentCanvas.hide();
					videoCanvas.hide();
					iframeCanvas.show();
					iframeCanvas.setContents(content);
				}				
			}
		}
	}
	public void clearImage() {
		videoCanvas.hide();
		documentCanvas.show();	
		image.setSrc("/theme/images/logo/ArchiveManager500_550.png");
	}
	public void setContextMenu(Menu contextMenu) {
		this.contextMenu = contextMenu;
	}

	protected void fetch(String id, int page) {
		getData(security.getServiceUrl() + "/content/stream/get.xml?id="+id+"&page="+page, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData().length > 0) {
					Record fileRec = response.getData()[0];
					if(fileRec != null) {
						try {
							String id = fileRec.getAttribute("id");
							String mimetype = fileRec.getAttribute("mimetype");
							if(mimetype.equals(Mimetypes.MIMETYPE_AVI) || 
									mimetype.equals(Mimetypes.MIMETYPE_AVI) || 
									mimetype.equals(Mimetypes.MIMETYPE_MOV) ||
									mimetype.equals(Mimetypes.MIMETYPE_MP4) ||
									mimetype.equals(Mimetypes.MIMETYPE_MP3) ||
									mimetype.equals(Mimetypes.MIMETYPE_MPEG) ||
									mimetype.equals(Mimetypes.MIMETYPE_WAV) ||
									mimetype.equals(Mimetypes.MIMETYPE_FLV)
								) {
								documentCanvas.hide();
								iframeCanvas.hide();
								videoCanvas.show();
								playVideo(id);
							} else {
								videoCanvas.hide();
								iframeCanvas.hide();
								documentCanvas.show();								
								String content = fileRec.getAttribute("content");
								if(content != null && content.length() > 0) {
									String pageStr = fileRec.getAttribute("page");
									String pagesStr = fileRec.getAttribute("pages");
									setPage(Integer.valueOf(pageStr));
									setPages(Integer.valueOf(pagesStr));
									refreshPaging();
									image.setSrc("data:image/png;base64," + content);
								} else {
									image.setSrc(rendering_image);
								}
							}
						} catch(Exception e) {
							image.setSrc(rendering_image);
						}
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.FILE_RECEIVED, fileRec));
					}
				}
				if(response != null) {
					int status = response.getStatus();
					if(status < 0)
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.FILE_ERROR));
				}
			}					
		});
	}
	protected void refreshPaging() {
		if(pages > 1) {
			pagingText.setContents("page "+page+" of "+pages);
			if(page == 1) previous.hide();
			else previous.show();
			if(page == pages) next.hide();
			else next.show();
			pagingLayout.show();
		} else pagingLayout.hide();
	}
	protected void setPage(int page) {
		this.page = page;
	}
	protected void setPages(int pages) {
		this.pages = pages;
	}
	protected void getData(String url, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
		builder.setHeader("Content-Type", "text/plain");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						DSResponse res = new DSResponse();
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	native void playVideo(String id) /*-{
		$wnd.playVideo(id);
	}-*/;
}