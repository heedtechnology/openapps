package org.heed.openapps.gwt.client.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.DSDataFormat;

public class DictionaryServiceDS {
	private OpenApps security = new OpenApps();
	private static DictionaryServiceDS instance = null; 
	
	private DictionaryDatasource dictionarySource;
	private ModelDatasource modelSource;
	private FieldDatasource fieldSource;
	private RelationDatasource relationSource;
	
	
	public static DictionaryServiceDS getInstance() {
		if (instance == null) {  
            instance = new DictionaryServiceDS();  
        }
		return instance;
	}
	public DictionaryServiceDS() {
		dictionarySource = new DictionaryDatasource();
		modelSource = new ModelDatasource();
		fieldSource = new FieldDatasource();
		relationSource = new RelationDatasource();
	}
		
	public RestDataSource getDictionaryDatasource() {
		return dictionarySource;
	}
	
	public void getModel(Criteria criteria, DSCallback callback) {
		modelSource.fetchData(criteria, callback);
	}
	
	public void addField(Record record, DSCallback callback) {
		fieldSource.addData(record, callback);
	}
	public void updateField(Record record, DSCallback callback) {
		fieldSource.updateData(record, callback);
	}
	public void removeField(Record record, DSCallback callback) {
		fieldSource.removeData(record, callback);
	}
	
	public void addRelation(Record record, DSCallback callback) {
		relationSource.addData(record, callback);
	}
	public void updateRelation(Record record, DSCallback callback) {
		relationSource.updateData(record, callback);
	}
	public void removeRelation(Record record, DSCallback callback) {
		relationSource.removeData(record, callback);
	}
	
	private class DictionaryDatasource extends RestDataSource {
		public DictionaryDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS1");
			String ticket = security.getTicket();
			if(ticket != null && ticket.length() > 0) {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/models/fetch.json?api_key="+ticket);
				setAddDataURL(security.getServiceUrl()+"service/dictionary/model/add.json?api_key="+ticket); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/model/remove.json?api_key="+ticket);
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/model/update.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/models/fetch.json");
				setAddDataURL(security.getServiceUrl()+"service/dictionary/model/add.json"); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/model/remove.json");
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/model/update.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
	private class ModelDatasource extends RestDataSource {
		public ModelDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS1");	
			String ticket = security.getTicket();
			if(ticket != null && ticket.length() > 0) {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/model/fetch.json?api_key="+ticket);
				setAddDataURL(security.getServiceUrl()+"service/dictionary/model/add.json?api_key="+ticket); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/model/remove.json?api_key="+ticket);
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/model/update.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/model/fetch.json");
				setAddDataURL(security.getServiceUrl()+"service/dictionary/model/add.json"); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/model/remove.json");
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/model/update.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
	private class FieldDatasource extends RestDataSource {
		public FieldDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS1");	
			String ticket = security.getTicket();
			if(ticket != null && ticket.length() > 0) {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/field/fetch.json?api_key="+ticket);
				setAddDataURL(security.getServiceUrl()+"service/dictionary/field/add.json?api_key="+ticket); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/field/remove.json?api_key="+ticket);
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/field/update.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/field/fetch.json");
				setAddDataURL(security.getServiceUrl()+"service/dictionary/field/add.json"); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/field/remove.json");
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/field/update.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
	private class RelationDatasource extends RestDataSource {
		public RelationDatasource() {
			setDataFormat(DSDataFormat.JSON);
			setID("EntityServiceDS1");
			String ticket = security.getTicket();
			if(ticket != null && ticket.length() > 0) {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/relation/fetch.json?api_key="+ticket);
				setAddDataURL(security.getServiceUrl()+"service/dictionary/relation/add.json?api_key="+ticket); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/relation/remove.json?api_key="+ticket);
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/relation/update.json?api_key="+ticket);
			} else {
				setFetchDataURL(security.getServiceUrl() + "service/dictionary/relation/fetch.json");
				setAddDataURL(security.getServiceUrl()+"service/dictionary/relation/add.json"); 
		        setRemoveDataURL(security.getServiceUrl()+"service/dictionary/relation/remove.json");
		        setUpdateDataURL(security.getServiceUrl()+"service/dictionary/relation/update.json");
			}
		}
		@Override
		public void removeData(Record data, DSCallback callback) {
			RestUtility.postJSON(getRemoveDataURL(), data, callback);
		}
	}
}
