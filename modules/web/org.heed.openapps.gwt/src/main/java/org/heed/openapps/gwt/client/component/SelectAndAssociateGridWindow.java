package org.heed.openapps.gwt.client.component;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;

public class SelectAndAssociateGridWindow extends Window {
	private TextItem searchField;
	private ListGrid addGrid;
	private SelectItem classificationSelect;
	
	protected Record selection;
	
	public static String MODE_UPDATE = "update";
	public static String MODE_STORE = "store";
	
	public SelectAndAssociateGridWindow(final String mode, final int width, final int height, final String title,
		final LinkedHashMap<String,String> searchTypes, final String association, 
		final String entity, final DynamicForm form, final ListGrid grid, final DSCallback callback) {
		setWidth(450);
		setHeight(350);
		setTitle(title);
		setAutoCenter(true);
		setIsModal(true);
		
		DynamicForm searchForm = new DynamicForm();
		searchForm.setWidth("100%");
		searchForm.setHeight(25);
		searchForm.setMargin(2);
		searchForm.setNumCols(3);
		searchForm.setCellPadding(2);
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
				
		if(searchTypes != null) {
			searchField.setWidth(width-150);
			classificationSelect = new SelectItem("classification");
			classificationSelect.setWidth(100);
			classificationSelect.setValueMap(searchTypes);
			//classificationSelect.setDefaultValue("{openapps.org_classification_1.0}person");
			classificationSelect.setShowTitle(false);
		} else searchField.setWidth(width-50);
		
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Criteria criteria = new Criteria();
				if(searchTypes != null) criteria.setAttribute("qname", classificationSelect.getValue());
				else criteria.setAttribute("qname", entity);
				criteria.setAttribute("field", "name");
				criteria.setAttribute("sort", "name_e");
				if(searchField.getValue() != null) {
					criteria.setAttribute("query", searchField.getValueAsString());
				} else {
					criteria.setAttribute("_startRow", "0");
					criteria.setAttribute("_endRow", "75");
				}
				addGrid.fetchData(criteria);
			}
		});
		if(searchTypes != null) searchForm.setFields(searchField,classificationSelect,searchButton);
		else searchForm.setFields(searchField,searchButton);
		addItem(searchForm);
		
		if(grid != null) addGrid = grid;
		else {
			addGrid = new ListGrid();
			ListGridField nameField = new ListGridField("name","Name");
			addGrid.setFields(nameField);
			addItem(addGrid);
		}
		addGrid.setWidth("100%");
		addGrid.setHeight("100%");
		addGrid.setMargin(2);
		addGrid.setShowHeader(false);
		addItem(addGrid);
		
		IButton linkItem = new IButton("Link");
		linkItem.setIcon("/theme/images/icons16/link.png");
		//linkItem.setHeight(30);
		linkItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(addGrid.anySelected()) {					
					if(mode.equals(MODE_UPDATE)) {	
						Record record = new Record();
						record.setAttribute("qname", association);					
						record.setAttribute("target", addGrid.getSelectedRecord().getAttribute("id"));
						record.setAttribute("source", selection.getAttribute("id"));
						EntityServiceDS.getInstance().addAssociation(record, callback);
					} else if(mode.equals(MODE_STORE)) {
						DSResponse res = new DSResponse();
						Record[] records = new Record[1];
						records[0] = addGrid.getSelectedRecord();
						res.setData(records);
						DSRequest req = new DSRequest();
						callback.execute(res, "", req);
					}
				}
			}			
		});
		if(form != null) {
			HLayout layout = new HLayout();
			layout.setMargin(5);
			layout.setHeight(30);
			layout.setWidth100();
			layout.setMembersMargin(5);
			layout.setAlign(Alignment.RIGHT);
			layout.addMember(form);
			layout.addMember(linkItem);
			addItem(layout);
		} else addItem(linkItem);
	}
	
	public void select(Record record) {
		this.selection = record;
	}
}
