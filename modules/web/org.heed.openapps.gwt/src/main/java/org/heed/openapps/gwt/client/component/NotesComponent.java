package org.heed.openapps.gwt.client.component;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.RichTextItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class NotesComponent extends VLayout {
	private AddNoteWindow addNoteWindow;
	private ListGrid grid;
	private HLayout buttons;
	private ImgButton delButton;
	private ImgButton editButton;
	
	private int addEvent = 0;
	private boolean html;
	
	private LinkedHashMap<String,String> valueMap;
	private Record selection;
	
	private int autoFitMaxHeight = 200;
	
	public NotesComponent(String id, String width, final LinkedHashMap<String,String> valueMap) {
		this(id, width, valueMap, false);
	}
	public NotesComponent(String id, String width, LinkedHashMap<String,String> valueMap, boolean html) {
		this.valueMap = valueMap;
		this.html = html;
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Notes</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.setPrompt("Add Note");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(addEvent > 0) EventBus.fireEvent(new OpenAppsEvent(addEvent));
				addNoteWindow.newRecord();
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		editButton = new ImgButton();
		editButton.setWidth(16);
		editButton.setHeight(16);
		editButton.setShowDown(false);
		editButton.setShowRollOver(false);
		editButton.setPrompt("View Note");
		editButton.setSrc("/theme/images/icons16/zoom.png");
		editButton.hide();
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(grid.getSelectedRecord() != null) {
					//String id = grid.getSelectedRecord().getAttribute("target_id");						
					addNoteWindow.show();
				} else SC.say("Please select a file to view");
			}
		});
		buttons.addMember(editButton);
						
		delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setPrompt("Delete Note");
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);					
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		addNoteWindow = new AddNoteWindow();
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				delButton.show();
				editButton.show();
				addNoteWindow.editRecord(grid.getSelectedRecord());
			}
		});
		
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth("30%");
		if(valueMap != null) typeField.setValueMap(valueMap);
		ListGridField contentField = new ListGridField("content","Content");
		contentField.setWidth("70%");
		grid.setFields(typeField, contentField);
		addMember(grid);
				
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("notes");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("notes");
					}
				}
				setData(notes);
			}
			delButton.hide();
			editButton.hide();
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	public class AddNoteWindow extends Window {
		private DynamicForm addForm;
		private ButtonItem submitItem;
		private ButtonItem saveItem;
		private FormItem contentItem;
		
		public AddNoteWindow() {
			setWidth(575);
			setHeight(255);
			setTitle("Add Note");
			setAutoCenter(true);
			setIsModal(true);
			
			List<FormItem> items = new ArrayList<FormItem>();
			
			addForm = new DynamicForm();
			addForm.setCellPadding(5);
			final SelectItem typeItem = new SelectItem("type", "Note Type");
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(300);
			items.add(typeItem);
					
			if(html) {
				contentItem = new RichTextItem("content","Content");
				contentItem.setHeight(150);
				items.add(contentItem);
			} else {
				contentItem = new TextAreaItem("content","Content");
				contentItem.setHeight(150);
				contentItem.setWidth(400);
				items.add(contentItem);
			}			
			
			submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String id = selection.getAttribute("id");
					//String uid = selection.getAttribute("uid");
					final Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_system_1_0_notes");
					record.setAttribute("entity_qname", "openapps_org_system_1_0_note");					
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("name", addForm.getValue("type"));
					record.setAttribute("content", addForm.getValue("content"));					
					if(id != null && id.length() > 0) {
						record.setAttribute("source", id);
						EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
							@Override
							public void execute(DSResponse response, Object data, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									Record record = response.getData()[0];
									select(record);									
									addForm.clearValues();
									addNoteWindow.hide();
								}
							}							
						});
					} 
				}			
			});
			items.add(submitItem);
			
			saveItem = new ButtonItem("save", "Save");
			saveItem.setIcon("/theme/images/icons16/save.png");
			saveItem.setStartRow(false);
			saveItem.setEndRow(false);
			saveItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					String id = grid.getSelectedRecord().getAttribute("target_id");
					if(id == null) id = grid.getSelectedRecord().getAttribute("id");
					record.setAttribute("id", id);
					record.setAttribute("qname", "openapps_org_system_1_0_note");
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("name", addForm.getValue("title"));
					record.setAttribute("content", addForm.getValue("content"));
					
					EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record activity = response.getData()[0];
								if(activity != null) {
									String act_id = activity.getAttribute("id");
									for(Record record : grid.getRecords()) {
										String rec_id = record.getAttribute("id");
										String rec_tgt = record.getAttribute("target_id");
										if(rec_tgt != null && rec_tgt.equals(act_id)) {
											activity.setAttribute("id", rec_id);
											activity.setAttribute("target_id", rec_tgt);
											grid.removeSelectedData();
											grid.addData(activity);
										} else if(rec_id.equals(act_id)){
											grid.removeData(record);
											grid.addData(activity);
										}
									}
								}
							}
							addNoteWindow.hide();
						}
					});
				}			
			});
			items.add(saveItem);
			
			FormItem[] fitems = new FormItem[items.size()];
			addForm.setItems(items.toArray(fitems));
			
			addItem(addForm);
		}
		public void newRecord() {
			addForm.clearValues();
			contentItem.setValue("");
			submitItem.show();
			saveItem.hide();
		}
		public void editRecord(Record record) {
			addForm.editRecord(record);
			submitItem.hide();
			saveItem.show();
		}
	}
	public int getAutoFitMaxHeight() {
		return autoFitMaxHeight;
	}
	public void setAutoFitMaxHeight(int autoFitMaxHeight) {
		this.autoFitMaxHeight = autoFitMaxHeight;
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
	}
	public void setOnAddEvent(int event) {
		addEvent = event;
	}
	public void setHtml(boolean html) {
		this.html = html;
	}
}
