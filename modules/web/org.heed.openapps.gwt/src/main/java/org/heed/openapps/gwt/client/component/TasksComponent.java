package org.heed.openapps.gwt.client.component;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class TasksComponent extends VLayout {
	private DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
	private HLayout buttons;
	private ListGrid grid;
	private AddNoteWindow addNoteWindow;
	private ImgButton editButton;
	private ImgButton delButton;
	
	private LinkedHashMap<String,String> valueMap;
	private Record selection;
	
	private int autoFitMaxHeight = 200;
	
	public TasksComponent(String title, String width, LinkedHashMap<String,String> valueMap) {
		this.valueMap = valueMap;
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Tasks</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
				
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addNoteWindow.toAddMode();
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		editButton = new ImgButton();
		editButton.setWidth(15);
		editButton.setHeight(15);
		editButton.hide();
		editButton.setSrc("/theme/images/icons16/pencil.png");
		editButton.hide();
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					addNoteWindow.toUpdateMode(record);
					addNoteWindow.show();
				}
			}
		});
		buttons.addMember(editButton);
		
		delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.hide();
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);				
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
		grid.setShowHeader(false);
		grid.addSelectionUpdatedHandler(new SelectionUpdatedHandler() {
			public void onSelectionUpdated(SelectionUpdatedEvent event) {
				editButton.show();
				delButton.show();
			}
		});
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth(100);
		
		ListGridField contentField = new ListGridField("name","Name");
		
		ListGridField statusField = new ListGridField("status","Status");
		statusField.setWidth(50);
		
		grid.setFields(typeField, contentField, statusField);
		addMember(grid);
		
		addNoteWindow = new AddNoteWindow();
	}
	
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("tasks");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("tasks");
					}
				}
				setData(notes);
			}
			delButton.hide();
			editButton.hide();
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	public class AddNoteWindow extends Window {
		private final DynamicForm addForm;
		private ButtonItem updateItem;
		private ButtonItem addItem;
		
		public AddNoteWindow() {
			setWidth(480);
			setHeight(325);
			setTitle("Add Task");
			setAutoCenter(true);
			setIsModal(true);
			
			addForm = new DynamicForm();
			addForm.setMargin(5);
			addForm.setColWidths("75","*");
			//addForm.setDataSource(new TaskDataSource());
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth(350);
			
			final DateItem startDateItem = new DateItem("start_date", "Start Date");
			//startDateItem.setUseTextField(true);
			final DateItem endDateItem = new DateItem("end_date", "End Date");
			//endDateItem.setUseTextField(true);
			
			final SelectItem typeItem = new SelectItem("type", "Task Type");			
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(190);
					
			final SelectItem statusItem = new SelectItem("status", "Status");			
			LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
			valueMap3.put("open","Open");
			valueMap3.put("process","In Process");
			valueMap3.put("paused","Paused");
			valueMap3.put("completed","Completed");
			valueMap3.put("rejected","Rejected");
			valueMap3.put("closed","Closed");
			statusItem.setValueMap(valueMap3);
			statusItem.setWidth(190);
			
			final TextAreaItem descriptionItem = new TextAreaItem("description","Description");
			descriptionItem.setHeight(125);
			descriptionItem.setWidth(350);
			/*		
			CanvasItem userItem = new CanvasItem("users", "");
			userItem.setWidth(340);
			userItem.setHeight(100);
			final UserAssociationComponent usersComponent = new UserAssociationComponent("345", "75");
			userItem.setCanvas(usersComponent);
			*/
			updateItem = new ButtonItem("update", "Update");
			updateItem.setStartRow(false);
			updateItem.setEndRow(false);
			updateItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String id = addForm.getValueAsString("target_id");
					if(id == null) id = addForm.getValueAsString("id");
					Record record = new Record();
					record.setAttribute("id", id);
					record.setAttribute("start_date", dateFormatter.format(startDateItem.getValueAsDate()));
					record.setAttribute("end_date", dateFormatter.format(endDateItem.getValueAsDate()));
					record.setAttribute("name", nameItem.getValue());
					record.setAttribute("type", typeItem.getValue());
					record.setAttribute("status", statusItem.getValue());
					//record.setAttribute("users", usersComponent.getUserIds());
					record.setAttribute("description", descriptionItem.getValue());
					
					EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record task = response.getData()[0];
								if(task != null) {
									String act_id = task.getAttribute("id");
									for(Record record : grid.getRecords()) {
										String rec_id = record.getAttribute("id");
										String rec_tgt = record.getAttribute("target_id");
										if(rec_tgt != null && rec_tgt.equals(act_id)) {
											task.setAttribute("id", rec_id);
											task.setAttribute("target_id", rec_tgt);
											grid.removeSelectedData();
											grid.addData(task);
										} else if(rec_id.equals(act_id)){
											grid.removeData(record);
											grid.addData(task);
										}
									}
								}
							}
							addNoteWindow.hide();
						}							
					});
				}			
			});
			
			addItem = new ButtonItem("submit", "Add");
			addItem.setStartRow(false);
			addItem.setEndRow(false);
			addItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_system_1_0_tasks");
					record.setAttribute("entity_qname", "openapps_org_system_1_0_task");
					record.setAttribute("source", selection.getAttribute("id"));
					record.setAttribute("start_date", addForm.getValue("start_date"));
					record.setAttribute("end_date", addForm.getValue("start_date"));
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("status", addForm.getValue("status"));
					record.setAttribute("name", addForm.getValue("name"));
					record.setAttribute("description", addForm.getValue("description"));
					//List<String> idList = usersComponent.getUserIds();
					//String ids = "";
					//for(String i : idList) ids += i+",";
					//record.setAttribute("user", ids);
					
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record record = response.getData()[0];
								select(record);									
								addForm.clearValues();
								addNoteWindow.hide();
							}
						}							
					});
				}			
			});			
			addForm.setFields(typeItem,statusItem,startDateItem,endDateItem,nameItem,descriptionItem,/*userItem,*/addItem,updateItem);
			addItem(addForm);						
		}
		public void toUpdateMode(Record activity) {
			addItem.hide();
			updateItem.show();
			addForm.editRecord(activity);
		}
		public void toAddMode() {
			addItem.show();
			updateItem.hide();
			addForm.clearValues();
		}
	}
	public int getAutoFitMaxHeight() {
		return autoFitMaxHeight;
	}
	public void setAutoFitMaxHeight(int autoFitMaxHeight) {
		this.autoFitMaxHeight = autoFitMaxHeight;
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
	}
	/*
	public class TaskDataSource extends RestDataSource {
		private OpenApps security = new OpenApps();
		
		public TaskDataSource(String id) {
			setDataFormat(DSDataFormat.JSON);
			setID(id);  
	        setTitleField("name");	        
	        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
	                
	        DataSourceTextField idField = new DataSourceTextField("id", "ID", 100);  
	        idField.setPrimaryKey(true);  
	        idField.setRequired(true);
	        
	        setFields(idField,nameField);
	        
			setAddDataURL(security.getServiceUrl() + "service/entity/associate.json");
			setFetchDataURL(security.getServiceUrl() + "/service/entity/get.json");
			setRemoveDataURL(security.getServiceUrl() + "/service/entity/remove.json");
			setUpdateDataURL(security.getServiceUrl() + "/service/entity/update.json");
		}
		@Override
		protected Object transformRequest(DSRequest dsRequest) {
			JavaScriptObject data = dsRequest.getData();
			
			//JSOHelper.setAttribute(data, "ticket", security.getTicket());
			
			return data;
		}
	}
	*/
}
