package org.heed.openapps.gwt.client.component;

import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class SubjectComponent extends VLayout {
	private Record selection;
	
	private HLayout buttons;
	private ImgButton delButton;
	
	private ListGrid grid;
	private AddSubjectWindow addWindow;
		
	private SelectionListener selectionListener;
	
	private int autoFitMaxHeight = 200;
	
	public SubjectComponent(String width, boolean editable) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Subjects</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Record rec = new Record();
					rec.setAttribute("id", record.getAttribute("id"));						
					EntityServiceDS.getInstance().removeAssociation(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}
					});				 
				}
			}
		});
		buttons.addMember(delButton);
			
		addWindow = new AddSubjectWindow();
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(autoFitMaxHeight);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		ListGridField nameField = new ListGridField("name");
		grid.setFields(nameField);
		addMember(grid);
		grid.addCellClickHandler(new CellClickHandler() {
			@Override
			public void onCellClick(CellClickEvent event) {
				delButton.show();
				selectionListener.select(event.getRecord());
			}			
		});		
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("subjects");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("subjects");
					}
				}
				setData(notes);
			}
			delButton.hide();
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	
	public class AddSubjectWindow extends Window {
		private TextItem searchField;
		private ListGrid addGrid;
		
		public AddSubjectWindow() {
			setWidth(450);
			setHeight(350);
			setTitle("Add Subject");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(3);
			searchForm.setCellPadding(2);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(380);
			
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", "openapps_org_classification_1_0_subject");
					criteria.setAttribute("field", "name");
					//criteria.setAttribute("sort", "openapps_org_system_1_0_name");
					if(searchField.getValue() != null) {
						criteria.setAttribute("query", searchField.getValueAsString());
					} else {
						criteria.setAttribute("_startRow", "0");
						criteria.setAttribute("_endRow", "75");
					}
					EntityServiceDS.getInstance().search(criteria, new DSCallback() {
						@Override
						public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
							addGrid.setData(dsResponse.getData());
						}						
					});
				}
			});
			searchForm.setFields(searchField,searchButton);
			addItem(searchForm);
			
			addGrid = new ListGrid();
			addGrid.setWidth("100%");
			addGrid.setHeight("100%");
			addGrid.setShowHeader(false);
			
			ListGridField nameField = new ListGridField("name","Name");
			addGrid.setFields(nameField);
			addItem(addGrid);
			
			final DynamicForm editForm = new DynamicForm();
			editForm.setHeight(25);
			editForm.setNumCols(6);
			editForm.setMargin(2);
			
			ButtonItem linkItem = new ButtonItem("link", "Link");
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if(addGrid.anySelected()) {
						String id = selection.getAttribute("id");
						//String uid = selection.getAttribute("uid");
						final Record record = new Record();
						record.setAttribute("assoc_qname", "openapps_org_classification_1_0_subjects");
						record.setAttribute("target", addGrid.getSelectedRecord().getAttribute("id"));
						if(id != null && id.length() > 0) {
							record.setAttribute("source", id);							
							EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
								@Override
								public void execute(DSResponse response, Object data, DSRequest request) {
									Record record = response.getData()[0];
									select(record);									
									editForm.clearValues();
									addWindow.hide();
								}								
							});
						} 					
					}
				}			
			});
			editForm.setFields(linkItem);
			addItem(editForm);
		}
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	public void setSelectionListener(SelectionListener selectionListener) {
		this.selectionListener = selectionListener;
	}
	
}
