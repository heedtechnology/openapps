package org.heed.openapps.gwt.client.data;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.widgets.grid.ListGridRecord;


public class RestUtility {
	private static DataSource ds = new DataSource();
	
	
	public static void post(String url, Record record, final DSCallback callback) {
		if(url.endsWith(".xml")) {
			postXML(url, record, callback);
		} else {
			postJSON(url, record, callback);
		}
	}
	public static void get(String url, Record record, final DSCallback callback) {
		if(url.endsWith(".xml")) {
			getXML(url, record, callback);
		} else {
			getJSON(url, record, callback);
		}
	}
	public static void get(String url, Record record, int resultBatchSize, final DSCallback callback) {
		if(url.endsWith(".xml")) {
			getXML(url, record, resultBatchSize, callback);
		} 
	}
	public static void get(String url, final DSCallback callback) {
		if(url.endsWith(".xml")) {
			getXML(url, callback);
		} 
	}
	
	public static  void getJSON(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
		try {
			builder.sendRequest(toParameterString(record), new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						JSONObject jsonValue = (JSONObject)JSONParser.parseLenient(response.getText());
			            if(jsonValue != null) {
			            	DSResponse res = new DSResponse();
							DSRequest req = new DSRequest();
							callback.execute(res, jsonValue, req);
			            }
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void postJSON(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, URL.encode(url));
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(toParameterString(record), new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						DSResponse res = new DSResponse();
						DSRequest req = new DSRequest();
						JSONObject jsonValue = (JSONObject)JSONParser.parseLenient(response.getText());
			            if(jsonValue != null) {
			            	JSONValue responseObject = jsonValue.get("response");
			            	if(responseObject != null && responseObject.isObject() != null) {
			            		JSONValue statusObject = responseObject.isObject().get("status");
			            		if(statusObject != null) {
			            			res.setStatus(Integer.valueOf((int)statusObject.isNumber().doubleValue()));
			            		}
				            	JSONValue dataObject = responseObject.isObject().get("data");
				            	if(dataObject != null) {
				            		if(dataObject.isArray() != null) {			            			
				            			ListGridRecord[] records = new ListGridRecord[dataObject.isArray().size()];
				            			for(int i=0; i < dataObject.isArray().size(); i++) {
				            				JSONValue record = dataObject.isArray().get(i);
				            				records[i] = new ListGridRecord();
				            				if(record.isObject() != null) {
				            					for(String key : record.isObject().keySet()) {
				            						records[i].setAttribute(key, record.isObject().get(key).isString());
				            					}
				            				}
				            			}
				            			res.setData(records);
				            		}
				            	}				            	
			            	}
			            }
			            if(callback != null) callback.execute(res, jsonValue, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void getData(String url, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
		builder.setHeader("Content-Type", "text/plain");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						DSResponse res = new DSResponse();
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void getXML(String url, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
		builder.setHeader("Content-Type", "text/plain");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						JsArray<JavaScriptObject> nodes = ((JavaScriptObject)XMLTools.selectNodes(data, "//data/node")).cast();
						String message = XMLTools.selectString(data, "//message");
						String totalRows = XMLTools.selectString(data, "//totalRows");
						String startRow = XMLTools.selectString(data, "//startRow");
						String endRow = XMLTools.selectString(data, "//endRow");
						DSResponse res = new DSResponse();
						res.setStatus(Integer.valueOf(XMLTools.selectString(data, "//status")));
						res.setAttribute("message", message);
						if(totalRows != null && totalRows.length() > 0) res.setAttribute("totalRows", Integer.valueOf(totalRows));
						if(startRow != null && startRow.length() > 0) res.setAttribute("startRow", Integer.valueOf(startRow));
						if(endRow != null && endRow.length() > 0) res.setAttribute("endRow", Integer.valueOf(endRow));
						res.setData(ds.recordsFromXML(nodes));
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void getXML(String url, Record record, int resultBatchSize, final DSCallback callback) {
		final DataSource highVolumeDS = new DataSource();
		highVolumeDS.setResultBatchSize(resultBatchSize);
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url)+"?"+toParameterString(record));
		//builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						JsArray<JavaScriptObject> nodes = ((JavaScriptObject)XMLTools.selectNodes(data, "//data/node")).cast();
						String message = XMLTools.selectString(data, "//message");
						String totalRows = XMLTools.selectString(data, "//totalRows");
						String startRow = XMLTools.selectString(data, "//startRow");
						String endRow = XMLTools.selectString(data, "//endRow");
						DSResponse res = new DSResponse();
						res.setData(highVolumeDS.recordsFromXML(nodes));
						res.setAttribute("message", message);
						if(totalRows != null && totalRows.length() > 0) res.setAttribute("totalRows", Integer.valueOf(totalRows));
						if(startRow != null && startRow.length() > 0) res.setAttribute("startRow", Integer.valueOf(startRow));
						if(endRow != null && endRow.length() > 0) res.setAttribute("endRow", Integer.valueOf(endRow));
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void getXML(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url)+"?"+toParameterString(record));
		//builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						JsArray<JavaScriptObject> nodes = ((JavaScriptObject)XMLTools.selectNodes(data, "//data/node")).cast();
						String message = XMLTools.selectString(data, "//message");
						String totalRows = XMLTools.selectString(data, "//totalRows");
						String startRow = XMLTools.selectString(data, "//startRow");
						String endRow = XMLTools.selectString(data, "//endRow");
						DSResponse res = new DSResponse();
						res.setData(ds.recordsFromXML(nodes));
						res.setAttribute("message", message);
						if(totalRows != null && totalRows.length() > 0) res.setAttribute("totalRows", Integer.valueOf(totalRows));
						if(startRow != null && startRow.length() > 0) res.setAttribute("startRow", Integer.valueOf(startRow));
						if(endRow != null && endRow.length() > 0) res.setAttribute("endRow", Integer.valueOf(endRow));
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public static void postXML(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, URL.encode(url));
		builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(toParameterString(record), new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						JsArray<JavaScriptObject> nodes = ((JavaScriptObject)XMLTools.selectNodes(data, "//data/node")).cast();
						String message = XMLTools.selectString(data, "//message");
						DSResponse res = new DSResponse();
						res.setData(ds.recordsFromXML(nodes));
						res.setAttribute("message", message);
						DSRequest req = new DSRequest();
						if(callback != null) callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	
	public static String toParameterString(Record record) {
		String requestData = new String();
		for(String key : record.getAttributes()) {
			if(!key.equals("__ref")) requestData += key+"="+record.getAttribute(key)+"&";
		}
		if(requestData.endsWith("&")) requestData = requestData.substring(0, requestData.length()-1);
		return URL.encode(requestData);
	}
}
