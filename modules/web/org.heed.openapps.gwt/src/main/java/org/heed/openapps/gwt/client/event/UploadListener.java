package org.heed.openapps.gwt.client.event;


public interface UploadListener {

	void uploadComplete(String fileName);
	
}
