package org.heed.openapps.gwt.client.event;

import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;

public class OpenAppsMessageEvent extends OpenAppsEvent {

	
	public OpenAppsMessageEvent(String status, String message) {
		Record r = new Record();
		r.setAttribute("status", status);
		r.setAttribute("message", message);
		setRecord(r);
	}
}
