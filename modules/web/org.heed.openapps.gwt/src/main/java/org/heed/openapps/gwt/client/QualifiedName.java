/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.gwt.client;


public class QualifiedName  implements java.io.Serializable {
	private static final long serialVersionUID = -3703913360078545820L;
	private Long id;
    private String namespace;
    private String localName;
    private String prefix;

    public static final char NAMESPACE_PREFIX = ':';
    public static final char NAMESPACE_BEGIN = '{';
    public static final char NAMESPACE_END = '}';
    
    public QualifiedName() {}
    public QualifiedName(String namespace, String localName) {
        this.namespace = namespace;
        this.localName = localName;
    }
    public QualifiedName(String prefix, String namespace, String localName) {
        this.prefix = prefix;
    	this.namespace = namespace;
        this.localName = localName;
    }
    
    public String getPrefixString() {
    	return prefix;
    }
    
    public static QualifiedName createQualifiedName(String namespace, String localName) throws InvalidQualifiedNameException {
    	if(namespace == null || localName == null)
    		throw new InvalidQualifiedNameException("Argument qname is mandatory");
    	return new QualifiedName(namespace, localName);
    }
    public static QualifiedName createQualifiedName(String qname) throws InvalidQualifiedNameException {
    	if (qname == null || qname.length() == 0) {
            throw new InvalidQualifiedNameException("Argument qname is mandatory qname:" + qname);
        }
        String namespaceURI = null;
        String localName = null;
        
        /**parse the old format if it starts properly, otherwise new format **/
        if(qname.charAt(0) == NAMESPACE_BEGIN) {
        	int namespaceBegin = qname.indexOf(NAMESPACE_BEGIN);
            int namespaceEnd = -1;            
            if(namespaceBegin != -1) {
                if(namespaceBegin != 0) {
                    throw new InvalidQualifiedNameException("QName '" + qname + "' must start with a namespaceURI");
                }
                namespaceEnd = qname.indexOf(NAMESPACE_END, namespaceBegin + 1);
                if(namespaceEnd == -1) {
                    throw new InvalidQualifiedNameException("QName '" + qname + "' is missing the closing namespace "+NAMESPACE_END+" token");
                }
                namespaceURI = qname.substring(namespaceBegin + 1, namespaceEnd);
            } else {
            	namespaceBegin = 0;
            	namespaceEnd = qname.lastIndexOf("_");
            	namespaceURI = qname.substring(namespaceBegin, namespaceEnd);
            }            
            // Parse name
            if(namespaceEnd > -1) {
    	        localName = qname.substring(namespaceEnd + 1);
    	        if(localName == null || localName.length() == 0) {
    	            throw new InvalidQualifiedNameException("QName '" + qname + "' must consist of a local name");
    	        }
            }
        } else {        
	        // Parse namespace
	        int namespaceEnd = qname.lastIndexOf("_");
	        namespaceURI = qname.substring(0, namespaceEnd);
	                
	        // Parse name
	        if(namespaceEnd > -1) {
		        localName = qname.substring(namespaceEnd + 1);
		        if(localName == null || localName.length() == 0) {
		            throw new InvalidQualifiedNameException("QName '" + qname + "' must consist of a local name");
		        }
	        } 
        }
        return new QualifiedName(namespaceURI, localName);
    }
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getNamespace() {
        return this.namespace;
    }
    
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getLocalName() {
        if(this.localName != null) return this.localName;
        else return "";
    }
    
    public void setLocalName(String localName) {
        this.localName = localName;
    }
    public boolean isComplete() {
    	return (namespace != null && localName != null);
    }
    public boolean equals(Object object) {
        if(this == object) return true;
        if(object instanceof QualifiedName) {
        	QualifiedName other = (QualifiedName)object;
            // namespaceURI and localname are not allowed to be null
            if(this.namespace == null && other.namespace == null) return this.localName.equals(other.localName);
        	if(this.namespace == null) return false;
            return (this.localName.equals(other.localName) && this.namespace.equals(other.namespace));
        } 
        return false;
    }
    public String toString() {
    	return new StringBuilder(80).append(NAMESPACE_BEGIN).append(namespace).append(NAMESPACE_END).append(localName).toString();
    }
   
}


