package org.heed.openapps.gwt.client;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class OpenAppsModel {

	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getYesNoValues() { 
		JavaScriptObject obj = getNativeYesNoValues();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeYesNoValues() /*-{
        return $wnd.yesno_values;
	}-*/;
}
