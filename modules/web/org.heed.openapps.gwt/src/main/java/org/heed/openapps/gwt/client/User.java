package org.heed.openapps.gwt.client;

import java.util.ArrayList;
import java.util.List;


public class User {
	private String username;
	private List<String> roles = new ArrayList<String>();
	
	
	public User(String username) {
		this.username = username;
	}
	
	public boolean isAdmin() {
		return hasRole("Administrator");
	}
	public boolean hasRole(String name) {
		for(String role : roles) {
			if(role.toLowerCase().equals(name.toLowerCase())) return true;			
		}
		return false;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
}
