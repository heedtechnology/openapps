package org.heed.openapps.gwt.client.component;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class EntitySelectionComponent extends VLayout {
	private String qname;
	private ListGrid grid;
	
	
	public EntitySelectionComponent(String qname, String title, int minHeight, int maxHeight) {
		this.qname = qname;
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+title+"</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight(minHeight);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(maxHeight);
		grid.setShowHeader(false);
		grid.setSortField("name");
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, event.getRecord()));
			}
		});
		ListGridField nameField = new ListGridField("name");
		grid.setFields(nameField);
		addMember(grid);
	}
	public void setData(Record[] data) {
		grid.setData(data);
	}
	public void fetchData(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", qname);
		criteria.setAttribute("field", "freetext");
		criteria.setAttribute("sort", "name_e");
		criteria.setAttribute("sources", "false");
		
		EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					grid.setData(response.getData());
				} else grid.setData(new Record[0]);
				grid.setEmptyMessage("No items to show.");
			}						
		});
	}
	public Record getSelectedRecord() {
		return grid.getSelectedRecord();
	}
}
