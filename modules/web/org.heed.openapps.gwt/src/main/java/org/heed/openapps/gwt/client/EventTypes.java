package org.heed.openapps.gwt.client;

public class EventTypes {
	public static final int FILE_SELECTED = 1;
	public static final int FILE_RECEIVED = 2;
	public static final int FILE_ERROR = 3;
	
	public static final int CRAWL_START = 4;
	public static final int CRAWL_END = 5;
	public static final int ADD = 6;
	public static final int DELETE = 7;
	public static final int SAVE = 8;
	public static final int SELECTION = 9;	
	public static final int REFRESH = 10;
	public static final int EDIT = 11;
	
	public static final int MODULE_SELECT = 12;
	public static final int MESSAGE = 13;
	
	public static final int PAGING = 14;
	public static final int PAGING_FIRST = 15;
	public static final int PAGING_PREV = 16;
	public static final int PAGING_NEXT = 17;
	public static final int PAGING_LAST = 18;
	
	public static final int BACK = 19;
	
	public static final int IMPORT_START = 20;
	public static final int IMPORT_END = 21;
	
	public static final int UPLOAD_MODEL = 22;
	public static final int UPLOAD_ENTITY = 23;
	
	public static final int CLEAN = 24;
	public static final int INDEX = 25;
	
	public static final int BROWSE = 29;
	public static final int SEARCH = 30;
	
	public static final int WEBLINK_SELECTED = 40;
	public static final int WEBCONTENT_SELECTED = 41;
	
	public static final int DATA_UPDATE = 50;
}
