package org.heed.openapps.gwt.client.form;
import java.util.LinkedHashMap;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class SelectTextItem extends CanvasItem {
	private HLayout layout;
	private TextItem spinnerItem;
	private SelectItem selectItem;
	
	public SelectTextItem(String name, String title, int width, LinkedHashMap<String,Object> valueMap) {
		super(name,title);
		setWidth("100%");
		setHeight("100%");
		
		layout = new HLayout();
		layout.setWidth(115);
		layout.setHeight(30);
		layout.setMargin(5);
		
		DynamicForm form = new DynamicForm();
		form.setWidth(150);
		form.setNumCols(5);
		
		spinnerItem = new TextItem("hours");
		spinnerItem.setWidth(75);
		spinnerItem.setShowTitle(false);
		spinnerItem.setAlign(Alignment.CENTER);
		
		selectItem = new SelectItem("minutes");
		selectItem.setWidth(width-60);
		selectItem.setShowTitle(false);
		selectItem.setAlign(Alignment.CENTER);
				
		selectItem.setValueMap(valueMap);
		
		form.setFields(selectItem,spinnerItem);
		layout.addChild(form);
		
		setCanvas(layout);
	}
	@Override
	public void setValue(String value) {
		if(value != null && value.contains(":")) {
			String[] vals = value.split(":");
			spinnerItem.setValue(vals[0]);
			selectItem.setValue(vals[1]);
		}
	}
	public String getSpinnerValue() {
		if(spinnerItem.getValue() == null) return "";
		return spinnerItem.getValueAsString();
	}
	public void setSpinnerValue(String value) {
		spinnerItem.setValue(value);
	}
	public String getSelectValue() {
		if(selectItem.getValue() == null) return "";
		return selectItem.getValueAsString();
	}
	public void setSelectValue(String value) {
		selectItem.setValue(value);
	}
}
