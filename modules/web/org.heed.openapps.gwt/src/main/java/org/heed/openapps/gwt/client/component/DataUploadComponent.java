package org.heed.openapps.gwt.client.component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.layout.HLayout;



public class DataUploadComponent extends HLayout {
	private OpenApps security = new OpenApps();
	private DynamicForm uploadForm;
	private UploadListener listener;
	private SelectItem modeItem;
	private UploadItem fileItem;
	
	
	public DataUploadComponent(LinkedHashMap<String,Object> modes) {
		setBorder("1px solid #A7ABB4");
		initComplete(this);
		
		List<FormItem> items = new ArrayList<FormItem>();
		
		ValuesManager vm = new ValuesManager();
		uploadForm = new DynamicForm();
		uploadForm.setMargin(5);
		uploadForm.setValuesManager(vm);
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setTarget("uploadTarget");
		uploadForm.setNumCols(8);
		
		modeItem = new SelectItem("mode","Mode");
		modeItem.setWidth(200);
		modeItem.setValueMap(modes);
		items.add(modeItem);
		
		fileItem = new UploadItem("file", "File");
		fileItem.setWidth(300);				
		items.add(fileItem);		
		
		ButtonItem uploadButton = new ButtonItem("upload","Upload");
		uploadButton.setStartRow(false);
		uploadButton.setEndRow(false);
		uploadButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler(){
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent e) {
				Object obj = fileItem.getDisplayValue();
				if (obj != null) {
					uploadForm.submitForm();
				} else
					SC.say("Please select a file.");
			}
		});
		items.add(uploadButton);
		
		NamedFrame frame = new NamedFrame("uploadTarget");
		frame.setWidth("1");
		frame.setHeight("1");
		frame.setVisible(false);
		addChild(frame);		
		
		ButtonItem saveButton = new ButtonItem("save", "Save");
		saveButton.setStartRow(false);
		saveButton.setEndRow(false);
		saveButton.setPrompt("Save");		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.IMPORT_START));
			}
		});
		items.add(saveButton);
		
		FormItem[] fitems = new FormItem[items.size()];
		items.toArray(fitems);
		uploadForm.setItems(fitems);
		addMember(uploadForm);
	}
	
	public void setAction(String url) {
		uploadForm.setAction(url);
	}
	
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}
	
	public void uploadComplete(String fileName) {
		if (listener != null) {
			listener.uploadComplete(fileName);
		}
	}
	public void setModel(LinkedHashMap<String,Object> modes) {
		modeItem.setValueMap(modes);
	}
	
	private native void initComplete(DataUploadComponent upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.heed.openapps.gwt.client.component.DataUploadComponent::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}
