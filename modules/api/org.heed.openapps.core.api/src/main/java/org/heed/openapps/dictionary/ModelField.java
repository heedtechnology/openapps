package org.heed.openapps.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.util.StringFormatUtility;


public class ModelField implements ModelObject {
	private static final long serialVersionUID = 4234751793033801218L;
	private long id;
	private String uid;
	private Model model;
	private QName qname;
	private String name;
	private String description;
	private String label;
	private int type;
	private int index;
	private boolean mandatory;
	private boolean unique;
	private boolean hidden;	
	private boolean tokenized;
	private int minSize;
	private int maxSize;
	private int order;
	private int format;
	private int sort;
	private List<ModelFieldValue> values = new ArrayList<ModelFieldValue>();
	private List<ModelFieldAspect> aspects = new ArrayList<ModelFieldAspect>();
	
	public static final int TYPE_NULL = 0;
    public static final int TYPE_BOOLEAN = 1;
    public static final int TYPE_INTEGER = 2;
    public static final int TYPE_LONG = 3;
    public static final int TYPE_DOUBLE = 4;
    public static final int TYPE_TEXT = 5;
    public static final int TYPE_DATE = 6;
    public static final int TYPE_LONGTEXT = 7;
    public static final int TYPE_SERIALIZABLE = 8;
    public static final int TYPE_ASPECT = 9;
    
    public static final int FORMAT_PASSWORD = 1;
    public static final int FORMAT_EMAIL = 2;
    public static final int FORMAT_HTML = 3;
    public static final int FORMAT_URL = 4;
    public static final int FORMAT_CURRENCY = 5;
    
    public ModelField(){}
    public ModelField(long id, QName qname) {
    	this.id = id;
    	this.qname = qname;
	}
        
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public QName getQName() {
		return qname;
	}
	public void setQName(QName qname) {
		this.qname = qname;
	}	
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public boolean isUnique() {
		return unique;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	public void setUnique(boolean unique) {
		this.unique = unique;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean isTokenized() {
		return tokenized;
	}
	public void setTokenized(boolean tokenized) {
		this.tokenized = tokenized;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getMinSize() {
		return minSize;
	}
	public void setMinSize(int minSize) {
		this.minSize = minSize;
	}
	public int getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getFormat() {
		return format;
	}
	public void setFormat(int format) {
		this.format = format;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public List<ModelFieldValue> getValues() {
		return values;
	}
	public void setValues(List<ModelFieldValue> values) {
		this.values = values;
	}
	public List<ModelFieldAspect> getAspects() {
		return aspects;
	}
	public void setAspects(List<ModelFieldAspect> aspects) {
		this.aspects = aspects;
	}
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public String getName() {
		if(name == null) return StringFormatUtility.toTitleCase(qname.getLocalName());
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}		
}
