package org.heed.openapps.entity;

import java.io.Serializable;
import java.util.Map;

import org.heed.openapps.entity.data.FormatInstructions;


public interface ExportProcessor extends Serializable {
			
	public Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException;
	public Object export(FormatInstructions instructions, Association association) throws InvalidEntityException;
	
	public Map<String,Object> exportMap(FormatInstructions instructions, Association association) throws InvalidEntityException;
}
