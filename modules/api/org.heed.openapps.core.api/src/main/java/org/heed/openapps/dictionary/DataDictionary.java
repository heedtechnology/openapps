package org.heed.openapps.dictionary;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.DictionaryModel;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;


public class DataDictionary implements ModelObject {
	private static final long serialVersionUID = 4736326787129643097L;
	private Long id;
	private String uid;
	private String name;
	private String description;
	private String inheritance;
	private boolean isPublic;
	
	private Map<String, Model> inheritedModels = new HashMap<String, Model>();
	private Map<String, Model> localModels = new HashMap<String, Model>();
	
	protected ModelSorter modelSorter = new ModelSorter();
	
	public DataDictionary() {}
	public DataDictionary(String name) {
		this.name = name;
	}
	
	public List<QName> getQNames(QName qname) throws InvalidQualifiedNameException {
		List<QName> qnames = new ArrayList<QName>();
		qnames.add(qname);
		try	{
			Model model = getModel(qname);
			if(model != null && model.getParentName() != null) {
				try {
					//Model parent = (Model)getModel(model.getParent());
					QName parentQname = model.getParentName();
					if(parentQname != null) {
						List<QName> parentNodes = getQNames(parentQname);
						if(parentNodes != null) 
							qnames.addAll(parentNodes);
					}
				} catch(Exception e) {
					throw new InvalidQualifiedNameException("getNames() failed, invalid parent:"+qname.toString());
				}
			}
		} catch(Exception e) {
			throw new InvalidQualifiedNameException("");
		}
		return qnames;
	}
	public ModelField getModelField(QName model, QName field) throws DataDictionaryException {
		List<ModelField> fields = getModelFields(model);
		for(ModelField modelField : fields) {
			if(modelField.getQName().equals(field))
				return modelField;
		}
		return null;
	}
	public List<Model> getChildModels(QName qname) {
		List<Model> models = new ArrayList<Model>();
		Model model = getModel(qname);
		assert(model != null);
		List<Model> allModels = getAllModels();
		for(Model m : allModels) {
			if(m.getParentName() != null && m.getParentName().equals(model.getQName()))
				models.add(m);
		}
		return models;
	}
	public boolean isDescendant(QName parent, QName child) throws InvalidQualifiedNameException {
		if(parent.equals(child)) return true;
		Model parentModel = getModel(parent);
		Model childModel = getModel(child);
		if(parentModel != null && childModel != null && childModel.getParent() != null) {
			if(childModel.getParent().equals(parentModel.getId())) return true;
		}
		return false;
	}
	public Model getModel(long id) {
		Model model = null;
		for(Model m : localModels.values()) {
			if(m.getId() == id) model = m;
		}
		for(Model m : inheritedModels.values()) {
			if(m.getId() == id) model = m;
		}
		return model;
	}
	public Model getModel(QName qname) {
		Model model = localModels.get(qname.toString());
		if(model == null) model = inheritedModels.get(qname.toString());
		return model;
	}
	public List<ModelField> getModelFields(QName qname) throws DataDictionaryException {
		List<ModelField> fields = new ArrayList<ModelField>();
		try {
			Model model = localModels.get(qname.toString());
			if(model == null) model = inheritedModels.get(qname.toString());
			while(model != null) {
				fields.addAll(model.getFields());
				model = model.getParent();
			}
		} catch(Exception e) {
			throw new DataDictionaryException("problem getting model fields for "+qname.toString(), e);
		}
		return fields;
	}
	public List<ModelRelation> getModelRelations(QName qname, int direction) throws DataDictionaryException {
		List<ModelRelation> relations = new ArrayList<ModelRelation>();
		try {
			Model model = localModels.get(qname.toString());
			if(model == null) model = inheritedModels.get(qname.toString());
			while(model != null) {
				if(direction == ModelRelation.DIRECTION_INCOMING) relations.addAll(model.getTargetRelations());
				else if(direction == ModelRelation.DIRECTION_OUTGOING) relations.addAll(model.getSourceRelations());
				model = model.getParent();
			}
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		}
		return relations;
	}
	public ModelRelation getModelRelation(QName modelQname, QName relationQname) throws DataDictionaryException {
		Model model = getModel(modelQname);
		while(model != null) {
			for(ModelRelation modelRelation : model.getRelations()) {
				if(modelRelation.getQName().toString().equals(relationQname.toString()))
					return modelRelation;
			}
			model = model.getParent();
		}
		throw new DataDictionaryException("no model relation found for model:"+modelQname.toString()+" relation:"+relationQname.toString());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Model> getAllModels() {
		List<Model> list = new ArrayList<Model>();
		list.addAll(inheritedModels.values());
		list.addAll(localModels.values());
		Collections.sort(list, modelSorter);		
		return list;
	}
	public List<Model> getInheritedModels() {
		List<Model> list = new ArrayList<Model>();
		list.addAll(inheritedModels.values());
		Collections.sort(list, modelSorter);		
		return list;
	}
	public List<Model> getLocalModels() {
		List<Model> list = new ArrayList<Model>();
		list.addAll(localModels.values());
		Collections.sort(list, modelSorter);		
		return list;
	}
	public void setInheritedModels(List<Model> models) {
		for(Model model : models) {
			inheritedModels.put(model.getQName().toString(), model);
		}
	}
	public void setLocalModels(List<Model> models) {
		for(Model model : models) {
			localModels.put(model.getQName().toString(), model);
		}
	}
	public String getOntology() {
		return "";
	}
	@Override
	public QName getQName() {
		return DictionaryModel.DICTIONARY;
	}
	public String getInheritance() {
		return inheritance;
	}
	public void setInheritance(String inheritance) {
		this.inheritance = inheritance;
	}
	
}
