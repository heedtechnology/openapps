package org.heed.openapps.search.indexing;

public interface IndexReader {

	Object getNativeIndexReader();
	
}
