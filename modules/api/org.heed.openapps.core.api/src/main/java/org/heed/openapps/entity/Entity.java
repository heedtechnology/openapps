/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.node.Node;
import org.heed.openapps.data.Sort;
import org.heed.openapps.util.SortedList;


public class Entity implements Serializable {
	private static final long serialVersionUID = -5414261842849202782L;
	private Node node;
	private long dictionary; //transient
	private String source; //the id from third-party data provider
    private QName parentQName;
    private QName labelField = SystemModel.NAME;
	private List<QName> childQNames = new ArrayList<QName>(0);
    private List<ACE> entries = new ArrayList<ACE>(0);
    private List<QName> aspects = new ArrayList<QName>(0); 
    private List<Property> properties = new ArrayList<Property>(0);
    private List<Association> sourceAssociations = new SortedList<Association>(new AssociationSorter(new Sort(Sort.STRING, "openapps_org_system_1_0_name", false)));
    private List<Association> targetAssociations = new SortedList<Association>(new AssociationSorter(new Sort(Sort.STRING, "openapps_org_system_1_0_name", false)));
    
    public Entity() {
    	this.node = new Node();
    }
    public Entity(Node node) {
    	this.node = node;
    }   
    public Entity(QName qname) {
    	this.node = new Node();
    	node.setQName(qname);
    }
    public Entity(long id, QName qname) {
    	this.node = new Node();
    	node.setId(id);
    	node.setQName(qname);
    }
    
    public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public long getDictionary() {
		return dictionary;
	}
	public void setDictionary(long dictionary) {
		this.dictionary = dictionary;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getLabel() {
    	if(hasProperty(labelField)) {
    		return getPropertyValue(labelField);
    	}
    	return "";
    }
    public QName getLabelField() {
		return labelField;
	}
	public void setLabelField(QName labelField) {
		this.labelField = labelField;
	}
	public List<ACE> getEntries() {
		return entries;
	}
	public void setEntries(List<ACE> entries) {
		this.entries = entries;
	}
	public void addAspect(QName qname) {
    	aspects.add(qname);
    }    
    public List<QName> getAspects() {
        return this.aspects;
    }
    public void setAspects(List<QName> aspects) {
        this.aspects = aspects;
    }    
    public List<Property> getProperties() {
        return this.properties;
    }
    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }
    public void addProperty(QName qname, Object value)  throws InvalidPropertyException {
    	if(qname == null) throw new InvalidPropertyException("");
    	Property prop = getProperty(qname);
    	if(prop != null) {
    		prop.setValue(value);
    		return;
    	}
    	prop = new Property(qname, value);
    	if(prop != null) properties.add(prop);
    }
    public void addProperty(int type, QName qname, Object value)  throws InvalidPropertyException {
    	if(qname == null) throw new InvalidPropertyException("");
    	Property prop = getProperty(qname);    	
    	if(prop != null) {
    		prop.setType(type);
    		prop.setValue(value);
    		return;
    	}
    	prop = new Property(qname, value);
    	if(prop != null) {
    		prop.setType(type);
    		properties.add(prop);
    	}
    }
    public void addProperty(String localname, Object value)  throws InvalidPropertyException {
    	addProperty(new QName(node.getQName().getNamespace(), localname), value);
    }
    public Property getProperty(String qname) {
    	try {
    		QName q = QName.createQualifiedName(qname);
    		return getProperty(q);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }
    public Property getProperty(QName qname) {
    	if(qname != null && qname.getNamespace() != null && qname.getLocalName() != null) {
    		for(Property prop : this.getProperties()) {
    			if(prop.getQName() != null && prop.getQName().getNamespace() != null && prop.getQName().getLocalName() != null &&
    					prop.getQName().getNamespace().equals(qname.getNamespace()) && prop.getQName().getLocalName().equals(qname.getLocalName())) 
    				return prop;
    		}
    	}
    	return null;
    }
    public String getPropertyValue(QName qname) {
    	Property prop = getProperty(qname.getNamespace(), qname.getLocalName());
    	if(prop != null) return prop.toString();
    	return "";
    }
    public Property getProperty(String namespace, String localname) {
    	return getProperty(new QName(namespace, localname));
    }
    public List<Property> getProperties(QName qname) {
    	List<Property> props = new ArrayList<Property>();
    	if(qname != null && qname.getNamespace() != null && qname.getLocalName() != null) {
    		for(Property prop : this.getProperties()) {
    			if(prop.getQName().getNamespace().equals(qname.getNamespace()) && prop.getQName().getLocalName().equals(qname.getLocalName())) 
    				props.add(prop);
    		}
    	}
    	return props;
    }
    public boolean hasProperty(String qname) {
    	try {
    		QName q = QName.createQualifiedName(qname);
    		return hasProperty(q);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return false;
    }
    public boolean hasProperty(QName qname) {
    	for(Property prop : this.getProperties()) {
    		if(prop.getQName().equals(qname))
    			return true;
    	}
    	return false;
    }
    public void removeProperty(QName qname) {
    	List<Property> props = new ArrayList<Property>();
    	for(Property prop : this.getProperties()) {
    		if(prop.getQName().equals(qname)) {
    			props.add(prop);
    		}
    	}
    	for(Property prop : props) this.getProperties().remove(prop);
    }
    public List<Association> getAssociations() {
    	List<Association> assocs = new ArrayList<Association>();
    	assocs.addAll(sourceAssociations);
    	assocs.addAll(targetAssociations);
    	return assocs;
    }
    public List<Association> getAssociations(QName qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : sourceAssociations) {
    		if(a.getQName().equals(qname)) assocs.add(a);
    	}
    	for(Association a : targetAssociations) {
    		if(a.getQName().equals(qname)) assocs.add(a);
    	}
    	return assocs;
    }
    public List<Association> getSourceAssociations() {
        return this.sourceAssociations;
    }
    public List<QName> getSourceAssociationQNames() {
    	List<QName> names = new ArrayList<QName>();
    	for(Association assoc : sourceAssociations) {
    		if(!names.contains(assoc.getQName())) names.add(assoc.getQName());
    	}
    	return names;
    }
    public Association getSourceAssociation(QName... qname) {
    	for(Association a : sourceAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) return a;
    		} 
    	}
    	return null;
    }
    public boolean containsSourceAssociation(QName qname) {
    	for(Association a : sourceAssociations) {
    		if(a.getQName().equals(qname)) return true;
    	}
    	return false;
    }
    public List<Association> getSourceAssociations(QName... qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : sourceAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) assocs.add(a);
    		}    		
    	}
    	return assocs;
    }
    public List<Association> clearSourceAssociations(QName qname) {
    	return sourceAssociations;
    }
    public void setSourceAssociations(List<Association> sourceAssociations) {
        this.sourceAssociations = sourceAssociations;
    }
    
    public List<Association> getTargetAssociations() {
        return this.targetAssociations;
    }
    public List<QName> getTargetAssociationQNames() {
    	List<QName> names = new ArrayList<QName>();
    	for(Association assoc : targetAssociations) {
    		if(!names.contains(assoc.getQName())) names.add(assoc.getQName());
    	}
    	return names;
    }
    public Association getTargetAssociation(QName... qname) {
    	for(Association a : targetAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) return a;
    		}  
    	}
    	return null;
    }
    public boolean containsTargetAssociation(QName qname) {
    	for(Association a : targetAssociations) {
    		if(a.getQName().equals(qname)) return true;
    	}
    	return false;
    }
    public List<Association> getTargetAssociations(QName... qname) {
    	List<Association> assocs = new ArrayList<Association>();
    	for(Association a : targetAssociations) {
    		for(int i=0; i < qname.length; i++) {
    			if(a.getQName().equals(qname[i])) assocs.add(a);
    		}    		
    	}
    	return assocs;
    }
    public void setTargetAssociations(List<Association> targetAssociations) {
        this.targetAssociations = targetAssociations;
    }
   
    public Association getParent() {
    	if(this.parentQName != null) {
    		return getTargetAssociation(this.parentQName);
    	} else if(targetAssociations.size() > 0) return targetAssociations.get(0);
        return null;
    }
    public List<Association> getChildren() {
    	if(this.childQNames.size() > 0) {
    		return getSourceAssociations((QName[])this.childQNames.toArray());
    	} else return getSourceAssociations();
    }    
    
    public Long getId() {
        return node.getId();
    }    
    public void setId(Long id) {
        node.setId(id);
    } 
    public Long getXid() {
        return node.getXid();
    }    
    public void setXid(Long id) {
        node.setXid(id);
    } 
    public QName getQName() {
    	return node.getQName();
    }    
    public void setQName(QName qname) {
    	node.setQName(qname);
    }    
    public String getUuid() {
        return node.getUuid();
    }
    public void setUuid(String uuid) {
        node.setUuid(uuid);
    }
    public String getName() {
    	return node.getName();
    }
    public void setName(String name) {
    	node.setName(name);
    }
	public long getUser() {
		return node.getUser();
	}
	public void setUser(long user) {
		node.setUser(user);
	}
	public long getCreator() {
		return node.getCreator();
	}
	public void setCreator(long creator) {
		node.setCreator(creator);
	}	
	public long getModifier() {
		return node.getModifier();
	}
	public void setModifier(long modifier) {
		node.setModifier(modifier);
	}
	public long getCreated() {
		return node.getCreated();
	}
	public void setCreated(long created) {
		node.setCreated(created);
	}
	public long getModified() {
		return node.getModified();
	}
	public void setModified(long modified) {
		node.setModified(modified);
	}
	public long getAccessed() {
		return node.getAccessed();
	}
	public void setAccessed(long accessed) {
		node.setAccessed(accessed);
	}
	public Boolean getDeleted() {
		return node.isDeleted();
	}
	public void setDeleted(Boolean deleted) {
		node.setDeleted(deleted);
	}
	
	public QName getParentQName() {
		return parentQName;
	}
	public void setParentQName(QName parentQName) {
		this.parentQName = parentQName;
	}
	public List<QName> getChildQNames() {
		return childQNames;
	}
	public void setChildQNames(List<QName> childQNames) {
		this.childQNames = childQNames;
	}	
	public Map<String,Object> getPropertyMap() {
		Map<String,Object> map = new HashMap<String,Object>();
		for(Property prop : properties) {
			map.put(prop.getQName().getLocalName(), prop.toString());
		}
		return map;
	}
	public Map<String,String> toMap() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("namespace", node.getQName().getNamespace());
		map.put("localname", node.getQName().getLocalName());
		for(Property property : properties) {
			map.put(property.getQName().getLocalName(), property.toString());
		}
		return map;
	}
	public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof Entity)) {
            return false;
        }
        Entity that = (Entity) obj;
        if(this.getId() != null && that.getId() != null && this.getId().equals(that.getId()))
            return true;
        
        return false;
    }
    
    public int hashCode() {
        return getUuid().hashCode();
    }
}