package org.heed.openapps.search.dictionary;

import org.heed.openapps.search.Token;
import org.heed.openapps.search.dictionary.DefinitionSupport;

public class NumericDefinition extends DefinitionSupport {

	
	public NumericDefinition(String value) {
		super(Token.NUMB, value, value);
	}
	@Override
	public String toString() {
		return "Number";
	}
}