package org.heed.openapps.util;

import org.heed.openapps.util.io.FastStringBuffer;

public class StringFormatUtility {

	/**
	 * Return a string which converts the milliseconds value in the elapsed
	 * parameter to day/hours/minutes/seconds.
	 */
	public static String getElapsedTime(long startTime) {
		float elapsed = System.currentTimeMillis() - startTime;
		FastStringBuffer buf = new FastStringBuffer();
		elapsed = elapsed / (24 * 60 * 60 * 1000);
		int days = (int) elapsed;
		elapsed -= days;
		elapsed *= 24;
		int hours = (int) elapsed;
		elapsed -= hours;
		elapsed *= 60;
		int minutes = (int) elapsed;
		elapsed -= minutes;
		elapsed *= 60;
		int seconds = (int) elapsed;
		elapsed -= seconds;
		elapsed *= 1000;
		int milliseconds = (int) elapsed;
		if (days > 0) buf.append(days + " days ");
		if (hours > 0) buf.append(hours + " hours ");
		if (minutes > 0) buf.append(minutes + " minutes ");
		if (seconds > 0) buf.append(seconds + " seconds ");
		if (milliseconds > 0) buf.append(milliseconds + " ms ");
		return buf.toString();
	}
	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}  
}
