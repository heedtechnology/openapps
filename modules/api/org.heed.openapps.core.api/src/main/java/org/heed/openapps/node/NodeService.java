package org.heed.openapps.node;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;

public interface NodeService extends Serializable {
	
	long count(NodeQuery query) throws NodeException;
	NodeResultset search(NodeQuery query) throws NodeException;
	
	List<Long> getAllNodes() throws NodeException;
	List<Long> getNodes(long parent) throws NodeException;
	List<Long> getNodes(QName qname) throws NodeException;
	Long getNodeId(String uid) throws NodeException;
	Node getNode(long id) throws NodeException;
	
	void removeNode(long id) throws NodeException;
	void removeNode(long id, TraversalQuery cascade) throws NodeException;
	long createNode(String uid) throws NodeException;
	
	void addUpdateNode(Node node) throws NodeException;
	Map<String, Object> getNodeProperties(long id) throws NodeException;
	boolean hasNodeProperty(long nodeId, String key) throws NodeException;
	Object getNodeProperty(long nodeId, String key) throws NodeException;
	void addUpdateNodeProperty(long nodeId, String key, Object value) throws NodeException;
	void removeNodeProperty(long nodeId, String key) throws NodeException;
	
	List<Long> getAllRelationships() throws NodeException;
	List<Relationship> getRelationships(long nodeId, Direction direction) throws NodeException;
	List<Relationship> getRelationships(long nodeId, Direction direction, QName... qnames) throws NodeException;
	Relationship getSingleRelationship(long nodeId, QName qname, Direction dir) throws NodeException;
	
	void removeRelationship(long id) throws NodeException;
	Relationship createRelationship(QName qname, long start, long end) throws NodeException;
	
	boolean hasRelationship(long id, Direction direction) throws NodeException;
	Relationship getRelationship(long id) throws NodeException;
	void addUpdateRelationship(String qname, long startNodeId, long endNodeId) throws NodeException;
	
	Map<String, Object> getRelationshipProperties(long id) throws NodeException;
	boolean hasRelationshipProperty(long relationshipId, String key) throws NodeException;
	Object getRelationshipProperty(long relationshipId, String key) throws NodeException;
	void addUpdateRelationshipProperty(long relationshipId, String key, Object value) throws NodeException;
	void removeRelationshipProperty(long relationshipId, String key) throws NodeException;
	
	QName getNodeQName(long id) throws NodeException;
	QName getRelationshipQName(long id) throws NodeException;
	
	Transaction getTransaction();
	void indexNode(long nodeId, Map<String,Object> properties) throws NodeException;
	void indexProperty(long nodeId, String propertyName, Object value) throws NodeException;
	void deindexNode(long nodeId) throws NodeException;
	
	void shutdown();
}
