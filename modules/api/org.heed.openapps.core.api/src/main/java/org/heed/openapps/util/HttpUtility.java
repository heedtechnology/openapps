/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


public class HttpUtility {
	private final static Logger log = Logger.getLogger(HttpUtility.class.getName());
	
	public static String getLastPathName(HttpServletRequest req) {
		return req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/")+1);
	}
		
	protected static String convertStreamToString(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();
			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else { 
			log.info("stream is empty returning ''.");
			return "";
		}
	}
	
	public static Double getParmDoub(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);
		if(NumberUtility.isDouble(value))
			return Double.valueOf(value);
		return 0.0;
	}
	public static Boolean getParmBool(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);
		if(value != null) {
			if(value.equals("true")) return Boolean.TRUE;
		}
		return false;
	}
	public static String getParmStr(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);
		if(value != null)
			return value;
		return "";
	}
	public static Integer getParmInt(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);
		if(NumberUtility.isInteger(value))
			return Integer.valueOf(value);
		return 0;
	}
	public static Long getParmLong(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);
		if(NumberUtility.isLong(value))
			return Long.valueOf(value);
		return 0L;
	}

}
