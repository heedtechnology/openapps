package org.heed.openapps.reporting;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;



public class ReportingDataSource {
	protected List<Entity> items;
	protected Map<String,Object> parameters;
	protected Iterator<Entity> listIterator = null;  
	protected Entity fixedItem = null;  
	    
	
	public ReportingDataSource(List<Entity> items) {
		this.items = items;
		listIterator = items.iterator();
	}
	public ReportingDataSource(List<Entity> items, Map<String,Object> parameters) {
		this.items = items;
		this.parameters = parameters;
		listIterator = items.iterator();
	}
	
	public Object getFieldValue(String field) {
		Object o = fixedItem.getPropertyValue(new QName("", field)); 
		
        return o;
	}

	public boolean next() {
		if(listIterator.hasNext()) {  
            fixedItem = listIterator.next();  
        }        
        return listIterator.hasNext();
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public List<Entity> getItems() {
		return items;
	}
	
}
