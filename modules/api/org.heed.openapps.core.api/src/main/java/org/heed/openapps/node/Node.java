package org.heed.openapps.node;

import java.io.Serializable;

import org.heed.openapps.QName;

public class Node implements Serializable {
    private static final long serialVersionUID = -2277626331167878122L;
	private long id;
	private long xid;
    private QName qname;
    private String uuid;
    private String name;
    private long created;
    private long modified;
    private long creator;
    private long modifier;
    private long accessed;
	private long user;
    private boolean deleted;
    
	public Node() {}
	public Node(long id, QName qname) {
		this.id = id;
		this.qname = qname;
	}
	public Node(long id, QName qname, String name) {
		this.id = id;
		this.qname = qname;
		this.name = name;
	}
	public Node(long id, QName qname, String name, long created, long modified, long creator, long modifier) {
		this.id = id;
		this.qname = qname;
		this.name = name;
		this.created = created;
		this.modified = modified;
		this.creator = creator;
		this.modifier = modifier;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getXid() {
		return xid;
	}
	public void setXid(long xid) {
		this.xid = xid;
	}
	public QName getQName() {
		return qname;
	}
	public void setQName(QName qname) {
		this.qname = qname;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public long getModified() {
		return modified;
	}
	public void setModified(long modified) {
		this.modified = modified;
	}
	public long getCreator() {
		return creator;
	}
	public void setCreator(long creator) {
		this.creator = creator;
	}
	public long getModifier() {
		return modifier;
	}
	public void setModifier(long modifier) {
		this.modifier = modifier;
	}
	public long getUser() {
		return user;
	}
	public void setUser(long user) {
		this.user = user;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public long getAccessed() {
		return accessed;
	}
	public void setAccessed(long accessed) {
		this.accessed = accessed;
	}
    
}
