package org.heed.openapps.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class DateUtility {
	final static TimeZone GMT = TimeZone.getTimeZone("GMT");
	
	private static final ThreadLocal<Calendar> TL_CAL = new ThreadLocal<Calendar>() {
	    @Override
	    protected Calendar initialValue() {
	      return Calendar.getInstance(GMT, Locale.US);
	    }
	};
	/**
	   * Converts a Date to a string suitable for indexing.
	   * 
	   * @param date the date to be converted
	   * @param resolution the desired resolution, see
	   *  {@link #round(Date, DateTools.Resolution)}
	   * @return a string in format <code>yyyyMMddHHmmssSSS</code> or shorter,
	   *  depending on <code>resolution</code>; using GMT as timezone 
	   */
	public static String dateToString(Date date, Resolution resolution) {
		  return timeToString(date.getTime(), resolution);
	}

	private static final ThreadLocal<SimpleDateFormat[]> TL_FORMATS = new ThreadLocal<SimpleDateFormat[]>() {
		@Override
		protected SimpleDateFormat[] initialValue() {
			SimpleDateFormat[] arr = new SimpleDateFormat[Resolution.MILLISECOND.formatLen+1];
		    for (Resolution resolution : Resolution.values()) {
		    	arr[resolution.formatLen] = (SimpleDateFormat)resolution.format.clone();
		    }
		    return arr;
		}
	};
		  
	  /**
	   * Converts a millisecond time to a string suitable for indexing.
	   * 
	   * @param time the date expressed as milliseconds since January 1, 1970, 00:00:00 GMT
	   * @param resolution the desired resolution, see
	   *  {@link #round(long, DateTools.Resolution)}
	   * @return a string in format <code>yyyyMMddHHmmssSSS</code> or shorter,
	   *  depending on <code>resolution</code>; using GMT as timezone
	   */
	  public static String timeToString(long time, Resolution resolution) {
	    final Date date = new Date(round(time, resolution));
	    return TL_FORMATS.get()[resolution.formatLen].format(date);
	  }
	  
	  /**
	   * Limit a date's resolution. For example, the date <code>1095767411000</code>
	   * (which represents 2004-09-21 13:50:11) will be changed to 
	   * <code>1093989600000</code> (2004-09-01 00:00:00) when using
	   * <code>Resolution.MONTH</code>.
	   * 
	   * @param resolution The desired resolution of the date to be returned
	   * @return the date with all values more precise than <code>resolution</code>
	   *  set to 0 or 1, expressed as milliseconds since January 1, 1970, 00:00:00 GMT
	   */
	  @SuppressWarnings("fallthrough")
	  public static long round(long time, Resolution resolution) {
	    final Calendar calInstance = TL_CAL.get();
	    calInstance.setTimeInMillis(time);
	    
	    switch (resolution) {
	      //NOTE: switch statement fall-through is deliberate
	      case YEAR:
	        calInstance.set(Calendar.MONTH, 0);
	      case MONTH:
	        calInstance.set(Calendar.DAY_OF_MONTH, 1);
	      case DAY:
	        calInstance.set(Calendar.HOUR_OF_DAY, 0);
	      case HOUR:
	        calInstance.set(Calendar.MINUTE, 0);
	      case MINUTE:
	        calInstance.set(Calendar.SECOND, 0);
	      case SECOND:
	        calInstance.set(Calendar.MILLISECOND, 0);
	      case MILLISECOND:
	        // don't cut off anything
	        break;
	      default:
	        throw new IllegalArgumentException("unknown resolution " + resolution);
	    }
	    return calInstance.getTimeInMillis();
	  }
	  
	  /** Specifies the time granularity. */
	  public static enum Resolution {
	    
	    YEAR(4), MONTH(6), DAY(8), HOUR(10), MINUTE(12), SECOND(14), MILLISECOND(17);

	    final int formatLen;
	    final SimpleDateFormat format;//should be cloned before use, since it's not threadsafe

	    Resolution(int formatLen) {
	      this.formatLen = formatLen;
	      // formatLen 10's place:                     11111111
	      // formatLen  1's place:            12345678901234567
	      this.format = new SimpleDateFormat("yyyyMMddHHmmssSSS".substring(0,formatLen),Locale.US);
	      this.format.setTimeZone(GMT);
	    }

	    /** this method returns the name of the resolution
	     * in lowercase (for backwards compatibility) */
	    @Override
	    public String toString() {
	      return super.toString().toLowerCase(Locale.ENGLISH);
	    }

	  }
}
