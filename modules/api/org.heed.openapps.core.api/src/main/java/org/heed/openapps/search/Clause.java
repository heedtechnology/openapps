package org.heed.openapps.search;

import java.util.ArrayList;
import java.util.List;


public class Clause {
	public String operator;
	public List<Parameter> parameters = new ArrayList<Parameter>();
	
	public static final String OPERATOR_OR = "OR";
	public static final String OPERATOR_AND = "AND";
	public static final String OPERATOR_NOT = "NOT";
	
	
	public Clause() {}
	public Clause(String operator, Parameter p1) {
		this.operator = operator;
		parameters.add(p1);
	}
	public Clause(String operator, Parameter p1, Parameter p2) {
		this.operator = operator;
		parameters.add(p1);
		parameters.add(p2);
	}
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public void addParameter(String name, String value) {
		parameters.add(new Parameter(name, value));
	}
	public void addParamater(Parameter parm) {
		parameters.add(parm);
	}
	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	
}
