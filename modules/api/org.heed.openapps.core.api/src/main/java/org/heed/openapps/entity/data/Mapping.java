/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity.data;

import java.util.ArrayList;
import java.util.List;

public class Mapping {
	private String name;
	private String namespace;
	private String localName;
	private String parser;
	private List<Column> columns = new ArrayList<Column>();
	
	
	
	public Mapping(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTo(String from) {
		for(Column col : columns) {
			if(col.getFrom().equals(from)) return col.getTo();
		}
		return null;
	}
	public boolean getAllowDuplicates(String from) {
		for(Column col : columns) {
			if(col.getFrom().equals(from)) return col.allowDuplicates();
		}
		return true;
	}
	public void addColumn(Column column) {
		columns.add(column);
	}
	public List<Column> getColumns() {
		return columns;
	}
	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getLocalName() {
		return localName;
	}
	public void setLocalName(String localName) {
		this.localName = localName;
	}
	public String getParser() {
		return parser;
	}
	public void setParser(String parser) {
		this.parser = parser;
	}
	
}
