package org.heed.openapps.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.User;
import org.heed.openapps.data.Sort;
import org.heed.openapps.search.Clause;
import org.heed.openapps.search.Parameter;
import org.heed.openapps.search.Property;


public class SearchRequest {
	private String searcherId = "default";
	private List<QName> qnames = new ArrayList<QName>();
	private String query;
	private String context = "";
	private int startRow = 0;
	private int endRow = 0;
	private String operator = "OR";
	private List<Property> properties = new ArrayList<Property>();
	private List<Parameter> parameters = new ArrayList<Parameter>();
	private List<Clause> clauses = new ArrayList<Clause>();
	private List<Sort> sorts = new ArrayList<Sort>();
	private boolean attributes = true;
	private boolean sources = false;
	private boolean caseSensitive = false;
	private User user;
	private long dictionary;
	private Map<String, String[]> requestParameters;
	
	public static final String OPERATOR_OR = "OR";
	public static final String OPERATOR_AND = "AND";
	
	public SearchRequest() {}
	public SearchRequest(QName qname, String query) {
		this.qnames.add(qname);
		this.query = query;
		if(qname != null) searcherId = qname.toString();
	}
	public SearchRequest(List<QName> qnames, String query) {
		this.qnames = qnames;
		this.query = query;
		//if(qname != null) searcherId = qname.toString();
	}
	
	public String getSearcherId() {
		return searcherId;
	}
	public void setSearcherId(String searcherId) {
		this.searcherId = searcherId;
	}
	public QName getQname() {
		if(qnames.size() > 0) return qnames.get(0);
		return null;
	}
	public void setQname(QName qname) {
		this.qnames.add(qname);
	}
	public List<QName> getQnames() {
		return qnames;
	}
	public void setQnames(List<QName> qnames) {
		this.qnames = qnames;
	}
	public void setQnames(QName[] qnames) {
		for(QName qname : qnames) {
			if(!this.qnames.contains(qname))
				this.qnames.add(qname);
		}
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public void addProperty(Property property) {
		this.properties.add(property);
	}
	public List<Property> getProperties() {
		return properties;
	}
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	public void addParameter(Parameter p) {
		parameters.add(p);
	}
	public void addParameter(String name, String value) {
		parameters.add(new Parameter(name, value));
	}
	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	public void addClause(Clause clause) {
		clauses.add(clause);
	}
	public List<Clause> getClauses() {
		return clauses;
	}
	public void setClauses(List<Clause> clauses) {
		this.clauses = clauses;
	}
	public List<Sort> getSorts() {
		return sorts;
	}
	public void setSorts(List<Sort> sorts) {
		this.sorts = sorts;
	}
	public void addSort(Sort sort) {
		sorts.add(sort);
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public boolean isAttributes() {
		return attributes;
	}
	public void setAttributes(boolean attributes) {
		this.attributes = attributes;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public boolean printSources() {
		return sources;
	}
	public void setPrintSources(boolean sources) {
		this.sources = sources;
	}
	public boolean isCaseSensitive() {
		return caseSensitive;
	}
	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Map<String, String[]> getRequestParameters() {
		return requestParameters;
	}
	public void setRequestParameters(Map<String, String[]> requestParameters) {
		this.requestParameters = requestParameters;
	}
	public long getDictionary() {
		return dictionary;
	}
	public void setDictionary(long dictionary) {
		this.dictionary = dictionary;
	}
	
}
