package org.heed.openapps.search.dictionary;

import org.heed.openapps.search.Token;

public class EntityDefinition extends DefinitionSupport {

	
	public EntityDefinition(String name, String value) {
		super(Token.ENTITY, name, value);
	}
}
