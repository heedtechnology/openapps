package org.heed.openapps.content;

public interface DigitalObjectService {

	FileNode addDigitalObject(long userId, String name, String description, byte[] payload);
	FileNode getDigitalObject(long nodeId);
	void removeDigitalObject(long nodeId);
	
}
