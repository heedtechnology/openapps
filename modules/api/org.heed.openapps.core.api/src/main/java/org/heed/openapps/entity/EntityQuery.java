package org.heed.openapps.entity;

import java.util.ArrayList;
import java.util.List;

//import org.apache.lucene.search.Query;









import org.heed.openapps.QName;
import org.heed.openapps.data.Sort;

public class EntityQuery {
	private int type = 3;
	private QName[] entityQnames;
	private String queryString;
	private long xid;
	private long user;
	private Sort sort;
	private String defaultOperator = "AND";
	private int size = 75;
	private int startRow = 0;
	private int endRow = size;
	private List<Property> properties = new ArrayList<Property>();
	private List<String> fields = new ArrayList<String>();
	private Object nativeQuery;
	private Object countQuery;
	
	public static final int TYPE_NATIVE = 1;
    public static final int TYPE_LUCENE = 2;
    public static final int TYPE_CYPHER = 3;
    
    
	public EntityQuery() {}
	public EntityQuery(Object nativeQuery) {
		this.nativeQuery = nativeQuery;
	}
	public EntityQuery(QName entityQname) {
		this.entityQnames = new QName[] {entityQname};
	}
	public EntityQuery(QName[] entityQnames) {
		this.entityQnames = entityQnames;
	}
	public EntityQuery(QName entityQname, String queryString) {
		this.entityQnames = new QName[] {entityQname};
		this.queryString = queryString;
	}
	public EntityQuery(QName[] entityQnames, String queryString) {
		this.entityQnames = entityQnames;
		this.queryString = queryString;
	}
	public EntityQuery(QName entityQname, String queryString, String sort, boolean reverse) {
		this.entityQnames = new QName[] {entityQname};
		this.queryString = queryString;
		this.sort = new Sort(Sort.STRING, sort, reverse);
	}
	public EntityQuery(QName[] entityQnames, String queryString, String sort, boolean reverse) {
		this.entityQnames = entityQnames;
		this.queryString = queryString;
		this.sort = new Sort(Sort.STRING, sort, reverse);
	}
	public EntityQuery(QName entityQname, String field, String queryString, String sort, boolean reverse) {
		this.entityQnames = new QName[] {entityQname};
		this.queryString = queryString;
		this.fields.add(field);
		this.sort = new Sort(Sort.STRING, sort, reverse);
	}
	public EntityQuery(QName[] entityQnames, String field, String queryString, String sort, boolean reverse) {
		this.entityQnames = entityQnames;
		this.queryString = queryString;
		this.fields.add(field);
		this.sort = new Sort(Sort.STRING, sort, reverse);
	}
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public QName[] getEntityQnames() {
		return entityQnames;
	}
	public void setEntityQnames(QName[] entityQnames) {
		this.entityQnames = entityQnames;
	}
	public List<Property> getProperties() {
		return properties;
	}
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	public List<String> getFields() {
		return fields;
	}
	public void setFields(List<String> fields) {
		this.fields = fields;
	}
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String query) {
		this.queryString = query;
	}
	public Sort getSort() {
		return sort;
	}
	public void setSort(String sort, boolean direction) {
		this.sort = new Sort(Sort.STRING, sort, direction);
	}
	public void setSort(Sort sort) {
		this.sort = sort;
	}
	public String getDefaultOperator() {
		return defaultOperator;
	}
	public void setDefaultOperator(String defaultOperator) {
		this.defaultOperator = defaultOperator;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public Object getNativeQuery() {
		return nativeQuery;
	}
	public void setNativeQuery(Object nativeQuery) {
		this.nativeQuery = nativeQuery;
	}
	public long getUser() {
		return user;
	}
	public void setUser(long user) {
		this.user = user;
	}
	public Object getCountQuery() {
		return countQuery;
	}
	public void setCountQuery(Object countQuery) {
		this.countQuery = countQuery;
	}
	public long getXid() {
		return xid;
	}
	public void setXid(long xid) {
		this.xid = xid;
	}
			
}
