package org.heed.openapps.node;

public class NodeException extends Exception {
	private static final long serialVersionUID = -2933721957236514635L;

	public NodeException(String msg) {
		super(msg);
	}
	public NodeException(String msg, Exception e) {
		super(msg, e);
	}
}
