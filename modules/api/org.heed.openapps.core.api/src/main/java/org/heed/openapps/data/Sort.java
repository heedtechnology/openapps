package org.heed.openapps.data;

public class Sort {
	private String field;
	private int type;
	private boolean reverse;
	private Object sortField;
	
	public static final int SCORE = 0;
	public static final int DOC = 1;
	public static final int DATE = 2;
	public static final int STRING = 3;	
	public static final int INTEGER = 4;
	public static final int FLOAT = 5;
	public static final int LONG = 6;
	public static final int DOUBLE = 7;
	
	
	public Sort() {}
	public Sort(int type, String field, boolean reverse) {
		this.type = type;
		this.field = field;
		this.reverse = reverse;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isReverse() {
		return reverse;
	}
	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Object getSortField() {
		return sortField;
	}
	public void setSortField(Object sortField) {
		this.sortField = sortField;
	}
	
}
