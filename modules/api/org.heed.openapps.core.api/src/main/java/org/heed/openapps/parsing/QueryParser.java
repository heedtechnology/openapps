package org.heed.openapps.parsing;

import org.heed.openapps.search.SearchQuery;
import org.heed.openapps.search.SearchRequest;


public interface QueryParser {

	SearchQuery parse(SearchRequest request);
		
}
