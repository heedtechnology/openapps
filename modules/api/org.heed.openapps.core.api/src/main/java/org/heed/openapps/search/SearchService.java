package org.heed.openapps.search;

import java.io.Serializable;
import java.util.List;

import org.heed.openapps.entity.Entity;


public interface SearchService extends Serializable {

	SearchResponse search(SearchRequest request);	
	void reload(Entity entity);
			
	void update(Long data, boolean wait);
	void update(List<Long> data);
	void remove(Long id);
	
}
