package org.heed.openapps.node;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.data.Sort;
import org.heed.openapps.property.Property;

public class NodeQuery {
	private int type;
	private QName qname;
	private long nodeId;
	private List<Property> properties = new ArrayList<Property>();
	private Object queryOrQueryObject;
    private Sort sorting;
    private int start;
    private int end;
    
    public static final int TYPE_NATIVE = 1;
    public static final int TYPE_LUCENE = 2;
    public static final int TYPE_CYPHER = 3;
    
    
    public NodeQuery(int type) {
    	this.type = type;
    }
    public NodeQuery(QName qname) {
    	this.qname = qname;
    }
    public NodeQuery(Object queryOrQueryObject) {
        this.queryOrQueryObject = queryOrQueryObject;
    }
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Sort getSorting() {
		return sorting;
	}
	public void setSorting(Sort sorting) {
		this.sorting = sorting;
	}
	public Object getQueryOrQueryObject() {
		return queryOrQueryObject;
	}
	public void setQueryOrQueryObject(Object query) {
		queryOrQueryObject = query;
	}
	public QName getQname() {
		return qname;
	}
	public void setQname(QName qname) {
		this.qname = qname;
	}
	public List<Property> getProperties() {
		return properties;
	}
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public long getNodeId() {
		return nodeId;
	}
	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}
	
}