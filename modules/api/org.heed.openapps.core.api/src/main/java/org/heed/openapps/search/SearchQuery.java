package org.heed.openapps.search;

import org.heed.openapps.QName;

import java.util.ArrayList;
import java.util.Collection;

public class SearchQuery {
	private String type;
	private Object value;
	private QName qname;
	private String parse;
	
	public static final String TYPE_LUCENE = "lucene";

    private final Collection<Exception> parserExceptions =
            new ArrayList<Exception>();


    public SearchQuery() {}
	public SearchQuery(String type) {
		this.type = type;
	}
	public SearchQuery(String type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public QName getQname() {
		return qname;
	}
	public void setQname(QName qname) {
		this.qname = qname;
	}

    public Collection<Exception> getParserExceptions() {
        return parserExceptions;
    }

    public void setQName(String name) {
		try {
			this.qname = QName.createQualifiedName(name);
		} catch(Exception e) {

			e.printStackTrace();
		}
		
	}
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}

    public void addParserException(Exception parserException) {
        parserExceptions.add(parserException);
    }
}