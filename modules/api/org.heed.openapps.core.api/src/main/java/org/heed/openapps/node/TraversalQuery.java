package org.heed.openapps.node;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;

public class TraversalQuery {
	private int order;
	private int direction;
	private int depth;
	
	private List<QName> qnames = new ArrayList<QName>();
	
	public static final int ORDER_BREADTH_FIRST = 1;
	public static final int ORDER_DEPTH_FIRST = 2;
		
	public static final int DIRECTION_OUT = 1;
	public static final int DIRECTION_IN = 2;
	
	public TraversalQuery() {}
	public TraversalQuery(int order, int direction, int depth) {
		this.order = order;
		this.direction = direction;
		this.depth = depth;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public List<QName> getQNames() {
		return qnames;
	}
	public void setQNames(List<QName> qnames) {
		this.qnames = qnames;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
		
}
