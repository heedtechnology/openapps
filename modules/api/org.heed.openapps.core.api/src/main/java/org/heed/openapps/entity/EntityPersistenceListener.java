package org.heed.openapps.entity;
import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.QName;
import org.heed.openapps.scheduling.Job;


public interface EntityPersistenceListener {

	void onBeforeAdd(Entity entity);
	void onAfterAdd(Entity entity);
	void onBeforeUpdate(Entity oldValue, Entity newValue);
	void onAfterUpdate(Entity entity);
	void onBeforeDelete(Entity entity);
	void onAfterDelete(Entity entity);
	
	void onBeforeAssociationAdd(Association assoc);
	void onAfterAssociationAdd(Association assoc);
	void onBeforeAssociationUpdate(Association assoc);
	void onAfterAssociationUpdate(Association assoc);
	void onBeforeAssociationDelete(Association assoc);
	void onAfterAssociationDelete(Association assoc);
	
	Entity extractEntity(HttpServletRequest request, QName entityQname) throws InvalidEntityException;
	Entity extractEntity(long node, QName entityQname);
	
	Job index(QName qname);
	void index(long id);
	
}
