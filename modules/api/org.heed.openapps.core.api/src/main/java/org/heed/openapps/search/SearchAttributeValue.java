/*
 * Copyright (C) 2010 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.search;

import java.util.BitSet;


public class SearchAttributeValue {
	private SearchAttribute attribute;
	private String name;
	private String query;
	private int count;
	private String value;
	private BitSet bits;
	
	
	public SearchAttributeValue(String name) {
		this.name = name;
	}
	public SearchAttributeValue(String name, String query) {
		this.name = name;
		this.query = query;
	}
	public SearchAttributeValue(String name,String query, int count) {
		this.name = name;
		this.query = query;
		this.count = count;
	}
	public SearchAttributeValue(String name,String query, String value) {
		this.name = name;
		this.query = query;
		this.value = value;
	}
	
	public SearchAttribute getAttribute() {
		return attribute;
	}
	public void setAttribute(SearchAttribute attribute) {
		this.attribute = attribute;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public int getCount() {
		return count;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public BitSet getBits() {
		return bits;
	}
	public void setBits(BitSet bits) {
		this.bits = bits;
	}
}
