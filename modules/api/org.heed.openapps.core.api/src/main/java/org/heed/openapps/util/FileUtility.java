package org.heed.openapps.util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.content.Mimetypes;


public class FileUtility {

	
	/**
	 * Return a filename's extension.
	 * @return the extension, or null if one cannot be detected
	 */
	public static String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		// get extension
		int pos = filename.lastIndexOf('.');
		if (pos == -1 || pos == filename.length() - 1)
			return null;
		String ext = filename.substring(pos + 1, filename.length());
		ext = ext.toLowerCase();
		return ext;
	}
	public static String readToString(File file) throws Exception {
		StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
	}
	public static File readToFile(InputStream in, String extension) throws Exception {
		File temp = File.createTempFile(java.util.UUID.randomUUID().toString(), extension);
		temp.deleteOnExit();
		OutputStream out = new FileOutputStream(temp);
		IOUtility.pipe(in, out);
		in.close();
		out.flush();
		out.close();
	    return temp;
	}
	public static boolean isImage(String fileName) {
		String mimetype = getMimetype(fileName);
        if(mimetype.equals(Mimetypes.MIMETYPE_IMAGE_GIF) || 
        		mimetype.equals(Mimetypes.MIMETYPE_IMAGE_JPEG) || 
        		mimetype.equals(Mimetypes.MIMETYPE_IMAGE_TIF) ||
        		mimetype.equals(Mimetypes.MIMETYPE_IMAGE_PNG))
        	return true;
		return false;
	}
	public static boolean hasChildren(File file) {
		if(!file.isDirectory()) return false;
		String[] dirs = file.list();
		return dirs.length > 0;
	}
	public static boolean hasChildFiles(File file) {
		if(!file.isDirectory()) return false;
		File[] dirs = file.listFiles(new FileFilter() {
		   	@Override
			public boolean accept(File f) {
		   		if(!f.isDirectory()) return true;
				return false;
			}
		});
		return dirs.length > 0;
	}
	public static boolean hasChildDirectories(File file) {
		if(!file.isDirectory()) return false;
		File[] dirs = file.listFiles(new FileFilter() {
		   	@Override
			public boolean accept(File f) {
				if(f.isDirectory()) return true;
				return false;
			}
		});
		return dirs.length > 0;
	}
	public static String getMimetype(String name) {
		if(name != null) {
			String fileName = name.toLowerCase();
			if(fileName.endsWith(".gif")) return Mimetypes.MIMETYPE_IMAGE_GIF;
			if(fileName.endsWith(".jpg")) return Mimetypes.MIMETYPE_IMAGE_JPEG;
			if(fileName.endsWith(".png")) return Mimetypes.MIMETYPE_IMAGE_PNG;
			if(fileName.endsWith(".pdf")) return Mimetypes.MIMETYPE_PDF;
			if(fileName.endsWith(".tif") || fileName.endsWith(".tiff")) return Mimetypes.MIMETYPE_IMAGE_TIF;
			if(fileName.endsWith(".mp3")) return Mimetypes.MIMETYPE_MP3;
			if(fileName.endsWith(".mp4")) return Mimetypes.MIMETYPE_MP4;
			if(fileName.endsWith(".flv")) return Mimetypes.MIMETYPE_FLV;
			if(fileName.endsWith(".xml")) return Mimetypes.MIMETYPE_XML;
			if(fileName.endsWith(".xls")) return Mimetypes.MIMETYPE_EXCEL;
			if(fileName.endsWith(".ppt")) return Mimetypes.MIMETYPE_PPT;
			if(fileName.endsWith(".doc")) return Mimetypes.MIMETYPE_WORD;
		}
		return "";
	}
	public static String getFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	public static List<File> listFiles(File dir) {
		if(dir == null) return new ArrayList<File>(0);
	    List<File> fileTree = new ArrayList<File>();
	    for (File entry : dir.listFiles()) {
	        if (entry.isFile()) fileTree.add(entry);
	        else fileTree.addAll(listFiles(entry));
	    }
	    return fileTree;
	}
}
