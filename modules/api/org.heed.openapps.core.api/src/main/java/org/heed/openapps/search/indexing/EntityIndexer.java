package org.heed.openapps.search.indexing;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.InvalidEntityException;

public interface EntityIndexer {

	IndexEntity index(Entity entity) throws InvalidEntityException;
	void deindex(QName qname, Entity entity) throws InvalidEntityException;
	
}
