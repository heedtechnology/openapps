package org.heed.openapps.search.indexing;
import java.io.IOException;
import java.util.List;


public interface IndexingService {

	void update(Long data, boolean wait);
	void update(List<Long> data);
	void remove(Long id);
	
	List<IndexReader> getIndexReaders() throws IOException;
	IndexSearcher getIndexSearcher(String ctx) throws IOException;
	
	void returnIndexReaders(List<IndexReader> readers);
	EntityIndexer getEntityIndexer(String name);
	
}
