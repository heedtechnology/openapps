package org.heed.openapps.logging;

public interface Logger {

	void debug(String msg);
	void error(String msg);
	void error(Throwable exception);
	void info(String msg);
	
}
