package org.heed.openapps.dictionary;

import java.io.Serializable;
import java.util.List;


public interface DataDictionaryService extends Serializable {

	DataDictionary getSystemDictionary();
	List<DataDictionary> getBaseDictionaries();
	DataDictionary getDataDictionary(long id);
		
	void addUpdate(ModelObject modelObject);
	void remove(long modelObjectId) throws DataDictionaryException;
	
	DataDictionary reloadDataDictionary(long id) throws DataDictionaryException;
	
}
