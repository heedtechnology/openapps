package org.heed.openapps.node;

public interface MultiUserNodeService {

	NodeService getNodeService(long userId);
	
}
