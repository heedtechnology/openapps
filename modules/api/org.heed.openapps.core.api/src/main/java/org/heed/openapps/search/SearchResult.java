package org.heed.openapps.search;

import java.util.HashMap;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.search.Property;


public class SearchResult {
	private float rawScore;
	private int normalizedScore;
	private String classification;
	private Entity entity;
	private Map<String,String> data = new HashMap<String,String>();
	
	
	public SearchResult(Entity entity) {
		this.entity = entity;
	}
	
	public long getId() {
		return entity.getId();
	}
	public String getUid() {
		return entity.getUuid();
	}
	public QName getQName() {
		return entity.getQName();
	}
	public long getCreated() {
		return entity.getCreated();
	}
	public boolean hasProperty(QName qname) {
		return entity.hasProperty(qname);
	}
	public Property getProperty(QName qname) {
		org.heed.openapps.entity.Property prop = entity.getProperty(qname);
		return new Property(prop.getQName().getLocalName(), String.valueOf(prop.getValue()));		
	}
	
	public String get(String key) {
		return data.get(key);
	}
	public void put(String key, String value) {
		data.put(key, value);
	}
	public Map<String,String> getData() {
		return data;
	}
	
	public float getRawScore() {
		return rawScore;
	}
	public void setRawScore(float score) {
		this.rawScore = score;
	}
	public int getNormalizedScore() {
		return normalizedScore;
	}
	public void setNormalizedScore(int normalizedScore) {
		this.normalizedScore = normalizedScore;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public Entity getEntity() {
		return entity;
	}
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
}
