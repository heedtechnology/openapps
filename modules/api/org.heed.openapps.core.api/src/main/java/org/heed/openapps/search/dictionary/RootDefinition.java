package org.heed.openapps.search.dictionary;

import org.heed.openapps.search.Token;

public class RootDefinition extends DefinitionSupport {

	
	public RootDefinition(String name, String value) {
		super(Token.ROOT, name, value);
	}
}
