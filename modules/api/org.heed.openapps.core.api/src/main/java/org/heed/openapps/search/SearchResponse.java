package org.heed.openapps.search;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.heed.openapps.search.SearchNode;
import org.heed.openapps.search.Token;


public class SearchResponse {
	private int resultSize;
	private int startRow;
	private int endRow;	
	private SearchQuery searchQuery;
	private List<SearchResult> results = new ArrayList<SearchResult>();
	private List<SearchAttribute> attributes = new ArrayList<SearchAttribute>();
	private List<SearchNode> breadcrumb;
	private List<Token> tokens;
	private BitSet bits;
	private float topScore;
	private long time;
	
	
	public int getResultSize() {
		return resultSize;
	}
	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}	
	public String getQueryParse() {
		if(searchQuery == null) return "";
		return searchQuery.getParse();
	}
	public String getQueryExplanation() {
		if(searchQuery == null || searchQuery.getValue() == null) return "";
		return searchQuery.getValue().toString();
	}
	public SearchQuery getSearchQuery() {
		return searchQuery;
	}
	public void setSearchQuery(SearchQuery searchQuery) {
		this.searchQuery = searchQuery;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public SearchResult getResult(int index) {
		return results.get(index);
	}
	public List<SearchResult> getResults() {
		return results;
	}
	public void setResults(List<SearchResult> results) {
		this.results = results;
	}
	public List<SearchAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<SearchAttribute> attributes) {
		this.attributes = attributes;
	}
	public SearchAttribute findAttribute(String name) {
		for(int i=0; i < attributes.size(); i++) {
			if(attributes.get(i).getName().equals(name)) return attributes.get(i);
		}
		SearchAttribute att = new SearchAttribute(name);
		attributes.add(att);
		return att;
	}
	public List<SearchNode> getBreadcrumb() {
		return breadcrumb;
	}
	public void setBreadcrumb(List<SearchNode> breadcrumb) {
		this.breadcrumb = breadcrumb;
	}
	public List<Token> getTokens() {
		return tokens;
	}
	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}
	public BitSet getBits() {
		return bits;
	}
	public void setBits(BitSet bits) {
		this.bits = bits;
	}
	public float getTopScore() {
		return topScore;
	}
	public void setTopScore(float topScore) {
		this.topScore = topScore;
	}
}
