package org.heed.openapps.entity;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.QName;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.scheduling.Job;


public interface EntityService extends Serializable {
	
	EntityResultSet search(EntityQuery query);
	
	Entity getEntity(long id) throws InvalidEntityException;
	Entity getEntity(String uid) throws InvalidEntityException;
	Entity getEntity(QName entityQname, String uid) throws InvalidEntityException;
	Entity getEntity(HttpServletRequest request, QName entityQname) throws InvalidEntityException;
	
	void addEntity(Entity entity) throws InvalidEntityException;
	Long addEntity(Long source, Long target, QName association, List<Property> associstionProperties, Entity entity) throws InvalidEntityException;
	void addEntities(Collection<Entity> entities) throws InvalidEntityException;
	
	void updateEntity(Entity entity) throws InvalidEntityException;
	void removeEntity(long id) throws InvalidEntityException;
	
	Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException;
	Object export(FormatInstructions instructions, Association association) throws InvalidEntityException;
	
	void cascadeProperty(Long entityId, QName association, QName propertyName, Serializable propertyValue) throws InvalidEntityException, InvalidPropertyException;
	
	Association getAssociation(long id) throws InvalidAssociationException;
	Association getAssociation(QName qname, long source, long target) throws InvalidAssociationException;
	Association getAssociation(HttpServletRequest request, QName associationQname) throws InvalidAssociationException;
	
	void addAssociation(Association association) throws InvalidAssociationException;
	void updateAssociation(Association association) throws InvalidAssociationException;
	void removeAssociation(long id) throws InvalidAssociationException;		
	
	ValidationResult validate(Entity entity) throws ModelValidationException;
	ValidationResult validate(Association association) throws ModelValidationException;
	
	List<ImportProcessor> getImportProcessors(String name);
	void registerImportProcessor(String name, ImportProcessor processor);
	
	ExportProcessor getExportProcessor(String name);
	void registerExportProcessor(String name, ExportProcessor processor);
		
	EntityPersistenceListener getEntityPersistenceListener(String name);
	void registerEntityPersistenceListener(String name, EntityPersistenceListener component);
	
	Job index(QName qname);
	void index(Long entity, boolean wait);
}
