package org.heed.openapps.search.indexing;

import java.util.HashMap;
import java.util.Map;

public class IndexEntity {
	private long id;
	private String content;
	private boolean delete;
	private Map<String, IndexEntityField> data = new HashMap<String, IndexEntityField>();
	
	public IndexEntity() {}
	public IndexEntity(long id) {
		this.id = id;
	}
	public IndexEntity(long id, Map<String, IndexEntityField> data) {
		this.id = id;
		this.data = data;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Map<String, IndexEntityField> getData() {
		return data;
	}
	public void setData(Map<String, IndexEntityField> data) {
		this.data = data;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
