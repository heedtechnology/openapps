package org.heed.openapps.entity;


public interface EntitySearcher {

	EntityResultSet search(EntityQuery query) throws Exception;
	
}
