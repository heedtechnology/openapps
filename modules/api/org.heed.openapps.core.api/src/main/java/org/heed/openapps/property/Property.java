package org.heed.openapps.property;

public class Property {
	private String group;
	private String name;
	private String value;
	
	
	public Property() {}
	public Property(String name, String value) {
		this.name = name;
		this.value = value;
	}
	public Property(String group, String name, String value) {
		this.group = group;
		this.name = name;
		this.value = value;
	}
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
