package org.heed.openapps.search.parsing;
import java.util.List;

import org.heed.openapps.search.Token;


public interface QueryTokenizer {

	List<Token> tokenize(String query);
	
}
