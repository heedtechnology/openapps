package org.heed.openapps.scheduling;

public enum Period {
	MINUTE("Minute"),
	HOUR("Hour"),
	DAY("Day"),
	WEEK("Week"),
	MONTH("Month"),
	YEAR("Year");
	
	private final String description;
	Period(String description) {
		this.description = description;
	}
}
