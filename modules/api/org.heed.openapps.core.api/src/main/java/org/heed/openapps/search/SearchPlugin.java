package org.heed.openapps.search;



public interface SearchPlugin {

	void initialize();
	void request(SearchRequest request);
	void response(SearchRequest request, SearchResponse response);
		
}
