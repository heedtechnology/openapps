package org.heed.openapps.entity;

import java.util.ArrayList;
import java.util.List;



//import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.data.Result;
import org.heed.openapps.entity.data.ResultItem;
import org.heed.openapps.entity.data.ResultSet;


public class EntityResultSet implements ResultSet {
	private static final long serialVersionUID = 8587358899721308561L;
	private int resultSize;
	private int startRow;
	private int endRow;
	private List<Entity> results = new ArrayList<Entity>();

	
	public int getResultSize() {
		return resultSize;
	}
	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public List<Entity> getResults() {
		return results;
	}
	public void setResults(List<Entity> results) {
		this.results = results;
	}
	@Override
	public Result getResult(int index) {
		ResultItem item = new ResultItem();
		Entity entity = results.get(index);
		item.setId(String.valueOf(entity.getId()));
		item.setQname(entity.getQName());
		item.setUid(entity.getUuid());
		for(Property property : entity.getProperties()) {
			item.addValue(property.getQName(), String.valueOf(property.getValue()));
		}
		return item;
	}
	@Override
	public String toXml() {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
