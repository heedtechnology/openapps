/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity.data.in;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.data.Column;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.data.Mapping;
import org.heed.openapps.util.XMLUtility;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ImportModel {
	public EntityService nodeSvc;
	private String name;
	private Map<String,FileImportProcessor> parsers = new HashMap<String,FileImportProcessor>();
	private Map<String,Mapping> mappings = new HashMap<String,Mapping>();
	protected Map<String,QName> qnames = new HashMap<String,QName>();
	
	
	public ImportModel(EntityService nodeSvc, String xmlfilename) {
		this.nodeSvc = nodeSvc;
        try {
        	InputStream modelStream = new FileInputStream(new File("config/"+xmlfilename));
            XMLUtility.SAXParse(false, modelStream, new ImportHandler());
            modelStream.close();
        } catch(Exception e){
            e.printStackTrace();
        }        
    }	
	public class ImportHandler extends DefaultHandler {
		Mapping mapping = null;
		boolean inData = false;
		StringBuffer data = null;
		int currField = 0;
		
		public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException	{
			if(qName.equals("mapping")) {
				qnames.clear();
				String mappingName = attrs.getValue("name");
				String namespace = attrs.getValue("namespace");
				String localName = attrs.getValue("localName");
				String parser = attrs.getValue("parser");
				mapping = new Mapping(mappingName);
				mapping.setNamespace(namespace);
				mapping.setLocalName(localName);
				mapping.setParser(parser);
			} else if(qName.equals("column")) {
				String from = attrs.getValue("from");
				String to = attrs.getValue("to");
				String duplicates = attrs.getValue("duplicates");
				String fixedValue = attrs.getValue("fixedValue");
				Column col = new Column(from, to);
				if(duplicates != null) col.setDuplicates(Boolean.parseBoolean(duplicates));
				if(fixedValue != null) col.setFixedValue(Boolean.parseBoolean(fixedValue));
				mapping.addColumn(col);
			} else if(qName.equals("name")) {
				String key = attrs.getValue("key");
				String namespace = attrs.getValue("namespace");
				String localName = attrs.getValue("localName");
				try {
					QName qname = QName.createQualifiedName(namespace, localName);
					if(qname == null) throw new Exception("unable to locate qname:{"+namespace+"}"+localName);
					qnames.put(key, qname);
				} catch(Exception e) {
					e.printStackTrace();
				}
			} 
		}
		
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException	{
			if(qName.equals("mapping")) {
				mappings.put(mapping.getName(), mapping);
				String pStr = mapping.getParser();
				try {
					Class cls = Class.forName(pStr);
					FileImportProcessor parser = (FileImportProcessor)cls.newInstance();
					//parser.setQName(new QName(mapping.getNamespace(), mapping.getLocalName()));
					parser.setMapping(mapping);
					parsers.put(mapping.getName(),parser);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, FileImportProcessor> getParsers() {
		return parsers;
	}
	public void setParsers(Map<String, FileImportProcessor> parsers) {
		this.parsers = parsers;
	}
	public Map<String, Mapping> getMappings() {
		return mappings;
	}
	public void setMappings(Map<String, Mapping> models) {
		this.mappings = models;
	}
	
}
