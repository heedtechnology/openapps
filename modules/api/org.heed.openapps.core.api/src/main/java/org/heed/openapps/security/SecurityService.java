package org.heed.openapps.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.Group;
import org.heed.openapps.User;


public interface SecurityService {

	List<User> getUsers(String query);
	List<Group> getGroups(String query);
	
	User getCurrentUser(HttpServletRequest request);
	User getUserByUsername(String username);
	User getUserByEmail(String email);
	
	void updateUser(User user);
	void removeUser(long id);
	
	int authenticate(String username, String password);
	String generatePassword();
		
}
