package org.heed.openapps.search.data;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.search.Property;
import org.heed.openapps.search.SearchRequest;
import org.heed.openapps.search.SearchResult;
import org.heed.openapps.InvalidQualifiedNameException;

public class SearchResultExportProcessor implements ExportProcessor {
	private static final long serialVersionUID = 8123694402664650037L;
	private EntityService entityService;
	private DataDictionaryService dictionaryService;
	
	@Override
	public Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException {
		ExportProcessor defaultProcessor = entityService.getExportProcessor("default");
		return defaultProcessor.export(instructions, entity);
	}

	@Override
	public Object export(FormatInstructions instructions, Association association)	throws InvalidEntityException {
		ExportProcessor defaultProcessor = entityService.getExportProcessor("default");
		return defaultProcessor.export(instructions, association);
	}
	public Object export(FormatInstructions instructions, SearchRequest request, SearchResult result) throws InvalidEntityException, InvalidQualifiedNameException {
		if(instructions.getFormat().equals(FormatInstructions.FORMAT_CSV)) return toCsv(request, result);
		else return toXml(request, result, instructions.printSources(), instructions.printTargets());
	}
	
	public String toCsv(SearchRequest request, SearchResult result) throws InvalidEntityException, InvalidQualifiedNameException {
		StringBuilder buff = new StringBuilder();
		DataDictionary dictionary = dictionaryService.getDataDictionary(request.getDictionary());
		Model model = dictionary.getModel(result.getQName());
		List<ModelField> fields = new ArrayList<ModelField>();
		List<ModelRelation> outgoing = new ArrayList<ModelRelation>();
		List<ModelRelation> incoming = new ArrayList<ModelRelation>();
		if(model == null) throw new InvalidEntityException("entity model not found for qname:"+result.getQName().toString());
		fields.addAll(model.getFields());
		outgoing.addAll(model.getSourceRelations());
		incoming.addAll(model.getTargetRelations());
		//if(model.getParent() != null) buff.append("<parentQName>"+model.getParent().getQName()+"</parentQName>");
		if(model.getParent() != null) {
			Model parent = getParentModel(model);
			if(parent != null) {
				fields.addAll(parent.getFields());
				outgoing.addAll(parent.getSourceRelations());
				incoming.addAll(parent.getTargetRelations());
				if(parent.getParent() != null) {
					Model grandParent = getParentModel(parent);
					if(grandParent != null) {
						fields.addAll(grandParent.getFields());
						outgoing.addAll(grandParent.getSourceRelations());
						incoming.addAll(grandParent.getTargetRelations());
					}
				}
			}
		}
		for(ModelField field : fields) {
			if(result.hasProperty(field.getQName())) {
				Property property = result.getProperty(field.getQName());
				if(property.getValue() != null && property.getValue().toString().length() > 0)
					buff.append(property.getValue()+",");
			}
		}
		buff.replace(buff.length()-1, buff.length(), "\n");
		return buff.toString();
	}

    private Model getParentModel(Model model) throws InvalidQualifiedNameException {
    	DataDictionary dictionary = model.getDictionary();
    	return dictionary.getModel(model.getParentName());
    }

    public String toXml(SearchRequest request, SearchResult result, boolean printSources, boolean printTargets) throws InvalidEntityException, InvalidQualifiedNameException {
    	DataDictionary dictionary = dictionaryService.getDataDictionary(request.getDictionary());
    	ExportProcessor defaultProcessor = entityService.getExportProcessor("default");
    	FormatInstructions instructions = new FormatInstructions(false, printSources, printTargets);
		Model model = dictionary.getModel(result.getQName());
		StringBuffer buff = new StringBuffer("<node id='"+result.getId()+"' uid='"+result.getUid()+"' qname='"+result.getQName().toString()+"' localName='"+result.getQName().getLocalName()+"' score='"+result.getNormalizedScore()+"' rawScore='"+result.getRawScore()+"'>");
		List<ModelField> fields = new ArrayList<ModelField>();
		List<ModelRelation> outgoing = new ArrayList<ModelRelation>();
		List<ModelRelation> incoming = new ArrayList<ModelRelation>();
		if(model == null) throw new InvalidEntityException("entity model not found for qname:"+result.getQName().toString());
		fields.addAll(model.getFields());
		outgoing.addAll(model.getSourceRelations());
		incoming.addAll(model.getTargetRelations());
		if(model.getParent() != null) buff.append("<parentQName>"+model.getParentName()+"</parentQName>");
		if(model.getParent() != null) {
			Model parent = getParentModel(model);
			if(parent != null) {
				fields.addAll(parent.getFields());
				outgoing.addAll(parent.getSourceRelations());
				incoming.addAll(parent.getTargetRelations());
				if(parent.getParent() != null) {
					Model grandParent = getParentModel(parent);
					if(grandParent != null) {
						fields.addAll(grandParent.getFields());
						outgoing.addAll(grandParent.getSourceRelations());
						incoming.addAll(grandParent.getTargetRelations());
					}
				}
			}
		}
		boolean printTitle = true;
		for(ModelField field : fields) {
			if(result.hasProperty(field.getQName())) {
				Property property = result.getProperty(field.getQName());
				if(property.getValue() != null && property.getValue().toString().length() > 0)
					buff.append("<"+property.getName()+"><![CDATA["+property.getValue()+"]]></"+property.getName()+">");
			} 
			if(field.getQName().getLocalName().equals("title")) printTitle = false;
		}
		if(printTitle) buff.append("<title><![CDATA["+getEntityTitle(result)+"]]></title>");
		//if(buffer.length() > 0) {
			//buff.append(buffer.toString().trim());
			//buffer = new StringBuffer();
		//}
		Entity entity = null;
		if(printSources) {
			if(entity == null) entity = entityService.getEntity(result.getId());
			buff.append("<source_associations>");			
			for(ModelRelation relation : outgoing) {
				Model startModel = dictionary.getModel(relation.getStartName());
				Model endModel = dictionary.getModel(relation.getEndName());
				try {
					String sourceName = startModel != null ? startModel.getQName().toString() : "";
					String targetName = endModel != null ? endModel.getQName().toString() : "";
					List<Association> source_associations = entity.getSourceAssociations(relation.getQName());
					if(source_associations.size() > 0) {
						buff.append("<"+relation.getQName().getLocalName()+" source='"+sourceName+"' target='"+targetName+"'>");
						for(Association assoc : source_associations) {	
							buff.append(defaultProcessor.export(instructions, assoc));			
						}
						buff.append("</"+relation.getQName().getLocalName()+">");
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			buff.append("</source_associations>");
		}
		if(printTargets) {
			if(entity == null) entity = entityService.getEntity(result.getId());
			buff.append("<target_associations>");			
			for(ModelRelation relation : incoming) {
				Model startModel = dictionary.getModel(relation.getStartName());
				Model endModel = dictionary.getModel(relation.getEndName());
				try {
					String sourceName = startModel != null ? startModel.getQName().toString() : "";
					String targetName = endModel != null ? endModel.getQName().toString() : "";
					List<Association> target_associations = entity.getTargetAssociations(relation.getQName());
					if(target_associations.size() > 0) {
						buff.append("<"+relation.getQName().getLocalName()+" source='"+sourceName+"' target='"+targetName+"'>");
						for(Association assoc : target_associations) {								
							buff.append(defaultProcessor.export(instructions, assoc));
						}
						buff.append("</"+relation.getQName().getLocalName()+">");
					}					
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			buff.append("</target_associations>");			
		}
		if(entity != null) {
			if(entity.getCreated() > 0) buff.append("<created>"+entity.getCreated()+"</created>");
			if(entity.getCreator() > 0) buff.append("<creator>"+entity.getCreator()+"</creator>");
			if(entity.getModified() > 0) buff.append("<modified>"+entity.getModified()+"</modified>");
			if(entity.getModifier() > 0) buff.append("<modifier>"+entity.getModifier()+"</modifier>");
		} else {
			if(result.getEntity().getCreated() > 0) {
				buff.append("<created>"+result.getEntity().getCreated()+"</created>");
			}
		}
		return buff.append("</node>").toString();
	}
	public String getEntityTitle(SearchResult entity) {
		Property name = entity.getProperty(new QName("openapps.org_system_1.0","name"));
		if(name == null) name = entity.getProperty(new QName("openapps.org_system_1.0","title"));
		if(name != null) return name.toString();
		return "";
	}

	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}

	@Override
	public Map<String, Object> exportMap(FormatInstructions format, Association association) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
