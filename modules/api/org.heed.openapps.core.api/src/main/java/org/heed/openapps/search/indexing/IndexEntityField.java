package org.heed.openapps.search.indexing;

public class IndexEntityField {
	private String name;
	private Object value;
	private boolean tokenized = true;
	
	public IndexEntityField() {}
	public IndexEntityField(String name, Object value, boolean tokenized) {
		this.name = name;
		this.value = value;
		this.tokenized = tokenized;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public boolean isTokenized() {
		return tokenized;
	}
	public void setTokenized(boolean tokenized) {
		this.tokenized = tokenized;
	}
}
