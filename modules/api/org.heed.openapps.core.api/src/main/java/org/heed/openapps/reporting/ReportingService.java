package org.heed.openapps.reporting;


public interface ReportingService {

	public static final int FORMAT_HTML = 1;
	public static final int FORMAT_PDF = 2;
	public static final int FORMAT_RTF = 3;
	
	
	byte[] generate(String id, int format, String template, ReportingDataSource data);
	void generate(String id, boolean scheduled, int format, String title, String template,ReportingDataSource data);
	
}
