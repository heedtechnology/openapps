package org.heed.openapps.util;

public class JSONUtility {

	
	public static String escape(String in) {
		if(in != null) {
			in = in.replace(",", "\\,");
			in = in.replace("'", "\\'");
			return in;
		}
		return "";
	}
	public static String unescape(String in) {
		if(in != null) {
			in = in.replace("&lt;", "<");
			in = in.replace("&amp;", "&");
			in = in.replace("&apos;", "'");
			in = in.replace("&quot;", "\"");
			//in = in.replaceAll("&#37;", "%");
			return in;
		} 
		return "";
	}
}
