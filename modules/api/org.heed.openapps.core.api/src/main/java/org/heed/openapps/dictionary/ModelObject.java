package org.heed.openapps.dictionary;

import java.io.Serializable;

import org.heed.openapps.QName;

public interface ModelObject extends Serializable {

	Long getId();
	String getUid();
	void setUid(String uid);
	QName getQName();
	
}
