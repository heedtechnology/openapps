package org.heed.openapps.node;

import org.heed.openapps.QName;


public class Relationship {
    private long id;
    private QName qname;
    private long start;
    private long end;
    
    public Relationship() {}
    public Relationship(long id, QName qname, long start, long end) {
    	this.id = id;
    	this.qname = qname;
    	this.start = start;
    	this.end = end;
    }
    
    public long getId() {
    	return id;
    }
    public long getStartNode() {
    	return start;
    }
    public long getEndNode() {
    	return end;
    }
    public QName getQName() {
    	return qname;
    }

}
