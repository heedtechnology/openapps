/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.entity;

import org.heed.openapps.QName;


public class Property implements java.io.Serializable {
	private static final long serialVersionUID = 2824085052507363147L;
	private Long id;
	private QName qname;
    //private Locale locale;
    private int type;
    private Object value;
        
    public static final int NULL = 0;
    public static final int BOOLEAN = 1;
    public static final int INTEGER = 2;
    public static final int LONG = 3;
    public static final int DOUBLE = 4;
    public static final int STRING = 5;
    public static final int DATE = 6;
    public static final int SERIALIZABLE = 7;
    public static final int LONGTEXT = 8;
    public static final int COMPUTED = 9;
    
    public Property() {
		// TODO Auto-generated constructor stub
	}
    public Property(QName qname) {
    	this.qname = qname;
	}
    public Property(QName qname, Object value) {
    	this.qname = qname;
		setValue(value);
	}
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public QName getQName() {
        return qname;
    }
    
    public void setQname(QName qname) {
    	this.qname = qname;
    }
        
    public String toString() {
    	return String.valueOf(value);
    }
    
    public int getType() {
		return type;
	}
    
	public void setType(int type) {
		this.type = type;
	}
	
	public Object getValue() {
    	return value;
    }
    public void setValue(int type, Object value) {
    	setType(type);
    	this.value = String.valueOf(value);
    }
	public void setValue(Object value) {
		this.value = value;
		/*
    	if(value == null) {
    		setType(NULL);
    	} else if(DataTypeUtility.isBoolean(value)) {
    		setType(BOOLEAN);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isInteger(value)) {
    		setType(INTEGER);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isLong(value)) {
    		setType(LONG);
    		this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isDouble(value)) {
        	setType(DOUBLE);
        	this.value = String.valueOf(value);
    	} else if(DataTypeUtility.isDate(value)) {
        	setType(DATE);
        	if(value instanceof String) this.value = ((String)value);
        	else {
        		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        		this.value = dateFormatter.format((Date)value);
        	}
        } else if(value instanceof String) {
        	String val = (String)value;
        	if(val.length() > 1024) {
        		setType(LONGTEXT);
        	} else {
        		setType(STRING);
        	}
        	this.value = ((String)value);
        }
        //else if(value instanceof ContentData) return CONTENT;
        else {
            // type is not recognised as belonging to any particular slot
        	setType(SERIALIZABLE);
        	this.value = new String((byte[])value);
        }
        */
    }
    public boolean isEmpty() {
    	return (value == null || toString().length() == 0);
    }
    public String toXml() {
    	if(type == 5 || type == 8) return "<![CDATA["+value+"]]>";
    	else return toString();
    }
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if (obj instanceof Property){
            Property that = (Property) obj;
            if(this.value.equals(that.getValue())) return true;
        }
        return false;
    }
}
