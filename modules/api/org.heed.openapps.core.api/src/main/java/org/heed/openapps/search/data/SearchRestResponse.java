package org.heed.openapps.search.data;
import java.io.Serializable;


public class SearchRestResponse<T> implements Serializable {
	private static final long serialVersionUID = 154584982807266691L;
	
	private RestData<T> response = new RestData<T>();
				
	public RestData<T> getResponse() {
		return response;
	}
	public void setResponse(RestData<T> response) {
		this.response = response;
	}

}
