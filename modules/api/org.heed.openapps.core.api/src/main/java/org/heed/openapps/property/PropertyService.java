package org.heed.openapps.property;

import java.util.List;

public interface PropertyService {

	boolean hasProperty(String name);
	void refresh() throws Exception;
	List<String> getGroupNames();
	List<Property> getProperties(String group);
	Property getProperty(String name);
	String getPropertyValue(String name);
	int getPropertyValueAsInt(String name);
	
}
