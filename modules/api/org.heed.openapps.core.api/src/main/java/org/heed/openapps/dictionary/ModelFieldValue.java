package org.heed.openapps.dictionary;

import org.heed.openapps.QName;



public class ModelFieldValue implements ModelObject {
	private static final long serialVersionUID = -6495065331933283259L;
	private long id;
	private String name;
	private String description;
	private String value;
	private String uid;
	private ModelField field;
	
	public ModelFieldValue(){}
	public ModelFieldValue(String name, String description) {
		this.name = name;
		this.description = description;
	}
	public ModelFieldValue(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}	
	public QName getQName() {
		return null;
	}
	public void setQName(QName qname) {
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ModelField getField() {
		return field;
	}
	public void setField(ModelField field) {
		this.field = field;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
