package org.heed.openapps.portal.util;

import com.liferay.portal.CompanyMaxUsersException;
import com.liferay.portal.CookieNotSupportedException;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.PasswordExpiredException;
import com.liferay.portal.UserEmailAddressException;
import com.liferay.portal.UserIdException;
import com.liferay.portal.UserLockoutException;
import com.liferay.portal.UserPasswordException;
import com.liferay.portal.UserScreenNameException;
import com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import org.json.JSONObject;


public class ErrorJSONSerializer {
	private static Log log = LogFactoryUtil.getLog(ErrorJSONSerializer.class);
	
	
	public static JSONObject toJSONObject(int status, String message) {
	    JSONObject jsonObj = new JSONObject();
	    put(jsonObj, "status", status);
	    put(jsonObj, "message", message);	    
	    return jsonObj;
	}
	public static JSONObject toJSONObject(Exception e) {
	    JSONObject jsonObj = new JSONObject();	    
	    put(jsonObj, "exception", e.getClass().getName());
	    put(jsonObj, "message", e.getMessage());
	    
	    if(e instanceof UserEmailAddressException) {
	    	put(jsonObj, "message", "<span>invalid email address, please try again</span><br/>");
	    } else if(e instanceof NoSuchUserException) {
	    	put(jsonObj, "message", "<span>invalid email address, please try again</span><br/>");
	    } else if(e instanceof UserScreenNameException) {
	    	put(jsonObj, "message", "<span>invalid username, please try again</span><br/>");
	    } else if(e instanceof UserPasswordException) {
	    	put(jsonObj, "message", "<span>invalid password, please try again</span><br/>");
	    } else if(e instanceof DuplicateUserScreenNameException) {
	    	put(jsonObj, "message", "<span>duplicate screen name</span><br/>");
	    } else if(e instanceof CaptchaTextException ||
	    	e instanceof CaptchaMaxChallengesException) {
	    	put(jsonObj, "message", "<span>invalid captcha, please try again</span><br/>");
	    } else if (e instanceof CompanyMaxUsersException ||
			e instanceof CookieNotSupportedException ||
			e instanceof PasswordExpiredException ||
			e instanceof UserIdException ||
			e instanceof UserLockoutException) {
			put(jsonObj, "message", "<span>general registration exception, please try again</span><br/>");
		}  
	    
	    return jsonObj;
	}

	protected static void put(JSONObject jsonObj, String key, Object value) {
		try {
			jsonObj.put(key, value);
		} catch(Exception e) {
			log.error("", e);
		}
	}
}