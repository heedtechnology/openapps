package org.heed.openapps.portal.service.scheduling;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.cache.TimedCache;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.scheduling.Period;
import org.heed.openapps.scheduling.SchedulingException;
import org.heed.openapps.scheduling.SchedulingService;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelperUtil;
import com.liferay.portal.kernel.scheduler.SchedulerEntry;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.TimeUnit;
import com.liferay.portal.kernel.scheduler.TriggerType;


public class LiferaySchedulingService implements SchedulingService {
	private static Log log = LogFactoryUtil.getLog(LiferaySchedulingService.class);
	
	private TimedCache<String,Job> jobCache = new TimedCache<String,Job>(60);
	
	final static public String GROUP_NAME = "openapps";
	

	@Override
	public void run(Job job) {
		try {
			jobCache.put(job.getUid(), job);
			SchedulerEntry schedulerEntry = new SchedulerEntryImpl();
			schedulerEntry.setPropertyKey(job.getUid());
			schedulerEntry.setEventListenerClass(SchedulerMessageListener.class.getName());  
			schedulerEntry.setTimeUnit(TimeUnit.SECOND);  
			schedulerEntry.setTriggerType(TriggerType.SIMPLE);  
			schedulerEntry.setTriggerValue(1);  			  
			try {  
				SchedulerEngineHelperUtil.schedule(schedulerEntry, StorageType.MEMORY_CLUSTERED, "", 0);  
			} catch (SchedulerException e) {  
				log.warn(e);  
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void runAndWait(Job job) {
		
	}
	public void run(Job job, int count, Period period) {
		
	}
	@Override
	public Job getJob(String jobid) {
		Job job = jobCache.get(jobid);
		return job;
	}
	@Override
	public List<Job> getJobs(String node) {
		List<Job> list = new ArrayList<Job>();
		for(String key : jobCache.keySet()) {
			Job job = jobCache.get(key);
			if(job != null && job.getId() != null) {
				if(job.getId().toString().equals(node)) list.add(job);
			}
		}
		return list;
	}
	@Override
	public List<Job> getJobs() {
		List<Job> list = new ArrayList<Job>();
		for(String key : jobCache.keySet()) {
			Job job = jobCache.get(key);
			if(job.getId() != null) {
				list.add(job);
			}
		}
		return list;
	}
	@Override
	public void updateJob(String id, String message) {
		Job job = getJob(id);
		job.setLastMessage(message);
	}
	
	@Override
	public void interruptJob(String id) throws SchedulingException {
		
	}
	@Override
	public boolean startJob(String id) throws SchedulingException {
		return false;
	}
	@Override
	public void removeJob(String id) throws SchedulingException {
		
	}
	
	public boolean isJobRunning(String jobName) throws SchedulerException {
		
		return false;
	}
	
	public class SchedulerMessageListener implements MessageListener {

		@Override
		public void receive(Message msg) throws MessageListenerException {
			Object payload = msg.getPayload();
			System.out.println(payload);
			Job job = jobCache.get((String)payload);
			job.execute();
		}
		
	}
}
