package org.heed.openapps.portal.service;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.property.Property;
import org.heed.openapps.property.PropertyService;

import com.liferay.portal.kernel.util.PropsUtil;

public class LiferayPropertiesService implements PropertyService {

	@Override
	public List<String> getGroupNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Property> getProperties(String group) {
		List<Property> properties = new ArrayList<Property>();
		String[] keys = PropsUtil.getArray(group);
		for(String key : keys) {
			properties.add(getProperty(key));
		}
		return properties;
	}

	@Override
	public Property getProperty(String name) {
		if(name == null || name.equals("")) return null;
		String value = PropsUtil.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		if(name == null || name.equals("")) return null;
		return PropsUtil.get(name);
	}

	@Override
	public int getPropertyValueAsInt(String name) {
		if(name == null || name.equals("")) return 0;
		String value = PropsUtil.get(name);
		if(value == null || name.equals("")) return 0;
		return Integer.valueOf(value);
	}

	@Override
	public boolean hasProperty(String name) {
		if(name == null || name.equals("")) return false;
		if(PropsUtil.get(name) != null) return true;
		return false;
	}

	@Override
	public void refresh() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
