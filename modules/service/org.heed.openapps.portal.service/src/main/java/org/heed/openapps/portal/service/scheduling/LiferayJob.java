package org.heed.openapps.portal.service.scheduling;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.scheduling.ExecutionContext;
import org.heed.openapps.scheduling.JobSupport;
import org.heed.openapps.scheduling.Status;


public class LiferayJob extends JobSupport {
	private static final long serialVersionUID = 2422823728239264592L;
	private static final Log log = LogFactory.getLog(LiferayJob.class);
	
	
	public LiferayJob() {
		setGroup("system");
	}
	
	@Override
	public void execute(ExecutionContext context) {
		super.execute(context);
		Status status = createStatus();
		status.setMessage("HeartBeatJob executed");
		log.info("[beep]");		
	}

	@Override
	public boolean isComplete() {
		return false;
	}
}