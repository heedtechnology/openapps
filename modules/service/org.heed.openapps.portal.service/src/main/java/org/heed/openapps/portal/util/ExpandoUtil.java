package org.heed.openapps.portal.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

public class ExpandoUtil {

	
	public static String getExpandoValue(ThemeDisplay themeDisplay, long classNameId, long classPK, String field) throws SystemException, PortalException {
		ExpandoTable table = ExpandoTableLocalServiceUtil.getDefaultTable(themeDisplay.getCompanyId(), classNameId);
		if(table != null) {
			ExpandoColumn column = ExpandoColumnLocalServiceUtil.getColumn(table.getTableId(), field);
			if(column != null) {
				ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(table.getTableId(), column.getColumnId(), classPK);
				if(value != null) {
					if(column.getType() == 15 && value.getString().length() > 0) {
						return value.getString();
					} else if(column.getType() == 16 && value.getStringArray().length > 0) {
						return value.getStringArray()[0];
					}
				}					
			}
		}
		return null;
	}
	public static void updateExpandoValue(ThemeDisplay themeDisplay, long classNameId, long classPK, String field, Object value) throws SystemException, PortalException {
		ExpandoTable table = ExpandoTableLocalServiceUtil.getDefaultTable(themeDisplay.getCompanyId(), classNameId);
		if(table != null) {
			ExpandoColumn column = ExpandoColumnLocalServiceUtil.getColumn(table.getTableId(), field);
			if(column != null) {
				ExpandoValue expandoValue = ExpandoValueLocalServiceUtil.getValue(table.getTableId(), column.getColumnId(), classPK);
				if(expandoValue != null) {
					if(value instanceof String)	expandoValue.setString((String)value);
					else if(value instanceof String[])	expandoValue.setStringArray((String[])value);
					ExpandoValueLocalServiceUtil.updateExpandoValue(expandoValue);
				} else {					
					if(value instanceof String)	ExpandoValueLocalServiceUtil.addValue(classNameId, table.getTableId(), column.getColumnId(), classPK, (String)value);
					else if(value instanceof String[])	{
						for(String str : (String[])value) {
							ExpandoValueLocalServiceUtil.addValue(classNameId, table.getTableId(), column.getColumnId(), classPK, str);
						}
					}
				}
			}
		}
	}
}
