package org.heed.openapps.portal.service;

import org.heed.openapps.SystemModel;
import org.heed.openapps.content.DigitalObjectService;
import org.heed.openapps.content.FileNode;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFileEntryMetadata;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryMetadataLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.storage.Field;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;
import com.liferay.portlet.dynamicdatamapping.storage.StorageEngineUtil;


public class LiferayDigitalObjectService implements DigitalObjectService {
	private static Log log = LogFactoryUtil.getLog(LiferayDigitalObjectService.class);
	private EntityService entityService;
	private long repositoryId;
	
	
	public FileNode addDigitalObject(long userId, String name, String description, byte[] payload) {
		FileNode fileNode = null;
		FileEntry fileEntry = null;
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGroupPermissions(true);
		//String contentType = MimeTypesUtil.getContentType(component.getFile().getName());
		
		Folder root = null;
		Folder folder = null;
		try {
			Entity entity = new Entity(ContentModel.FILE);
			entity.setName(name);
			entity.addProperty(SystemModel.DESCRIPTION, description);
			entityService.addEntity(entity);						
			try {
				root = DLAppLocalServiceUtil.getFolder(repositoryId, 0, "ARCHIVEMANAGER");
			} catch(Exception e) {
				root = DLAppLocalServiceUtil.addFolder(userId, repositoryId, 0, "ARCHIVEMANAGER", "", serviceContext);													
			}
			if(root != null) {
				try {
					folder = DLAppLocalServiceUtil.getFolder(repositoryId, root.getFolderId(), String.valueOf(entity.getId()));
				} catch(Exception ex) {
					folder = DLAppLocalServiceUtil.addFolder(userId, repositoryId, root.getFolderId(), String.valueOf(entity.getId()), "", serviceContext);
				}
			}
			if(folder != null) {
				fileEntry = null;
				try {
					fileEntry = DLAppLocalServiceUtil.addFileEntry(userId, repositoryId, folder.getFolderId(), name, "", name, description, "", payload, serviceContext);
				} catch(Exception e) {
					fileEntry = DLAppLocalServiceUtil.getFileEntry(repositoryId, folder.getFolderId(), name);
				}
				if(fileEntry != null) {
										
					entity.setName(fileEntry.getTitle());
					entity.addProperty(SystemModel.FOREIGN_KEY, String.valueOf(fileEntry.getFileEntryId()));
					entity.addProperty(SystemModel.URL, "/c/document_library/get_file?uuid="+fileEntry.getUuid()+"&groupId="+repositoryId);
					entityService.updateEntity(entity);
					
					fileNode = new FileNode();
					fileNode.setName(fileEntry.getTitle());
					fileNode.setId(String.valueOf(entity.getId()));
					fileNode.setFid(String.valueOf(fileEntry.getFileEntryId()));
				}
			}
		} catch(Exception e) {
			log.error("", e);
		}
		return fileNode;
	}
	@Override
	public FileNode getDigitalObject(long nodeId) {
		FileNode fileNode = null;
		try {
			Entity entity = entityService.getEntity(nodeId);
			if(entity != null) {
				String foreignKey = entity.getPropertyValue(SystemModel.FOREIGN_KEY);
				if(foreignKey != null) {
					FileEntry fileEntry = DLAppLocalServiceUtil.getFileEntry(Long.valueOf(foreignKey));
					fileNode = new FileNode();
					fileNode.setName(fileEntry.getTitle());
					fileNode.setId(String.valueOf(entity.getId()));
					fileNode.setFid(String.valueOf(fileEntry.getFileEntryId()));
					
					FileVersion latestVersion = fileEntry.getLatestFileVersion();
					DLFileEntryMetadata fileEntryMetadata = DLFileEntryMetadataLocalServiceUtil.getFileEntryMetadata(10318, latestVersion.getFileVersionId());
					Fields fields = StorageEngineUtil.getFields(fileEntryMetadata.getDDMStorageId());
					
					Field contentType = fields.get("HttpHeaders_CONTENT_TYPE");
					Field width = fields.get("TIFF_IMAGE_WIDTH");
					Field height = fields.get("TIFF_IMAGE_LENGTH");
					
					if(contentType != null) fileNode.setContentType((String)contentType.getValue());
					if(width != null) fileNode.setWidth(Integer.valueOf(width.getValue().toString()));
					if(height != null) fileNode.setHeight(Integer.valueOf(height.getValue().toString()));
					fileNode.setSize(fileEntry.getSize());
				}
			}
		} catch(Exception e) {
			log.error("", e);
		}
		return fileNode;
	}
	public void removeDigitalObject(long nodeId) {
		try {
			Entity entity = entityService.getEntity(nodeId);
			if(entity != null) {
				String foreignKey = entity.getPropertyValue(SystemModel.FOREIGN_KEY);
				if(foreignKey != null && foreignKey.length() > 0) {
					try {
						DLAppLocalServiceUtil.deleteFileEntry(Long.valueOf(foreignKey));
						Folder root = DLAppLocalServiceUtil.getFolder(repositoryId, 0, "ARCHIVEMANAGER");
						if(root != null) {
							Folder folder = DLAppLocalServiceUtil.getFolder(repositoryId, root.getFolderId(), String.valueOf(entity.getId()));
							if(folder != null) {
								DLAppLocalServiceUtil.deleteFolder(folder.getFolderId());
							}
						}
					} catch(Exception e) {
						log.info("problem cleaning up Liferay FileEntry:"+foreignKey);
					}
					entityService.removeEntity(nodeId);
				}				
			}
		} catch(Exception e) {
			log.error("", e);
		}
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setRepository(long repositoryId) {
		this.repositoryId = repositoryId;
	}
	
}
