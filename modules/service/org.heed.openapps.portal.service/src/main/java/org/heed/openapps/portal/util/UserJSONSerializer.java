package org.heed.openapps.portal.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;

import org.json.JSONObject;


public class UserJSONSerializer {
	private static Log log = LogFactoryUtil.getLog(UserJSONSerializer.class);
	
	
	public static JSONObject toJSONObject(String id) {
	    JSONObject jsonObj = new JSONObject();
	    put(jsonObj, "subscriptionId", id);	    
	    return jsonObj;
	}
	
	public static JSONObject toJSONObject(int status, User model) {
	    JSONObject jsonObj = new JSONObject();
	    put(jsonObj, "status", status);
	    put(jsonObj, "userid", model.getUserId());
	    put(jsonObj, "screenname", model.getScreenName());
	    
	    return jsonObj;
	}
	public static JSONObject toJSONObject(int status, String message, User model) {
	    JSONObject jsonObj = new JSONObject();
	    put(jsonObj, "status", status);
	    put(jsonObj, "userid", model.getUserId());
	    put(jsonObj, "screenname", model.getScreenName());
	    put(jsonObj, "message", message);
	    return jsonObj;
	}
	
	protected static void put(JSONObject jsonObj, String key, Object value) {
		try {
			jsonObj.put(key, value);
		} catch(Exception e) {
			log.error("", e);
		}
	}
}