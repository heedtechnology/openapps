package org.heed.openapps.portal.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.Group;
import org.heed.openapps.Role;
import org.heed.openapps.SystemModel;
import org.heed.openapps.User;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.portal.model.LiferayGuestUser;
import org.heed.openapps.security.SecurityService;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;


public class LiferaySecurityService implements SecurityService {
	private EntityService entityService;
	private CacheService cacheService;
	
	
	@Override
	public User getCurrentUser(HttpServletRequest request) {
		try {			
			com.liferay.portal.model.User liferayUser = PortalUtil.getUser(request);
			if(liferayUser == null)	{
				PermissionChecker checker = PermissionThreadLocal.getPermissionChecker();
				if(checker != null) {
					liferayUser = PermissionThreadLocal.getPermissionChecker().getUser();
				}
			}
			if(liferayUser != null) {
				User user = getUser(liferayUser);
				return user;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new LiferayGuestUser();
	}
	@Override
	@SuppressWarnings("unchecked")
	public User getUserByUsername(String username) {
		try {			
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(com.liferay.portal.model.User.class);
			dynamicQuery.add(PropertyFactoryUtil.forName("screenName").eq(Long.valueOf(username)));
			List<com.liferay.portal.model.User> liferayUsers = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
			for(com.liferay.portal.model.User liferayUser : liferayUsers) {
				User user = getUser(liferayUser);
				return user;
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public User getUserByEmail(String email) {
		try {			
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(com.liferay.portal.model.User.class);
			dynamicQuery.add(PropertyFactoryUtil.forName("email").eq(Long.valueOf(email)));
			List<com.liferay.portal.model.User> liferayUsers = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
			for(com.liferay.portal.model.User liferayUser : liferayUsers) {
				User user = getUser(liferayUser);
				return user;
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<User> getUsers(String query) {
		List<User> users = new ArrayList<User>();
		List<com.liferay.portal.model.User> liferayUsers = null;
		try {
			if(query != null && query.length() > 0) {
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(com.liferay.portal.model.User.class);
				dynamicQuery.add(PropertyFactoryUtil.forName("screenName").like(query));
				liferayUsers = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
			} else {
				liferayUsers = UserLocalServiceUtil.getUsers(0, 75);
			}
			if(liferayUsers != null) {
				for(com.liferay.portal.model.User liferayUser : liferayUsers) {
					if(!liferayUser.getScreenName().equals("10161")) {
						User user = getUser(liferayUser);
						users.add(user);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return users;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Group> getGroups(String query) {
		List<Group> groups = new ArrayList<Group>();
		List<com.liferay.portal.model.UserGroup> liferayGroups = null;
		try {			
			if(query != null && query.length() > 0) {
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(com.liferay.portal.model.UserGroup.class);
				dynamicQuery.add(PropertyFactoryUtil.forName("name").like(query));
				liferayGroups = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
			} else {
				liferayGroups = UserGroupLocalServiceUtil.getUserGroups(0, 75);
			}
			if(liferayGroups != null) {
				for(com.liferay.portal.model.UserGroup liferayGroup : liferayGroups) {
					Group group = getGroup(liferayGroup);
					groups.add(group);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return groups;
	}
	
	protected Role getRole(com.liferay.portal.model.Role liferayRole) throws Exception {
		Long nodeId = (Long)cacheService.get("openapps.security.roles", String.valueOf(liferayRole.getRoleId()));
		Role role = new Role();
		if(nodeId == null) {
			EntityQuery query = new EntityQuery(SystemModel.ROLE);
			query.setType(EntityQuery.TYPE_LUCENE);
			query.setXid(liferayRole.getRoleId());
			EntityResultSet roleEntities = entityService.search(query);
			Entity roleEntity = null;
			if(roleEntities != null && roleEntities.getResults().size() > 0) {
				if(roleEntities.getResults().size() > 1) {
					for(Entity u : roleEntities.getResults()) {
						if(roleEntity == null) roleEntity = u;
						else entityService.removeEntity(u.getId());
					}
				} else roleEntity = roleEntities.getResults().get(0);
				nodeId = roleEntity.getId();
				cacheService.put("openapps.security.groups", String.valueOf(liferayRole.getRoleId()), roleEntity.getId());
			} else {
				roleEntity = new Entity(SystemModel.ROLE);
				if(roleEntity != null) {
					roleEntity.setName(liferayRole.getName());
					roleEntity.setXid(liferayRole.getRoleId());
					entityService.addEntity(roleEntity);
					nodeId = roleEntity.getId();
					cacheService.put("openapps.security.roles", String.valueOf(liferayRole.getRoleId()), roleEntity.getId());
				}
			}
		}
		role.setXid(liferayRole.getRoleId());
		role.setName(liferayRole.getName());
		role.setId(nodeId);
		return role;
	}
	protected Group getGroup(com.liferay.portal.model.UserGroup liferayGroup) throws Exception {
		Long nodeId = (Long)cacheService.get("openapps.security.groups", String.valueOf(liferayGroup.getUserGroupId()));
		Group group = new Group();
		if(nodeId == null) {
			EntityQuery query = new EntityQuery(SystemModel.GROUP);
			query.setType(EntityQuery.TYPE_LUCENE);
			query.setXid(liferayGroup.getUserGroupId());
			EntityResultSet userEntities = entityService.search(query);
			Entity groupEntity = null;
			if(userEntities != null && userEntities.getResults().size() > 0) {
				if(userEntities.getResults().size() > 1) {
					for(Entity u : userEntities.getResults()) {
						if(groupEntity == null) groupEntity = u;
						else entityService.removeEntity(u.getId());
					}
				} else groupEntity = userEntities.getResults().get(0);
				nodeId = groupEntity.getId();
				cacheService.put("openapps.security.groups", String.valueOf(liferayGroup.getUserGroupId()), groupEntity.getId());
			} else {
				groupEntity = new Entity(SystemModel.GROUP);
				if(groupEntity != null) {
					groupEntity.setName(liferayGroup.getName());
					groupEntity.setXid(liferayGroup.getUserGroupId());
					entityService.addEntity(groupEntity);
					nodeId = groupEntity.getId();
					cacheService.put("openapps.security.groups", String.valueOf(liferayGroup.getUserGroupId()), groupEntity.getId());
				}
			}
		}
		group.setXid(liferayGroup.getUserGroupId());
		group.setName(liferayGroup.getName());
		group.setId(nodeId);
		return group;
	}
	protected User getUser(com.liferay.portal.model.User liferayUser) throws Exception {
		Long nodeId = (Long)cacheService.get("openapps.security.users", String.valueOf(liferayUser.getUserId()));
		User user = new User();
		if(nodeId == null) {
			EntityQuery query = new EntityQuery(SystemModel.USER);
			query.setType(EntityQuery.TYPE_LUCENE);
			query.setXid(liferayUser.getUserId());
			EntityResultSet userEntities = entityService.search(query);
			Entity userEntity = null;
			if(userEntities != null && userEntities.getResults().size() > 0) {
				if(userEntities.getResults().size() > 1) {
					for(Entity u : userEntities.getResults()) {
						if(userEntity == null) userEntity = u;
						else entityService.removeEntity(u.getId());
					}
				} else userEntity = userEntities.getResults().get(0);
				nodeId = userEntity.getId();
				cacheService.put("openapps.security.users", String.valueOf(liferayUser.getUserId()), userEntity.getId());
			} else {
				userEntity = new Entity(SystemModel.USER);
				if(userEntity != null) {
					userEntity.setXid(liferayUser.getUserId());
					userEntity.setName(liferayUser.getScreenName());
					userEntity.addProperty(SystemModel.FIRSTNAME, liferayUser.getFirstName());
					userEntity.addProperty(SystemModel.LASTNAME, liferayUser.getLastName());
					userEntity.addProperty(SystemModel.EMAIL, liferayUser.getEmailAddress());				
					entityService.addEntity(userEntity);
					nodeId = userEntity.getId();
					cacheService.put("openapps.security.users", String.valueOf(liferayUser.getUserId()), userEntity.getId());
				}
			}
		}
		user.setId(nodeId);
		user.setXid(liferayUser.getUserId());
		user.setUsername(liferayUser.getScreenName());
		user.setEmail(liferayUser.getEmailAddress());
		for(com.liferay.portal.model.Role role : liferayUser.getRoles()) {
			Role r = getRole(role);
			if(r != null) user.getRoles().add(r);
		}
		for(com.liferay.portal.model.UserGroup group : liferayUser.getUserGroups()) {
			Group g = getGroup(group);
			if(g != null) user.getGroups().add(g);
		}
		return user;
	}
	
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	@Override
	public int authenticate(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String generatePassword() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void removeUser(long arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void updateUser(User arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
