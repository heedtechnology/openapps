package org.heed.openapps.portal.util;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.property.Property;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.util.NumberUtility;

import com.liferay.portal.kernel.util.PropsUtil;


public class LiferayPropertyService implements PropertyService {

	@Override
	public boolean hasProperty(String name) {
		String value = PropsUtil.get(name);
		if(value != null) return true;
		return false;
	}

	@Override
	public void refresh() throws Exception {
		
	}

	@Override
	public List<String> getGroupNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Property> getProperties(String group) {
		List<Property> props = new ArrayList<Property>();
		String[] values = PropsUtil.getArray(group);
		if(values != null && values.length > 0) {
			for(String value : values) {
				props.add(new Property(group, value));
			}
		}
		return props;
	}

	@Override
	public Property getProperty(String name) {
		String value = PropsUtil.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		String value = PropsUtil.get(name);
		return value;
	}

	@Override
	public int getPropertyValueAsInt(String name) {
		String value = PropsUtil.get(name);
		if(NumberUtility.isInteger(value)) {
			return Integer.valueOf(value);
		}
		return 0;
	}

}
