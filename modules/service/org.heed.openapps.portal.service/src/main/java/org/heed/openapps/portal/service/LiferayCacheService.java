package org.heed.openapps.portal.service;
import java.util.List;

import org.heed.openapps.cache.CacheService;

import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class LiferayCacheService implements CacheService {
	private static final long serialVersionUID = -3868741680802846933L;
	private static Log log = LogFactoryUtil.getLog(LiferayCacheService.class);
	
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object get(String cacheName, String id) {
		PortalCache cache = MultiVMPoolUtil.getCache(cacheName);
		if(cache != null) {
			return cache.get(id);
		} else log.error("no cache found : "+cacheName);
		return null;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<?> getKeys(String cacheName) {
		PortalCache cache = MultiVMPoolUtil.getCache(cacheName);
		if(cache != null) {
			return (List<?>)cache.get(cacheName);
		} else log.error("no cache found : "+cacheName);
		return null;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void put(String cacheName, String id, Object object) {
		PortalCache cache = MultiVMPoolUtil.getCache(cacheName);
		if(cache != null) {
			cache.put(id, object);
		} else log.error("no cache found : "+cacheName);
	}
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void remove(String cacheName, String id) {
		PortalCache cache = MultiVMPoolUtil.getCache(cacheName);
		if(cache != null) {
			cache.remove(id);
		} else log.error("no cache found : "+cacheName);
	}

}
