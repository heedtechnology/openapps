package org.heed.openapps.node.service.neo4j;


public class Neo4jRelationshipType implements org.neo4j.graphdb.RelationshipType {
	private String qname;
	
	
	public Neo4jRelationshipType(String qname) {
		this.qname = qname;
	}
	
	@Override
	public String name() {
		return qname;
	}

}
