package org.heed.openapps.node.service.neo4j;

import org.heed.openapps.util.DateUtility;
import org.heed.openapps.util.DateUtility.Resolution;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseBuilder;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;


public class NodeStoreMerge {
	private static GraphDatabaseService graphDb1;
	//private static GraphDatabaseService graphDb2;
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		GraphDatabaseFactory gdbf = new GraphDatabaseFactory();        		
		GraphDatabaseBuilder builder = gdbf.newEmbeddedDatabaseBuilder("/opt/programming/BUPortal/code/db_");
		graphDb1 = builder.newGraphDatabase();
		
		//GraphDatabaseBuilder builder2 = gdbf.newEmbeddedDatabaseBuilder("");
		//graphDb2 = builder2.newGraphDatabase();
		
		long start = 1404172800000L;
		long end = 1405515600000L;
		System.out.println("checking nodes between "+DateUtility.timeToString(start, Resolution.MINUTE)+" to "+DateUtility.timeToString(end, Resolution.MINUTE));
		int count = 0;
		for(Node node : graphDb1.getAllNodes()) {
			if(node.hasProperty("created")) {
				long created = (Long)node.getProperty("created");
				if(created >= start && created <= end) {
					System.out.println();
					System.out.println("node:"+node.getId()+" updated:"+DateUtility.timeToString(created, Resolution.MINUTE));
				}
			}
			if(node.hasProperty("modified")) {
				long modified = (Long)node.getProperty("modified");
				if(modified >= start && modified <= end) {
					System.out.println();
					System.out.println("node:"+node.getId()+" updated:"+DateUtility.timeToString(modified, Resolution.MINUTE));
				}
			}
			count++;
			//if(count % 1000 == 0) System.out.println("processed:"+count);
		}
	}

}
