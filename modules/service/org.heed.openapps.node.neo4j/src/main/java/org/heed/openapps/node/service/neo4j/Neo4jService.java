package org.heed.openapps.node.service.neo4j;

import java.io.Serializable;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.RelationshipIndex;

public interface Neo4jService extends Serializable {

	GraphDatabaseService getDatabaseService();
	ExecutionEngine getExecutionEngine();
	
	Index<Node> getIndex(String qname);
	RelationshipIndex getRelationshipIndex(String qname);
	Iterable<Node> getAllNodes();
	Iterable<Relationship> getAllRelationships();
	Iterable<RelationshipType> getAllRelationshipTypes();
	
	void removeRelationship(long id);
	void removeNode(long id);
	long createRelationship(long start, long end, RelationshipType type);
	
	void start();
	void stop();
}
