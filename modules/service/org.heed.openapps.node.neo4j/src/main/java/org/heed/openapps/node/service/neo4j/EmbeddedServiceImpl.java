package org.heed.openapps.node.service.neo4j;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.property.PropertyService;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.factory.GraphDatabaseBuilder;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.graphdb.index.RelationshipIndex;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.kernel.impl.util.StringLogger;
import org.neo4j.tooling.GlobalGraphOperations;


public class EmbeddedServiceImpl implements Neo4jService {
	private static final long serialVersionUID = 8141040427641756584L;
	private final static Log log = LogFactory.getLog(EmbeddedServiceImpl.class.getName());
	private GraphDatabaseFactory gdbf = new GraphDatabaseFactory();
	private GraphDatabaseService graphDb;
	private ExecutionEngine engine;
	private PropertyService propertyService;
	private String homeDirectory = "/data/db";
	
	public EmbeddedServiceImpl() {}
	
	@Override
	public void start() {
		if(graphDb == null) {
			log.info("Embedded Neo4j Service startup at "+homeDirectory+"...");
			GraphDatabaseBuilder builder = gdbf.newEmbeddedDatabaseBuilder(propertyService.getPropertyValue("home.dir")+homeDirectory);
			builder.setConfig(GraphDatabaseSettings.allow_store_upgrade, "true");
			builder.setConfig(GraphDatabaseSettings.keep_logical_logs, "false");
			graphDb = builder.newGraphDatabase();		
			engine = new ExecutionEngine(graphDb, StringLogger.SYSTEM);
			
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					synchronized(graphDb) {
						if(graphDb == null) {
							log.info("Embedded Neo4j Service shutdown...");
							graphDb.shutdown();
							graphDb = null;
						}
					}
				}
			});
		}
	}
	@Override
	public void stop() {
		if(graphDb == null) {
			log.info("Embedded Neo4j Service shutdown...");
			graphDb.shutdown();
			graphDb = null;
		}
	}
	
	public GraphDatabaseService getDatabaseService() {
		if(graphDb == null) start();
		return graphDb;
	}
	public ExecutionEngine getExecutionEngine() {
		if(graphDb == null) start();
		return engine;
	}
	public Index<Node> getIndex(String qname) {
		IndexManager mgr = getIndexManager();
		return mgr.forNodes(qname.toString(), MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext"));
	}
	public RelationshipIndex getRelationshipIndex(String qname) {
		return getIndexManager().forRelationships(qname.toString(), MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext"));
	}
	public Iterable<Node> getAllNodes() {
		return GlobalGraphOperations.at(graphDb).getAllNodes();
	}
	public Iterable<Relationship> getAllRelationships() {
		return GlobalGraphOperations.at(graphDb).getAllRelationships();
	}
	public Iterable<RelationshipType> getAllRelationshipTypes() {
		return GlobalGraphOperations.at(graphDb).getAllRelationshipTypes();
	}
	@Override
	public long createRelationship(long start, long end, RelationshipType type) {
		Relationship relationship = null;		
		Node startNode = getDatabaseService().getNodeById(start);
		Node endNode = getDatabaseService().getNodeById(end);
		relationship = startNode.createRelationshipTo(endNode, type);
		return relationship.getId();
	}
	@Override
	public void removeRelationship(long id) {
		Relationship node = getDatabaseService().getRelationshipById(id);
		node.delete();	
	}
	@Override
	public void removeNode(long id) {
		Node node = getDatabaseService().getNodeById(id);
		node.delete();
	}
	
	protected IndexManager getIndexManager() {
		if(graphDb == null) start();
		return graphDb.index();
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public String getHomeDirectory() {
		return homeDirectory;
	}
	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}	
}
