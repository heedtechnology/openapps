package org.heed.openapps.node.service;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.FieldCache.StringIndex;
import org.apache.lucene.search.FieldComparator;


public class StringOrdValComparator extends FieldComparator<String> {
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	private final int[] ords;
	private final String[] values;
	private final int[] readerGen;

	private int currentReaderGen = -1;
	private String[] lookup;
	private int[] order;
	private final String field;
	private int type;
	private boolean reversed;
	
	private int bottomSlot = -1;
	private int bottomOrd;
	private boolean bottomSameReader;
	private String bottomValue;

	public static final int NULL = 0;
	public static final int BOOLEAN = 1;
	public static final int STRING = 3;
	public static final int INTEGER = 4;
	public static final int LONG = 6;
	public static final int DOUBLE = 7;	
	public static final int DATE = 8;
	public static final int SERIALIZABLE = 9;
	public static final int LONGTEXT = 10;
	public static final int COMPUTED = 11;
	
	
	public StringOrdValComparator(int numHits, int type, String field, int sortPos, boolean reversed) {
		ords = new int[numHits];
		values = new String[numHits];
		readerGen = new int[numHits];
		this.field = field;
		this.type = type;
		this.reversed = reversed;
	}

	@Override
	public int compare(int slot1, int slot2) {
		//if (readerGen[slot1] == readerGen[slot2]) {
			//return ords[slot1] - ords[slot2];
		//}
		final String val1 = values[slot1];
		final String val2 = values[slot2];
		if (val1 == null) {
			if (val2 == null) {
				return 0;
			}
			return -1;
		} else if (val2 == null) {
			return 1;
		}
		
		String field1 = reversed ? removeTags(val2) : removeTags(val1);
		String field2 = reversed ? removeTags(val1) : removeTags(val2);
		if(type == DATE) {
			try {
				return dateFormatter.parse(field1).compareTo(dateFormatter.parse(field2));
			} catch(Exception e) {}
		} else if(type == INTEGER || type == DOUBLE || type == LONG) {
			return Long.valueOf(field1).compareTo(Long.valueOf(field2));
		} else {
			for(int i=0; i < field1.length(); i++) {
				if(field2.length() > i && field1.charAt(i) != field2.charAt(i)) {
					if(Character.isLetter(field1.charAt(i)) && Character.isLetter(field2.charAt(i)))
						return Character.valueOf(field1.charAt(i)).compareTo(Character.valueOf(field2.charAt(i)));
					else if(Character.isDigit(field1.charAt(i)) && Character.isDigit(field2.charAt(i))) {
						StringBuffer digit1 = new StringBuffer();
						StringBuffer digit2 = new StringBuffer();
						int index = i;
						while(field1.length() > index && Character.isDigit(field1.charAt(index))) {
							digit1.append(field1.charAt(index));
							index++;
						}
						index = i;
						while(field2.length() > index && Character.isDigit(field2.charAt(index))) {
							digit2.append(field2.charAt(index));
							index++;
						}
						if(Long.valueOf(digit1.toString()) > Long.valueOf(digit2.toString()))
							return 1;
						else return -1;
					}
				}
			}
			return field1.compareTo(field2);
		}
		return 0;
	}

	@Override
	public int compareBottom(int doc) {
		assert bottomSlot != -1;
		if (bottomSameReader) {
			// ord is precisely comparable, even in the equal case
			return bottomOrd - this.order[doc];
		} else {
			// ord is only approx comparable: if they are not
			// equal, we can use that; if they are equal, we
			// must fallback to compare by value
			final int order = this.order[doc];
			final int cmp = bottomOrd - order;
			if (cmp != 0) {
				return cmp;
			}
			final String val2 = lookup[order];
			if (bottomValue == null) {
				if (val2 == null) {
					return 0;
				}
				// bottom wins
				return -1;
			} else if (val2 == null) {
				// doc wins
				return 1;
			}
			return bottomValue.compareTo(val2);
		}
	}

	@Override
	public void copy(int slot, int doc) {
		final int ord = order[doc];
		ords[slot] = ord;
		assert ord >= 0;
		values[slot] = lookup[ord];
		readerGen[slot] = currentReaderGen;
	}

	@Override
	public void setNextReader(IndexReader reader, int docBase) throws IOException {
		StringIndex currentReaderValues = FieldCache.DEFAULT.getStringIndex(reader, field);
		currentReaderGen++;
		order = currentReaderValues.order;
		lookup = currentReaderValues.lookup;
		assert lookup.length > 0;
		if (bottomSlot != -1) {
			setBottom(bottomSlot);
		}
	}
  
	@Override
	public void setBottom(final int bottom) {
		bottomSlot = bottom;

		bottomValue = values[bottomSlot];
		if (currentReaderGen == readerGen[bottomSlot]) {
			bottomOrd = ords[bottomSlot];
			bottomSameReader = true;
	    } else {
	      if (bottomValue == null) {
	        ords[bottomSlot] = 0;
	        bottomOrd = 0;
	        bottomSameReader = true;
	        readerGen[bottomSlot] = currentReaderGen;
	      } else {
	        final int index = binarySearch(lookup, bottomValue);
	        if (index < 0) {
	          bottomOrd = -index - 2;
	          bottomSameReader = false;
	        } else {
	          bottomOrd = index;
	          // exact value match
	          bottomSameReader = true;
	          readerGen[bottomSlot] = currentReaderGen;            
	          ords[bottomSlot] = bottomOrd;
	        }
	      }
	    }
	}

	@Override
	public String value(int slot) {
		return values[slot];
	}
  
	@Override
	public int compareValues(String val1, String val2) {
	    if (val1 == null) {
	      if (val2 == null) {
	        return 0;
	      }
	      return -1;
	    } else if (val2 == null) {
	      return 1;
	    }
	    return val1.compareTo(val2);
	}

	public String[] getValues() {
		return values;
	}

	public int getBottomSlot() {
		return bottomSlot;
	}

	public String getField() {
		return field;
	}
	
	protected String removeTags(String in) {
		if(in == null) return "";
    	return in.replaceAll("\\<.*?>","");
	}
}