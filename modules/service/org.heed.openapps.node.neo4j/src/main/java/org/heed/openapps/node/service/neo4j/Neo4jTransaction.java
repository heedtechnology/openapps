package org.heed.openapps.node.service.neo4j;

import org.heed.openapps.node.Transaction;


public class Neo4jTransaction implements Transaction {
	private org.neo4j.graphdb.Transaction tx;
	
	public Neo4jTransaction(org.neo4j.graphdb.Transaction tx) {
		this.tx = tx;
	}
	
	@Override
	public void failure() {
		tx.failure();
	}

	@Override
	public void finish() {
		tx.finish();
		
	}

	@Override
	public void success() {
		tx.success();
	}

}
