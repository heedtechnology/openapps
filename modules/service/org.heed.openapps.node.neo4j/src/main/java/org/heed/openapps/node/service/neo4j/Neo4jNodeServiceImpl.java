package org.heed.openapps.node.service.neo4j;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryParser.QueryParser.Operator;
import org.apache.lucene.search.FieldComparator;
import org.apache.lucene.search.FieldComparatorSource;
import org.apache.lucene.search.SortField;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.data.Sort;
import org.heed.openapps.node.Node;
import org.heed.openapps.node.NodeException;
import org.heed.openapps.node.NodeQuery;
import org.heed.openapps.node.NodeResultset;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.node.Transaction;
import org.heed.openapps.node.TraversalQuery;
import org.heed.openapps.node.service.StringOrdValComparator;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.TransactionFailureException;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.index.lucene.QueryContext;
import org.neo4j.kernel.Traversal;


@SuppressWarnings("deprecation")
public class Neo4jNodeServiceImpl implements NodeService {
	private static final long serialVersionUID = 1639517584980826310L;
	private final static Log log = LogFactory.getLog(Neo4jNodeServiceImpl.class);
	private Neo4jService neo4jService;
	
	private boolean loggingOn = false;
	
	private Map<String, Long> uids = new HashMap<String, Long>();
	
	
	public void initialize() {
		List<Long> nodes = getAllNodes();
		log.info("populating uid cache...");
		int count = 0;
		int dup = 0;
		if(nodes.size() == 0) {
			try {
				createNode(java.util.UUID.randomUUID().toString());
			} catch(NodeException e) {
				e.printStackTrace();
			}
		}
		for(Long node : nodes) {
			if(hasNodeProperty(node, "uuid")) {
				try {
					String uid = String.valueOf(getNodeProperty(node, "uuid"));
					if(uids.containsKey(uid)) {					
						dup++;
					} else	uids.put(uid, node);
				} catch(NodeException e) {
					e.printStackTrace();
				}
			} 
			count++;
		}
		log.info("added "+uids.size()+" uids, missing uid:"+(count - uids.size())+", duplicate:"+dup);
	}
	public void shutdown() {
		neo4jService.stop();
	}
	@Override
	public List<Long> getAllNodes() {
		List<Long> nodes = new ArrayList<Long>();
		Transaction tx = getTransaction();
		try {
			Iterator<org.neo4j.graphdb.Node> iterator = neo4jService.getDatabaseService().getAllNodes().iterator();
			while(iterator.hasNext()) {
				long id = iterator.next().getId();
				if(id != 0) nodes.add(id);
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return nodes;
	}
	@Override
	public List<Long> getAllRelationships() {
		List<Long> relationships = new ArrayList<Long>();
		Iterator<org.neo4j.graphdb.Node> iterator = neo4jService.getDatabaseService().getAllNodes().iterator();
		while(iterator.hasNext()) {
			org.neo4j.graphdb.Node node = iterator.next();
			for(org.neo4j.graphdb.Relationship relationship : node.getRelationships()) {
				relationships.add(relationship.getId());
			}
		}
		log.info("returning "+relationships.size()+" relationships");
		return relationships;
	}
	@Override
	public List<Long> getNodes(long parent) {
		List<Long> nodes = new ArrayList<Long>();
		org.neo4j.graphdb.Node parentNode = neo4jService.getDatabaseService().getNodeById(parent);
		for(org.neo4j.graphdb.Relationship relation : parentNode.getRelationships(Direction.OUTGOING)) {
			org.neo4j.graphdb.Node n = relation.getEndNode();
			nodes.add(n.getId());				
		}
		//Collections.sort(nodes, sorter);
		return nodes;
	}
	@Override
	public List<Long> getNodes(QName qname) throws NodeException {
		List<Long> nodes = new ArrayList<Long>();
		Transaction tx = getTransaction();
		try {
			Iterator<org.neo4j.graphdb.Node> iterator = neo4jService.getDatabaseService().getAllNodes().iterator();
			while(iterator.hasNext()) {
				org.neo4j.graphdb.Node node = iterator.next();
				if(node.hasProperty("qname")) {
					String nodeQname = (String)node.getProperty("qname");
					if(nodeQname.equals(qname.toString())) {
						nodes.add(node.getId());
					}
				}			
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return nodes;
	}
	@Override
	public Node getNode(long id) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(id);
			if(n != null) {
				logNode(n);
				String name = n.hasProperty(SystemModel.NAME.toString()) ? (String)n.getProperty(SystemModel.NAME.toString()) : null;
				String uuid = n.hasProperty("uid") ? (String)n.getProperty("uuid") : java.util.UUID.randomUUID().toString();
				long xid = n.hasProperty("xid") ? (Long)n.getProperty("xid") : 0;
				long created = n.hasProperty("created") ? (Long)n.getProperty("created") : 0;
				long modified = n.hasProperty("modified") ? (Long)n.getProperty("modified") : 0;
				long creator = n.hasProperty("creator") ? (Long)n.getProperty("creator") : 0;
				long modifier = n.hasProperty("modifier") ? (Long)n.getProperty("modifier") : 0;
				long accessed = n.hasProperty("accessed") ? (Long)n.getProperty("accessed") : 0;
				long user = n.hasProperty("user") ? (Long)n.getProperty("user") : 0;
				boolean deleted = n.hasProperty("deleted") ? (Boolean)n.getProperty("deleted") : false;
				Node node = new Node(n.getId(), getNodeQName(id));
				node.setXid(xid);
				node.setName(name);
				node.setAccessed(accessed);
				node.setCreated(created);
				node.setCreator(creator);
				node.setDeleted(deleted);
				node.setModified(modified);
				node.setModifier(modifier);
				node.setUser(user);
				node.setUuid(uuid);
				tx.success();
				return node;
			} else throw new NodeException("no node found for id:"+id);
		} catch(Exception e) {
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public Long getNodeId(String uid) {
		Long id = uids.get(uid);
		if(id == null) {
			log.info("no id found for uid:"+uid);
		}
		return id;
	}
	@Override
	public long createNode(String uid) throws NodeException {
		Transaction tx = getTransaction();
		try	{
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().createNode();
			if(uid == null || uid.length() == 0) uid = UUID.randomUUID().toString();
			//log.info("creating node with uid:"+uid);
			addUpdateStringProperty(n, "uuid", uid);
			addUpdateLongProperty(n, "created", System.currentTimeMillis());
			uids.put(uid, n.getId());
			long id = n.getId();
			
			tx.success();
			return id;
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void addUpdateNode(Node node) throws NodeException {
		if(node.getQName() == null) throw new NodeException("invalid node with no qname");
		if(node.getName() == null) throw new NodeException("invalid node with no name");
		Transaction tx = getTransaction();
		try	{
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(node.getId());
			addUpdateLongProperty(n, "xid", node.getXid());
			addUpdateStringProperty(n, "uuid", node.getUuid());
			addUpdateLongProperty(n, "creator", node.getCreator());
			addUpdateLongProperty(n, "modifier", node.getModifier());
			addUpdateLongProperty(n, "created", node.getCreated());
			addUpdateLongProperty(n, "modified", node.getModified());
			addUpdateBooleanProperty(n, "deleted", node.isDeleted());
			addUpdateLongProperty(n, "accessed", node.getAccessed());
			addUpdateStringProperty(n, "qname", node.getQName().toString());
			addUpdateLongProperty(n, "user", node.getUser());
			addUpdateStringProperty(n, SystemModel.NAME.toString(), node.getName());			
			tx.success();
			logNode(n);
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void addUpdateNodeProperty(long nodeId, String qname, Object value) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(value instanceof String) addUpdateStringProperty(n, qname, (String)value);
			else if(value instanceof Long) addUpdateLongProperty(n, qname, (Long)value);
			else if(value instanceof Integer) addUpdateIntegerProperty(n, qname, (Integer)value);
			else if(value instanceof Boolean) addUpdateBooleanProperty(n, qname, (Boolean)value);
			else addUpdateStringProperty(n, qname, String.valueOf(value));
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@SuppressWarnings("unused")
	@Override
	public void addUpdateRelationship(String qname, long startNodeId, long endNodeId) throws NodeException {
		Transaction tx = getTransaction();
		try	{
			org.neo4j.graphdb.Node startNode = neo4jService.getDatabaseService().getNodeById(startNodeId);
			for(org.neo4j.graphdb.Relationship rel : startNode.getRelationships(new Neo4jRelationshipType(qname), Direction.OUTGOING)) {
				if(rel.getEndNode().getId() == endNodeId)
					return;
			}
			org.neo4j.graphdb.Node endNode = neo4jService.getDatabaseService().getNodeById(endNodeId);
			org.neo4j.graphdb.Relationship r = startNode.createRelationshipTo(endNode, new Neo4jRelationshipType(qname));
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void removeNode(long id) throws NodeException {
		Transaction tx = getTransaction();
		try	{
			String uid = null;
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(id);
			//if(n.hasRelationship(Direction.OUTGOING)) throw new NodeException("Invalide node deletion, outgoing realtionships still exist.");
			if(n.hasProperty("uuid")) {
				uid = (String)n.getProperty("uuid");				
			}
			for(org.neo4j.graphdb.Relationship relation : n.getRelationships()) {
				relation.delete();
			}
			n.delete();
			if(uid != null) {
				uids.remove(uid);
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void removeNode(long id, TraversalQuery query) throws NodeException {
		Transaction tx = getTransaction();
		try	{
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(id);
			String uid = (String)n.getProperty("uuid");
			//if(n.hasRelationship(Direction.OUTGOING)) throw new NodeException("Invalide node deletion, outgoing realtionships still exist.");
			for(org.neo4j.graphdb.Relationship relation : n.getRelationships(Direction.INCOMING)) {
				relation.delete();
			}
		
			TraversalDescription traverser = Traversal.description().depthFirst().evaluator(Evaluators.toDepth(1));
			for(QName qname : query.getQNames()) {
				traverser.relationships(new Neo4jRelationshipType(qname.toString()), Direction.OUTGOING);
			}
			for(Path position : traverser.traverse(n)) {
				String name = position.endNode().hasProperty(SystemModel.NAME.toString()) ? (String)position.endNode().getProperty(SystemModel.NAME.toString()) : "";
				log.info("cascading delete to:"+name+" ("+position.endNode().getId()+")");
				removeNode(position.endNode().getId());
			}
			uids.remove(uid);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}	
	@Override
	public Relationship createRelationship(QName qname, long start, long end) throws NodeException {
		Transaction tx = getTransaction();
		try {
			long id = neo4jService.createRelationship(start, end, new Neo4jRelationshipType(qname.toString()));
			Relationship relationship =  getRelationship(id);
			tx.success();
			return relationship;
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void removeRelationship(long id) {
		Transaction tx = getTransaction();
		try {
			neo4jService.removeRelationship(id);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
	}
	public void setNeo4jService(Neo4jService neo4jService) {
		this.neo4jService = neo4jService;
	}
	
	@Override
	public void addUpdateRelationshipProperty(long relationshipId, String key, Object value) throws NodeException {
		assert(key != null);
		assert(value != null);
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Relationship n = neo4jService.getDatabaseService().getRelationshipById(relationshipId);
			n.removeProperty(key);
			n.setProperty(key, value);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public Map<String, Object> getNodeProperties(long nodeId) throws NodeException {
		Map<String, Object> data = new HashMap<String, Object>();
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			for(String key : n.getPropertyKeys()) {
				data.put(key, n.getProperty(key));
			}
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return data;
	}
	@Override
	public Object getNodeProperty(long nodeId, String key) throws NodeException {
		Object value = null;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(n.hasProperty(key))
				value = n.getProperty(key);
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return value;
	}
	@Override
	public QName getRelationshipQName(long id) throws NodeException {
		QName qname = null;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Relationship r = neo4jService.getDatabaseService().getRelationshipById(id);
			qname = new QName(r.getType().name());
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return qname;
	}
	@Override
	public QName getNodeQName(long id) throws NodeException {
		QName qname = null;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(id);
			if(n != null && n.hasProperty("qname")) {
				qname = new QName((String)n.getProperty("qname"));
			}
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return qname;
	}
	
	@Override
	public Relationship getRelationship(long id) throws NodeException {
		Relationship relationship = null;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Relationship r = neo4jService.getDatabaseService().getRelationshipById(id);
			relationship = new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId());
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return relationship;
	}
	@Override
	public Map<String, Object> getRelationshipProperties(long id) throws NodeException {
		Map<String, Object> value = new HashMap<String, Object>();
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Relationship n = neo4jService.getDatabaseService().getRelationshipById(id);
			for(String key : n.getPropertyKeys()) {
				value.put(key, n.getProperty(key));
			}
			tx.success();		
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return value;
	}
	@Override
	public Object getRelationshipProperty(long relationshipId, String key) throws NodeException {
		Transaction tx = getTransaction();
		Object value = null;
		try {
			org.neo4j.graphdb.Relationship n = neo4jService.getDatabaseService().getRelationshipById(relationshipId);
			value = n.hasProperty(key) ? n.getProperty(key) : null;
			tx.success();			
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
		return value;
	}
	@Override
	public List<Relationship> getRelationships(long nodeId,	org.heed.openapps.node.Direction direction) {
		List<Relationship> relationships = new ArrayList<Relationship>();
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node node = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(direction.equals(org.heed.openapps.node.Direction.INCOMING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.INCOMING)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			} else if(direction.equals(org.heed.openapps.node.Direction.OUTGOING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			} else {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.BOTH)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return relationships;
	}
	@Override
	public List<Relationship> getRelationships(long nodeId,	org.heed.openapps.node.Direction direction, QName... qnames) {
		List<Relationship> relationships = new ArrayList<Relationship>();
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node node = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(direction.equals(org.heed.openapps.node.Direction.INCOMING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.INCOMING)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			} else if(direction.equals(org.heed.openapps.node.Direction.OUTGOING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			} else {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.BOTH)) {
					relationships.add(new Relationship(r.getId(), new QName(r.getType().name()), r.getStartNode().getId(), r.getEndNode().getId()));
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return relationships;
	}
	@Override
	public Relationship getSingleRelationship(long nodeId, QName qname, org.heed.openapps.node.Direction direction) throws NodeException {
		Relationship relationship = null;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node node = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(direction.equals(org.heed.openapps.node.Direction.INCOMING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.INCOMING, convert(qname))) {
					if(r.getType().name().equals(qname.toString())) 
						relationship = new Relationship(r.getId(), qname, r.getStartNode().getId(), r.getEndNode().getId()); 
				}
			} else if(direction.equals(org.heed.openapps.node.Direction.OUTGOING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING, convert(qname))) {
					if(r.getType().name().equals(qname.toString())) 
						relationship = new Relationship(r.getId(), qname, r.getStartNode().getId(), r.getEndNode().getId()); 
				}
			} else {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.BOTH, convert(qname))) {
					if(r.getType().name().equals(qname.toString())) 
						relationship = new Relationship(r.getId(), qname, r.getStartNode().getId(), r.getEndNode().getId()); 
				}
			}			
			tx.success();
			return relationship;
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public boolean hasNodeProperty(long nodeId, String key) {
		boolean state = false;
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			state = n.hasProperty(key);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return state;
	}
	@SuppressWarnings("unused")
	@Override
	public boolean hasRelationship(long id,	org.heed.openapps.node.Direction direction) {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node node = neo4jService.getDatabaseService().getNodeById(id);
			if(direction.equals(org.heed.openapps.node.Direction.INCOMING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.INCOMING)) {
					return true;
				}
			} else if(direction.equals(org.heed.openapps.node.Direction.OUTGOING)) {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING)) {
					return true;
				}
			} else {
				for(org.neo4j.graphdb.Relationship r : node.getRelationships(org.neo4j.graphdb.Direction.BOTH)) {
					return true; 
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return false;
	}
	@Override
	public boolean hasRelationshipProperty(long id, String key) throws NodeException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void removeNodeProperty(long nodeId, String key) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			n.removeProperty(key);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void removeRelationshipProperty(long relationshipId, String key) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Relationship n = neo4jService.getDatabaseService().getRelationshipById(relationshipId);
			n.removeProperty(key);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	public void deindexNode(long nodeId) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			if(n != null) {
				Index<org.neo4j.graphdb.Node> idx = neo4jService.getIndex("default");
				if(idx != null) {
					idx.remove(n);
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	public void indexNode(long nodeId, Map<String,Object> properties) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			Index<org.neo4j.graphdb.Node> idx = neo4jService.getIndex("default");
			if(idx != null) {
				for(String key : properties.keySet()) {
					idx.remove(n, key);
					idx.add(n, key, properties.get(key));
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	public void indexProperty(long nodeId, String propertyName, Object value) throws NodeException {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(nodeId);
			indexProperty(n, propertyName, value);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new NodeException("", e);
		} finally {
			tx.finish();
        }
	}
	protected void indexProperty(org.neo4j.graphdb.Node n, String propertyName, Object value) {
		Index<org.neo4j.graphdb.Node> idx = neo4jService.getIndex("default");
		if(idx != null) {
			idx.remove(n, propertyName);
			idx.add(n, propertyName, value.toString());
		}
	}
	protected org.neo4j.graphdb.RelationshipType[] convert(QName... qnames) {
		Transaction tx = getTransaction();
		try {
			org.neo4j.graphdb.RelationshipType[] neo4jTypes = new org.neo4j.graphdb.RelationshipType[qnames.length];
			for(int i=0; i < qnames.length; i++) {
				QName qname = qnames[i];
				neo4jTypes[i] = new Neo4jRelationshipType(qname.toString());
			}	
			tx.success();
			return neo4jTypes;			
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }
		return new org.neo4j.graphdb.RelationshipType[0];
	}
	protected void addUpdateStringProperty(org.neo4j.graphdb.Node n, String propertyName, String value) {
		if(n.hasProperty(propertyName)) {
			if(!value.equals(n.getProperty(propertyName))) {
				n.removeProperty(propertyName);
				n.setProperty(propertyName, value);
			}
		} else n.setProperty(propertyName, value);
		indexProperty(n, propertyName, value);
	}
	protected void addUpdateLongProperty(org.neo4j.graphdb.Node n, String propertyName, Long value) {
		if(n.hasProperty(propertyName)) {
			if(!value.equals(n.getProperty(propertyName))) {
				n.removeProperty(propertyName);
				n.setProperty(propertyName, value);
			}
		} else n.setProperty(propertyName, value);
		indexProperty(n, propertyName, value);
	}
	protected void addUpdateIntegerProperty(org.neo4j.graphdb.Node n, String propertyName, Integer value) {
		if(n.hasProperty(propertyName)) {
			if(!value.equals(n.getProperty(propertyName))) {
				n.removeProperty(propertyName);
				n.setProperty(propertyName, value);
			}
		} else n.setProperty(propertyName, value);
		indexProperty(n, propertyName, value);
	}
	protected void addUpdateBooleanProperty(org.neo4j.graphdb.Node n, String propertyName, Boolean value) {
		if(n.hasProperty(propertyName)) {
			if(!value.equals(n.getProperty(propertyName))) {
				n.removeProperty(propertyName);
				n.setProperty(propertyName, value);
			}
		} else n.setProperty(propertyName, value);
		indexProperty(n, propertyName, value);
	}
	
	protected void logNode(org.neo4j.graphdb.Node n) {
		StringWriter out = new StringWriter();
		out.write("Node[");
		for(String key : n.getPropertyKeys()) {
			out.write(key + ":" + n.getProperty(key) + ",");
		}
		out.write("]");
		if(loggingOn) log.info(out.toString());
	}
	public Transaction getTransaction() {
		Transaction tx = null;
		try {
			GraphDatabaseService svc = neo4jService.getDatabaseService();
			tx = new Neo4jTransaction(svc.beginTx());			
		} catch(TransactionFailureException e) {
			e.printStackTrace();
		}
		return tx;		
	}
	@Override
	public long count(NodeQuery query) throws NodeException {
		if(query != null) {
			Transaction tx = getTransaction();
			try {
				if(query.getType() == NodeQuery.TYPE_CYPHER) {
					ExecutionResult result = neo4jService.getExecutionEngine().execute((String)query.getQueryOrQueryObject());
					while(result.iterator().hasNext()) {
						Map<String,Object> node = result.iterator().next();	
						long count = (Long)node.get("count(n)");
						return count;
					}
				} else if(query.getType() == NodeQuery.TYPE_LUCENE) {
					
				} 
				tx.success();
			} catch(Exception e) {
				tx.failure();
				throw new NodeException("", e);
			} finally {
				tx.finish();
	        }
		}
		return 0;
	}
	@Override
	public NodeResultset search(NodeQuery query) throws NodeException {
		NodeResultset resultset = new NodeResultset();
		if(query != null) {
			Transaction tx = getTransaction();
			try {
				if(query.getType() == NodeQuery.TYPE_CYPHER) {
					ExecutionResult result = neo4jService.getExecutionEngine().execute((String)query.getQueryOrQueryObject());
					if(query.getEnd() > 0) {
						int count = 0;
						while(result.iterator().hasNext() && count < query.getEnd()) {
							Map<String,Object> node = result.iterator().next();	
							if(count >= query.getStart()) {												
								org.neo4j.graphdb.Node n = (org.neo4j.graphdb.Node)node.get("n");
								if(n.getId() != 0) resultset.getIds().add(n.getId());								
							}
							count++;
						}
					} else {
						while(result.iterator().hasNext()) {
							Map<String,Object> node = result.iterator().next();					
							org.neo4j.graphdb.Node n = (org.neo4j.graphdb.Node)node.get("n");
							if(n.getId() != 0) resultset.getIds().add(n.getId());
						}
					}
				} else if(query.getType() == NodeQuery.TYPE_LUCENE) {
					Index<org.neo4j.graphdb.Node> idx = neo4jService.getIndex("default");
					if(idx != null) {
						int count = 0;
						IndexHits<org.neo4j.graphdb.Node> nodes = query(idx, query);						
						while(nodes.hasNext() && count < query.getEnd()) {
							org.neo4j.graphdb.Node node = nodes.next();	
							if(count >= query.getStart() && node.getId() > 0) {												
								resultset.getIds().add(node.getId());								
							}
							count++;
						}
						resultset.setTotal(count);
					}
				} else if(query.getType() == NodeQuery.TYPE_NATIVE) { //Traversal Query
					TraversalQuery traversalQuery = (TraversalQuery)query.getQueryOrQueryObject();
					org.neo4j.graphdb.Node n = neo4jService.getDatabaseService().getNodeById(query.getNodeId());
					
					TraversalDescription traverser = Traversal.description().depthFirst().evaluator(Evaluators.toDepth(1));
					for(QName qname : traversalQuery.getQNames()) {
						traverser.relationships(new Neo4jRelationshipType(qname.toString()), Direction.OUTGOING);
					}
					for(Path position : traverser.traverse(n)) {
						resultset.getIds().add(position.endNode().getId());
					}					
					resultset.setTotal(resultset.getIds().size());
				}
				tx.success();
			} catch(Exception e) {
				tx.failure();
				throw new NodeException("", e);
			} finally {
				tx.finish();
	        }
		}
		return resultset;
	}
	
	protected IndexHits<org.neo4j.graphdb.Node> query(Index<org.neo4j.graphdb.Node> index, NodeQuery query) {
		Sort sort = query.getSorting();
		if(sort != null) return query(index, query, sort);
		QueryContext ctx = new QueryContext(query.getQueryOrQueryObject()).sortByScore();
		try {
			org.neo4j.graphdb.index.IndexHits<org.neo4j.graphdb.Node> hits = index.query(ctx);
			return hits;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	protected IndexHits<org.neo4j.graphdb.Node> query(Index<org.neo4j.graphdb.Node> idx, NodeQuery query, final Sort sort) {
		org.apache.lucene.search.Sort lSort = new org.apache.lucene.search.Sort();
		SortField field = new SortField(query.getSorting().getField(), new FieldComparatorSource() {
			private static final long serialVersionUID = -2617736422667364994L;
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public FieldComparator newComparator(final String fieldname, final int numHits, int sortPos, boolean reversed) throws IOException {
				return new StringOrdValComparator(numHits, sort.getType(), fieldname, sortPos, reversed);
			}			
		}, sort.isReverse());
		lSort.setSort(field);
		
		QueryContext ctx = new QueryContext(query.getQueryOrQueryObject()).sort(lSort);
		ctx.defaultOperator(Operator.AND);
		org.neo4j.graphdb.index.IndexHits<org.neo4j.graphdb.Node> hits = idx.query(ctx);
		return hits;
	}
	
	public boolean isLoggingOn() {
		return loggingOn;
	}
	public void setLoggingOn(boolean loggingOn) {
		this.loggingOn = loggingOn;
	}
		
}
