package org.heed.openapps.node.service.neo4j;

import org.heed.openapps.cache.CacheService;
import org.heed.openapps.node.MultiUserNodeService;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.property.PropertyService;

/*
 * <!-- Node Service -->
	<bean id="neo4jService" class="org.heed.openapps.node.service.neo4j.EmbeddedServiceImpl" init-method="start" destroy-method="stop">
		<property name="homeDirectory" value="/data/db" />
		<property name="propertyService" ref="propertyService" />
		<property name="loggingService" ref="loggingService" />
	</bean>	
	<bean id="nodeService" class="org.heed.openapps.node.service.neo4j.Neo4jNodeServiceImpl" init-method="initialize" destroy-method="shutdown">
		<property name="loggingService" ref="loggingService" />
		<property name="neo4jService" ref="neo4jService" />
		<property name="loggingOn" value="false" />
	</bean>
 */
public class MultiUserNodeServiceImpl implements MultiUserNodeService {
	private CacheService cacheService;
	private PropertyService propertyService;
		
	@Override
	public NodeService getNodeService(long userId) {
		Neo4jNodeServiceImpl service = (Neo4jNodeServiceImpl)cacheService.get("MultiUserNodeServiceImpl", String.valueOf(userId));
		if(service == null) {
			service = new Neo4jNodeServiceImpl();
			service.setLoggingOn(false);
			EmbeddedServiceImpl neo4jService = new EmbeddedServiceImpl();
			neo4jService.setPropertyService(propertyService);
			neo4jService.setHomeDirectory("/data/neo4j/db"+userId);
			service.setNeo4jService(neo4jService);
			neo4jService.start();
			service.initialize();
			cacheService.put("MultiUserNodeServiceImpl", String.valueOf(userId), service);
		}
		return service;
	}
	
	public void setCasheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
}
