package org.heed.openapps.node.service.neo4j;
import java.util.ArrayList;
import java.util.Hashtable;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.graphdb.index.RelationshipIndex;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.kernel.impl.cache.CacheProvider;
import org.neo4j.kernel.impl.cache.SoftCacheProvider;
import org.neo4j.kernel.impl.util.StringLogger;
import org.neo4j.tooling.GlobalGraphOperations;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;


public class OsgiServiceImpl implements Neo4jService, BundleActivator {
	private static final long serialVersionUID = 4807139298500438633L;
	private static GraphDatabaseService graphDb;	
	private ExecutionEngine engine;
	private ServiceRegistration serviceRegistration;
    private ServiceRegistration indexServiceRegistration;
	private String rootPath = "data/db";
	
	
	@Override
	public void start(BundleContext context) throws Exception {
		//the cache providers
        ArrayList<CacheProvider> cacheList = new ArrayList<CacheProvider>();
        cacheList.add( new SoftCacheProvider() );
 
        //the index providers
        //IndexProvider lucene = new LuceneIndexProvider();
        //ArrayList<IndexProvider> provs = new ArrayList<IndexProvider>();
        //provs.add( lucene );
        //ListIndexIterable providers = new ListIndexIterable();
        //providers.setIndexProviders( provs );
 
        //the database setup
        GraphDatabaseFactory gdbf = new GraphDatabaseFactory();
        //gdbf.setIndexProviders( providers );
        gdbf.setCacheProviders( cacheList );
        graphDb = gdbf.newEmbeddedDatabaseBuilder(rootPath).loadPropertiesFromFile("configuration/neo4j.properties").newGraphDatabase();
        
        //the OSGi registration
        serviceRegistration = context.registerService(
                GraphDatabaseService.class.getName(), graphDb, new Hashtable<String,String>() );
        System.out.println( "registered " + serviceRegistration.getReference() );
        indexServiceRegistration = context.registerService(
                Index.class.getName(), graphDb.index().forNodes( "nodes" ),
                new Hashtable<String,String>() );
        engine = new ExecutionEngine(graphDb, StringLogger.SYSTEM);
	}
	@Override
	public void stop(BundleContext context) throws Exception {
		serviceRegistration.unregister();
        indexServiceRegistration.unregister();
        graphDb.shutdown();
	}
	
	public GraphDatabaseService getDatabaseService() {
		return graphDb;
	}
	public ExecutionEngine getExecutionEngine() {
		return engine;
	}
	public Index<Node> getIndex(String qname) {
		IndexManager mgr = getIndexManager();
		return mgr.forNodes(qname.toString(), MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext"));
	}
	public RelationshipIndex getRelationshipIndex(String qname) {
		return getIndexManager().forRelationships(qname.toString(), MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext"));
	}
	public Iterable<Node> getAllNodes() {
		return GlobalGraphOperations.at(graphDb).getAllNodes();
	}
	public Iterable<Relationship> getAllRelationships() {
		return GlobalGraphOperations.at(graphDb).getAllRelationships();
	}
	public Iterable<RelationshipType> getAllRelationshipTypes() {
		return GlobalGraphOperations.at(graphDb).getAllRelationshipTypes();
	}
	@Override
	public long createRelationship(long start, long end, RelationshipType type) {
		Transaction tx = getDatabaseService().beginTx();
		Relationship relationship = null;
		try	{
			Node startNode = getDatabaseService().getNodeById(start);
			Node endNode = getDatabaseService().getNodeById(end);
			relationship = startNode.createRelationshipTo(endNode, type);
			tx.success();
			return relationship.getId();
		} finally	{
		    tx.finish();
		}
	}
	@Override
	public void removeRelationship(long id) {
		Transaction tx = getDatabaseService().beginTx();
		try	{
			Relationship node = getDatabaseService().getRelationshipById(id);
			node.delete();
			tx.success();
		} finally	{
		    tx.finish();
		}	
	}
	@Override
	public void removeNode(long id) {
		Transaction tx = getDatabaseService().beginTx();
		try	{
			Node node = getDatabaseService().getNodeById(id);
			node.delete();
			tx.success();
		} finally	{
		    tx.finish();
		}
	}
	
	protected IndexManager getIndexManager() {
		return graphDb.index();
	}
	public void cypher() {
		//ExecutionEngine engine = new ExecutionEngine( db );
		//ExecutionResult result = engine.execute( "start n=node(0) return n, n.name" );
	}
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
				
}
