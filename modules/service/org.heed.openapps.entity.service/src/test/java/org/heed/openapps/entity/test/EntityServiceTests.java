package org.heed.openapps.entity.test;

import junit.framework.TestCase;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@ContextConfiguration("rest-servlet.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class EntityServiceTests extends TestCase {
	@Autowired private EntityService entityService;
	
	
	@Before
	public void setUp() throws Exception {
		
	}
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSearch() {
		
	}

	@Test
	public void testGetEntityLong() throws Exception {		
		long id = 1L;		
		Entity entity = entityService.getEntity(id);
		if(entity == null) fail("no entity found for id:"+id);
		id = entity.getId();
		//if(id == 0) fail("user id:0 found");	
	}

	@Test
	public void testGetEntityString() throws Exception {
		String uid = "";
		Entity entity = entityService.getEntity(uid);
		if(entity == null) fail("no entity found for id:"+uid);
		uid = entity.getUuid();
		
	}
	
	@Test
	public void testAddEntityEntity() throws Exception {
		Entity entity = new Entity();
		entity.setQName(SystemModel.USER);
		entity.addProperty("email", "testuser@heedtech.com");
		entityService.addEntity(entity);			
		if(entity.getId() == null || entity.getId().equals(0L)) fail("invalid id for user");
		
		entityService.removeEntity(entity.getId());
	}
	
	@Test
	public void testUpdateEntity() throws Exception {
		long id = 1L;
		if(id == 0) fail("user id:"+id+" found");		
		Entity entity = entityService.getEntity(id);
		if(entity == null) fail("no entity found for id:"+id);
		
		entity.addProperty("email", "user2@heedtech.com.saved");
		entityService.updateEntity(entity);
		
		entity = entityService.getEntity(id);
		assertEquals("", "user2@heedtech.com.saved", entity.getPropertyValue(SystemModel.EMAIL));
		
		entity.addProperty("email", "user2@heedtech.com");
		entityService.updateEntity(entity);
		
		entity = entityService.getEntity(id);
		assertEquals("", "user2@heedtech.com", entity.getPropertyValue(SystemModel.EMAIL));		
	}
}
