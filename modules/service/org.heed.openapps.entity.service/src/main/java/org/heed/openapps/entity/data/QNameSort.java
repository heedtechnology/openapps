package org.heed.openapps.entity.data;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.FieldComparator;
import org.apache.lucene.search.FieldComparatorSource;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.FieldCache.StringIndex;
import org.heed.openapps.data.Sort;


public class QNameSort extends Sort {
	
	
	
	public QNameSort(final List<String> types, boolean reverse) {
		//super(Sort.STRING, "qname", reverse);
		SortField field = new SortField("localName", new FieldComparatorSource() {
			private static final long serialVersionUID = -2617736422667364994L;
			@SuppressWarnings("rawtypes")
			@Override
			public FieldComparator newComparator(final String fieldname, final int numHits, int sortPos, boolean reversed) throws IOException {
				return new QNameOrdValComparator(types, numHits, Sort.STRING, fieldname, sortPos, reversed);
			}			
		}, reverse);
		setSortField(field);
	}
	
	public class QNameOrdValComparator extends FieldComparator<String> {
		private List<String> types;
		private final int[] ords;
		private final String[] values;
		private final int[] readerGen;

		private int currentReaderGen = -1;
		private String[] lookup;
		private int[] order;
		private final String field;
		private boolean reversed;
		
		private int bottomSlot = -1;
		private int bottomOrd;
		private boolean bottomSameReader;
		private String bottomValue;

		
		public QNameOrdValComparator(List<String> types, int numHits, int type, String field, int sortPos, boolean reversed) {
			this.types = types;
			ords = new int[numHits];
			values = new String[numHits];
			readerGen = new int[numHits];
			this.field = field;
			this.reversed = reversed;
		}

		@Override
		public int compare(int slot1, int slot2) {
			//if (readerGen[slot1] == readerGen[slot2]) {
				//return ords[slot1] - ords[slot2];
			//}
			final String val1 = values[slot1];
			final String val2 = values[slot2];
			if (val1 == null) {
				if (val2 == null) {
					return 0;
				}
				return -1;
			} else if (val2 == null) {
				return 1;
			}
			if(val1.equals(val2)) return 0;
						
			Integer idx1 = types.indexOf(val1);
			Integer idx2 = types.indexOf(val2);
			
			if(reversed) {
				return idx2.compareTo(idx1);
			} else {
				return idx1.compareTo(idx2);
			}
		}

		@Override
		public int compareBottom(int doc) {
			assert bottomSlot != -1;
			if (bottomSameReader) {
				// ord is precisely comparable, even in the equal case
				return bottomOrd - this.order[doc];
			} else {
				// ord is only approx comparable: if they are not
				// equal, we can use that; if they are equal, we
				// must fallback to compare by value
				final int order = this.order[doc];
				final int cmp = bottomOrd - order;
				if (cmp != 0) {
					return cmp;
				}
				final String val2 = lookup[order];
				if (bottomValue == null) {
					if (val2 == null) {
						return 0;
					}
					// bottom wins
					return -1;
				} else if (val2 == null) {
					// doc wins
					return 1;
				}
				return bottomValue.compareTo(val2);
			}
		}

		@Override
		public void copy(int slot, int doc) {
			final int ord = order[doc];
			ords[slot] = ord;
			assert ord >= 0;
			values[slot] = lookup[ord];
			readerGen[slot] = currentReaderGen;
		}

		@Override
		public void setNextReader(IndexReader reader, int docBase) throws IOException {
			StringIndex currentReaderValues = FieldCache.DEFAULT.getStringIndex(reader, field);
			currentReaderGen++;
			order = currentReaderValues.order;
			lookup = currentReaderValues.lookup;
			assert lookup.length > 0;
			if (bottomSlot != -1) {
				setBottom(bottomSlot);
			}
		}
	  
		@Override
		public void setBottom(final int bottom) {
			bottomSlot = bottom;

			bottomValue = values[bottomSlot];
			if (currentReaderGen == readerGen[bottomSlot]) {
				bottomOrd = ords[bottomSlot];
				bottomSameReader = true;
		    } else {
		      if (bottomValue == null) {
		        ords[bottomSlot] = 0;
		        bottomOrd = 0;
		        bottomSameReader = true;
		        readerGen[bottomSlot] = currentReaderGen;
		      } else {
		        final int index = binarySearch(lookup, bottomValue);
		        if (index < 0) {
		          bottomOrd = -index - 2;
		          bottomSameReader = false;
		        } else {
		          bottomOrd = index;
		          // exact value match
		          bottomSameReader = true;
		          readerGen[bottomSlot] = currentReaderGen;            
		          ords[bottomSlot] = bottomOrd;
		        }
		      }
		    }
		}

		@Override
		public String value(int slot) {
			return values[slot];
		}
	  
		@Override
		public int compareValues(String val1, String val2) {
		    if (val1 == null) {
		      if (val2 == null) {
		        return 0;
		      }
		      return -1;
		    } else if (val2 == null) {
		      return 1;
		    }
		    return val1.compareTo(val2);
		}

		public String[] getValues() {
			return values;
		}

		public int getBottomSlot() {
			return bottomSlot;
		}

		public String getField() {
			return field;
		}
		
		protected String removeTags(String in) {
			if(in == null) return "";
	    	return in.replaceAll("\\<.*?>","");
		}
	}
}
