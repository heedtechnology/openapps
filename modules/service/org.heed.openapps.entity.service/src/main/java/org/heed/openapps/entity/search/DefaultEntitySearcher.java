package org.heed.openapps.entity.search;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.search.Query;
import org.heed.openapps.data.Sort;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntitySearcher;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.node.NodeQuery;
import org.heed.openapps.node.NodeResultset;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.util.StringFormatUtility;


public class DefaultEntitySearcher implements EntitySearcher {
	private final static Log log = LogFactory.getLog(DefaultEntitySearcher.class);
	private LuceneEntityQueryParser luceneParser = new LuceneEntityQueryParser();
	private CypherEntityQueryParser cypherParser = new CypherEntityQueryParser();
	private NodeService nodeService;
	private EntityService entityService;
	private String homeDirectory = "/data/lucene/default";
	private boolean loggingOn = false;
	
	
	@Override
	public EntityResultSet search(EntityQuery query) throws Exception {
		EntityResultSet entities = new EntityResultSet();	
		long startTime = System.currentTimeMillis();
		int start = 0;
		int end = 100;
		if(query.getEndRow() > 0) {
			start = query.getStartRow();
			end = query.getEndRow();
		}		
		
		NodeResultset results = null;
		NodeQuery nodeQuery = new NodeQuery(query.getEntityQnames()[0]);
		nodeQuery.setStart(query.getStartRow());
		nodeQuery.setEnd(query.getEndRow());
		
		if(query.getType() == EntityQuery.TYPE_CYPHER) {
			/** count query **/
			String countQueryStr = cypherParser.parseCountQuery(query);
			NodeQuery countQuery = new NodeQuery(countQueryStr);
			countQuery.setType(NodeQuery.TYPE_CYPHER);
			long count = nodeService.count(countQuery);
					
			/** results query **/
			if(query.getEntityQnames()[0] != null || query.getQueryString() != null) {
				if(query.getSort() != null && query.getSort().getField() != null) 
					nodeQuery.setSorting(new org.heed.openapps.data.Sort(Sort.STRING, query.getSort().getField(), query.getSort().isReverse()));
				else {
					Sort sort = new Sort(Sort.STRING, "openapps_org_system_1_0_name", false);
					query.setSort(sort);
					nodeQuery.setSorting(sort);
				}
				nodeQuery.setType(NodeQuery.TYPE_CYPHER);
				String cypherQuery = cypherParser.parse(query);
				nodeQuery.setQueryOrQueryObject(cypherQuery);
				results = nodeService.search(nodeQuery);
				log.info("entity search : " +results.getTotal()+" results in "+StringFormatUtility.getElapsedTime(startTime)+" parsed to " + cypherQuery);
			}
			entities.setResultSize((int)count);
		} else  if(query.getType() == EntityQuery.TYPE_LUCENE) {
			if(query.getSort() != null && query.getSort().getField() != null) 
				nodeQuery.setSorting(new org.heed.openapps.data.Sort(Sort.STRING, query.getSort().getField(), query.getSort().isReverse()));
			else {
				Sort sort = new Sort(Sort.STRING, "openapps_org_system_1_0_name_e", false);
				query.setSort(sort);
				nodeQuery.setSorting(sort);
			}
			nodeQuery.setType(NodeQuery.TYPE_LUCENE);
			Query luceneQuery = luceneParser.parse(query);
			nodeQuery.setQueryOrQueryObject(luceneQuery);
			results = nodeService.search(nodeQuery);			
			log.info("entity search : " +results.getTotal()+" results in "+StringFormatUtility.getElapsedTime(startTime)+" parsed to " + luceneQuery);
		}
		if(results != null) {
			for(int i=0; i < results.getIds().size(); i++) {
				long nodeId = results.getIds().get(i);
				Entity entity = entityService.getEntity(nodeId);
				entities.getResults().add(entity);
			}
		}
		entities.setResultSize(results.getTotal());
		entities.setStartRow(start);
		entities.setEndRow(end);
		return entities;
	}
	
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public String getHomeDirectory() {
		return homeDirectory;
	}
	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}
	public boolean isLoggingOn() {
		return loggingOn;
	}
	public void setLoggingOn(boolean loggingOn) {
		this.loggingOn = loggingOn;
	}	
}
