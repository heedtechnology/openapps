package org.heed.openapps.entity.search;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.queryParser.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.Property;


public class LuceneEntityQueryParser {
	private QueryParser parser = new QueryParser(Version.LUCENE_33, "name", new StandardAnalyzer(Version.LUCENE_33));
	
	public Query parse(EntityQuery entityQuery) throws ParseException {
		BooleanQuery query = new BooleanQuery();
		if(entityQuery.getDefaultOperator().equals("AND")) parser.setDefaultOperator(Operator.AND);
		else parser.setDefaultOperator(Operator.OR);
		query.add(new BooleanClause(new TermQuery(new Term("qname", entityQuery.getEntityQnames()[0].toString())), Occur.MUST));
		if(entityQuery.getUser() > 0) {
			BooleanQuery userQuery = new BooleanQuery();			
			userQuery.add(new BooleanClause(new TermQuery(new Term("target_assoc", String.valueOf(entityQuery.getUser()))), Occur.SHOULD));
			userQuery.add(new BooleanClause(new TermQuery(new Term("user", String.valueOf(entityQuery.getUser()))), Occur.SHOULD));
			query.add(userQuery, Occur.MUST);
		}
		if(entityQuery.getXid() > 0) {
			query.add(new TermQuery(new Term("xid", String.valueOf(entityQuery.getXid()))), Occur.MUST);
		}
		if(entityQuery.getQueryString() != null && entityQuery.getQueryString().length() > 0) {
			if(entityQuery.getFields().isEmpty()) {
				Query q = parser.parse(entityQuery.getQueryString());
				query.add(q, Occur.MUST);
				return query;
			} else {
				String[] chunks = entityQuery.getQueryString().split(" ");
				if(chunks.length == 1 && entityQuery.getFields().size() == 1) {
					if(entityQuery.getFields().get(0).equals("uuid")) return new TermQuery(new Term(entityQuery.getFields().get(0),chunks[0]));
					String name = entityQuery.getFields().get(0);				
					Query q = null;
					if(name.equals("name")) 
						q = new PrefixQuery(new Term(name,chunks[0].toLowerCase()));
					else
						q = new TermQuery(new Term(name,chunks[0].toLowerCase()));
					query.add(q, Occur.MUST);
					return query;
				} else {
					for(int i=0; i < chunks.length; i++) {
						for(String field : entityQuery.getFields()) {
							if(entityQuery.getDefaultOperator().equals("AND")) query.add(new TermQuery(new Term(field,chunks[i].toLowerCase())), Occur.MUST);
							else query.add(new TermQuery(new Term(field,chunks[i].toLowerCase())), Occur.SHOULD);
						}
					}
				}
			}
		}
		if(!entityQuery.getProperties().isEmpty()) {	
			BooleanQuery q = new BooleanQuery();
			for(Property prop : entityQuery.getProperties()) {
				if(entityQuery.getDefaultOperator().equals("AND")) 
					q.add(new TermQuery(new Term(prop.getQName().toString(), (String)prop.getValue())), Occur.MUST);
				else
					q.add(new TermQuery(new Term(prop.getQName().toString(), (String)prop.getValue())), Occur.SHOULD);
			}
			query.add(q, Occur.MUST);
		} 
		if(entityQuery.getUser() == 0 && entityQuery.getXid() == 0 && entityQuery.getProperties().isEmpty() && (entityQuery.getQueryString() == null || entityQuery.getQueryString().length() == 0)) 
			query.add(new MatchAllDocsQuery(), Occur.MUST);
		return query;
	}
}
