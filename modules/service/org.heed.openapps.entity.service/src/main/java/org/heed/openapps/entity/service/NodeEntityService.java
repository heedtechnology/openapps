package org.heed.openapps.entity.service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityPersistenceListener;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntitySearcher;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationField;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.entity.data.in.OAXMLImportProcessor;
import org.heed.openapps.entity.service.job.EntityCleaningJob;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.Node;
import org.heed.openapps.node.NodeException;
import org.heed.openapps.node.NodeQuery;
import org.heed.openapps.node.NodeResultset;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.node.Transaction;
import org.heed.openapps.node.TraversalQuery;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.scheduling.SchedulingService;
import org.heed.openapps.util.NumberUtility;


public class NodeEntityService implements EntityService {
	private static final long serialVersionUID = 4922262363213062391L;
	private final static Log log = LogFactory.getLog(NodeEntityService.class.getName());
	protected NodeService nodeService;
	protected DataDictionaryService dictionaryService;
	private SchedulingService schedulingService;
	private CacheService cacheService;
	
	private Map<String, List<ImportProcessor>> importers = new HashMap<String, List<ImportProcessor>>();
	private Map<String, ExportProcessor> exporters = new HashMap<String, ExportProcessor>();
	private Map<String, EntityPersistenceListener> persistenceListeners = new HashMap<String, EntityPersistenceListener>();
	private Map<String, EntitySearcher> searchers = new HashMap<String, EntitySearcher>();
		
	public static final QName OPENAPPS_DELETED = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "deleted");
	public static final QName OPENAPPS_ENTITIES = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entities");
	public static final QName OPENAPPS_ENTITY = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entity");
	
	private ImportProcessor defaultImportProcessor = new OAXMLImportProcessor("default", "OpenApps XML");
	//private AssociationSorter associationSorter = new AssociationSorter(new Sort(Sort.STRING, "openapps_org_system_1_0_name", false));
	
	private IndexingWorkQueue indexingQueue;
	
	public void initialize() {

	}
	public EntityResultSet search(EntityQuery query) {
		EntitySearcher searcher = query.getEntityQnames()[0] != null ? searchers.get(query.getEntityQnames()[0].toString()) : null;
		if(searcher == null) searcher = searchers.get("default");
		if(searcher != null) {
			try {
				return searcher.search(query);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return new EntityResultSet();
	}
	public Entity getEntity(String uid) throws InvalidEntityException {
		try {
			Long id = nodeService.getNodeId(uid);
			if(id != null) return getEntity(id);
			else return null;//throw new InvalidEntityException("entity not found:"+uid);
		} catch(NodeException e) {
			throw new InvalidEntityException("", e);
		}
	}
	public Entity getEntity(QName entityQname, String uid) throws InvalidEntityException {
		return getEntity(uid);
	}
	
	public Entity getEntity(Entity entity) throws InvalidEntityException {
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			Model model = dictionary.getModel(entity.getQName());
			if(model != null) {
				String query = "";
				List<ModelField> uniqueFields = model.getUniqueFields(true);
				for(ModelField field : uniqueFields) {
					query += field.getQName().getLocalName()+":"+entity.getPropertyValue(field.getQName())+" AND ";
				}
				query = query.substring(0, query.length()-5);
				log.info("searching for entity : "+query);
				EntityQuery entityQuery = new EntityQuery(entity.getQName(), query);
				EntityResultSet entities = search(entityQuery);
				if(entities != null && entities.getResultSize() > 0) {
					return entities.getResults().get(0);
				}
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Entity getEntity(long nodeId) throws InvalidEntityException {
		Object entityObj = cacheService.get("entityCache", String.valueOf(nodeId));
		if(entityObj != null && entityObj instanceof Entity) return (Entity)entityObj;
		Entity entity = null;
		try {
			QName qname = nodeService.getNodeQName(nodeId);
			if(qname != null) {
				EntityPersistenceListener listener = persistenceListeners.get(qname.toString());		
				if(listener != null) {
					entity = listener.extractEntity(nodeId, qname);
				}
				Node node = nodeService.getNode(nodeId);
				if(entity == null) entity = new Entity(node);
				try {
					DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
					for(ModelField field : dictionary.getModelFields(qname)) {
						try {
							Object value = nodeService.hasNodeProperty(nodeId, field.getQName().toString()) ? nodeService.getNodeProperty(nodeId, field.getQName().toString()) : "";
							Property property = new Property(field.getQName());
							if(field.getType() == ModelField.TYPE_TEXT) {
								property.setType(Property.STRING);
								property.setValue(value);
							} else if(field.getType() == ModelField.TYPE_LONGTEXT) {
								property.setType(Property.LONGTEXT);
								property.setValue(value);
							} else if(field.getType() == ModelField.TYPE_DATE) {
								property.setType(Property.DATE);
								property.setValue(value);
							}
							else if(field.getType() == ModelField.TYPE_INTEGER) {
								property.setType(Property.INTEGER);
								if(value instanceof Integer) property.setValue(value);
								else if(value instanceof String && !value.equals(""))
									property.setValue(Integer.valueOf((String)value));
							}
							else if(field.getType() == ModelField.TYPE_LONG) {
								property.setType(Property.LONG);
								if(value instanceof Long) property.setValue(value);
								else if(value instanceof String && !value.equals(""))
									property.setValue(Long.valueOf((String)value));
							}
							else if(field.getType() == ModelField.TYPE_BOOLEAN) {
								property.setType(Property.BOOLEAN);
								if(value instanceof Boolean) property.setValue(value);
								else if(value instanceof String && !value.equals("")) 
									property.setValue(Boolean.valueOf((String)value));
							} 
							entity.getProperties().add(property);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				} catch(Exception e) {
					//log.error("problem locating model fields for qname:"+qname);
				}
				for(Relationship rel : nodeService.getRelationships(nodeId, Direction.OUTGOING)) {
					try {
						Association assoc = getAssociation(rel);
						if(assoc != null) {
							entity.getSourceAssociations().add(assoc);
						}
					} catch(InvalidAssociationException e) {
						throw new InvalidEntityException(e.getMessage());
					}
				}		
				for(Relationship rel : nodeService.getRelationships(nodeId, Direction.INCOMING)) {
					try {
						Association assoc = getAssociation(rel);
						if(assoc != null) {
							entity.getTargetAssociations().add(assoc);					
						}
					} catch(InvalidAssociationException e) {
						throw new InvalidEntityException(e.getMessage());
					}
				}				
				cacheService.put("entityCache", String.valueOf(entity.getId()), entity);
			} else {
				log.error("error fetching node with no qname, id:" + nodeId);
			}
		} catch(Exception e) {
			cacheService.remove("entityCache", String.valueOf(nodeId));
			throw new InvalidEntityException("", e);
		}
		return entity;
	}
	public Entity getEntity(HttpServletRequest request, QName entityQname) throws InvalidEntityException {
		Entity entity = null;
		EntityPersistenceListener listener = persistenceListeners.get(entityQname.toString());
		if(listener == null) {
			listener = persistenceListeners.get("default");
		}
		if(listener != null) {
			entity = listener.extractEntity(request, entityQname);
		}		
		return entity;
	}
	@Override
	public void addEntity(Entity entity) throws InvalidEntityException {
		try	{
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			if(entity.getQName() == null) throw new InvalidEntityException("entity model not found for missing qname");
			EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
			if(listener == null) listener = persistenceListeners.get("default");
			if(listener != null) listener.onBeforeAdd(entity);
			long nodeId = (entity.getUuid() != null && entity.getUuid().length() > 0) ? nodeService.createNode(entity.getUuid()) : nodeService.createNode(null);			
			if(nodeId > 0) {
				entity.setId(nodeId);
				if(entity.getUuid() == null || entity.getUuid().length() == 0)
					entity.setUuid(java.util.UUID.randomUUID().toString());
				nodeService.addUpdateNode(entity.getNode());			
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(Property property : entity.getProperties()) {
					ModelField field = null;
					for(ModelField f : fields) {
						if(f.getQName().equals(property.getQName())) {
							field = f;
							break;
						}
					}						
					if(field != null) {							
						if(property.getValue() != null) {
							nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
						}
					} else if(property.getType() == Property.COMPUTED) {
						nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
					} else 
						log.info("Model field not found for : " + property.getQName().toString());
				}
				for(Association assoc : entity.getSourceAssociations()) {
					if(assoc.getId() == null) {
						assoc.setSourceEntity(entity);
						addAssociation(assoc);
					}
				}
			}
			if(listener != null) {
				listener.onAfterAdd(entity);
				listener.index(entity.getId());
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new InvalidEntityException("entity model not found for qname:"+entity.getQName().toString(), e);			
		} 		
	}
	@Override
	public Long addEntity(Long source, Long target, QName association, List<Property> properties, Entity entity) throws InvalidEntityException {
		try	{
			addEntity(entity);
			Association assoc = null;
			if(source != null) assoc = getAssociation(association, source, entity.getId());
			else if(target != null) assoc = getAssociation(association, entity.getId(), target);
			if(assoc != null) {
				if(properties != null) {
					for(Property property : properties) {
						assoc.getProperties().add(property);
					}
				}
				addAssociation(assoc);
				return assoc.getId();
			} else {
				throw new InvalidEntityException("invalid association source:"+source+" target:"+target);
			}			
		} catch(InvalidAssociationException e1) {
			e1.printStackTrace();
		} 
		return null;
	}
	public void addEntities(Collection<Entity> entities) throws InvalidEntityException {
		Map<String,Long> idMap = new HashMap<String,Long>();
		try	{
			int i = 0;
			for(Entity entity : entities) {
				addEntity(entity);
				idMap.put(entity.getUuid(), entity.getId());
				i++;
				if(i % 10 == 0) log.info(i+" of "+entities.size()+" nodes migrated");
			}
			i = 0;
			for(Entity entity : entities) {	    		
				for(Association assoc : entity.getSourceAssociations()) {		    				
					try	{
						//String qname = assoc.getQName().toString();
						Long sourceid = entity.getId();
						Long targetid = assoc.getTarget();
						if(targetid == null) targetid = idMap.get(assoc.getTargetUid());
						if(sourceid != null && targetid != null) {
							assoc.setSource(sourceid);
							assoc.setTarget(targetid);
							addAssociation(assoc);
						} else {
							log.info("bad news on id lookup:"+sourceid+" to "+targetid);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				for(Association assoc : entity.getTargetAssociations()) {		    				
					try	{
						//String qname = assoc.getQName().toString();
						Long sourceid = assoc.getSource();
						Long targetid = entity.getId();
						if(sourceid == null) sourceid = idMap.get(assoc.getSourceUid());
						if(sourceid != null && targetid != null) {
							assoc.setSource(sourceid);
							assoc.setTarget(targetid);
							addAssociation(assoc);
						} else {
							log.info("bad news on id lookup:"+sourceid+" to "+targetid);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				i++;
				if(i % 10 == 0) log.info(i+" of "+entities.size()+" nodes relationships migrated");
			}
		} catch(InvalidEntityException e) {
			throw new InvalidEntityException("", e);
		} 
	}	
	public void removeEntity(long id) throws InvalidEntityException {
		cacheService.remove("entityCache", String.valueOf(id));
		Transaction tx = nodeService.getTransaction();
		try	{
			/** Cascade the delete to entities marked so in the data dictionary 
			 *  regardless, delete them from the cache so they are reloaded on next request. **/
			Entity entity = getEntity(id);
			if(entity != null) {
				DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
				if(dictionary != null) {
					EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
					if(listener != null) listener.onBeforeDelete(entity);
					
					List<QName> cascades = new ArrayList<QName>();
					Model model = dictionary.getModel(entity.getQName());
					while(model != null) {						
						for(ModelRelation relation : model.getSourceRelations()) {
							if(relation.isCascade()) {
								cascades.add(relation.getQName());
							}
						}
						model = model.getParent();
					}
					
					if(cascades.size() > 0) {
						TraversalQuery query = new TraversalQuery(TraversalQuery.ORDER_BREADTH_FIRST, TraversalQuery.DIRECTION_OUT, 1);
						query.setDirection(TraversalQuery.DIRECTION_OUT);
						for(QName cascade : cascades) {
							query.getQNames().add(cascade);
						}
						NodeQuery nodeQuery = new NodeQuery(NodeQuery.TYPE_NATIVE);
						nodeQuery.setQueryOrQueryObject(query);
						nodeQuery.setNodeId(id);
						NodeResultset result = nodeService.search(nodeQuery);
						List<Long> processedIds = new ArrayList<Long>();
						for(long resultId : result.getIds()) {
							if(resultId > 0) {
								for(Association assoc : entity.getSourceAssociations()) {
									if(assoc.getTarget() == resultId) {
										if(resultId != id && !processedIds.contains(resultId)) {
											removeEntity(resultId);
											processedIds.add(resultId);
											cacheService.remove("entityCache", String.valueOf(resultId));
										}
									}
								}								
							}
						}
					}
					for(Association assoc : entity.getTargetAssociations()) {
						removeAssociation(assoc.getId());
					}
					nodeService.removeNode(id);
					log.info("removing node:"+entity.getId());
					if(listener != null) listener.onAfterDelete(entity);
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidEntityException("", e);
		} finally {
			tx.finish();
        }		
	}
	
	public void cascadeProperty(Long id, QName association, QName propertyName, Serializable propertyValue) throws InvalidEntityException, InvalidPropertyException {
		Transaction tx = nodeService.getTransaction();
		try	{
			Entity entity = getEntity(id);
			List<Association> associations = entity.getSourceAssociations(association);
			for(Association assoc : associations) {
				Entity targetEntity = getEntity(assoc.getTarget());
				targetEntity.addProperty(propertyName, propertyValue);
				updateEntity(targetEntity);
				cascadeProperty(targetEntity.getId(), association, propertyName, propertyValue);
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidEntityException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void updateEntity(Entity entity) throws InvalidEntityException {
		//log.info("received entity update id:"+entity.getId());
		Transaction tx = nodeService.getTransaction();
		try	{
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
			if(listener == null) listener = persistenceListeners.get("default");
			if(listener != null) {
				Entity oldValue = getEntity(entity.getId());
				listener.onBeforeUpdate(oldValue, entity);
			}
			if(entity.getId() != null) {				
				nodeService.addUpdateNode(entity.getNode());
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(ModelField f : fields) {
					Property property = entity.getProperty(f.getQName());
					if(property == null)
						nodeService.removeNodeProperty(entity.getId(), f.getQName().toString());
				}
				for(Property property : entity.getProperties()) {
					ModelField field = null;
					for(ModelField f : fields) {
						if(f.getQName().equals(property.getQName())) {
							field = f;
							break;
						}
					}						
					if(field != null) {							
						if(property.getValue() != null && !property.getValue().equals("")) {
							nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
						} else nodeService.removeNodeProperty(entity.getId(), property.getQName().toString());
					} else if(property.getType() == Property.COMPUTED) {
						nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
					} else 
						log.info("Model field not found for : "+property.getQName().toString());
				}
				for(Association assoc : entity.getSourceAssociations()) {
					if(assoc.getId() == null) {
						assoc.setSourceEntity(entity);
						addAssociation(assoc);
					}
				}
				cacheService.remove("entityCache", String.valueOf(entity.getId()));
			}
			if(listener != null) {
				listener.onAfterUpdate(entity);
				listener.index(entity.getId());
			}			
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }	
	}
	public Association getAssociation(long id) throws InvalidAssociationException {
		try {
			Relationship node = nodeService.getRelationship(id);
			return getAssociation(node);
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}
	public Association getAssociation(QName qname, long source, long target) throws InvalidAssociationException {
		try {
			Association assoc = new Association(qname, source, target);
			assoc.setSourceName(nodeService.getNodeQName(source));
			assoc.setTargetName(nodeService.getNodeQName(target));
			return assoc;
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}
	
	protected Association getAssociation(Relationship rel) throws InvalidAssociationException {
		Association assoc = null;
		QName relQname =null;
		try {
			relQname = rel.getQName();
			if(!relQname.equals(OPENAPPS_ENTITY)) {
				assoc = new Association(relQname, rel.getStartNode(), rel.getEndNode());
				assoc.setId(rel.getId());
				assoc.setSourceName(nodeService.getNodeQName(rel.getStartNode()));
				assoc.setTargetName(nodeService.getNodeQName(rel.getEndNode()));
				Map<String,Object> properties = nodeService.getRelationshipProperties(rel.getId());
				for(String key : properties.keySet()) {
					assoc.addProperty(QName.createQualifiedName(key), properties.get(key));
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException(e.getMessage());
		}
		return assoc;
	}
	public Association getAssociation(HttpServletRequest request, QName associationQname) throws InvalidAssociationException {
		Association entity = null;
		String id = request.getParameter("id");
		String source = request.getParameter("source");
		String target = request.getParameter("target");
		if(id != null && id.length() > 0) {
			entity = getAssociation(Long.valueOf(id));
		} else {			
			if(source == null) throw new InvalidAssociationException("association source is null for qname:"+associationQname.toString());
			if(target == null) throw new InvalidAssociationException("association target is null for qname:"+associationQname.toString());
			try {
				entity = new Association(associationQname);
				if(NumberUtility.isLong(source)) entity.setSource(Long.valueOf(source)); 
				else entity.setSourceUid(source);
				if(NumberUtility.isLong(target)) entity.setTarget(Long.valueOf(target)); 
				else entity.setTargetUid(target);
			} catch(Exception e) {
				throw new InvalidAssociationException("invalid entities source:"+source+" target:"+target, e);
			}
		}
		if(entity == null) throw new InvalidAssociationException("association cannot be found for qname:"+associationQname.toString());
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			QName entityQname = nodeService.getNodeQName(Long.valueOf(source));
			ModelRelation model = dictionary.getModelRelation(entityQname, associationQname);
			if(model == null) throw new InvalidAssociationException("entity relation not found for qname:"+associationQname.toString());
			for(ModelField field : model.getFields()) {
				String value = request.getParameter(field.getQName().toString());
				if(value == null) {
					value = field.getQName().getLocalName().equals("function") ? 
						request.getParameter("_"+field.getQName().getLocalName()) : 
						request.getParameter(field.getQName().getLocalName());
				}
				if(value != null && !value.equals("null")) {
					/*
					if(field.getFormat() == ModelField.FORMAT_PASSWORD) {
						SimpleHash simpleHash = new SimpleHash(hash, value);
						value = simpleHash.toHex();
					}
					*/
					try {
						entity.addProperty(field.getQName(), value);
					} catch(Exception e) {
						e.toString();
					}
				} else {
					if(entity.hasProperty(field.getQName())) entity.removeProperty(field.getQName());
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
		return entity;
	}
	@Override
	public void addAssociation(Association association)	throws InvalidAssociationException {
		if((association.getSource() == null && association.getSourceEntity() == null) || (association.getTarget() == null && association.getTargetEntity() == null))
			return;
		cacheService.remove("entityCache", String.valueOf(association.getSource()));
		cacheService.remove("entityCache", String.valueOf(association.getTarget()));
		Transaction tx = nodeService.getTransaction();
		try	{
			association.setSourceName(nodeService.getNodeQName(association.getSource()));
			association.setTargetName(nodeService.getNodeQName(association.getTarget()));
			
			EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
			if(startListener != null) startListener.onBeforeAssociationAdd(association);
			EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
			if(endListener != null) endListener.onBeforeAssociationAdd(association);
			
			Relationship relationship = nodeService.createRelationship(association.getQName(), association.getSource(), association.getTarget());
			for(Property prop : association.getProperties()) {
				nodeService.addUpdateRelationshipProperty(relationship.getId(), prop.getQName().toString(), prop.getValue());
			}
			association.setId(relationship.getId());
			
			if(startListener != null) {
				startListener.onAfterAssociationAdd(association);
				startListener.index(association.getSource());
			}
			if(endListener != null) {
				endListener.onAfterAssociationAdd(association);
				endListener.index(association.getTarget());
			}
			
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void updateAssociation(Association association) throws InvalidAssociationException {
		cacheService.remove("entityCache", String.valueOf(association.getSource()));
		cacheService.remove("entityCache", String.valueOf(association.getTarget()));
		Transaction tx = nodeService.getTransaction();
		try	{
			//Node startNode = association.getSource() != null ? nodeService.getNode(association.getSource()) : nodeService.getNode(association.getSourceEntity().getId());
			//Node endNode = association.getTarget() != null ? nodeService.getNode(association.getTarget()) : nodeService.getNode(association.getTargetEntity().getId());
			Entity sourceEntity = getEntity(association.getSource());
			Entity targetEntity = getEntity(association.getTarget());
			
			association.setSourceName(sourceEntity.getQName());
			association.setTargetName(targetEntity.getQName());
						
			EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
			if(startListener != null) startListener.onBeforeAssociationUpdate(association);
			EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
			if(endListener != null) endListener.onBeforeAssociationUpdate(association);
			
			removeAssociation(association.getId());
			addAssociation(association);
			
			if(startListener != null) {
				startListener.onAfterAssociationUpdate(association);
				startListener.index(association.getSource());
			}
			if(endListener != null) {
				endListener.onAfterAssociationUpdate(association);
				endListener.index(association.getTarget());
			}
			
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	public void removeAssociation(long id) throws InvalidAssociationException {
		Transaction tx = nodeService.getTransaction();
		try	{
			Association association = getAssociation(id);
			cacheService.remove("entityCache", String.valueOf(association.getSource()));
			cacheService.remove("entityCache", String.valueOf(association.getTarget()));
			association.setSourceName(nodeService.getNodeQName(association.getSource()));
			association.setTargetName(nodeService.getNodeQName(association.getTarget()));
			
			EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
			if(startListener != null) startListener.onBeforeAssociationDelete(association);
			EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
			if(endListener != null) endListener.onBeforeAssociationDelete(association);
			
			nodeService.removeRelationship(id);
			
			if(startListener != null) startListener.onAfterAssociationDelete(association);
			if(endListener != null) endListener.onAfterAssociationDelete(association);
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	public ValidationResult validate(Entity entity) {
		ValidationResult result = new ValidationResult();
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			Model model = dictionary.getModel(entity.getQName());
			if(model == null) result.setException(ValidationResult.MODEL_MISSING);
			else {
				for(ModelField field : model.getFields()) {
					if(field.getQName() != null && field.getQName().getNamespace() != null && field.getQName().getLocalName() != null) {
						Property property = entity.getProperty(field.getQName().getNamespace(), field.getQName().getLocalName());
						if(property == null) property = entity.getProperty("", field.getQName().getLocalName());
						if(property != null) {
							if(property.getQName().getNamespace() == null) property.getQName().setNamespace(field.getQName().getNamespace());
							if(property.toString().length() < field.getMinSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MINIMUM_VALUE_RESTRICTION));
							else if(field.getMaxSize() > 0 && property.toString().length() > field.getMaxSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MAXIMUM_VALUE_RESTRICTION));
						} else if(field.isMandatory()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.REQUIRED_VALUE_MISSING));
					}
				}
			}
		} catch(Exception e) {
			result.setValid(false);
			result.setMessage(e.getLocalizedMessage());
			log.error(result.toString());
		}
		return result;
	}
	public ValidationResult validate(Association association) throws ModelValidationException {
		DataDictionary dictionary = dictionaryService.getDataDictionary(association.getDictionary());
		ValidationResult result = new ValidationResult(true);
		if(association.getSource() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("source", ValidationResult.MISSING_VALUE));
		}
		if(association.getTarget() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("target", ValidationResult.MISSING_VALUE));
		}
		if(result.isValid()) {
			try {
				Model model = dictionary.getModel(association.getSourceName());
				QName endQname = nodeService.getNodeQName(association.getTarget());
				if(model != null) {
					if(association.getQName() == null) {				
						for(ModelRelation rel : model.getRelations()) {
							if(rel.getEndName().equals(endQname)) {
								association.setQname(rel.getQName());
								result.setValid(true);
								return result;
							}
						}
					} else {
						Model m = model;
						while(m != null) {
							if(m.containsRelation(association.getQName())) {
								result.setValid(true);
								return result;
							}
							m = dictionary.getModel(m.getParentName());
						}
					}
					result.setValid(false);
					result.setMessage("Association of type "+association.getQName()+" not found.");
				}
			} catch(Exception e) {
				throw new ModelValidationException(result);
			}
		}
		return result;
	}
	
	public void registerImportProcessor(String name, ImportProcessor processor) {
		List<ImportProcessor> list = importers.get(name);
		if(list == null) {
			list = new ArrayList<ImportProcessor>();
			importers.put(name, list);
		}
		list.add(processor);
	}
	public void registerExportProcessor(String name, ExportProcessor processor) {
		exporters.put(name, processor);
	}
	public void registerEntityPersistenceListener(String name, EntityPersistenceListener component) {
		persistenceListeners.put(name, component);
	}
	public void registerEntitySearcher(String qname, EntitySearcher value) {
		searchers.put(qname, value);
	}
	@Override
	public List<ImportProcessor> getImportProcessors(String name) {
		List<ImportProcessor> processors = importers.get(name);
		if(processors == null) processors = new ArrayList<ImportProcessor>();
		if(!processors.contains(defaultImportProcessor)) processors.add(defaultImportProcessor);
		return processors;
	}
	@Override
	public ExportProcessor getExportProcessor(String name) {
		ExportProcessor processor = exporters.get(name);
		if(processor != null) return processor;
		else return exporters.get("default");
	}
	@Override
	public EntityPersistenceListener getEntityPersistenceListener(String name) {
		return persistenceListeners.get(name);
	}	
	public Job cleanEntities(QName qname) throws InvalidQualifiedNameException {
		EntityCleaningJob job = new EntityCleaningJob(nodeService, qname);
		schedulingService.run(job);
		return job;
	}
	public void setImporters(Map<String, List<ImportProcessor>> importers) {
		this.importers = importers;
	}
	public void addExporter(String name, ExportProcessor exporter) {
		exporters.put(name, exporter);
	}
	public void setExporters(Map<String, ExportProcessor> exporters) {
		this.exporters = exporters;
	}
	public void addPersistenceListener(String name, EntityPersistenceListener listener) {
		persistenceListeners.put(name, listener);
	}
	public void setPersistenceListeners(Map<String, EntityPersistenceListener> persistenceListeners) {
		this.persistenceListeners = persistenceListeners;
	}
	public void addSearcher(String name, EntitySearcher searcher) {
		searchers.put(name, searcher);
	}
	public void setSearchers(Map<String, EntitySearcher> searchers) {
		this.searchers = searchers;
	}
	public void setSchedulingService(SchedulingService schedulingService) {
		this.schedulingService = schedulingService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	@Override
	public Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException {
		ExportProcessor defaultProcessor = getExportProcessor("default");
		ExportProcessor processor = getExportProcessor(entity.getQName().toString());		
		if(processor != null) {
			return processor.export(instructions, entity);
		} else {
			return defaultProcessor.export(instructions, entity);
		}
	}
	@Override
	public Object export(FormatInstructions instructions, Association association)	throws InvalidEntityException {
		ExportProcessor defaultProcessor = getExportProcessor("default");
		ExportProcessor processor = getExportProcessor(association.getTargetName().toString());
		if(processor != null) {
			return processor.export(instructions, association);
		} else {
			return defaultProcessor.export(instructions, association);
		}
	}
	@Override
	public void index(QName qname) {
		List<Model> models = dictionaryService.getSystemDictionary().getChildModels(qname);
		Model rootModel = dictionaryService.getSystemDictionary().getModel(qname);
		models.add(rootModel);		
		for(Model model : models) {
			try {
				EntityPersistenceListener listener = persistenceListeners.get(model.getQName().toString());
				if(listener == null) listener = persistenceListeners.get("default");
				if(listener != null) {
					listener.index(model.getQName());
				}				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void index(Long entity) {
		indexingQueue.add(entity);
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public void setIndexingQueue(IndexingWorkQueue indexingQueue) {
		this.indexingQueue = indexingQueue;
	}
		
}
