package org.heed.openapps.entity.service;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.SystemModel;
import org.heed.openapps.node.Node;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.util.HTMLUtility;


public class IndexingWorkQueue {
	private final static Log log = LogFactory.getLog(IndexingWorkQueue.class.getName());
	private NodeService nodeService;
	private boolean silent;
	private PoolWorker[] threads;
    private LinkedList<Long> queue;
    private int lastReport = 0;
    
    public void add(Long entity) {
        synchronized(queue) {
        	if(!queue.contains(entity)) {
        		queue.addLast(entity);            	
        	}
        	queue.notify();
        }
    }
    public void add(List<Long> entities) {
        synchronized(queue) {
        	queue.addAll(entities);
        	queue.notifyAll();
        }
    }
    public int size() {
    	return queue.size();
    }
    public void setThreadCount(int nThreads) {
    	queue = new LinkedList<Long>();
        threads = new PoolWorker[nThreads];
        for (int i=0; i<nThreads; i++) {
            threads[i] = new PoolWorker();
            threads[i].start();
        }
    }
    private class PoolWorker extends Thread {
        public void run() {
        	Long entityId;
            while(true) {
                synchronized(queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException ignored) {}
                    }
                    entityId = queue.removeFirst();
                    int size = queue.size();
                    if(!silent && size % 500 == 0) {
            			if(lastReport != size) {
            				log.info("equity indexing queue : "+size+" waiting tasks...");
            				lastReport = size;
            			}
            		}
                }
                try {
                	Node node = nodeService.getNode(entityId);
                	if(node != null) {
                		if(node.getQName() != null) {                			
                			Map<String,Object> properties = nodeService.getNodeProperties(entityId);
                			properties.put("xid", node.getXid());
                			/* Index All Properties **/
                			for(String key : properties.keySet()) {
                				if(key.equals(SystemModel.NAME.toString())) {
                					String value = (String)properties.get(key);
                					if(value != null) {
                						value = HTMLUtility.removeTags(value.replace(",", ""));
                						properties.put(key, value);    
                					}
                				}
                			}
                			nodeService.indexNode(node.getId(), properties);
                		}
                	}
        	    } catch(Exception e) {
        	    	e.printStackTrace();
        	    } 
            }
        }
    }
	public boolean isSilent() {
		return silent;
	}
	public void setSilent(boolean silent) {
		this.silent = silent;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
    
}
