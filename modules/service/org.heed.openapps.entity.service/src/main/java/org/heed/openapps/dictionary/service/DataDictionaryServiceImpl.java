package org.heed.openapps.dictionary.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.DictionaryModel;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryException;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.DataDictionarySorter;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelFieldSorter;
import org.heed.openapps.dictionary.ModelFieldValue;
import org.heed.openapps.dictionary.ModelObject;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.dictionary.ModelSorter;
import org.heed.openapps.dictionary.service.util.ModelUtility;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.node.NodeException;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.TraversalQuery;
import org.heed.openapps.property.PropertyService;


public class DataDictionaryServiceImpl implements DataDictionaryService {
	private static final long serialVersionUID = -8594531361920252635L;
	private final static Log log = LogFactory.getLog(DataDictionaryServiceImpl.class.getName());
	private PropertyService propertyService;
	private NodeService nodeService;
	protected EntityService entityService;
	private ModelUtility modelUtility;
	
	private DataDictionary systemDictionary = new DataDictionary("System Dictionary");
	private Map<String,DataDictionary> baseDictionaries = new HashMap<String,DataDictionary>();	
	private Map<Long,DataDictionary> userDictionaries = new HashMap<Long,DataDictionary>();	
		
	private List<String> systemImports = new ArrayList<String>();
	private List<String> baseImports = new ArrayList<String>();
	
	protected ModelSorter modelSorter = new ModelSorter();
	protected ModelFieldSorter modelFieldSorter = new ModelFieldSorter();
	protected DataDictionarySorter  dataDictionarySorter = new  DataDictionarySorter();
	
	public void initialize() {			
		try	{
			//system dictionary
			systemDictionary.setId(0L);
			systemDictionary.setUid(java.util.UUID.randomUUID().toString());
			List<Model> systemModels = processImports(systemImports);
			systemDictionary.setLocalModels(systemModels);
			log.info("system data dictionary loaded, "+systemModels.size()+" models loaded...");
						
			//base dictionaries
			for(String importFile : baseImports) {						
				List<Model> baseModels = processImport(importFile);
				DataDictionary dictionary = new DataDictionary();
				String name = importFile.replace(".xml", "");
				dictionary.setName(name);
				dictionary.setLocalModels(baseModels);
				baseDictionaries.put(name, dictionary);
				log.info("base data dictionary " +name+" loaded, "+baseModels.size()+" models loaded...");
			}			
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	public DataDictionary getDataDictionary(long dictionaryid) {
		if(dictionaryid == 0) return systemDictionary;
		DataDictionary dictionary = userDictionaries.get(dictionaryid);
		return dictionary;		
	}
	
	@Override
	public List<DataDictionary> getBaseDictionaries() {
		List<DataDictionary> dictionaries = new ArrayList<DataDictionary>();
		dictionaries.addAll(baseDictionaries.values());
		Collections.sort(dictionaries, dataDictionarySorter);
		return dictionaries;
	}
	public List<DataDictionary> getUserDictionaries() {
		//user dictionaries
		List<DataDictionary> dictionaries = new ArrayList<DataDictionary>();
		modelUtility = new ModelUtility(getEntityService());
		EntityQuery query = new EntityQuery(DictionaryModel.DICTIONARY);
		EntityResultSet results = entityService.search(query);
		for(Entity dictionaryEntity : results.getResults()) {
			DataDictionary dictionary = getDataDictionary(dictionaryEntity);
			userDictionaries.put(dictionary.getId(), dictionary);
			dictionaries.add(dictionary);
		}
		return dictionaries;
	}
	@Override
	public DataDictionary reloadDataDictionary(long dictionaryid) throws DataDictionaryException {
		try {
			refreshDictionary(dictionaryid);
			return userDictionaries.get(dictionaryid);
		} catch(Exception e) {
			throw new DataDictionaryException("problem reloading dictionary id:"+dictionaryid, e);
		}
	}
	@Override
	public void remove(long id) throws DataDictionaryException {
		try	{
			Entity entity = entityService.getEntity(id);
			if(entity.getQName().equals(DictionaryModel.DICTIONARY)) {
				List<Association> modelsAssoc = entity.getSourceAssociations(DictionaryModel.MODELS);
				for(Association modelAssoc : modelsAssoc) {
					Entity modelEntity = entityService.getEntity(modelAssoc.getTarget());
					removeModel(modelEntity);
				}
				entityService.removeEntity(entity.getId());
				userDictionaries.remove(entity.getId());
			} else if(entity.getQName().equals(DictionaryModel.MODEL)) {
				Association dictionaryAssoc = entity.getTargetAssociation(DictionaryModel.MODELS);
				removeModel(entity);
				refreshDictionary(dictionaryAssoc.getSource());
			} else if(entity.getQName().equals(DictionaryModel.MODEL_FIELD))	{
				Association modelAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_FIELD_VALUES);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			} else if(entity.getQName().equals(DictionaryModel.MODEL_RELATION))	{
				Association modelAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_RELATIONS);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			} else if(entity.getQName().equals(DictionaryModel.MODEL_FIELD_VALUE))	{
				Association fieldAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_FIELD_VALUES);
				Entity field = entityService.getEntity(fieldAssoc.getSource());
				Association modelAssoc = field.getTargetAssociation(DictionaryModel.MODEL_FIELDS);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			}
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 	
	}
	protected void removeModel(Entity entity) throws NodeException {
		TraversalQuery query = new TraversalQuery(TraversalQuery.ORDER_BREADTH_FIRST, TraversalQuery.DIRECTION_OUT, 1);
		query.getQNames().add(DictionaryModel.MODEL_FIELDS);
		query.getQNames().add(DictionaryModel.MODEL_FIELD_VALUES);
		query.getQNames().add(DictionaryModel.MODEL_RELATIONS);
		query.getQNames().add(DictionaryModel.MODEL_ASPECTS);							
		getNodeService().removeNode(entity.getId(), query);
	}
	@Override
	public void addUpdate(ModelObject object) {
		try	{
			if(object instanceof DataDictionary) {
				updateDataDictionary((DataDictionary)object);
			} else if(object instanceof Model) {
				updateModel((Model)object);				
			} else if(object instanceof ModelField)	{
				updateModelField((ModelField)object);
			} else if(object instanceof ModelRelation)	{
				updateModelRelation((ModelRelation)object);
			} else if(object instanceof ModelFieldValue)	{
				updateModelFieldValue((ModelFieldValue)object);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 	
	}
	
	protected List<Model> processImport(String importFile) throws Exception {
		List<Model> models = new ArrayList<Model>();
		SAXModelImporter2 importer = new SAXModelImporter2();
				
		InputStream stream = new FileInputStream(new File(getPropertyService().getPropertyValue("home.dir")+"/resources/dictionaries/" + importFile));
		importer.process(stream, new ArrayList<Model>());
		for(Model model : importer.getDictionary().getLocalModels()) {
			models.add(model);
		}
		for(Model model : models) {
			for(Model m : models) {
				for(ModelRelation relation : m.getSourceRelations()) {
					if(relation.getEndName().equals(model.getQName()))
						model.getTargetRelations().add(relation);
				}
				if(m.getParentName() != null && m.getParentName().toString().equals(model.getQName().toString())) {
					m.setParent(model);
					model.getChildren().add(m);
				}
			}
		}
		return models;
	}
	protected List<Model> processImports(List<String> imports) throws Exception {
		List<Model> models = new ArrayList<Model>();
		for(String importFile : systemImports) {						
			models.addAll(processImport(importFile));
			
		}
		return models;
	}
	protected void updateDataDictionary(DataDictionary dictionary) throws DataDictionaryException {
		try	{
			Entity dictionaryEntity = (dictionary.getId() != null && dictionary.getId() > 0) ? entityService.getEntity(dictionary.getId()) : null;
			if(dictionaryEntity == null) dictionaryEntity = new Entity(DictionaryModel.DICTIONARY);
			
			if(dictionary.getName() != null) {
				dictionaryEntity.setName(dictionary.getName());				
			}
			if(dictionary.getDescription() != null) {
				dictionaryEntity.addProperty(SystemModel.DESCRIPTION, dictionary.getDescription());
			}
			if(dictionary.getInheritance() != null) {
				dictionaryEntity.addProperty(DictionaryModel.INHERITANCE, dictionary.getInheritance());
			}
			if(dictionaryEntity.getId() != null && dictionaryEntity.getId() > 0) entityService.updateEntity(dictionaryEntity);
			else entityService.addEntity(dictionaryEntity);
			dictionary.setId(dictionaryEntity.getId());
			refreshDictionary(dictionaryEntity.getId());
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 
	}
	protected void updateModel(Model model) throws DataDictionaryException {
		try	{
			Entity modelEntity = (model.getId() != null && model.getId() > 0) ? entityService.getEntity(model.getId()) : null;
			if(modelEntity == null) {
				modelEntity = new Entity(DictionaryModel.MODEL);
			}
			if(model.getQName() != null) {
				modelEntity.addProperty(DictionaryModel.QUALIFIED_NAME, model.getQName().toString());
			}
			if(model.getName() != null) {
				modelEntity.setName(model.getName());				
			}
			if(model.getDescription() != null) {
				modelEntity.addProperty(SystemModel.DESCRIPTION, model.getDescription());
			}
			if(model.getParent() != null) {
				modelEntity.addProperty(DictionaryModel.INHERITANCE, model.getParent().getQName().toString());
			}
			Entity dictionaryEntity = entityService.getEntity(model.getDictionary().getId());
			if(modelEntity.getId() != null && modelEntity.getId() > 0) {
				entityService.updateEntity(modelEntity);
			} else {
				if(model.getDictionary() == null || model.getDictionary().getId() == 0) throw new DataDictionaryException("no dictionary associated with this model");
				if(dictionaryEntity != null) {
					entityService.addEntity(modelEntity);
					Association assoc = new Association(DictionaryModel.MODELS, dictionaryEntity.getId(), modelEntity.getId());					
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no dictionary associated with this model");
			}
			model.setId(modelEntity.getId());
			refreshDictionary(model.getDictionary().getId());
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		}  	
	}
	protected void updateModelField(ModelField field) throws DataDictionaryException {
		try	{
			Entity fieldEntity = (field.getId() != null && field.getId() > 0) ? entityService.getEntity(field.getId()) : null;
			if(fieldEntity == null) {
				fieldEntity = new Entity(DictionaryModel.MODEL_FIELD);
			}
			if(fieldEntity != null) {
				if(field.getName() != null) {
					fieldEntity.setName(field.getName());			
				}
				if(field.getDescription() != null) {
					fieldEntity.addProperty(SystemModel.DESCRIPTION, field.getDescription());
				}				
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"mandatory"), field.isMandatory());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"unique"), field.isUnique());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"min_size"), field.getMinSize());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"max_size"), field.getMaxSize());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"format"), field.getFormat());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"type"), field.getType());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"qualified_name"), field.getQName().toString());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"order"), field.getOrder());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"index"), field.getIndex());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"sort"), field.getSort());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"hidden"), field.isHidden());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"label"), field.getLabel());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"tokenized"), field.isTokenized());
								
				if(field.getModel() == null || field.getModel().getId() == 0) throw new DataDictionaryException("no model associated with this field");
				Entity modelEntity = entityService.getEntity(field.getModel().getId());
				if(fieldEntity.getId() != null && fieldEntity.getId() > 0) {
					entityService.updateEntity(fieldEntity);
				} else {
					if(modelEntity != null) {
						entityService.addEntity(fieldEntity);
						Association assoc = new Association(DictionaryModel.MODEL_FIELDS, modelEntity.getId(), fieldEntity.getId());
						entityService.addAssociation(assoc);
					} else throw new DataDictionaryException("no model associated with this field");
				}
				for(ModelFieldValue value : field.getValues()) {
					value.setField(field);
					updateModelFieldValue(value);
				}
			}
			field.setId(fieldEntity.getId());
			long dictionaryId = field.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	protected void updateModelRelation(ModelRelation relation) throws DataDictionaryException {
		try	{
			assert(relation.getStartName() != null);
			assert(relation.getEndName() != null);
			assert(relation.getQName() != null);
			Entity relationEntity = (relation.getId() != null && relation.getId() > 0) ? entityService.getEntity(relation.getId()) : null;
			if(relationEntity == null) {
				relationEntity = new Entity(DictionaryModel.MODEL_RELATION);
			}			
			if(relation.getName() != null) {
				relationEntity.setName(relation.getName());				
			}
			if(relation.getDescription() != null) {
				relationEntity.addProperty(SystemModel.DESCRIPTION, relation.getDescription());
			}			
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"qualified_name"), relation.getQName().toString());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"many"), relation.isMany());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"cascade"), relation.isCascade());
			
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"start"), relation.getStartName().toString());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"end"), relation.getEndName().toString());
			
			if(relationEntity.getId() != null && relationEntity.getId() > 0) entityService.updateEntity(relationEntity);
			else {
				if(relation.getModel() == null || relation.getModel().getId() == 0) throw new DataDictionaryException("no model associated with this relation");
				Entity modelEntity = entityService.getEntity(relation.getModel().getId());
				if(modelEntity != null) {
					entityService.addEntity(relationEntity);
					Association assoc = new Association(DictionaryModel.MODEL_RELATIONS, modelEntity.getId(), relationEntity.getId());
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no dictionary associated with this model");
				relation.setId(relationEntity.getId());
			}			
			long dictionaryId = relation.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	protected void updateModelFieldValue(ModelFieldValue value) throws DataDictionaryException {
		try	{
			Entity valueEntity = (value.getId() != null && value.getId() > 0) ? entityService.getEntity(value.getId()) : null;
			if(valueEntity == null) {
				valueEntity = new Entity(DictionaryModel.MODEL_FIELD_VALUE);
			}			
			valueEntity.setName(value.getName());
			if(value.getValue() != null)
				valueEntity.addProperty(SystemModel.VALUE, value.getValue());
			else
				valueEntity.addProperty(SystemModel.VALUE, value.getName());
				
			if(valueEntity.getId() != null && valueEntity.getId() > 0) entityService.updateEntity(valueEntity);
			else {
				if(value.getField() == null || value.getField().getId() == 0) throw new DataDictionaryException("no field associated with this value");
				Entity fieldEntity = entityService.getEntity(value.getField().getId());
				if(fieldEntity != null) {
					entityService.addEntity(valueEntity);
					Association assoc = new Association(DictionaryModel.MODEL_FIELD_VALUES, fieldEntity.getId(), valueEntity.getId());
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no field associated with this value");
			}
			value.setId(valueEntity.getId());
			ModelField field = modelUtility.getModelField(value.getField().getId());
			long dictionaryId = field.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 
	}
	
	protected DataDictionary getDataDictionary(Entity entity) {
		try {
			DataDictionary dictionary = new DataDictionary();
			dictionary.setId(entity.getId());
			dictionary.setUid(entity.getUuid());
			dictionary.setName(entity.getName());
			dictionary.setDescription(entity.getPropertyValue(SystemModel.DESCRIPTION));
			dictionary.setInheritance(entity.getPropertyValue(DictionaryModel.INHERITANCE));
			if(dictionary.getInheritance() != null && dictionary.getInheritance().length() > 0) {
				DataDictionary parent = baseDictionaries.get(dictionary.getInheritance());
				if(parent != null) dictionary.setInheritedModels(parent.getAllModels());
			}
			userDictionaries.put(dictionary.getId(), dictionary);
			List<Association> modelAssocs = entity.getSourceAssociations(DictionaryModel.MODELS);
			List<Model> list = new ArrayList<Model>();
			for(Association modelAssoc : modelAssocs) {
				Model model = modelUtility.getModel(modelAssoc.getTarget());
				model.setDictionary(dictionary);
				list.add(model);
			}
			for(Model model : list) {
				if(model.getParent() == null && model.getParentName() != null) {
					Model parentModel = dictionary.getModel(model.getParentName());
					if(parentModel != null)
						model.setParent(parentModel);
				}
			}
			dictionary.setLocalModels(list);
			return dictionary;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	protected void refreshDictionary(long id) throws InvalidEntityException {
		Entity entity = entityService.getEntity(id);
		DataDictionary dictionary = getDataDictionary(entity);
		userDictionaries.put(id, dictionary);
		log.info("refreshed " +dictionary.getName() +" dictionary");
	}
	
	protected void addUpdateModelParent(Model model) {
		try	{
			if(model.getId() > 0) {
				if(model.getParent() != null) getNodeService().addUpdateNodeProperty(model.getId(), "parent", model.getParent());
				else getNodeService().removeNodeProperty(model.getId(), "parent");
			}
			initialize();
		} catch(Exception e) {
			e.printStackTrace();
		} 	
	}
	/*
	protected void addUpdateModelFieldAspect(ModelFieldAspect aspect) throws DataDictionaryException {
		try	{
			Long node = null;
			if(aspect.getId() > 0) {
				node = aspect.getId();
			} else {				
				node = getNodeService().createNode(null);
				long fieldNode = aspect.getFieldId();
				getNodeService().addUpdateRelationship(DictionaryModel.MODEL_ASPECTS.toString(), fieldNode, node);
			}
			if(node != null) {				
				getNodeService().addUpdateNodeProperty(node, "qname", aspect.getQName().toString());
				getNodeService().addUpdateNodeProperty(node, "description", aspect.getDescription());
				//for(ModelField field : aspect.getFields()) {
					//field.setModel(node);
					//update(field);
				//}
			}
			aspect.setId(node);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	*/
	public DataDictionary getSystemDictionary() {
		return systemDictionary;
	}
	public void setSystemDictionary(DataDictionary systemDictionary) {
		this.systemDictionary = systemDictionary;
	}
	public List<String> getSystemImports() {
		return systemImports;
	}
	public void setSystemImports(List<String> systemImports) {
		this.systemImports = systemImports;
	}
	public List<String> getBaseImports() {
		return baseImports;
	}
	public void setBaseImports(List<String> baseImports) {
		this.baseImports = baseImports;
	}
	public NodeService getNodeService() {
		return nodeService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}	
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}

}
