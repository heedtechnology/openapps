package org.heed.openapps.entity.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.EntityCleaningService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.Node;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;


public class EntityCleaningServiceImpl implements EntityCleaningService {
	private final static Logger log = Logger.getLogger(EntityCleaningServiceImpl.class.getName());
	private NodeService nodeService;
	private EntityService entityService;
	private NodeIdComparator comparator = new NodeIdComparator();
	
	public static final QName OPENAPPS_ENTITY = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entity");
	
	
	@Override
	public void merge(Long[] ids, int mergeType) {
		log.info("cleaning started...");
		List<Node> targets = new ArrayList<Node>();
		try {
			for(Long id : ids) {
				targets.add(nodeService.getNode(id));
			}
			Collections.sort(targets, comparator);
			if(mergeType == EntityCleaningService.MERGE_LAST_TO_FIRST) {
				Collections.reverse(targets);
			}
			for(int i=0; i < targets.size()-1; i++) {
				Node node1 = targets.get(i);
				Node node2 = targets.get(i+1);
				log.info("merging node1:"+node1.getId()+" with node2:"+node2.getId());
				Map<String, Object> properties1 = nodeService.getNodeProperties(node1.getId());
				Map<String, Object> properties2 = nodeService.getNodeProperties(node2.getId());
				for(String key1 : properties1.keySet()) {
					Object p1 = properties1.get(key1);
					Object p2 = properties2.get(key1);
					if(!p1.equals(p2)) {
						if(!isEmpty(p1) && isEmpty(p2)) {
							nodeService.addUpdateNodeProperty(node2.getId(), key1, p1);
							log.info("adding property:"+key1+" value:"+p1);
						} 									
					}
				} 
				List<Relationship> relationships = nodeService.getRelationships(node1.getId(), Direction.BOTH);
				for(Relationship rel1 : relationships) {
					boolean exists = false;
					if(!rel1.getQName().equals(OPENAPPS_ENTITY)) {
						List<Relationship> relationships2 = nodeService.getRelationships(node2.getId(), Direction.BOTH);
						for(Relationship rel2 : relationships2) {
							if(rel1.getQName().equals(rel2.getQName()) && 
								rel1.getStartNode() == rel2.getStartNode() &&
								rel1.getEndNode() == rel2.getEndNode()) {
								exists = true;
							}
						}
					}
					if(!exists) {
						if(rel1.getStartNode() == node1.getId())
							nodeService.createRelationship(rel1.getQName(), node2.getId(), rel1.getEndNode());
						else
							nodeService.createRelationship(rel1.getQName(), node2.getId(), rel1.getStartNode());
					}
				}
				log.info("deleting entity:"+node1.getId());
				entityService.removeEntity(node1.getId());
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	protected boolean isEmpty(Object value) {
		return (value == null || String.valueOf(value).length() == 0);
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	
	public class NodeIdComparator implements Comparator<Node> {
		@Override
		public int compare(Node node1, Node node2) {
			if(node1.getId() > node2.getId()) return 1;
			else if(node1.getId() < node2.getId()) return -1;
			return 0;
		}		
	}
}
