package org.heed.openapps.entity.service;

import org.heed.openapps.entity.EntityService;


public class EntityBootstrapService {
	private EntityService entityService;
	
	
	public void initialize() {
		
	}
	
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	
}
