package org.heed.openapps.search.indexing;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.search.parsing.date.DateParser;


public abstract class EntityIndexerSupport implements EntityIndexer {
	private static final Log log = LogFactory.getLog(EntityIndexerSupport.class);
	private NodeService nodeService;
	private DataDictionaryService dictionaryService;
	private EntityService entityService;
	
	//private SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM, yyyy");
	
	
	public IndexEntity index(Entity entity) throws InvalidEntityException {
		StringBuffer freeText = new StringBuffer();
		IndexEntity indexEntity = new IndexEntity(entity.getId());
		
		if(entity != null) {
			String id = String.valueOf(entity.getId());
			if(id != null) {
				try {
					DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
					indexEntity.getData().put("qname", new IndexEntityField("qname", entity.getQName().toString(), false));
					indexEntity.getData().put("localName", new IndexEntityField("localName", entity.getQName().getLocalName(), false));
					indexEntity.getData().put("uuid", new IndexEntityField("uuid", entity.getUuid(), false));
					
					Property name = entity.getProperty(SystemModel.NAME.toString());
					if(name != null) {
						indexEntity.getData().put("name", new IndexEntityField("name", name.getValue(), true));
						indexEntity.getData().put("name_e", new IndexEntityField("name_e", name.getValue(), false));
					} else if(entity.getName() != null) {
						indexEntity.getData().put("name", new IndexEntityField("name", entity.getName(), true));
						indexEntity.getData().put("name_e", new IndexEntityField("name_e", entity.getName(), false));
					}
				
					List<QName> qnames = dictionary.getQNames(entity.getQName());
					StringWriter writer = new StringWriter();
        			for(QName qname : qnames) {
        				writer.append(qname.toString()+" ");
        			}
        			indexEntity.getData().put("qnames", new IndexEntityField("qnames", writer.toString().trim(), true));
        								
					if(entity.getCreated() > 0) indexEntity.getData().put("created", new IndexEntityField("created", entity.getCreated(), false));
					if(entity.getModified() > 0) indexEntity.getData().put("modified", new IndexEntityField("modified", entity.getModified(), false));
					indexEntity.getData().put("deleted", new IndexEntityField("deleted", entity.getDeleted(), false));
					if(entity.getUser() > 0) indexEntity.getData().put("user", new IndexEntityField("user", entity.getUser(), false));
					for(Property property : entity.getProperties()) {
						Object propertyValue = property.getValue();
						if(propertyValue != null) {
							if(propertyValue instanceof String) {
								String textVal = (String)propertyValue;
								if(textVal.length() > 0) {
									if(property.getType() == Property.DATE) {
										indexEntity.getData().put(property.getQName().getLocalName(), new IndexEntityField(property.getQName().getLocalName(), textVal, false));
										Date date_value = parseTextDate(textVal);
										if(date_value != null) {
											long epoch = date_value.getTime();
											indexEntity.getData().put(property.getQName().getLocalName()+"_", new IndexEntityField(property.getQName().getLocalName()+"_", epoch, false));
										} else {
											indexEntity.getData().put(property.getQName().getLocalName()+"_", new IndexEntityField(property.getQName().getLocalName()+"_", -99999999999999L, false));
										}
									} else if(property.getType() == Property.DOUBLE) {
										indexEntity.getData().put(property.getQName().getLocalName(), new IndexEntityField(property.getQName().getLocalName(), Double.valueOf(textVal), false));
									} else if(property.getType() == Property.INTEGER) {
										indexEntity.getData().put(property.getQName().getLocalName(), new IndexEntityField(property.getQName().getLocalName(), Integer.valueOf(textVal), false));
									} else if(textVal.length() > 0) {
										if(property.getQName().equals(SystemModel.NAME) || property.getQName().equals(SystemModel.DESCRIPTION)) 
											appendFreeText(freeText, textVal);
										else {
											ModelField field = dictionary.getModelField(entity.getQName(), property.getQName());
											indexEntity.getData().put(property.getQName().getLocalName(), new IndexEntityField("", textVal, field.isTokenized()));
										}
									}
								}
							} else {
								indexEntity.getData().put(property.getQName().getLocalName(), new IndexEntityField("", String.valueOf(property.getValue()), false));
							}
						}
					}
					
					List<Long> associations = new ArrayList<Long>();
					StringBuffer buff = new StringBuffer();
					for(Association assoc : entity.getSourceAssociations()) {
						if(associations.contains(assoc.getTarget())) {
							//log.info("duplicate source association for id:"+assoc.getTarget()+" qname:"+assoc.getQName().toString());
						} else {
							//log.info("added source association for id:"+assoc.getTarget()+" qname:"+assoc.getQName().toString());
							associations.add(assoc.getTarget());
						}
					}
					for(Long assoc_id : associations) {
						buff.append(assoc_id+" ");
					}
					if(buff.length() > 0) {
						indexEntity.getData().put("source_assoc", new IndexEntityField("source_assoc", buff.toString().trim(), true));
					}
					associations.clear();
					buff = new StringBuffer();
					for(Association assoc : entity.getTargetAssociations()) {
						if(associations.contains(assoc.getSource())) {
							//log.info("duplicate target association for id:"+assoc.getSource()+" qname:"+assoc.getQName().toString());
						} else {
							//log.info("added target association for id:"+assoc.getSource()+" qname:"+assoc.getQName().toString());
							associations.add(assoc.getSource());
						}
					}
					for(Long assoc_id : associations) {
						buff.append(assoc_id+" ");
					}
					if(buff.length() > 0) {
						indexEntity.getData().put("target_assoc", new IndexEntityField("target_assoc", buff.toString().trim(), true));
					}
					
					/** Access Control List **/
					List<Association> permissionAssocs = entity.getSourceAssociations(SystemModel.PERMISSIONS);
					if(permissionAssocs != null && permissionAssocs.size() > 0) {
						buff = new StringBuffer();
						for(Association assoc : permissionAssocs) {
							Entity permission = entityService.getEntity(assoc.getTarget());
							String nodeid = permission.getPropertyValue(SystemModel.PERMISSION_TARGET);
							if(!nodeid.equals("null")) buff.append(nodeid+" ");
						}
						indexEntity.getData().put("acl", new IndexEntityField("acl", buff.toString().trim(), true));
					} else indexEntity.getData().put("acl", new IndexEntityField("acl", "0", true));
					
					indexFreeText(freeText, indexEntity);					
				} catch(Exception e) {
					throw new InvalidEntityException("", e);
				}
			}
			
		}				
		return indexEntity;
	}
	public void deindex(QName qname, Entity entity)	throws InvalidEntityException {
		if(entity != null) {
			try {
				//nodeService.deindexNode(entity.getId(), qname.toString());
			} catch(Exception e) {
				log.error("", e);
			}
		}
	}
	
	private SimpleDateFormat[] dateFormats = new SimpleDateFormat[]{
		new SimpleDateFormat("yyyy MMMM d"),
		new SimpleDateFormat("MMM yyyy"),
		new SimpleDateFormat("MMM d, yyyy")
		/*
		new SimpleDateFormat("yyyy/MM/dd"), 
		new SimpleDateFormat("MMMM d, yyyy"), 
		new SimpleDateFormat("M/d/yy"), 
		new SimpleDateFormat("M/d/yyyy"), 
		new SimpleDateFormat("yyyy MMMM"), 
		new SimpleDateFormat("yyyy MMMM dd"), 
		new SimpleDateFormat("d MMMM yyyy"), 
		new SimpleDateFormat("yyyy"),
		new SimpleDateFormat("[yyyy]"),
		new SimpleDateFormat("MMMM, yyyy"),
		new SimpleDateFormat("M/yyyy"),
		new SimpleDateFormat("M/yy"),
		new SimpleDateFormat("MM. d, yyyy"),
		new SimpleDateFormat("MM yyyy")
		*/
	};
	private DateParser parser = new DateParser();
	protected Date parseTextDate(String in) {
		in = in.replace("circa ", "");
		in = in.replace("after ", "");
		in = in.replace("received ", "");
		in = in.replace("postmarked ", "");
		in = in.replace(".", "");
		in = in.replace("[", "");
		in = in.replace("]", "");
		for(SimpleDateFormat format : dateFormats) {
			try {
				Date date = format.parse(in);
				if(date != null)
					return date;
			} catch(Exception e) {}
		}
		
		Date date = parser.parse(in);
		if(date == null && !in.toLowerCase().equals("undated")) 
			log.info("problem parsing :"+in);
		return date;
	}
	protected void indexFreeText(StringBuffer freeText, IndexEntity data) throws InvalidEntityException {
		String textVal = freeText.toString();
		if(textVal != null && textVal.length() > 0) {
			textVal = textVal.replace(",", " ");
			textVal = textVal.replaceAll("\\<.*?>"," ");
		}
		try {
			if(textVal != null && textVal.length() > 0)
				data.getData().put("freetext", new IndexEntityField("freetext", textVal.trim(), true));
		} catch(Exception e) {
			throw new InvalidEntityException("");
		}
	}
	protected void appendFreeText(StringBuffer freeText, String text) {
		String cleanText = text.replace(",", "").replace("\"", "").replace(";", "").replace(".", "").replace(":", "");
		String[] parts = cleanText.split(" ");
		for(String part : parts) {
			if(freeText.indexOf(part) == -1) freeText.append(part.toLowerCase()+" ");
		}
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public NodeService getNodeService() {
		return nodeService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	
}
