package org.heed.openapps.web;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.map.ObjectMapper;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.User;
import org.heed.openapps.data.IDName;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.util.NumberUtility;


public class EntityServiceSupport extends ServiceSupport {
	private final static Logger log = Logger.getLogger(EntityServiceSupport.class.getName());
	private SecurityService securityService;
	private NodeService nodeService;
	private DataDictionaryService dictionaryService;
	
	private Map<String,Map<String,Entity>> entityCache = new HashMap<String,Map<String,Entity>>();
	private String qname;
	
	public EntityServiceSupport(EntityService entityService) {
		setEntityService(entityService);
	}
	
	public RestResponse<Object> search(QName[] qnames, String query, String field, String sort, int startRow, int endRow, boolean sources, boolean targets) {
		RestResponse<Object> data = new RestResponse<Object>();
		long startTime = System.currentTimeMillis();
		
		EntityResultSet results = null;
		List<Entity> entities = new ArrayList<Entity>();
		if(query != null && !query.equals("null")) {
			if(NumberUtility.isLong(query)) {
				try {
					Entity entity = getEntityService().getEntity(Long.valueOf(query));
					if(entity != null) {
						entities.add(entity);
						data.getResponse().setStartRow(0);
						data.getResponse().setEndRow(1);
						data.getResponse().setTotalRows(1);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			} else if(query.equals("orphans")) {
				log.info("received orphans query...");
				List<Entity> list = new ArrayList<Entity>();
				EntityQuery eQuery = new EntityQuery(qnames, null, sort, false);
				eQuery.setStartRow(0);
				eQuery.setEndRow(10000);
				results = getEntityService().search(eQuery);
				for(Entity entity : results.getResults()) {
					if(entity.getTargetAssociations().size() == 0)
						list.add(entity);
				}
				for(int i=startRow; i < endRow; i++) {
					if(list.size() > i) entities.add(list.get(i));
				}
				data.getResponse().setStartRow(startRow);
				if(list.size() >= results.getEndRow()) data.getResponse().setEndRow(results.getEndRow());
				else data.getResponse().setEndRow(list.size());
				data.getResponse().setTotalRows(list.size());
			} else {
				EntityQuery eQuery = (query != null && !query.equals("null")) ? new EntityQuery(qnames, query, sort, false) : new EntityQuery(qnames, null, sort, false);			
				eQuery.setType(EntityQuery.TYPE_LUCENE);
				eQuery.setStartRow(startRow);
				eQuery.setEndRow(endRow);
				if(field != null && field.length() > 0 && !field.equals("null")) {
					String[] chunks = field.split(",");
					for(int i=0; i < chunks.length; i++) {
						eQuery.getFields().add(chunks[i]);
					}
				}		
				results = getEntityService().search(eQuery);
				entities = results.getResults();
			}
		} else {
			EntityQuery q = new EntityQuery(qnames, null, sort, false);
			q.setType(EntityQuery.TYPE_LUCENE);
			q.setStartRow(startRow);
			q.setEndRow(endRow);
			results = getEntityService().search(q);
			entities = results.getResults();
		}
		
		FormatInstructions instr = new FormatInstructions();

		instr.setFormat(FormatInstructions.FORMAT_JSON);
		instr.setPrintSources(sources);
		instr.setPrintTargets(targets);
		
		try {
			for(Entity entity : entities) {
				data.getResponse().addData(getEntityService().export(instr, entity));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		data.getResponse().setTime(System.currentTimeMillis() - startTime);
		if(results != null) {
			data.getResponse().setStartRow(results.getStartRow());
			if(results.getResultSize() >= results.getEndRow()) data.getResponse().setEndRow(results.getEndRow());
			else data.getResponse().setEndRow(results.getResultSize());
			data.getResponse().setTotalRows(results.getResultSize());
		}		
		if(qnames.length > 1)	
			data.getResponse().addMessage((entities.size()+" "+qnames[0].getLocalName()+" fetched"));
		else {
			String msg = "";
			for(QName qname : qnames)
				msg += qname.toString()+" ";
			data.getResponse().addMessage((entities.size()+" "+msg.trim()+" fetched"));
		}
		return data;
	}
	public RestResponse<Object> getEntity(long id, String uid, String view, boolean sources, boolean targets) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			Entity entity = id != 0 ? getEntityService().getEntity(id) : getEntityService().getEntity(uid);
			if(entity != null) {
				FormatInstructions instr = new FormatInstructions();
				instr.setFormat(FormatInstructions.FORMAT_JSON);
				instr.setPrintSources(sources);
				instr.setPrintTargets(targets);
				if(view != null) instr.setView(view);		
				data.getResponse().addData(getEntityService().export(instr, entity));
			}
			data.getResponse().setStatus(0);
		} catch(Exception e) {
			e.printStackTrace();
			data.getResponse().setStatus(-1);
			data.getResponse().addMessage(e.getMessage());
		}
		return data;
	}
	public RestResponse<Object> addEntity(QName qname, HttpServletRequest request) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			Entity entity = getEntityService().getEntity(request, qname);
			//entity.setUser(user.getId());
			String fmt = request.getParameter("format");
			ValidationResult result = getEntityService().validate(entity);
			if(result.isValid()) {
				getEntityService().addEntity(entity);
				if(fmt != null && fmt.equals("tree")) {
					FormatInstructions instructions = new FormatInstructions(true);
					instructions.setFormat(FormatInstructions.FORMAT_JSON);
					data.getResponse().addData(getEntityService().export(instructions, entity));
				} else {
					FormatInstructions instructions = new FormatInstructions(false, true, false);
					instructions.setFormat(FormatInstructions.FORMAT_JSON);
					data.getResponse().addData(getEntityService().export(instructions, entity));
				}
				data.getResponse().setStatus(0);
			} else {
				data.getResponse().addData(result); 
			}
		} catch(Exception e) {
			e.printStackTrace();
			data.getResponse().setStatus(-1);
			data.getResponse().addMessage(e.getMessage());
		}
		return data;
	}
	public RestResponse<Object> updateEntity(long id, HttpServletRequest request) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			Entity entity = getEntityService().getEntity(id);
			//entity.setUser(user.getId());
			String printTargets = request.getParameter("targets");
			String printSources = request.getParameter("sources");
			boolean sources = printSources != null ? Boolean.valueOf(printSources) : true;
			boolean targets = printTargets != null ? Boolean.valueOf(printTargets) : false;			
			Entity newEntity = getEntityService().getEntity(request, entity.getQName());
			ValidationResult result = getEntityService().validate(newEntity);
			if(result.isValid()) {
				for(Property prop : newEntity.getProperties()) {
					QName q = prop.getQName();
					entity.addProperty(q, prop.getValue());
				}
				entity.setQName(newEntity.getQName());
				entity.setName(newEntity.getName());
				if(newEntity.getUser() > 0 && newEntity.getUser() != entity.getUser()) {
					User user = getSecurityService().getCurrentUser(request);
					if(user != null && user.hasRole("administrator") || user.getId() == entity.getUser()) {
						entity.setUser(newEntity.getUser());
					}
				}
				getEntityService().updateEntity(entity);			
			}			
			FormatInstructions instr = new FormatInstructions(false, sources, targets);
			instr.setFormat(FormatInstructions.FORMAT_JSON);					
			data.getResponse().addData(getEntityService().export(instr, entity));
			data.getResponse().setStatus(0);
		} catch(Exception e) {
			data.getResponse().setStatus(-1);
			data.getResponse().addMessage(e.getMessage());
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> removeEntity(long id) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			Entity entity = getEntityService().getEntity(id);
			getEntityService().removeEntity(id);
			
			Map<String,Object> record = new HashMap<String,Object>();
			record.put("id", entity.getId());
			record.put("uid", entity.getUuid());
			data.getResponse().addData(record);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> indexEntities(QName qname) {
		RestResponse<Object> data = new RestResponse<Object>();			
		Job job = getEntityService().index(qname);
		if(job != null) {
			Map<String,Object> statusData = new HashMap<String,Object>();
			String uid = job.getUid();
			String message = job.getLastMessage();
			statusData.put("uid", uid);
			statusData.put("lastMessage", message);
			statusData.put("isRunning", !job.isComplete());
			data.getResponse().addData(statusData);
		}
		return data;
	}
	public RestResponse<Object> getChildren(long id, boolean sources, boolean targets) {
		RestResponse<Object> data = new RestResponse<Object>();		
		try {
			Entity entity = getEntityService().getEntity(id);
			List<Association> assocs = entity.getSourceAssociations();
			
			for(Association assoc : assocs) {
				Entity target = getEntityService().getEntity(assoc.getTarget());
				FormatInstructions instructions = new FormatInstructions(false, sources, targets);
				instructions.setFormat(FormatInstructions.FORMAT_JSON);
				data.getResponse().addData(getEntityService().export(instructions, target));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> addAssociation(QName assocQname, QName entityQname, String source, String target, HttpServletRequest request, boolean targets) {
		RestResponse<Object> data = new RestResponse<Object>();		
		try {
			FormatInstructions instructions = new FormatInstructions(false, true, true);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			if(target == null) {
				Entity entity = getEntityService().getEntity(request, entityQname);
				ValidationResult entityResult = getEntityService().validate(entity);
				if(entityResult.isValid()) {
					if(entity.getId() == null || entity.getId() == 0) {
						if(NumberUtility.isLong(source)) {
							getEntityService().addEntity(Long.valueOf(source), null, assocQname, null, entity);					
							Entity sourceEntity = getEntityService().getEntity(Long.valueOf(source));
							data.getResponse().addData(getEntityService().export(instructions, sourceEntity));
						}
					}
				}
			} else {
				Association assoc = getEntityService().getAssociation(request, assocQname);
				getEntityService().addAssociation(assoc);
				Entity sourceEntity = NumberUtility.isLong(source) ?  getEntityService().getEntity(Long.valueOf(source)) : getEntityService().getEntity(source);
				Entity targetEntity = NumberUtility.isLong(target) ?  getEntityService().getEntity(Long.valueOf(target)) : getEntityService().getEntity(target);
				
				if(targets) data.getResponse().addData(getEntityService().export(instructions, targetEntity));
				else data.getResponse().addData(getEntityService().export(instructions, sourceEntity));
			}			
			return data;
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> switchAssociation(long id, long targetEntityId, long sourceEntityId) {
		RestResponse<Object> data = new RestResponse<Object>();		
		try {
			Association assoc = getEntityService().getAssociation(id);
			Entity target = getEntityService().getEntity(targetEntityId);
			Entity source = getEntityService().getEntity(sourceEntityId);
			if(assoc != null && source != null && target != null) {
				if(assoc.getSource() == source.getId()) {
					assoc.setTarget(target.getId());
				} else {
					assoc.setSource(source.getId());
				}
				getEntityService().updateAssociation(assoc);
			}
			FormatInstructions instructions = new FormatInstructions(false, true, false);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			data.getResponse().addData(getEntityService().export(instructions, source));
			return data;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> removeAssociation(long id) {
		RestResponse<Object> data = new RestResponse<Object>();		
		try {
			getEntityService().removeAssociation(id);			
			Map<String,Object> record = new HashMap<String,Object>();
			record.put("id", id);
			data.getResponse().addData(record);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	/** Import **/
	public RestResponse<Object> importAdd(String session) {
		RestResponse<Object> resp = new RestResponse<Object>();		
		try {
			Map<String,Entity> data = entityCache.remove(session);			
			getEntityService().addEntities(data.values());
			resp.getResponse().setStatus(0);
			resp.getResponse().getMessages().add("Success");
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	public RestResponse<Object> importFetch(String session, int start, int end) {
		RestResponse<Object> resp = new RestResponse<Object>();
		Map<String,Entity> data = entityCache.get(session);
		if(data != null) {
			List<Entity> entitySubset = new ArrayList<Entity>();
			int index = 0;
			for(Entity e : data.values()) {
				if(index >= start && index < end)
					entitySubset.add(e);
			}			
			List<Object> entities = getEntityData(entitySubset);			
			resp.getResponse().setTotalRows(data.values().size());
			resp.getResponse().setStartRow(start);
			if(end <= data.values().size()) resp.getResponse().setEndRow(end);
			else resp.getResponse().setEndRow(data.values().size());
			resp.getResponse().setData(entities);
		}
		return resp;
	}
	public RestResponse<Object> importSelect(String session, String id) {
		RestResponse<Object> resp = new RestResponse<Object>();
		StringBuffer buff = new StringBuffer();
		Map<String,Entity> data = entityCache.get(session);
		ExportProcessor exporter = getEntityService().getExportProcessor(qname);
		if(exporter == null) exporter = getEntityService().getExportProcessor("default");
		Entity node = data.get(id);
		try {
			buff.append(exporter.export(new FormatInstructions(false), node));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void importUpload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String qname = null;
		String sessionKey = "";
		Map<String,Entity> entities = new HashMap<String,Entity>();
		byte[] payload = null;
		String mode = null;
		// Parse the request
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> items = (List<FileItem>)upload.parseRequest(request);
			for(FileItem item : items) {
				if(!item.isFormField()) {
					payload = item.get();
				} else if(item.getFieldName().endsWith("data")) {
					String metaData = item.getString();
			    	if(metaData != null && metaData.length() > 0) {
			    		ObjectMapper mapper = new ObjectMapper();
			    		Map map = mapper.readValue(metaData, Map.class);
			    		qname = (String)map.get("qname");
			    	}
			    } else if(item.getFieldName().equals("mode")) {
			    	mode = item.getString();
			    }
			}
		} catch(Exception e) {
			e.printStackTrace();
	    }
		if(mode != null && mode.length() > 0) {
		   	List<FileImportProcessor> parsers = (List)getEntityService().getImportProcessors(qname);
		   	if(parsers == null) parsers = (List)getEntityService().getImportProcessors(mode);
		   	if(parsers != null) {
		   		Integer indx = Integer.valueOf(mode.substring(9));
		   		FileImportProcessor processor = parsers.get(indx);
		   		if(processor == null) {
		   			for(FileImportProcessor parser : parsers) {
		   				if(parser.getId().equals(mode)) {
		   					processor = parser;
		   				}
		   			}
		   		}
		   		if(processor != null) {
		   			try {
		   				processor.process(new ByteArrayInputStream(payload), null);
		   				entities = processor.getEntities();
		   			} catch(Exception e) {
		   				e.printStackTrace();
		   			}
		   		}
		   	}
	    } else {
	    	CSVFormat format = CSVFormat.DEFAULT;
	    	CSVParser parser = null;
	    	try {
		   		parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(payload)), format);
	    		List<CSVRecord> records = parser.getRecords();
		   		for(CSVRecord entry : records) {
	    			Entity entity = new Entity(new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "csv"));
	    			String uid = java.util.UUID.randomUUID().toString();
	    			entity.setUuid(uid);
	    			int colCount = 0;
	    			for(String str : entry) {
	    				entity.addProperty("column"+colCount, str);
	    				colCount++;
	    			}
	    			entities.put(uid, entity);
	    		}
		   		parser.close();
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    }
		sessionKey = java.util.UUID.randomUUID().toString();
		entityCache.put(sessionKey, entities);
		response.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.uploadComplete('"+sessionKey+"','"+qname+"');</script>");
	}
	public RestResponse<Object> importProcessors(String namespace, String localname) {
		RestResponse<Object> resp = new RestResponse<Object>();
		qname = "{"+namespace+"}"+localname;
		List<ImportProcessor> processors = getEntityService().getImportProcessors(qname);
		for(ImportProcessor p : processors) {
			resp.getResponse().addData(new IDName(p.getId(), p.getName()));
		}
		return resp;
	}
	
	public SecurityService getSecurityService() {
		return securityService;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public NodeService getNodeService() {
		return nodeService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
		
}
