package org.heed.openapps.security;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.Group;
import org.heed.openapps.SystemModel;
import org.heed.openapps.User;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.security.SecurityService;

import com.Ostermiller.util.RandPass;


public class OpenAppsSecurityService implements SecurityService {
	private final static Logger log = Logger.getLogger(OpenAppsSecurityService.class.getName());
	private EntityService entityService;
	
	
	@Override
	public int authenticate(String username, String password) {		
		try {
		    User user = getUserByUsername(username);
		    if(user == null) return -1;
		    if(user.getPassword().equals(password))
		    	return 1;
		    else return -2;
		} catch (Exception e) {
			return -1;
		}
	}
	@Override
	public User getCurrentUser(HttpServletRequest request) {
		User user = (User)request.getSession().getAttribute("user");
		if(user != null) return user;
		return new OpenAppsGuestUser();
	}
	@Override
	public User getUserByUsername(String username) {
		EntityQuery query = new EntityQuery(SystemModel.USER);
		//query.setType(EntityQuery.TYPE_LUCENE);
		query.getProperties().add(new Property(SystemModel.NAME, username));
		EntityResultSet userEntities = entityService.search(query);
		if(userEntities != null && userEntities.getResults().size() > 0) {
			Entity userEntity = userEntities.getResults().get(0);
			User user = getUser(userEntity);
			return user;
		}	
		return null;
	}
	@Override
	public User getUserByEmail(String email) {
		EntityQuery query = new EntityQuery(SystemModel.USER);
		//query.setType(EntityQuery.TYPE_LUCENE);
		query.getProperties().add(new Property(SystemModel.EMAIL, email));
		EntityResultSet userEntities = entityService.search(query);
		if(userEntities != null && userEntities.getResults().size() > 0) {
			Entity userEntity = userEntities.getResults().get(0);
			User user = getUser(userEntity);
			return user;
		}	
		return null;
	}
	@Override
	public List<User> getUsers(String query) {
		List<User> users = new ArrayList<User>();
		log.info("getUsers() not implemented");
		return users;
	}
	@Override
	public List<Group> getGroups(String query) {
		List<Group> users = new ArrayList<Group>();
		log.info("getGroups() not implemented");
		return users;
	}
	@Override
	public String generatePassword() {
		RandPass generator = new RandPass();
		return generator.getPass();
	}
	@Override
	public void removeUser(long id) {
		
	}
	@Override
	public void updateUser(User user) {
		try {
			Entity userEntity = entityService.getEntity(user.getId());
			if(user.getPassword() != null && user.getPassword().length() > 0) 
				userEntity.addProperty(SystemModel.USER_PASSWORD, user.getPassword());
			if(user.getFirstName() != null && user.getFirstName().length() > 0) 
				userEntity.addProperty(SystemModel.FIRSTNAME, user.getFirstName());
			if(user.getLastName() != null && user.getLastName().length() > 0) 
				userEntity.addProperty(SystemModel.LASTNAME, user.getLastName());
			if(user.getEmail() != null && user.getEmail().length() > 0) 
				userEntity.addProperty(SystemModel.EMAIL, user.getEmail());
			entityService.updateEntity(userEntity);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected User getUser(Entity userEntity) {
		User user = new User();
		user.setId(userEntity.getId());
		user.setFirstName(userEntity.getPropertyValue(SystemModel.FIRSTNAME));
		user.setLastName(userEntity.getPropertyValue(SystemModel.LASTNAME));
		user.setEmail(userEntity.getPropertyValue(SystemModel.EMAIL));
		user.setUsername(userEntity.getPropertyValue(SystemModel.USERNAME));
		return user;
	}
	
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
		
}
