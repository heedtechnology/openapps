package org.heed.openapps.search;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.search.indexing.IndexingService;



public class SearchServiceImpl implements SearchService {
	private static final long serialVersionUID = 586282630873987158L;
	private final static Logger log = Logger.getLogger(SearchServiceImpl.class.getName());
	private IndexingService indexingService;
		
	private Map<Object, Searcher> searchers = new HashMap<Object,Searcher>();
	
	
	public void initialize() {
		
	}
	public void shutdown() {
		
	}
	@Override
	public void reload(Entity entity) {
		
		if(entity.getQName().equals(RepositoryModel.ITEM)) {
			Searcher searcher = searchers.get(RepositoryModel.ITEM.toString());
			if(searcher != null) {
				searcher.reload(entity);
			}
		} else if(entity.getQName().equals(RepositoryModel.COLLECTION)) {
			searchers.remove(entity.getId());
		}
	}
	
	@Override
	public SearchResponse search(SearchRequest request) {
		Searcher searcher = null;
		SearchResponse response = new SearchResponse();
		QName qname = request.getQname();
		if(qname != null) searcher = searchers.get(qname.toString());
		if(searcher == null) searcher = searchers.get("default");
		if(searcher != null) {
			searcher.search(request, response);		
		}
		log.info("search for '"+request.getQuery()+"' returned "+response.getResultSize()+" results parsed to:"+response.getQueryExplanation());
		return response;
	}
	@Override
	public void remove(Long id) {
		indexingService.remove(id);
	}
	
	protected Map<String,String> getLabels() {
		Map<String,String> labels = new HashMap<String,String>();
		labels.put("manuscript", "Manuscript");
		labels.put("correspondence", "Correspondence");
		labels.put("printed_material", "Printed Material");
		labels.put("audio", "Audio");
		labels.put("professional", "Professional Material");
		labels.put("memorabilia", "Memorabilia");
		labels.put("journals", "Journals");
		labels.put("scrapbooks", "Scrapbooks");
		labels.put("financial", "Financial");
		labels.put("legal", "Legal Material");
		labels.put("artwork", "Artwork");
		labels.put("photographs", "Photographs");
		labels.put("notebooks", "Notebooks");
		labels.put("medical", "Medical");
		labels.put("research", "Research");
		labels.put("miscellaneous", "Miscellaneous");
		labels.put("video", "Video");
		return labels;
	}
	
	@Override
	public void update(Long data, boolean wait) {
		indexingService.update(data, wait);
	}
	@Override
	public void update(List<Long> data) {
		indexingService.update(data);
	}
	public void setIndexingService(IndexingService indexingService) {
		this.indexingService = indexingService;
	}
	public void setSearchers(Map<Object, Searcher> searchers) {
		this.searchers = searchers;
	}
	
}