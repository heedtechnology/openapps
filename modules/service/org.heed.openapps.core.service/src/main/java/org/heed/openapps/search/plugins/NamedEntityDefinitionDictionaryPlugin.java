/*
 * Copyright (C) 2010 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.search.plugins;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.search.Definition;
import org.heed.openapps.search.DictionaryPlugin;
import org.heed.openapps.search.dictionary.NamedEntityDefinition;


public class NamedEntityDefinitionDictionaryPlugin implements DictionaryPlugin {
	private EntityService entityService;
	
	
	public List<Definition> getDefinitions() {
		List<Definition> defs = new ArrayList<Definition>();
		try {
			EntityResultSet names = entityService.search(new EntityQuery(ClassificationModel.NAMED_ENTITY, null, "name", true));
			if(names != null) {
			for(Entity name : names.getResults()) {
				Long id = name.getId();
				String search_values = name.getProperty(ClassificationModel.NAMED_ENTITY_SEARCH_VALUES).toString();
				if(search_values != null && !search_values.equals("")) {
					String[] values = search_values.split(",");
					for(int i=0; i < values.length; i++) {
						NamedEntityDefinition def = new NamedEntityDefinition(values[i].trim(), String.valueOf(id));
						defs.add(def);
					}
				}
			}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return defs;
	}
	
}
