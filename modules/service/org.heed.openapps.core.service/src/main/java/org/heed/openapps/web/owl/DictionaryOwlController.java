package org.heed.openapps.web.owl;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.util.DecodingUtility;
import org.heed.openapps.web.WebserviceSupport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/service/dictionary")
public class DictionaryOwlController extends WebserviceSupport {
	private final static Logger log = Logger.getLogger(DictionaryOwlController.class.getName());
	
	public static final String HEADER = "<?xml version=\"1.0\"?>" +
		"<rdf:RDF xmlns=\"http://www.w3.org/2002/07/owl#\"" +
		" xml:base=\"http://www.w3.org/2002/07/owl\"" +
		" xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"" +
		" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"" +
		" xmlns:owl=\"http://www.w3.org/2002/07/owl#\"" +
		" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" +
		" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\">";
	public static final String FOOTER = "</rdf:RDF>";
	
	private String entities;
	
	public DictionaryOwlController() {
		
	}
	
	protected void initialize() {
		//iterate over subjects and named entities, creating an individual for each
		StringWriter writer = new StringWriter();
		EntityQuery subjectQuery  = new EntityQuery(ClassificationModel.SUBJECT);
		subjectQuery.setEndRow(10000);
		EntityResultSet subjectResults = getEntityService().search(subjectQuery);
		for(Entity subject : subjectResults.getResults()) {
			writer.append("<"+subject.getQName()+" rdf:ID=\""+subject.getId()+"\">");
			writer.append("<rdfs:label><![CDATA["+clean(subject.getName())+"]]></rdfs:label>");
			writer.append("</"+subject.getQName()+">");
		}
		EntityQuery entityQuery  = new EntityQuery(ClassificationModel.PERSON);
		entityQuery.setEndRow(10000);
		EntityResultSet entityResults = getEntityService().search(entityQuery);
		for(Entity subject : entityResults.getResults()) {
			writer.append("<"+subject.getQName()+" rdf:ID=\""+subject.getId()+"\">");
			writer.append("<rdfs:label><![CDATA["+clean(subject.getName())+"]]></rdfs:label>");
			writer.append("</"+subject.getQName()+">");
		}
		EntityQuery corpQuery  = new EntityQuery(ClassificationModel.CORPORATION);
		corpQuery.setEndRow(10000);
		EntityResultSet corpResults = getEntityService().search(corpQuery);
		for(Entity subject : corpResults.getResults()) {
			writer.append("<"+subject.getQName()+" rdf:ID=\""+subject.getId()+"\">");
			writer.append("<rdfs:label><![CDATA["+clean(subject.getName())+"]]></rdfs:label>");
			writer.append("</"+subject.getQName()+">");
		}
		entities = writer.toString().trim();
		log.info("OWL generation initialized...");
	}
	@RequestMapping(value="/fetch.owl", method = RequestMethod.GET)
	public ModelAndView fetchDictionary(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(entities == null) initialize();
		String id = request.getParameter("id");
		DataDictionary dictionaryNode = null;
		if(id != null && !id.equals("null")) {
			if(id.equals("0")) dictionaryNode = getDictionaryService().getSystemDictionary();
			else dictionaryNode = getDictionaryService().getDataDictionary(Long.valueOf(id));
		} 		
		if(dictionaryNode != null) {
			response.getWriter().append(toOwl(dictionaryNode));
		}
		return null;
	}
	@RequestMapping(value="/model/fetch.owl", method = RequestMethod.GET)
	public ModelAndView fetchModel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(entities == null) initialize();
		String id = request.getParameter("id");
		String qname = request.getParameter("qname");
		Model modelNode = null;
		if(id != null && !id.equals("null")) {
			Entity node = getEntityService().getEntity(Long.valueOf(id));
			qname = (String)node.getQName().toString();
			modelNode = getDictionaryService().getSystemDictionary().getModel(QName.createQualifiedName(qname));			
		} else if(qname != null && !qname.equals("null")) {
			modelNode = getDictionaryService().getSystemDictionary().getModel(QName.createQualifiedName(qname));
		}		
		if(modelNode != null) {
			response.getWriter().append(HEADER + toOwl(modelNode));
		}
		return null;
	}
	protected String toOwl(Model model) {
		try {
			
		} catch(Exception e) {
			
		}
		return null;
	}
	
	protected String toOwl(DataDictionary dictionary) {
		StringWriter writer = new StringWriter();
		writer.append(HEADER);
		try {
			for(Model model : dictionary.getAllModels()) {
				writer.append("<owl:Class rdf:about=\""+model.getQName()+"\">");
				writer.append("<rdfs:label xml:lang=\"en\">"+model.getName()+"</rdfs:label>");
					
				if(model.getParent() != null) {
					writer.append("<rdfs:subClassOf>");
					writer.append("<owl:Class rdf:about=\""+model.getParent().getQName()+"\"/>");
					writer.append("</rdfs:subClassOf>");
				}
				writer.append("</owl:Class>");
				
				for(ModelField field : model.getFields()) {
					writer.append("<owl:DatatypeProperty rdf:ID=\""+field.getQName()+"\">");
					writer.append("<rdfs:label>"+field.getName()+"</rdfs:label>");
					if(field.getType() == ModelField.TYPE_TEXT) writer.append("<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#string\"/>");
					writer.append("<rdfs:domain rdf:resource=\""+model.getQName()+"\"/>");
					writer.append("</owl:DatatypeProperty>");
				}
				
				for(ModelRelation relation : model.getRelations()) {
					writer.append("<owl:ObjectProperty rdf:ID=\""+relation.getQName()+"\">");
					writer.append("<rdfs:range rdf:resource=\""+relation.getEndName()+"\"/>");
					writer.append("<rdfs:label>"+relation.getQName()+"</rdfs:label>");
					writer.append("<rdfs:domain rdf:resource=\""+relation.getStartName()+"\"/>");
					writer.append("</owl:ObjectProperty>");
				}
			}
			writer.append(entities);
		} catch(Exception e) {
			
		}
		writer.append(FOOTER);
		return writer.toString().trim();
	}
	protected String clean(String in) {
		return DecodingUtility.decodeString(in);
	}
}
