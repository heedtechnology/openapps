package org.heed.openapps.entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.data.FormatInstructions;


public class DefaultExportProcessor implements ExportProcessor {
	private static final long serialVersionUID = -1870317040144154420L;
	private final static Logger log = Logger.getLogger(DefaultExportProcessor.class.getName());
	private EntityService entityService;
	private DataDictionaryService dictionaryService;
	private QName qname;
	
	protected StringBuffer buffer = new StringBuffer();
	
		
	@Override
	public Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException {
		if(instructions.getFormat().equals(FormatInstructions.FORMAT_CSV)) return toCsv(entity);
		else if(instructions.getFormat().equals(FormatInstructions.FORMAT_JSON)) return toMap(entity, instructions);
		else return toXml(entity, instructions.printSources(), instructions.printTargets());
	}
	@Override
	public Object export(FormatInstructions instructions, Association association) throws InvalidEntityException {
		if(instructions.getFormat().equals(FormatInstructions.FORMAT_JSON)) return exportMap(instructions, association);
		return toXml(association, false, instructions.isTree());
	}
	public void toXml(StringBuffer buff, Association association) {
		
	}	
	public String getEntityTitle(Entity entity) {
		Property name = entity.getProperty(new QName("openapps.org_system_1.0","name"));
		if(name == null) name = entity.getProperty(new QName("openapps.org_system_1.0","title"));
		if(name != null) return name.toString();
		return "";
	}
	
	public Map<String,Object> toMap(Entity entity, FormatInstructions instructions) throws InvalidEntityException {
		SortedMap<String,Object> data = new TreeMap<String,Object>();
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			//Model model = getDataDictionaryService().getModel(entity.getQName());		
			data.put("id", entity.getId());
			data.put("uuid", entity.getUuid());
			data.put("qname", entity.getQName().toString());
			data.put("localName", entity.getQName().getLocalName());
			data.put("name", entity.getName());
			//if(instructions.hasChildren()) data.put("isFolder", true);
			//else data.put("isFolder", false);
		
			//if(model == null) throw new InvalidEntityException("entity model not found for qname:"+entity.getQName().toString());
			for(ModelField field : dictionary.getModelFields(entity.getQName())) {
				if(entity.hasProperty(field.getQName())) {
					Property property = entity.getProperty(field.getQName());
					if(property.getValue() != null && property.getValue().toString().length() > 0 && !property.getValue().equals("null")) {
						if(field.getQName().getLocalName().equals("function")) {
							if(property.getValue() != null) data.put("_function", property.getValue());
							else data.put("_function", "");
						} else { 
							if(property.getValue() != null) data.put(property.getQName().getLocalName(), property.getValue());
							else data.put(property.getQName().getLocalName(), "");
						}
					}
				}
			}
			if(entity.getUser() > 0) {
				try {
					Entity user = entityService.getEntity(entity.getUser());
					if(user != null)
						data.put("username", user.getName());
				} catch(Exception e) {
					log.severe("unable to locate User : "+entity.getUser());
				}
			}
			data.put("user", entity.getUser());
			if(entity.getCreated() > 0) data.put("created", entity.getCreated());
			if(entity.getCreator() > 0) data.put("creator", entity.getCreator());
			if(entity.getModified() > 0) data.put("modified", entity.getModified());
			if(entity.getModifier() > 0) data.put("modifier", entity.getModifier());
			data.put("deleted", entity.getDeleted());
			if(instructions.printSources()) {
				List<Map<String,Object>> relationList = new ArrayList<Map<String,Object>>();
				List<ModelRelation> relations = dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_OUTGOING);
				for(ModelRelation relation : relations) {
					Model startModel = dictionary.getModel(relation.getStartName());
					Model endModel = dictionary.getModel(relation.getEndName());
					try {
						String sourceName = startModel != null ? startModel.getQName().toString() : "";
						String targetName = endModel != null ? endModel.getQName().toString() : "";
						List<Association> source_associations = entity.getSourceAssociations(relation.getQName());
						if(source_associations.size() > 0) {
							Map<String,Object> associationList = new HashMap<String,Object>();
							associationList.put("source", sourceName);
							associationList.put("target", targetName);
							associationList.put("qname", relation.getQName().toString());
							List<Map<String,Object>> sourceList = new ArrayList<Map<String,Object>>();
							
							FormatInstructions instructions2 = new FormatInstructions(false);
							instructions2.setFormat(FormatInstructions.FORMAT_JSON);
							instructions2.setPrintSources(false);
							instructions2.setPrintTargets(true);
							ExportProcessor processor = entityService.getExportProcessor(targetName);
							for(Association assoc : source_associations) {								
								if(processor != null) 
									sourceList.add(processor.exportMap(instructions2, assoc));
								else 
									sourceList.add(exportMap(instructions2, assoc));
							}
							associationList.put(relation.getQName().getLocalName(), sourceList);
							relationList.add(associationList);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				data.put("source_associations", relationList);
			}
			if(instructions.printTargets()) {
				List<Map<String,Object>> relationList = new ArrayList<Map<String,Object>>();
				List<ModelRelation> relations = dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_INCOMING);
				for(ModelRelation relation : relations) {
					Model startModel = dictionary.getModel(relation.getStartName());
					Model endModel = dictionary.getModel(relation.getEndName());
					try {
						String sourceName = startModel != null ? startModel.getQName().toString() : "";
						String targetName = endModel != null ? endModel.getQName().toString() : "";
						List<Association> target_associations = entity.getTargetAssociations(relation.getQName());
						if(target_associations.size() > 0) {
							List<Association> filtered_targets = new ArrayList<Association>();
							for(Association assoc : target_associations) {
								List<QName> assocSourceNames = dictionary.getQNames(assoc.getSourceName());
								List<QName> assocTargetNames = dictionary.getQNames(assoc.getTargetName());
								if(assocSourceNames.contains(startModel.getQName()) && assocTargetNames.contains(endModel.getQName())) { 
									if(!filtered_targets.contains(assoc)) {
										filtered_targets.add(assoc);
									}
								}
							}
							if(filtered_targets.size() > 0) {
								Map<String,Object> associationList = new HashMap<String,Object>();
								associationList.put("source", sourceName);
								associationList.put("target", targetName);
								associationList.put("qname", relation.getQName().toString());
								List<Map<String,Object>> sourceList = new ArrayList<Map<String,Object>>();
								
								FormatInstructions instructions2 = new FormatInstructions(false);
								instructions2.setFormat(FormatInstructions.FORMAT_JSON);
								instructions2.setPrintSources(true);
								instructions2.setPrintTargets(false);
								ExportProcessor processor = entityService.getExportProcessor(sourceName);
								for(Association assoc : filtered_targets) {	
									if(processor != null) 
										sourceList.add(processor.exportMap(instructions2, assoc));
									else 
										sourceList.add(exportMap(instructions2, assoc));					
								}
								associationList.put(relation.getQName().getLocalName(), sourceList);
								relationList.add(associationList);
							}
						}					
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				data.put("target_associations", relationList);
			}
			return data;
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	public Map<String,Object> exportMap(FormatInstructions instructions, Association association) {
		Map<String,Object> data = new HashMap<String,Object>();
		try {
			Entity target = instructions.printSources() ? association.getSourceEntity() : association.getTargetEntity();
			if(target == null) target = instructions.printSources() ? getEntityService().getEntity(association.getSource()) : getEntityService().getEntity(association.getTarget());
			if(instructions.isTree()) {
				data.put("localName", target.getQName().getLocalName());
				data.put("id", association.getTarget());
				data.put("parentLocalName", association.getSourceName().getLocalName());
				data.put("parent", association.getSource());
			} else {
				data.put("localName", target.getQName().getLocalName());
				data.put("id", association.getId());				
				data.put("source_id", association.getSource());
				data.put("target_id",association.getTarget());
			}
			data.put("uuid", target.getUuid());
			data.put("name", target.getName());
			if(instructions.hasChildren()) data.put("isFolder", true);
			else data.put("isFolder", false);
			for(Property entityProperty : target.getProperties()) {
				if(entityProperty.getValue() != null && entityProperty.getValue().toString().length() > 0) {
					if(entityProperty.getQName().getLocalName().equals("function"))
						data.put("_function", entityProperty.getValue());
					else
						data.put(entityProperty.getQName().getLocalName(), entityProperty.getValue());
				}
			}
			for(Property assocProperty : association.getProperties()) {
				if(assocProperty.getQName().getLocalName().equals("function"))
					data.put("_function", assocProperty.getValue());
				else 
					data.put(assocProperty.getQName().getLocalName(), assocProperty.getValue());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public String toCsv(Entity entity) throws InvalidEntityException {
		DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
		StringBuilder buff = new StringBuilder();
		try {
			for(ModelField field : dictionary.getModelFields(entity.getQName())) {
				if(entity.hasProperty(field.getQName())) {
					Property property = entity.getProperty(field.getQName());
					if(property.getValue() != null && property.getValue().toString().length() > 0)
						buff.append(property.getValue()+",");
				}
			}
			buff.replace(buff.length()-1, buff.length(), "\n");
			return buff.toString();
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	public String toXml(Entity entity, boolean printSources, boolean printTargets) throws InvalidEntityException {
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			StringBuffer buff = new StringBuffer("<node id='"+entity.getId()+"' uid='"+entity.getUuid()+"' qname='"+entity.getQName().toString()+"' localName='"+entity.getQName().getLocalName()+"'>");
			boolean printTitle = true;
			for(ModelField field : dictionary.getModelFields(entity.getQName())) {
				if(entity.hasProperty(field.getQName())) {
					Property property = entity.getProperty(field.getQName());
					if(property.getValue() != null && property.getValue().toString().length() > 0)
						buff.append("<"+property.getQName().getLocalName()+"><![CDATA["+clean(String.valueOf(property.getValue()))+"]]></"+property.getQName().getLocalName()+">");
				} 
				if(field.getQName().getLocalName().equals("title")) printTitle = false;
			}
			if(printTitle) buff.append("<title><![CDATA["+clean(getEntityTitle(entity))+"]]></title>");
			if(entity.getCreated() > 0) buff.append("<created>"+entity.getCreated()+"</created>");
			if(entity.getCreator() > 0) buff.append("<creator>"+entity.getCreator()+"</creator>");
			if(entity.getModified() > 0) buff.append("<modified>"+entity.getModified()+"</modified>");
			if(entity.getModifier() > 0) buff.append("<modifier>"+entity.getModifier()+"</modifier>");
			buff.append("<deleted>"+entity.getDeleted()+"</deleted>");
			if(buffer.length() > 0) {
				buff.append(buffer.toString().trim());
				buffer = new StringBuffer();
			}
			if(printSources) {
				buff.append("<source_associations>");			
				for(ModelRelation relation : dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_OUTGOING)) {
					Model startModel = dictionary.getModel(relation.getStartName());
					Model endModel = dictionary.getModel(relation.getEndName());
					try {
						String sourceName = startModel != null ? startModel.getQName().toString() : "";
						String targetName = endModel != null ? endModel.getQName().toString() : "";
						List<Association> source_associations = entity.getSourceAssociations(relation.getQName());
						if(source_associations.size() > 0) {
							buff.append("<"+relation.getQName().getLocalName()+" source='"+sourceName+"' target='"+targetName+"'>");
							for(Association assoc : source_associations) {	
								buff.append(toXml(assoc, false, false));			
							}
							buff.append("</"+relation.getQName().getLocalName()+">");
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				buff.append("</source_associations>");
			}
			if(printTargets) {
				buff.append("<target_associations>");			
				for(ModelRelation relation : dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_INCOMING)) {
					Model startModel = dictionary.getModel(relation.getStartName());
					Model endModel = dictionary.getModel(relation.getEndName());
					try {
						String sourceName = startModel != null ? startModel.getQName().toString() : "";
						String targetName = endModel != null ? endModel.getQName().toString() : "";
						List<Association> target_associations = entity.getTargetAssociations(relation.getQName());
						if(target_associations.size() > 0) {
							List<Association> filtered_targets = new ArrayList<Association>();
							for(Association assoc : target_associations) {
								boolean sourceMatch = dictionary.isDescendant(startModel.getQName(), assoc.getSourceName()) || assoc.getSourceName().toString().equals(sourceName);
								boolean targetMatch = dictionary.isDescendant(endModel.getQName(), assoc.getTargetName()) || assoc.getTargetName().toString().equals(targetName);
								if(sourceMatch && targetMatch)
									filtered_targets.add(assoc);
							}
							if(filtered_targets.size() > 0) {
								buff.append("<"+relation.getQName().getLocalName()+" source='"+sourceName+"' target='"+targetName+"'>");
								for(Association assoc : filtered_targets) {								
									buff.append(toXml(assoc, true, false));
								}
								buff.append("</"+relation.getQName().getLocalName()+">");
							}
						}					
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				buff.append("</target_associations>");
			}
			return buff.append("</node>").toString();
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	public String toXml(Association association, boolean printSource, boolean isTree) {
		StringBuffer buff = new StringBuffer("");
		try {
			Entity target = printSource ? association.getSourceEntity() : association.getTargetEntity();
			if(target == null) target = printSource ? getEntityService().getEntity(association.getSource()) : getEntityService().getEntity(association.getTarget());
			if(isTree) buff.append("<node localName='"+target.getQName().getLocalName()+"' id='"+association.getTarget()+"' uid='"+target.getUuid()+"' parentLocalName='"+association.getSourceName().getLocalName()+"' parent='"+association.getSource()+"'>");
			else buff.append("<node localName='"+target.getQName().getLocalName()+"' id='"+association.getId()+"' uid='"+target.getUuid()+"' source_id='"+association.getSource()+"' target_id='"+association.getTarget()+"'>");	
			boolean printTitle = true;
			for(Property entityProperty : target.getProperties()) {
				if(entityProperty.getValue() != null && !entityProperty.getValue().equals("")) {
					buff.append("<"+entityProperty.getQName().getLocalName()+"><![CDATA["+clean(entityProperty.getValue())+"]]></"+entityProperty.getQName().getLocalName()+">");
					if(entityProperty.getQName().getLocalName().equals("title")) printTitle = false;
				}
			}
			if(printTitle) buff.append("<title><![CDATA["+clean(getEntityTitle(target))+"]]></title>");
			for(Property assocProperty : association.getProperties()) {
				buff.append("<"+assocProperty.getQName().getLocalName()+"><![CDATA["+clean(assocProperty.getValue())+"]]></"+assocProperty.getQName().getLocalName()+">");
			}
			if(buffer.length() > 0) {
				buff.append(buffer.toString().trim());
				buffer = new StringBuffer();
			}
			buff.append("</node>");	
		} catch(Exception e) {
			e.printStackTrace();
		}
		return buff.toString();
	}
	
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public QName getQName() {
		return qname;
	}
	public void setQname(QName qname) {
		this.qname = qname;
	}
	public StringBuffer getBuffer() {
		return buffer;
	}
	public void setBuffer(StringBuffer buffer) {
		this.buffer = buffer;
	}	
	
	protected String clean(Object in) {
		return in.toString();
	}
}
