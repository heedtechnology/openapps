package org.heed.openapps.web.service;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.heed.openapps.util.IOUtility;


public class MutilthreadedHttpService {
	private final static Logger log = Logger.getLogger(MutilthreadedHttpService.class.getName());
	private final PoolWorker[] threads;
    private final LinkedList<HttpTask> queue;
    
    private int taskTimeout = 30 * 1000;//30 seconds
    
    public MutilthreadedHttpService(int nThreads) {
    	queue = new LinkedList<HttpTask>();
        threads = new PoolWorker[nThreads];
        PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
        HttpClient httpClient = new DefaultHttpClient(cm);
        for (int i=0; i<nThreads; i++) {
            threads[i] = new PoolWorker(httpClient);
            threads[i].start();
        }
	}
	protected String httpGet(String url) {
		HttpGet httpget = new HttpGet(url);
		HttpGetTask task = new HttpGetTask(httpget);
		long start = System.currentTimeMillis();
		while(!task.isComplete()) {
			long current = System.currentTimeMillis();
			if(current - start >= taskTimeout) {
				log.severe("HttpGetTask timeout : "+task.getUrl());
			}
		}
		return task.getResponse();
	}
	/**
	 * Standard http client POST request
	 **/
	protected String httpPost(String url, String body) {		
		HttpPost httppost = new HttpPost(url);
		HttpTask task = new HttpPostTask(httppost);
		while(!task.isComplete()) {
			
		}
		return task.getResponse();
	}
	
	private class PoolWorker extends Thread {
		private final HttpClient httpClient;
		private final HttpContext context;
        
		public PoolWorker(HttpClient httpClient) {
			this.httpClient = httpClient;
			this.context = new BasicHttpContext();
		}
		public void run() {
        	HttpTask task;
            while(true) {
                synchronized(queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException ignored) {}
                    }
                    task = queue.removeFirst();
                }
                try {
                	String url = null;
                	HttpResponse response = null;                
                	if(task instanceof HttpGetTask) {
                		HttpGetTask t = (HttpGetTask)task;
                		url = t.getUrl();
                		response = httpClient.execute(t.getMethod(), context);
                	} else {
                		HttpPostTask t = (HttpPostTask)task;
                		url = t.getUrl();
                		response = httpClient.execute(t.getMethod(), context);
                	}
                	String result = IOUtility.convertStreamToString(response.getEntity().getContent());
	        		task.setResponse(result);
	        		task.setComplete(true);
	        		EntityUtils.consume(response.getEntity());
                	 
        			log.info("----------------------------------------");
        			log.info("request : "+url);
        			log.info("response: "+task.getResponse());
        			log.info("----------------------------------------");
        		
        		} catch(ClientProtocolException e) {
        			e.printStackTrace();
        		} catch(IOException e2) {
        			e2.printStackTrace();
        		}
            }
        }
    }
	
	private class HttpGetTask extends HttpTask {
		private HttpGet method;
		
		public HttpGetTask(HttpGet method) {
			this.method = method;
		}
		@Override
    	public boolean equals(Object obj) {
    		if(obj instanceof HttpGetTask) {
    			HttpGetTask task = (HttpGetTask)obj;
    			if(task.getMethod().getURI().equals(method.getURI()))
    				return true;
    		} 
    		return false;
    	}
		public HttpGet getMethod() {
			return method;
		}
		public String getUrl() {
			return method.getURI().toString();
		}
	}
	private class HttpPostTask extends HttpTask {
		private HttpPost method;
		
		public HttpPostTask(HttpPost method) {
			this.method = method;
		}
		@Override
    	public boolean equals(Object obj) {
    		if(obj instanceof HttpPostTask) {
    			HttpPostTask task = (HttpPostTask)obj;
    			if(task.getMethod().getURI().equals(method.getURI()))
    				return true;
    		} 
    		return false;
    	}
		public HttpPost getMethod() {
			return method;
		}
		public String getUrl() {
			return method.getURI().toString();
		}
	}
    private class HttpTask {
    	private String response;
    	private boolean complete;
    	   	
		public String getResponse() {
			return response;
		}		
		public void setResponse(String response) {
			this.response = response;
		}
		public boolean isComplete() {
			return complete;
		}
		public void setComplete(boolean complete) {
			this.complete = complete;
		}
    }
    
}
