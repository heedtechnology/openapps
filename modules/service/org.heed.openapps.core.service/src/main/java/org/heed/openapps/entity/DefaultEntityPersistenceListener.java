package org.heed.openapps.entity;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.QName;
import org.heed.openapps.User;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.job.EntityIndexingJob;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.scheduling.SchedulingService;
import org.heed.openapps.security.SecurityService;


public class DefaultEntityPersistenceListener implements EntityPersistenceListener {
	private final static Log log = LogFactory.getLog(DefaultEntityPersistenceListener.class);
	protected NodeService nodeService;
	protected EntityService entityService;
	protected DataDictionaryService dictionaryService;
	protected SecurityService securityService;
	protected SchedulingService schedulingService;
	protected String hash = "SHA-1";
	
	
	@Override
	public void index(long id) {
		try {
			Entity entity = entityService.getEntity(id);
			Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
			if(model != null && model.isEntityIndexed()) {
				log.info("adding entity of type:"+entity.getQName().toString()+" to equity indexing queue.");
				entityService.index(entity.getId(), false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public Job index(QName qname) {
		Model model = dictionaryService.getSystemDictionary().getModel(qname);
		if(model != null) {
			try {				
				if(model.isEntityIndexed()) {
					EntityIndexingJob job = new EntityIndexingJob(nodeService, entityService, null, qname, true, false);					
					return job;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	@Override
	public Entity extractEntity(HttpServletRequest request, QName qname) throws InvalidEntityException {
		Entity entity = null;
		String id = request.getParameter("id");
		if(id != null && id.length() > 0) {
			entity = getEntityService().getEntity(Long.valueOf(id));
		} else {
			User user = securityService.getCurrentUser(request);
			if(user != null) {
				entity = new Entity(qname);
				entity.setCreated(System.currentTimeMillis());
				entity.setModified(System.currentTimeMillis());
				String uid = request.getParameter("uid");
				if(uid != null && uid.length() > 0) entity.setUuid(uid);
				else entity.setUuid(UUID.randomUUID().toString());
				entity.setUser(user.getId());
			}
		}
		if(entity != null) {
			try {
				String name = request.getParameter("name");
				if(name != null) entity.setName(name);
				DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(ModelField field : fields) {
					try {
						String value = field.getQName().getLocalName().equals("function") ? 
							request.getParameter("_"+field.getQName().getLocalName()) : 
								request.getParameter(field.getQName().getLocalName());
						if(value != null && value.length() > 0 && !value.equals("null")) {
							if(field.getType() == ModelField.TYPE_DATE) {
								entity.addProperty(Property.DATE, field.getQName(), value);
							}
							else if(field.getType() == ModelField.TYPE_INTEGER) {
								int val = Integer.valueOf(value);
								entity.addProperty(Property.INTEGER, field.getQName(), val);
							}
							else if(field.getType() == ModelField.TYPE_LONG) {
								long val = Long.valueOf(value);
								entity.addProperty(Property.LONG, field.getQName(), val);
							}
							else if(field.getType() == ModelField.TYPE_BOOLEAN) {
								boolean val = Boolean.valueOf(value);
								entity.addProperty(Property.BOOLEAN, field.getQName(), val);
							}
							else entity.addProperty(field.getQName(), value);				
						} else {
							if(field.getType() == ModelField.TYPE_DATE) entity.addProperty(Property.DATE, field.getQName(), value);
							else if(field.getType() == ModelField.TYPE_INTEGER) entity.addProperty(Property.INTEGER, field.getQName(), 0);
							else if(field.getType() == ModelField.TYPE_LONG) entity.addProperty(Property.LONG, field.getQName(), 0);
							else if(field.getType() == ModelField.TYPE_BOOLEAN) entity.addProperty(Property.BOOLEAN, field.getQName(), false);
							else entity.addProperty(field.getQName(), value);
						}
					} catch(Exception e) {
						e.toString();
					}
				}
				for(ModelRelation relation : dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_OUTGOING)) {
					String values = request.getParameter(relation.getQName().getLocalName());
					if(values != null) {
						String[] ids = values.replaceFirst("\\[", "").replaceAll("\\]", "").split(",");
						List<Association> assocs = entity.getAssociations(relation.getQName());
						for(String target_id : ids) {
							if(target_id.length() > 0) {
								boolean match = false;
								for(Association assoc : assocs) {
									if(assoc.getTarget().equals(Long.valueOf(target_id))) match = true;
								}
								if(!match) {						
									Entity target = getEntityService().getEntity(Long.valueOf(target_id));
									Association a = new Association(relation.getQName(), entity, target);
									entity.getSourceAssociations().add(a);
								}
							}
						}
					}
				}			
			} catch(Exception e) {
				throw new InvalidEntityException("entity model not found for qname:"+qname.toString(), e);
			}
		}
		return entity;
	}

	@Override
	public Entity extractEntity(long id, QName qname) {
		Entity entity = new Entity(id, qname);
		return entity;
	}

	@Override
	public void onAfterAdd(Entity entity) {
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model.isEntityIndexed()) {
			entityService.index(entity.getId(), false);
		}
	}

	@Override
	public void onAfterAssociationAdd(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel.isEntityIndexed()) {
			entityService.index(association.getSource(), false);
		}
		if(targetModel.isEntityIndexed()) {
			entityService.index(association.getTarget(), false);
		}
	}

	@Override
	public void onAfterAssociationDelete(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel.isEntityIndexed()) {
			entityService.index(association.getSource(), false);
		}
		if(targetModel.isEntityIndexed()) {
			entityService.index(association.getTarget(), false);
		}
	}

	@Override
	public void onAfterAssociationUpdate(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel.isEntityIndexed()) {
			entityService.index(association.getSource(), false);
		}
		if(targetModel.isEntityIndexed()) {
			entityService.index(association.getTarget(), false);
		}
	}

	@Override
	public void onAfterDelete(Entity entity) {
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model.isEntityIndexed()) {
			try {
				nodeService.deindexNode(entity.getId());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onAfterUpdate(Entity entity) {
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model.isEntityIndexed()) {
			entityService.index(entity.getId(), false);
		}
	}

	@Override
	public void onBeforeAdd(Entity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationAdd(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationDelete(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationUpdate(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeDelete(Entity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeUpdate(Entity oldValue, Entity newValue) {
		// TODO Auto-generated method stub
		
	}

	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public void setSchedulingService(SchedulingService schedulingService) {
		this.schedulingService = schedulingService;
	}
	public SchedulingService getSchedulingService() {
		return schedulingService;
	}
	
}
