package org.heed.openapps.search.indexing.zoie;

import org.heed.openapps.search.indexing.IndexReader;


public class LuceneIndexReader implements IndexReader {
	private org.apache.lucene.index.IndexReader reader;
	
	public LuceneIndexReader(org.apache.lucene.index.IndexReader reader) {
		this.reader = reader;
	}
	
	public org.apache.lucene.index.IndexReader getNativeIndexReader() {
		return reader;
	}
}
