/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.openapps.search.indexing;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.search.indexing.IndexEntity;


public class DefaultEntityIndexer extends EntityIndexerSupport {
			
	
	public IndexEntity index(Entity entity) throws InvalidEntityException {
		return super.index(entity);
	}
	public void deindex(QName qname, Entity entity) throws InvalidEntityException {
		super.deindex(qname, entity);
	}
	/*
	protected void appendFreeText(String text) {
		String cleanText = text.replace(",", "").replace("\"", "").replace(";", "").replace(".", "").replace(":", "");
		String[] parts = cleanText.split(" ");
		for(String part : parts) {
			if(freeText.indexOf(part) == -1) freeText.append(part.toLowerCase()+" ");
		}
	}
	*/
	
}
