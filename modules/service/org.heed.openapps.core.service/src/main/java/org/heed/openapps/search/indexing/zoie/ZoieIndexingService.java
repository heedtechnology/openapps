package org.heed.openapps.search.indexing.zoie;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.MultiReader;
import org.apache.lucene.util.Version;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.search.indexing.EntityIndexer;
import org.heed.openapps.search.indexing.IndexEntity;
import org.heed.openapps.search.indexing.IndexEntityField;
import org.heed.openapps.search.indexing.IndexReader;
import org.heed.openapps.search.indexing.IndexSearcher;
import org.heed.openapps.search.indexing.IndexingService;

import com.browseengine.bobo.api.FacetSpec;
import com.browseengine.bobo.api.FacetSpec.FacetSortSpec;
import com.browseengine.bobo.facets.FacetHandler;
import com.browseengine.bobo.facets.impl.MultiValueFacetHandler;
import com.browseengine.bobo.facets.impl.SimpleFacetHandler;

import proj.zoie.api.DataConsumer.DataEvent;
import proj.zoie.api.indexing.IndexReaderDecorator;
import proj.zoie.impl.indexing.ZoieConfig;
import proj.zoie.impl.indexing.ZoieSystem;

@SuppressWarnings("rawtypes")
public class ZoieIndexingService implements IndexingService {
	private EntityService entityService; 
	private DataDictionaryService dictionaryService;
	private PropertyService propertyService;
	private IndexReaderDecorator decorator;
	private String homeDirectory = "/data/lucene/default";
	
	private int batchSize = 10000;
	private int batchDelay = 15000;
	
	private ZoieSystem zoie;
	private IndexingWorkQueue queue;
	private Map<String, EntityIndexer> indexers = new HashMap<String, EntityIndexer>();
	
	@SuppressWarnings("unchecked")
	public void start() {
		if(decorator == null) {
			SimpleFacetHandler qnameHandler = new SimpleFacetHandler("qname");        
			MultiValueFacetHandler sourceHandler = new MultiValueFacetHandler("source_assoc");        
			MultiValueFacetHandler pathHandler = new MultiValueFacetHandler("path");		
	        List<FacetHandler<?>> handlerList = Arrays.asList(new FacetHandler<?>[]{qnameHandler,sourceHandler,pathHandler});
	                
			Map<String, FacetSpec> facets = new HashMap<String, FacetSpec>();	        
	        FacetSpec qnameSpec = new FacetSpec();
	        qnameSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
	        facets.put("qname", qnameSpec);
	        
	        FacetSpec sourceSpec = new FacetSpec();
	        sourceSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
	        facets.put("source_assoc", sourceSpec);
	        
	        FacetSpec pathSpec = new FacetSpec();
	        pathSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
	        facets.put("path", pathSpec);
	        
	        decorator = new BoboIndexReaderDecorator(handlerList);
		}		
		File dir = new File(propertyService.getPropertyValue("home.dir") + homeDirectory);
		DataIndexableInterpreter interpreter = new DataIndexableInterpreter();
		ZoieConfig config = new ZoieConfig();
		config.setMaxBatchSize(batchSize);
		config.setBatchDelay(batchDelay);
		config.setBatchSize(batchSize);
		//config.setAnalyzer(new LowerCaseWhitespaceAnalyzer());
		config.setAnalyzer(new StandardAnalyzer(Version.LUCENE_36));
		zoie = new ZoieSystem(dir, interpreter, decorator, config);
		zoie.start();
	}
	public void shutdown() {
		zoie.shutdown();
	}
	
	@SuppressWarnings("unchecked")
	public void update(Long entityId, boolean wait) {
		if(wait) {
			ArrayList<DataEvent<IndexEntity>> eventList = new ArrayList<DataEvent<IndexEntity>>();		
    	    try {
    	    	Entity entity = getEntityService().getEntity(entityId);
    	    	DataDictionary dictionary = getDictionaryService().getDataDictionary(entity.getDictionary());
    	    	List<QName> qnames = dictionary.getQNames(entity.getQName());
            	EntityIndexer defaultIndexer = getEntityIndexer("default");
            	IndexEntity data = null;
    			EntityIndexer indexer = getEntityIndexer(entity.getQName().toString());
    			if(indexer != null) {
    				data = indexer.index(entity);				
    			} else {
    				data = defaultIndexer.index(entity);
    			}
    			
    			StringWriter writer = new StringWriter();
    			for(QName qname : qnames) {
    				writer.append(qname.toString()+" ");
    			}
    			if(data == null) data = new IndexEntity();
    			data.getData().put("qnames", new IndexEntityField("qnames", writer.toString().trim(), true));       			
    			data.setId(entity.getId());
    	    	
    	    	//IndexEntity deleteEntity = new IndexEntity(data.getId());
    	    	//deleteEntity.setDelete(true);
    	    	//eventList.add(new DataEvent<IndexEntity>(deleteEntity, "0"));
    	    	eventList.add(new DataEvent<IndexEntity>(data, "1"));
    	    	getZoie().consume(eventList);
    	    } catch(Exception e) {
    	    	e.printStackTrace();
    	    } 
		} else queue.add(entityId);
	}
	@Override
	public void update(List<Long> nodeData) {
		for(Long node : nodeData) {
			queue.add(node);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void remove(Long id) {
		try {
			ArrayList<DataEvent> eventList = new ArrayList<DataEvent>();
			IndexEntity entity = new IndexEntity(id);
			entity.setDelete(true);
			eventList.add(new DataEvent<IndexEntity>(entity, "1"));
			zoie.consume(eventList);
		} catch(Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	@SuppressWarnings("unchecked")
	public IndexReader getIndexReader() throws IOException {
		List<org.apache.lucene.index.IndexReader> indexReaders  = zoie.getIndexReaders();
		org.apache.lucene.index.IndexReader multiReader = new MultiReader(indexReaders.toArray(new org.apache.lucene.index.IndexReader[indexReaders.size()]),false);
		return new LuceneIndexReader(multiReader);
	}
	@SuppressWarnings("unchecked")
	public IndexSearcher getIndexSearcher(String ctx) throws IOException {
		List<IndexReader> indexReaders  = zoie.getIndexReaders();
		org.apache.lucene.index.IndexReader multiReader = new MultiReader(indexReaders.toArray(new org.apache.lucene.index.IndexReader[indexReaders.size()]),false);
		return new LuceneIndexSearcher(new org.apache.lucene.search.IndexSearcher(multiReader));
	}
	/*
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	*/
	
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public ZoieSystem getZoie() {
		return zoie;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}
	public void setBatchDelay(int batchDelay) {
		this.batchDelay = batchDelay;
	}
	public IndexReaderDecorator getDecorator() {
		return decorator;
	}
	@Override
	public EntityIndexer getEntityIndexer(String name) {
		return indexers.get(name);
	}
	public void setIndexers(Map<String, EntityIndexer> indexers) {
		this.indexers = indexers;
	}
	public void setDecorator(IndexReaderDecorator decorator) {
		this.decorator = decorator;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<IndexReader> getIndexReaders() throws IOException {
		List<org.apache.lucene.index.IndexReader> readers = zoie.getIndexReaders();
		List<IndexReader> list = new ArrayList<IndexReader>();
		for(org.apache.lucene.index.IndexReader reader : readers) {
			list.add(new LuceneIndexReader(reader));
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void returnIndexReaders(List<IndexReader> list) {
		List<org.apache.lucene.index.IndexReader> readers = new ArrayList<org.apache.lucene.index.IndexReader>();
		for(IndexReader reader : list) {
			readers.add((org.apache.lucene.index.IndexReader)reader.getNativeIndexReader());
		}
		zoie.returnIndexReaders(readers);
	}
	public String getHomeDirectory() {
		return homeDirectory;
	}
	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}
	public void setQueue(IndexingWorkQueue queue) {
		this.queue = queue;
	}
	
}
