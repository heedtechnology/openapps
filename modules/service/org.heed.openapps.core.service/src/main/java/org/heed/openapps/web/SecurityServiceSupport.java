package org.heed.openapps.web;

import java.util.List;

import org.heed.openapps.Group;
import org.heed.openapps.User;
import org.heed.openapps.data.IDTypeName;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.security.SecurityService;

public class SecurityServiceSupport extends ServiceSupport {
	private SecurityService securityService;
	
	public SecurityServiceSupport(SecurityService securityService) {
		setSecurityService(securityService);
	}
	
	public RestResponse<IDTypeName> search(String query, int startRow, int endRow, boolean sources, boolean targets) {
		RestResponse<IDTypeName> data = new RestResponse<IDTypeName>();
		long startTime = System.currentTimeMillis();
		
		List<Group> groups = securityService.getGroups(query);
		List<User> users = securityService.getUsers(query);
		
		for(Group group : groups) {
			data.getResponse().addData(new IDTypeName(String.valueOf(group.getId()), "group", group.getName()));
		}
		for(User user : users) {
			data.getResponse().addData(new IDTypeName(String.valueOf(user.getId()), "user", user.getUsername()));
		}
		data.getResponse().setTotalRows(groups.size() + users.size());
		data.getResponse().setTime(System.currentTimeMillis() - startTime);
		return data;
	}
	public SecurityService getSecurityService() {
		return securityService;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
}
