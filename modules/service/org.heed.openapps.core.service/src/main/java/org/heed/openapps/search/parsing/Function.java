package org.heed.openapps.search.parsing;

/**
* Created by babar on 1/1/14.
*/
interface Function<ResultType> {

    ResultType apply();
}
