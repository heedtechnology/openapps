package org.heed.openapps.web;

import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.cache.CacheService;
import org.heed.openapps.data.TreeNode;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.reporting.ReportingService;
import org.heed.openapps.scheduling.SchedulingService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.util.XMLUtility;


public abstract class WebserviceSupport {
	protected NodeService nodeService;
	protected EntityService entityService;
	protected DataDictionaryService dictionaryService;
	protected CacheService cacheService;
	protected SchedulingService schedulingService;
	protected ReportingService reportingService;
	protected SecurityService securityService;
	protected PropertyService propertyService;
	protected EntityServiceSupport entityServiceSupport;
	protected NodeServiceSupport nodeServiceSupport;
	protected DictionaryServiceSupport dictionaryServiceSupport;
	protected SchedulingServiceSupport schedulingServiceSupport;
	
	protected TreeNode getTreeNode(String id, String uid, String parent, String type, String namespace, String title, Boolean isFolder) {
		TreeNode node = new TreeNode();
		node.setId(uid);
		node.setUid(uid);
		node.setParent(parent);
		node.setType(type);
		node.setNamespace(namespace);
		node.setTitle(title);
		node.setIsFolder(isFolder.toString());
		if(type != null) node.setIcon("/theme/images/tree_icons/"+type+".png");
		return node;
	}
	
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public CacheService getCacheService() {
		return cacheService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	public SchedulingService getSchedulingService() {
		return schedulingService;
	}
	public void setSchedulingService(SchedulingService schedulingService) {
		this.schedulingService = schedulingService;
	}
	public ReportingService getReportingService() {
		return reportingService;
	}
	public void setReportingService(ReportingService reportingService) {
		this.reportingService = reportingService;
	}
	public NodeService getNodeService() {
		return nodeService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public SecurityService getSecurityService() {
		return securityService;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public EntityServiceSupport getEntityServiceSupport() {
		if(entityServiceSupport == null) {
			entityServiceSupport = new EntityServiceSupport(getEntityService());
		}
		return entityServiceSupport;
	}
	public NodeServiceSupport getNodeServiceSupport() {
		if(nodeServiceSupport == null) {
			nodeServiceSupport = new NodeServiceSupport(getNodeService());
		}
		return nodeServiceSupport;
	}
	public DictionaryServiceSupport getDictionaryServiceSupport() {
		if(dictionaryServiceSupport == null) {
			dictionaryServiceSupport = new DictionaryServiceSupport(getDictionaryService());
		}
		return dictionaryServiceSupport;
	}
	public SchedulingServiceSupport getSchedulingServiceSupport() {
		if(entityServiceSupport == null) {
			schedulingServiceSupport = new SchedulingServiceSupport(getSchedulingService());
		}
		return schedulingServiceSupport;
	}
	
	public static String toXmlData(Object o) {
		if(o == null) return "";
		else return XMLUtility.escape(o.toString());
	}
	protected void prepareResponse(HttpServletResponse response) {
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );		
	}
}
