package org.heed.openapps.web.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class HttpService {
	
	
	protected String httpGet(String targetUrl) {
		URL url;
	    HttpURLConnection connection = null;  
	    try {
	    	//Create connection
	    	url = new URL(targetUrl);
		    connection = (HttpURLConnection)url.openConnection();
		    connection.setRequestMethod("GET");
		    //connection.setRequestProperty("Content-Type",  "application/x-www-form-urlencoded");				
		    //connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");  
					
		    connection.setUseCaches (false);
		    connection.setDoInput(true);
		    connection.setDoOutput(true);
			    
		    //Get Response	
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    String line;
		    StringBuffer response = new StringBuffer(); 
		    while((line = rd.readLine()) != null) {
		    	response.append(line);
		        response.append('\r');
		    }
		    rd.close();
		    return response.toString();

	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return "";
	    } finally {
	    	if(connection != null) {
	    		connection.disconnect(); 
	    	}
	    }
	}
	
	protected String httpPost(String targetUrl, String body) {	
		URL url;
	    HttpURLConnection connection = null;  
	    try {
	    	//Create connection
	    	url = new URL(targetUrl);
		    connection = (HttpURLConnection)url.openConnection();
		    connection.setRequestMethod("POST");
		    //connection.setRequestProperty("Content-Type",  "application/x-www-form-urlencoded");				
		    //connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");  
					
		    connection.setUseCaches (false);
		    connection.setDoInput(true);
		    connection.setDoOutput(true);
	
		    //Send request
		    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		    wr.writeBytes(body);
		    wr.flush ();
		    wr.close ();
	
		    //Get Response	
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    String line;
		    StringBuffer response = new StringBuffer(); 
		    while((line = rd.readLine()) != null) {
		    	response.append(line);
		        response.append('\r');
		    }
		    rd.close();
		    return response.toString();

	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return "";
	    } finally {
	    	if(connection != null) {
	    		connection.disconnect(); 
	    	}
	    }
	}

}
