package org.heed.openapps.search.data;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.entity.DefaultEntityPersistenceListener;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.job.EntityIndexingJob;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.search.SearchService;


public class SearchEntityPersistenceListener extends DefaultEntityPersistenceListener {
	private final static Log log = LogFactory.getLog(SearchEntityPersistenceListener.class);
	protected SearchService searchService;
	
	
	@Override
	public void index(long id) {
		try {
			Entity entity = getEntityService().getEntity(id);
			Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
			if(model != null) {			
				if(model.isEntityIndexed()) {
					log.info("adding entity of type:"+entity.getQName().toString()+" to equity indexing queue.");
					entityService.index(entity.getId(), false);
				}
				if(model.isSearchIndexed()) {
					log.info("adding entity of type:"+entity.getQName().toString()+" to search indexing queue.");
					searchService.update(entity.getId(), false);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public Job index(QName qname) {
		List<Model> models = dictionaryService.getSystemDictionary().getChildModels(qname);
		Model rootModel = dictionaryService.getSystemDictionary().getModel(qname);
		models.add(rootModel);		
		for(Model model : models) {
			try {
				EntityIndexingJob job = new EntityIndexingJob(nodeService, entityService, searchService, qname, model.isEntityIndexed(), model.isSearchIndexed());
				getSchedulingService().run(job);
				return job;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public SearchService getSearchService() {
		return searchService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
}
