package org.heed.openapps.web;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.NodeException;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.util.NumberUtility;

public class NodeServiceSupport {
	private NodeService nodeService;
	
	private Map<Long, List<Long>> cache = new HashMap<Long, List<Long>>();
	//private Format dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	public static final QName OPENAPPS_ENTITIES = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entities");
	
	
	public NodeServiceSupport(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
	public RestResponse<Object> browse(String parent, String mode, int start, int end) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			List<Long> nodes = null;
			if(parent == null || parent.equals("null")) {
				nodes = cache.get(0L);
				if(nodes == null) {
					nodes = new ArrayList<Long>(0);//getNodeService().getAllNodes();
					cache.put(0L, nodes);
				}
			} else {
				Long parentId = NumberUtility.isLong(parent) ? Long.valueOf(parent) : getNodeService().getNodeId(parent);
				if(mode != null && mode.equals("id")) {
					nodes = new ArrayList<Long>();
					if(parentId != null) nodes.add(parentId);
				} else if(mode != null && mode.equals("name")) {
					//getNodeService().
				} else {
					if(parentId != null) {
						nodes = cache.get(parentId);
						if(nodes == null) {
							nodes = getNodeService().getNodes(parentId);
							cache.put(parentId, nodes);
						}
					}
				}
			}
			if(nodes != null && nodes.size() > 0) {
				data.getResponse().setStartRow(start);
				if(nodes.size() >= end) data.getResponse().setEndRow(end);
				else data.getResponse().setEndRow(nodes.size()-1);
				data.getResponse().setTotalRows(nodes.size());
				int index = 0;
				for(Long node : nodes) {
					if(index >= start && index < end) {
						data.getResponse().getData().add(getNodeData(node, false, false));
					}
					index++;
					if(index == end) break;
				}			
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> getNode(long id) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			data.getResponse().getData().add(getNodeData(id, true, true));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public RestResponse<Object> removeNode(long id) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			getNodeService().removeNode(id);
			Map<String,Object> nodeData = new HashMap<String,Object>();
			nodeData.put("id", id);
			data.getResponse().getData().add(nodeData);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	protected Map<String,Object> getNodeData(Long node, boolean sources, boolean targets) {
		Map<String,Object> nodeData = new HashMap<String,Object>();
		try {
			String qname = String.valueOf(getNodeService().getNodeProperty(node, "qname"));
			nodeData.put("id", String.valueOf(node));
			if(qname.equals("{openapps.org_system_1.0}entities")) {
				nodeData.put("name", "Entities");
			} else if(qname.equals("{openapps.org_system_1.0}models")) nodeData.put("name", "Models");
			if(getNodeService().hasNodeProperty(node, "name")) nodeData.put("name", getNodeService().getNodeProperty(node, "name"));
			else if(getNodeService().hasNodeProperty(node, "description")) nodeData.put("name", getNodeService().getNodeProperty(node, "description"));
			else nodeData.put("name", getNodeService().getNodeProperty(node, "qname"));
			if(getNodeService().hasRelationship(node, Direction.OUTGOING)) nodeData.put("children", "true");
			Map<String,Object> nodeProperties = getNodeService().getNodeProperties(node);
			for(String key : nodeProperties.keySet()) {
				nodeData.put(key, String.valueOf(nodeProperties.get(key)));
			}
			if(sources) {
				List<Map<String, Object>> sourceData = new ArrayList<Map<String, Object>>();			
				for(Relationship relationship : getNodeService().getRelationships(node, Direction.OUTGOING)) {
					Map<String, Object> source = new HashMap<String, Object>();
					source.put("id", relationship.getId());
					source.put("start", relationship.getStartNode());
					source.put("end", relationship.getEndNode());
					source.put("type", relationship.getQName());
					Map<String, Object> relationshipProperties = getNodeService().getRelationshipProperties(relationship.getId());
					for(String key : relationshipProperties.keySet()) {
						source.put(key, String.valueOf(relationshipProperties.get(key)));
					}
					sourceData.add(source);
				}
				nodeData.put("outgoing", sourceData);
			}
			if(targets) {
				List<Map<String, Object>> sourceData = new ArrayList<Map<String, Object>>();			
				for(Relationship relationship : getNodeService().getRelationships(node, Direction.INCOMING)) {
					Map<String, Object> source = new HashMap<String, Object>();
					source.put("id", relationship.getId());
					source.put("start", relationship.getStartNode());
					source.put("end", relationship.getEndNode());
					source.put("type", relationship.getQName());
					Map<String, Object> relationshipProperties = getNodeService().getRelationshipProperties(relationship.getId());
					for(String key : relationshipProperties.keySet()) {
						source.put(key, String.valueOf(relationshipProperties.get(key)));
					}
					sourceData.add(source);
				}
				nodeData.put("incoming", sourceData);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 
		return nodeData;
	}
	protected void printNode(StringWriter buff, Long node) throws NodeException {
		Map<String,Object> nodeProperties = getNodeService().getNodeProperties(node);
		for(String propertyName : nodeProperties.keySet()) {
			buff.append("<"+propertyName+">"+nodeProperties.get(propertyName)+"</"+propertyName+">");
		}
	}
	protected Long getEntityNode(QName qname) {
		try {
			Relationship modelsRelation = getNodeService().getSingleRelationship(0, new QName(OPENAPPS_ENTITIES.toString()), Direction.OUTGOING);
			if(modelsRelation != null) {
				for(Relationship relation : getNodeService().getRelationships(modelsRelation.getEndNode(), Direction.OUTGOING)) {
					String relQname = getNodeService().hasNodeProperty(relation.getEndNode(), "qname") ? (String)getNodeService().getNodeProperty(relation.getEndNode(), "qname") : "";
					if(relQname.equals(qname.toString()))
						return relation.getEndNode();				
				}
			}
		} catch(Exception e) {
			//getLoggingService().error(e);
		}
		return null;
	}
	
	public NodeService getNodeService() {
		return nodeService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
}
