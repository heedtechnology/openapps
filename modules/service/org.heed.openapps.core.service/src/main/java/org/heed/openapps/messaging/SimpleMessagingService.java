package org.heed.openapps.messaging;

import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.MimeMessage;

import org.heed.openapps.messaging.MessagingService;
import org.heed.openapps.property.PropertyService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;


public class SimpleMessagingService implements MessagingService {
	private PropertyService propertyService;
	private JavaMailSender mailSender;
	
	
	
	public void sendEmail(String emailAddress, String text, String html, String from, String subject) {
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(emailAddress);
            helper.setFrom(from);
            helper.setSubject(subject);
            if(html != null) helper.setText(text, html);
            else helper.setText(text);
            mailSender.send(message);
        }catch(Exception e){
             e.printStackTrace();
        }
    }
	
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(propertyService.getPropertyValue("mail.host"));
		sender.setPort(propertyService.getPropertyValueAsInt("mail.port"));
		sender.setProtocol(propertyService.getPropertyValue("mail.protocol"));
		sender.setUsername(propertyService.getPropertyValue("mail.username"));
		sender.setPassword(propertyService.getPropertyValue("mail.password"));
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth", "true");
		javaMailProperties.put("mail.smtp.starttls.enable", "true");
		javaMailProperties.put("mail.smtp.quitwait", "false");
		sender.setJavaMailProperties(javaMailProperties);
		mailSender = sender;
		Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);

	}
	
}
