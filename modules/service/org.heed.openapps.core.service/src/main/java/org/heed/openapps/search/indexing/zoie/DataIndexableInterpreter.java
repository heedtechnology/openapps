package org.heed.openapps.search.indexing.zoie;

import org.heed.openapps.search.indexing.IndexEntity;

import proj.zoie.api.indexing.ZoieIndexable;
import proj.zoie.api.indexing.ZoieIndexableInterpreter;


public class DataIndexableInterpreter implements ZoieIndexableInterpreter<IndexEntity> {
    
	
	public ZoieIndexable interpret(IndexEntity src){
        return new DataIndexable(src);
    }
	@Override
	public ZoieIndexable convertAndInterpret(IndexEntity src) {
		return new DataIndexable(src);
	}
}