package org.heed.openapps.web.service;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.heed.openapps.util.IOUtility;


public class ApacheComponentsHttpService {
	private final static Logger log = Logger.getLogger(ApacheComponentsHttpService.class.getName());
	
	protected String httpGet(String url) {
		String result = "";
		HttpGet httpget = new HttpGet(url);
		HttpClient httpClient = new DefaultHttpClient();
		try {        	
			HttpResponse response = httpClient.execute(httpget, new BasicHttpContext());
        	
        	result = IOUtility.convertStreamToString(response.getEntity().getContent());
    		EntityUtils.consume(response.getEntity());
        	 
			log.info("----------------------------------------");
			log.info("request : "+url);
			log.info("response: "+result);
			log.info("----------------------------------------");
			
		} catch(ClientProtocolException e) {
			e.printStackTrace();
		} catch(IOException e2) {
			e2.printStackTrace();
		}
		return result;
	}
	
	protected String httpPost(String url, String body) {	
		String result = "";
		HttpPost httppost = new HttpPost(url);
		HttpClient httpClient = new DefaultHttpClient();
		try {   
			httppost.setEntity(new StringEntity(body));
			
			HttpResponse response = httpClient.execute(httppost, new BasicHttpContext());
        	
        	result = IOUtility.convertStreamToString(response.getEntity().getContent());
    		EntityUtils.consume(response.getEntity());
        	 
			log.info("----------------------------------------");
			log.info("request : "+url);
			log.info("response: "+result);
			log.info("----------------------------------------");
			
		} catch(ClientProtocolException e) {
			e.printStackTrace();
		} catch(IOException e2) {
			e2.printStackTrace();
		}
		return result;
	}

}
