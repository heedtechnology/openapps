package org.heed.openapps.web;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.imaging.Imaging;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.heed.openapps.User;
import org.heed.openapps.content.DigitalObjectService;
import org.heed.openapps.content.FileNode;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.util.FileUtility;
import org.imgscalr.Scalr;



public class ContentServiceSupport extends ServiceSupport {
	private SecurityService securityService;
	private DigitalObjectService digitalObjectService;
	
	
	@SuppressWarnings({ "unchecked" })
	public void importUpload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User user = securityService.getCurrentUser(request);
				
		byte[] payload = null;
		String fileName = null;
		String processor = null;
		String description = null;
		String type = null;
		long nodeId = 0;
				
		// Parse the request
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> items = (List<FileItem>)upload.parseRequest(request);
			for(FileItem item : items) {
				if(!item.isFormField()) {
					payload = item.get();
					String name = item.getName();
				    if (name != null) {
				        fileName = FilenameUtils.getName(name);
				    }
			    } else if(item.getFieldName().equals("processor")) {
			    	processor = item.getString();
			    } else if(item.getFieldName().equals("description")) {
			    	description = item.getString();
			    } else if(item.getFieldName().equals("type")) {
			    	type = item.getString();
			    } else if(item.getFieldName().equals("id")) {
			    	nodeId = Long.valueOf(item.getString());
			    }
			}
		} catch(Exception e) {
			e.printStackTrace();
	    }		
		if(nodeId > 0 && user != null) {
			try {
				boolean isImage = FileUtility.isImage(fileName);
				if(isImage && processor != null && processor.length() > 0 && !processor.equals("undefined")) {
					String[] dimensions = processor.split("_");
					if(dimensions.length == 2) {
						int width = Integer.valueOf(dimensions[0]);
						int height = Integer.valueOf(dimensions[1]);
						
						final Map<String, Object> params = new HashMap<String, Object>();						
						ByteArrayInputStream bais = new ByteArrayInputStream(payload);
						BufferedImage rawImage = Imaging.getBufferedImage(bais, params);
				        
						BufferedImage scaledImage = Scalr.resize(rawImage, Scalr.Method.BALANCED, Scalr.Mode.AUTOMATIC, width, height, Scalr.OP_ANTIALIAS);
						
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write(scaledImage, "png", baos);
						baos.flush();
						payload = baos.toByteArray();
						baos.close();					    
					}
				}
				String name = FilenameUtils.removeExtension(fileName)+".png";
				FileNode node = digitalObjectService.addDigitalObject(user.getXid(), name, description, payload);
				Association association = new Association(ContentModel.FILES, nodeId, Long.valueOf(node.getId()));
				association.addProperty(ContentModel.TYPE, type);
				getEntityService().addAssociation(association);
			} catch(Exception e) {
		    	e.printStackTrace();
		    }
	    }		
		response.getWriter().print(request.getSession().getId());
	}
	public RestResponse<Object> removeDigitalObject(long id) {
		RestResponse<Object> data = new RestResponse<Object>();
		try {
			digitalObjectService.removeDigitalObject(id);
			data.getResponse().addData(id);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public void setDigitalObjectService(DigitalObjectService digitalObjectService) {
		this.digitalObjectService = digitalObjectService;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	
}
