package org.heed.openapps.search;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SortField;
import org.heed.openapps.QName;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.search.indexing.IndexingService;
import org.heed.openapps.search.parsing.QueryParser;
import org.heed.openapps.util.NumberUtility;

import com.browseengine.bobo.api.BoboBrowser;
import com.browseengine.bobo.api.BoboIndexReader;
import com.browseengine.bobo.api.BrowseFacet;
import com.browseengine.bobo.api.BrowseHit;
import com.browseengine.bobo.api.BrowseRequest;
import com.browseengine.bobo.api.BrowseResult;
import com.browseengine.bobo.api.FacetAccessible;
import com.browseengine.bobo.api.FacetSpec;
import com.browseengine.bobo.api.FacetSpec.FacetSortSpec;
import com.browseengine.bobo.api.MultiBoboBrowser;
import com.browseengine.bobo.facets.FacetHandler;
import com.browseengine.bobo.facets.impl.MultiValueFacetHandler;
import com.browseengine.bobo.facets.impl.SimpleFacetHandler;


public class BoboBrowseSearcher implements Searcher {
	private static final Log log = LogFactory.getLog(BoboBrowseSearcher.class);
	protected EntityService entityService;
	private IndexingService indexingService;
	
	private String id;
	private QueryParser parser;
	protected List<SearchPlugin> plugins = new ArrayList<SearchPlugin>();
	protected Map<String, FacetSpec> facets = new HashMap<String, FacetSpec>();
	private Map<String,String> contentTypes;
	
	public BoboBrowseSearcher() {}
	
	public void initialize() {
		Map<String, FacetSpec> facets = new HashMap<String, FacetSpec>();	        
        FacetSpec qnameSpec = new FacetSpec();
        qnameSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
        facets.put("qname", qnameSpec);
        
        FacetSpec sourceSpec = new FacetSpec();
        sourceSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
        facets.put("source_assoc", sourceSpec);
        
        FacetSpec pathSpec = new FacetSpec();
        pathSpec.setOrderBy(FacetSortSpec.OrderHitsDesc);
        facets.put("path", pathSpec);
        
        setFacets(facets);
        /*
        BoboIndexReaderDecorator decorator = new BoboIndexReaderDecorator(handlerList);
        if(indexingService instanceof ZoieIndexingService) {
        	ZoieIndexingService idx = ((ZoieIndexingService)indexingService);
        	idx.setDecorator(decorator);        
        	idx.start();
        }
        */
        contentTypes = getQNameLabels();
	}
	public void reload(Entity entity) {}
	
	@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
	public void search(SearchRequest request, SearchResponse response) {
		long startTime = System.currentTimeMillis();
		
		for(SearchPlugin plugin : plugins) {
			plugin.request(request);
		}
			
		SearchQuery query = parser.parse(request);
		if(query.getValue() == null) {
			log.error("invalid SearchQuery value, problem parsing");
			return;
		}
		
		int size = request.getEndRow() - request.getStartRow();
						
		BrowseRequest br = new BrowseRequest();
	    br.setCount(size);
	    br.setOffset(request.getStartRow());
		br.setQuery((Query)query.getValue());
		br.setFacetSpecs(facets);
		
	    if(request.getSorts().size() > 0) {
			SortField[] fields = new SortField[request.getSorts().size()];
	    	for(int i=0; i < fields.length; i++) {
	    		Sort sort = request.getSorts().get(i);
	    		fields[i] = new SortField(sort.getField(), sort.getType(), sort.isReverse());
	    	}
			br.setSort(fields);
		} else br.setSort(new SortField[]{new SortField("name_e", SortField.STRING, false)});
	        
	    //List<IndexReader> readerList = null;
		//MultiBoboBrowser browser = null;
		BrowseHit[] hits = null;
		MultiBoboBrowser browseService = null;
		List<org.heed.openapps.search.indexing.IndexReader> readerList = null;
		try {
			SimpleFacetHandler qnameHandler = new SimpleFacetHandler("qname");        
			MultiValueFacetHandler sourceHandler = new MultiValueFacetHandler("source_assoc");        
			MultiValueFacetHandler pathHandler = new MultiValueFacetHandler("path");		
	        List<FacetHandler> handlerList = Arrays.asList(new FacetHandler[]{qnameHandler,sourceHandler,pathHandler});
	        
			readerList = indexingService.getIndexReaders();
			List<BoboIndexReader> readers = new ArrayList<BoboIndexReader>();
			for(org.heed.openapps.search.indexing.IndexReader r : readerList) {
				BoboIndexReader boboReader = BoboIndexReader.getInstance((IndexReader)r.getNativeIndexReader(), (Collection)handlerList);
				readers.add(boboReader);
			}
			browseService = new MultiBoboBrowser(BoboBrowser.createBrowsables(readers));	        
			
			/*
			if(browseService == null) {
				SimpleFacetHandler qnameHandler = new SimpleFacetHandler("qname");        
				MultiValueFacetHandler sourceHandler = new MultiValueFacetHandler("source_assoc");        
				MultiValueFacetHandler pathHandler = new MultiValueFacetHandler("path");		
		        List<FacetHandler> handlerList = Arrays.asList(new FacetHandler[]{qnameHandler,sourceHandler,pathHandler});
		        
		        try {
		        	File directory = new File(propertyService.getPropertyValue("home.dir")+"/data/lucene/default");
			        if(directory.exists()) {
			        	Directory idx = FSDirectory.open(directory);
				        IndexReader reader = IndexReader.open(idx);	        
				        BoboIndexReader boboReader = BoboIndexReader.getInstance(reader, (Collection)handlerList);
				        browseService = new BoboBrowser(boboReader);
			        }
		        } catch(Exception e) {
		        	log.error("", e);
		        }
		        
			}
			*/
			BrowseResult result = browseService.browse(br);
			response.setSearchQuery(query);
			hits = result.getHits();
			if(hits != null) {
				for(BrowseHit hit : hits) {
					if(response.getTopScore() == 0.0) response.setTopScore(hit.getScore());
					try {
						Document document = browseService.doc(hit.getDocid());
						String id = document.get("id");
						Entity entity = entityService.getEntity(Long.valueOf(id));
						if(entity != null) {
							SearchResult r = new SearchResult(entity);
							r.setRawScore(NumberUtility.Round(hit.getScore(), 2));
							response.getResults().add(r);
						} else log.error("no entity returned for id:"+id);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				response.setResultSize(result.getNumHits());
				response.setStartRow(request.getStartRow());
				if(request.getEndRow() > response.getResultSize()) 
					response.setEndRow(response.getResultSize());
				else 
					response.setEndRow(request.getEndRow());
				for(SearchPlugin plugin : plugins) {
					plugin.response(request, response);
				}
			}
			
			SearchAttribute qnameAttribute = new SearchAttribute("qname");
			SearchAttribute personAttribute = new SearchAttribute("person");
			SearchAttribute corporateAttribute = new SearchAttribute("corporate");
			SearchAttribute subjAttribute = new SearchAttribute("subject");
			SearchAttribute collectionAttribute = new SearchAttribute("collection");
			if(result != null) {
				for(String key : result.getFacetMap().keySet()) {
					FacetAccessible facets = result.getFacetMap().get(key);
					SearchAttribute attribute = new SearchAttribute(key);
					for(BrowseFacet facet : facets.getFacets()) {
						SearchAttributeValue value = new SearchAttributeValue(facet.getValue(), key+":"+facet.getValue(), facet.getFacetValueHitCount());
						if(key.equals("source_assoc")) {
							try {
								Entity entity = entityService.getEntity(Long.valueOf(facet.getValue()));
								if(entity != null) {
									String name = entity.getName();
									if(name != null) {
										value.setName(name);
										if(entity.getQName().equals(ClassificationModel.SUBJECT)) {
											value.setQuery("subj:"+entity.getId());
											subjAttribute.getValues().add(value);
										} else if(entity.getQName().equals(ClassificationModel.PERSON)) {
											value.setQuery("name:"+entity.getId());
											personAttribute.getValues().add(value);
										} else if(entity.getQName().equals(ClassificationModel.CORPORATION)) {
											value.setQuery("name:"+entity.getId());
											corporateAttribute.getValues().add(value);
										}
									}
								}
							} catch(Exception e) {
								//log.error("no entity found for source reference:"+facet.getValue());
							}
						} else if(key.equals("path")) {
							try {
								Entity entity = entityService.getEntity(Long.valueOf(facet.getValue()));
								if(entity != null && entity.getQName().equals(RepositoryModel.COLLECTION)) {
									String name = entity.getName();
									if(name != null) {
										value.setName(name);
										value.setQuery("path:"+entity.getId());
										collectionAttribute.getValues().add(value);
									}
								}
							} catch(Exception e) {
								
							}
						} else if(key.equals("qname")) {
							QName qname = new QName(value.getName());
							value.setQuery("localName:"+qname.getLocalName());
							qnameAttribute.getValues().add(value);
						}
					}
					if(attribute.getValues().size() > 0) response.getAttributes().add(attribute);
				}				
				if(personAttribute.getValues().size() > 0) response.getAttributes().add(personAttribute);
				if(corporateAttribute.getValues().size() > 0) response.getAttributes().add(corporateAttribute);
				if(subjAttribute.getValues().size() > 0) response.getAttributes().add(subjAttribute);
				if(qnameAttribute.getValues().size() > 0) response.getAttributes().add(qnameAttribute);
				if(collectionAttribute.getValues().size() > 0) response.getAttributes().add(collectionAttribute);
				
				for(SearchAttribute attribute : response.getAttributes()) {
					if(attribute.getName().equals("qname")) {
						attribute.setName("Content Type");
						List<SearchAttributeValue> attributes = new ArrayList<SearchAttributeValue>();
						for(SearchAttributeValue value : attribute.getValues()) {
							String label = contentTypes.get(value.getName());
							if(label != null) {
								value.setName(label);
								attributes.add(value);
							}
						}
						attribute.setValues(attributes);
					}
					else if(attribute.getName().equals("subject")) attribute.setName("Subjects");
					else if(attribute.getName().equals("person")) attribute.setName("Personal Entities");
					else if(attribute.getName().equals("corporate")) attribute.setName("Corporate Entities");
					else if(attribute.getName().equals("collection")) attribute.setName("Collections");
				}
			}
		} catch(Exception e) {
			log.error("", e);
		} finally {			
			try {
				if(browseService != null){
					try {
						browseService.close();
					} catch(IOException e) {
						log.error("", e);
					}
				}
			} finally {
				if(readerList!=null){
					indexingService.returnIndexReaders(readerList);
				}
			}
		}			
		response.setTime(System.currentTimeMillis() - startTime);
	}
	
	protected Map<String,String> getQNameLabels() {
		Map<String,String> labels = new HashMap<String,String>();
		labels.put("openapps.org_repository_1.0_manuscript", "Manuscript");
		labels.put("openapps.org_repository_1.0_correspondence", "Correspondence");
		labels.put("openapps.org_repository_1.0_printed_material", "Printed Material");
		labels.put("openapps.org_repository_1.0_audio", "Audio");
		labels.put("openapps.org_repository_1.0_professional", "Professional Material");
		labels.put("openapps.org_repository_1.0_memorabilia", "Memorabilia");
		labels.put("openapps.org_repository_1.0_journals", "Journals");
		labels.put("openapps.org_repository_1.0_scrapbooks", "Scrapbooks");
		labels.put("openapps.org_repository_1.0_financial", "Financial");
		labels.put("openapps.org_repository_1.0_legal", "Legal Material");
		labels.put("openapps.org_repository_1.0_artwork", "Artwork");
		labels.put("openapps.org_repository_1.0_photographs", "Photographs");
		labels.put("openapps.org_repository_1.0_notebooks", "Notebooks");
		labels.put("openapps.org_repository_1.0_medical", "Medical");
		labels.put("openapps.org_repository_1.0_research", "Research");
		labels.put("openapps.org_repository_1.0_miscellaneous", "Miscellaneous");
		labels.put("openapps.org_repository_1.0_video", "Video");
		return labels;
	}
	
	public void registerSearchPlugin(SearchPlugin plugin) {
		plugins.add(plugin);
	}
	public List<SearchPlugin> getPlugins() {
		return plugins;
	}
	public void setPlugins(List<SearchPlugin> plugins) {
		this.plugins = plugins;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setParser(QueryParser parser) {
		this.parser = parser;
	}
	public void setFacets(Map<String, FacetSpec> facets) {
		this.facets = facets;
	}
	public IndexingService getIndexingService() {
		return indexingService;
	}
	public void setIndexingService(IndexingService indexingService) {
		this.indexingService = indexingService;		
	}	
}
