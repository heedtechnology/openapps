package org.heed.openapps.search.indexing.zoie;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.search.indexing.EntityIndexer;
import org.heed.openapps.search.indexing.IndexEntity;
import org.heed.openapps.search.indexing.IndexEntityField;

import proj.zoie.api.DataConsumer.DataEvent;


public class IndexingWorkQueue {
	private final static Logger log = Logger.getLogger(IndexingWorkQueue.class.getName());
	private ZoieIndexingService indexingService;
	private boolean silent;
	private PoolWorker[] threads;
    private LinkedList<Long> queue;
    private int lastReport = 0;
    

    public void add(Long entity) {
        synchronized(queue) {
        	if(!queue.contains(entity)) {
        		queue.addLast(entity);            	
        	}
        	queue.notify();
        }
    }
    public void add(List<Long> entities) {
        synchronized(queue) {
        	for(Long entity : entities) {
        		if(!queue.contains(entity)) {
        			queue.addLast(entity);            	
        		}
        	}
        	queue.notifyAll();
        }
    }
    public int size() {
    	return queue.size();
    }
    public void setThreadCount(int nThreads) {
    	queue = new LinkedList<Long>();
        threads = new PoolWorker[nThreads];
        for (int i=0; i<nThreads; i++) {
            threads[i] = new PoolWorker();
            threads[i].start();
        }
    }
    private class PoolWorker extends Thread {
        @SuppressWarnings("unchecked")
		public void run() {
        	Long entityId;
            while(true) {
                synchronized(queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException ignored) {}
                    }
                    entityId = queue.removeFirst();
                    int size = queue.size();
                    if(!silent && size % 500 == 0) {
            			if(lastReport != size) {
            				log.info("search indexing queue : "+size+" waiting tasks...");
            				lastReport = size;
            			}
            		}
                }
                ArrayList<DataEvent<IndexEntity>> eventList = new ArrayList<DataEvent<IndexEntity>>();		
        	    try {
        	    	Entity entity = indexingService.getEntityService().getEntity(entityId);
        	    	DataDictionary dictionary = indexingService.getDictionaryService().getDataDictionary(entity.getDictionary());
        	    	List<QName> qnames = dictionary.getQNames(entity.getQName());
                	EntityIndexer defaultIndexer = indexingService.getEntityIndexer("default");
                	IndexEntity data = null;
        			EntityIndexer indexer = indexingService.getEntityIndexer(entity.getQName().toString());
        			if(indexer != null) {
        				data = indexer.index(entity);				
        			} else {
        				data = defaultIndexer.index(entity);
        			}
        			
        			StringWriter writer = new StringWriter();
        			for(QName qname : qnames) {
        				writer.append(qname.toString()+" ");
        			}
        			if(data == null) data = new IndexEntity();
        			data.getData().put("qnames", new IndexEntityField("qnames", writer.toString().trim(), true));       			
        			data.setId(entity.getId());
        	    	
        	    	//IndexEntity deleteEntity = new IndexEntity(data.getId());
        	    	//deleteEntity.setDelete(true);
        	    	//eventList.add(new DataEvent<IndexEntity>(deleteEntity, "0"));
        	    	eventList.add(new DataEvent<IndexEntity>(data, "1"));
        	    	indexingService.getZoie().consume(eventList);
        	    } catch(Exception e) {
        	    	e.printStackTrace();
        	    } 
            }
        }
    }
    public boolean isSilent() {
		return silent;
	}
	public void setSilent(boolean silent) {
		this.silent = silent;
	}
	public void setIndexingService(ZoieIndexingService indexingService) {
		this.indexingService = indexingService;
	}
	
}
