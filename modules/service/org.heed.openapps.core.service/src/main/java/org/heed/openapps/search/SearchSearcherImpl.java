package org.heed.openapps.search;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.search.parsing.QueryParser;
import org.heed.openapps.search.parsing.QueryTokenizer;
import org.heed.openapps.util.NumberUtility;


public class SearchSearcherImpl implements Searcher {
	private static final Log log = LogFactory.getLog(SearchSearcherImpl.class);
	private String id;
	protected EntityService entityService;
	protected DataDictionaryService dictionaryService;
	private PropertyService propertyService;
	private SearchService searchService;
	private QueryParser parser;
	private QueryTokenizer tokenizer;
	protected Dictionary dictionary;
	protected List<SearchPlugin> plugins = new ArrayList<SearchPlugin>();

	
	public SearchSearcherImpl(QueryParser parser, QueryTokenizer tokenizer, Dictionary dictionary) {
		this.parser = parser;
		this.tokenizer = tokenizer;
		this.dictionary = dictionary;
	}
	
	public void initialize() {
		propertyService.getProperty("");
        
	}
	public void reload(Entity entity) {}
	public void search(SearchRequest request, SearchResponse response) {
		long startTime = System.currentTimeMillis();
		SearchQuery query = parser.parse(request);
		if(query.getValue() == null) {
			log.error("invalid SearchQuery value, problem parsing");
			return;
		}
		response.setSearchQuery(query);
		//response.setBits(entityService.getBitSet(request.getQname(), query.getValue()));		
		//QName qname = response.getSearchQuery().getQname() != null ? query.getQname() : request.getQname();
			
		EntityQuery ctx = new EntityQuery(query.getValue());
		
		if(request.getSorts().size() > 0) {
			Sort sort = request.getSorts().get(0);
			if(sort.getField().endsWith("_")) ctx.setSort(new Sort(Sort.LONG, sort.getField(), sort.isReverse()));
			else ctx.setSort(new Sort(Sort.STRING, sort.getField(), sort.isReverse()));
		}
		
		int count = 0;
		//int size = request.getEndRow() - request.getStartRow();
		//int page = request.getStartRow() / size;
		for(SearchPlugin plugin : plugins) {
			plugin.request(request);
		}
		
		ctx.setStartRow(request.getStartRow());
		ctx.setEndRow(request.getEndRow());
		SearchResponse hits = null;
		try {
			hits = searchService.search(request);
					
			if(hits != null) {
				for(SearchResult result : hits.getResults()) {
					if(response.getTopScore() == 0.0) response.setTopScore(result.getRawScore());
					SearchResult r = new SearchResult(result.getEntity());
					r.setRawScore(NumberUtility.Round(result.getRawScore(), 2));
					response.getResults().add(r);
					count++;
				}
				if(request.getEndRow() > count) response.setEndRow(count);
				response.setResultSize(hits.getResultSize());
				response.setStartRow(request.getStartRow());
				response.setEndRow(request.getEndRow());
				for(SearchPlugin plugin : plugins) {
					plugin.response(request, response);
				}
				processScores(response);
			}
		} catch(Exception e) {
			log.error("", e);
		} 			
		/*
		response.setPageNumber(page);
		if(size == 0 || response.getResultSize() < size) response.setPageCount(1);
		else {
			double ratio = (double)response.getResultSize() / size;
			response.setPageCount((int)(Math.ceil(ratio)));
		}
		*/
		response.setTime(System.currentTimeMillis() - startTime);
	}
	protected void processScores(SearchResponse response) {
		if(response.getResults().size() > 0) {
			Float topScore = response.getTopScore();
			for(int i=0; i < response.getResults().size(); i++) {
				float score = response.getResults().get(i).getRawScore();
				double nScore = (double)(score/topScore)*100;
				int normScore = (int)nScore;
				response.getResults().get(i).setNormalizedScore(normScore);
			}
		}
	}
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public void registerSearchPlugin(SearchPlugin plugin) {
		plugins.add(plugin);
	}
	public List<SearchPlugin> getPlugins() {
		return plugins;
	}
	public void setPlugins(List<SearchPlugin> plugins) {
		this.plugins = plugins;
	}
	public Dictionary getDictionary() {
		return dictionary;
	}
	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	public QueryParser getParser() {
		return parser;
	}
	public void setParser(QueryParser parser) {
		this.parser = parser;
	}
	public QueryTokenizer getTokenizer() {
		return tokenizer;
	}
	public void setTokenizer(QueryTokenizer tokenizer) {
		this.tokenizer = tokenizer;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
}
