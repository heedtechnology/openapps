package org.heed.openapps.search.indexing.zoie;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.NumericField;
import org.heed.openapps.search.indexing.IndexEntity;
import org.heed.openapps.search.indexing.IndexEntityField;

import proj.zoie.api.indexing.ZoieIndexable;

public class DataIndexable implements ZoieIndexable {
	private static final Log log = LogFactory.getLog(DataIndexable.class);
    private IndexEntity _data;
    
    public DataIndexable(IndexEntity data) {
        _data = data;
    }

    public long getUID() {
        return _data.getId();
    }

    public IndexingReq[] buildIndexingReqs() {
        Document doc = new Document();
        doc.add(new Field("id", String.valueOf(_data.getId()), Store.YES, Index.NOT_ANALYZED));
        if(_data.getData() != null) {
	        for(String key : _data.getData().keySet()) {
	        	IndexEntityField field = _data.getData().get(key);
	        	if(field.isTokenized()) {
	        		if(field.getValue() instanceof Long)
	        			doc.add(new NumericField(key, Store.YES, true).setLongValue((Long)field.getValue()));
	        		else
	        			doc.add(new Field(key, String.valueOf(field.getValue()).toLowerCase(), Store.YES, Index.ANALYZED));
	        	} else {
	        		if(field.getValue() instanceof Long)
	        			doc.add(new NumericField(key, Store.YES, true).setLongValue((Long)field.getValue()));
	        		else
	        			doc.add(new Field(key, String.valueOf(field.getValue()).toLowerCase(), Store.YES, Index.NOT_ANALYZED));
	        	}
	        }
	        return new IndexingReq[]{new IndexingReq(doc)};
        } else log.error("no IndexingReqs for id:"+_data.getId());
        return new IndexingReq[0];
    }

    public boolean isDeleted() {
        return _data.isDelete();
    }

    public boolean isSkip(){
        return false;
    }

	@Override
	public byte[] getStoreValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isStorable() {
		// TODO Auto-generated method stub
		return false;
	}
}
