package org.heed.openapps.search.indexing.zoie;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LogByteSizeMergePolicy;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.search.indexing.IndexEntity;


public class LuceneIndexingService {
	private static final Log log = LogFactory.getLog(LuceneIndexingService.class);
	private PropertyService propertyService;
	
	protected Map<String, IndexWriter> writers = new HashMap<String, IndexWriter>();
	
	public void update(IndexEntity data) {
		try {			
			Document document = new Document();
			for(String key : data.getData().keySet()) {
				document.add(new Field(key, String.valueOf(data.getData().get(key)), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.NO));
			}
			IndexWriter indexWriter = getIndexWriter("");
			indexWriter.updateDocument(new Term("id", String.valueOf(data.getId())), document);
		} catch(Exception e) {
			log.error("", e);
		}
	}

	public void update(List<IndexEntity> data) {
		for(IndexEntity entity : data) {
			update(entity);
		}
	}

	protected IndexSearcher getIndexSearcher(String ctx) throws IOException {
		IndexSearcher indexSearcher = null;
		IndexWriter indexWriter = getIndexWriter(ctx);
		if(indexWriter != null) {
			IndexReader indexReader = IndexReader.open(indexWriter, true);;
			indexSearcher = new IndexSearcher(indexReader);
		}
		return indexSearcher;
	}
	protected IndexWriter getIndexWriter(String ctx) throws IOException {
		IndexWriter indexWriter = writers.get(ctx);
		if(indexWriter == null) {
			synchronized(writers) {
				Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
				File indexDir = new File(propertyService.getPropertyValue("home.dir")+"/data/lucene/"+ctx);
				Directory directory = FSDirectory.open(indexDir);
				IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
				LogByteSizeMergePolicy mergePolicy = new LogByteSizeMergePolicy();
				mergePolicy.setUseCompoundFile(false);
				config.setMergePolicy(mergePolicy);
				indexWriter = new IndexWriter(directory, config);
				writers.put(ctx, indexWriter);
			}
		}
		return indexWriter;
	}
	/*
	try {			
		IndexSearcher indexSearcher = getIndexSearcher(ctx);
		TopDocs hits = indexSearcher.search((org.apache.lucene.search.Query)query.getQueryOrQueryObject(), 1000);
		NodeIndexHitsImpl result = new NodeIndexHitsImpl();
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
		    Document document = indexSearcher.doc(scoreDoc.doc);
			long nodeId = GetterUtil.getLong(document.get("nodeId"));
			result.getHits().add(nodeId);
			result.getScores().add(scoreDoc.score);
		}
		return result;
	} catch(Exception e) {
		throw new NodeException("", e);
	}
	*/
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
}
