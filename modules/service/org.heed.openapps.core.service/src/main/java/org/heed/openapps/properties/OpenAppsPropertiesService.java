package org.heed.openapps.properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.core.io.Resource;

import org.heed.openapps.property.Property;
import org.heed.openapps.property.PropertyService;


public class OpenAppsPropertiesService implements PropertyService {
	private Resource configLocation;
	private Properties properties;
	
	
	@Override
	public List<String> getGroupNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Property> getProperties(String group) {
		List<Property> properties = new ArrayList<Property>();
		/*
		String[] keys = PropsUtil.getArray(group);
		for(String key : keys) {
			properties.add(getProperty(key));
		}
		*/
		return properties;
	}

	@Override
	public Property getProperty(String name) {
		if(name == null || name.equals("")) return null;
		loadProperties();
		String value = (String)properties.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		if(name == null || name.equals("")) return null;
		loadProperties();
		return (String)properties.get(name);
	}

	@Override
	public int getPropertyValueAsInt(String name) {
		if(name == null || name.equals("")) return 0;
		String value = (String)properties.get(name);
		if(value == null || name.equals("")) return 0;
		return Integer.valueOf(value);
	}

	@Override
	public boolean hasProperty(String name) {
		if(name == null || name.equals("")) return false;
		if(properties.get(name) != null) return true;
		return false;
	}

	@Override
	public void refresh() throws Exception {
		loadProperties();
	}
	
	protected void loadProperties() {
		try {
			if(properties == null) {
				properties = new Properties();
				properties.load(configLocation.getInputStream());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setPropertyFile(Resource configLocation) {
		this.configLocation = configLocation;
	}
		
}
