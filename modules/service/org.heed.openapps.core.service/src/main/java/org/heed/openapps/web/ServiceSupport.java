package org.heed.openapps.web;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.SystemModel;
import org.heed.openapps.data.GridDataItem;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;

public abstract class ServiceSupport {
	private EntityService entityService;
	
	
	protected List<Object> getEntityData(List<Entity> data) {
		List<Object> entities = new ArrayList<Object>();
		for(Entity entity : data) {
			GridDataItem item  = new GridDataItem(entity.getUuid());
			int columnCount = entity.getProperties().size();
			for(int i=0; i < entity.getProperties().size(); i++) {
				if(i == 0) item.setValue0(entity.getProperties().get(i).getValue());
				else if(i == 1) item.setValue1(entity.getProperties().get(i).getValue());
				else if(i == 2) item.setValue2(entity.getProperties().get(i).getValue());
				else if(i == 3) item.setValue3(entity.getProperties().get(i).getValue());
				else if(i == 4) item.setValue4(entity.getProperties().get(i).getValue());
				else if(i == 5) item.setValue5(entity.getProperties().get(i).getValue());
				else if(i == 6) item.setValue6(entity.getProperties().get(i).getValue());
				else if(i == 7) item.setValue7(entity.getProperties().get(i).getValue());
				else if(i == 8) item.setValue8(entity.getProperties().get(i).getValue());
				else if(i == 9) item.setValue9(entity.getProperties().get(i).getValue());
				else if(i == 10) item.setValue10(entity.getProperties().get(i).getValue());
			}
			for(int i=0; i < entity.getSourceAssociations().size(); i++) {
				Association assoc = entity.getSourceAssociations().get(i);
				try {
					Entity target = getEntityService().getEntity(assoc.getTarget());
					String targetName = target.getPropertyValue(SystemModel.NAME);
					if(columnCount+i == 0) item.setValue0(targetName);
					else if(columnCount+i == 1) item.setValue1(targetName);
					else if(columnCount+i == 2) item.setValue2(targetName);
					else if(columnCount+i == 3) item.setValue3(targetName);
					else if(columnCount+i == 4) item.setValue4(targetName);
					else if(columnCount+i == 5) item.setValue5(targetName);
					else if(columnCount+i == 6) item.setValue6(targetName);
					else if(columnCount+i == 7) item.setValue7(targetName);
					else if(columnCount+i == 8) item.setValue8(targetName);
					else if(columnCount+i == 9) item.setValue9(targetName);
					else if(columnCount+i == 10) item.setValue10(targetName);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			for(int i=0; i < entity.getTargetAssociations().size(); i++) {
				Association assoc = entity.getTargetAssociations().get(i);
				try {
					Entity target = getEntityService().getEntity(assoc.getSource());
					String targetName = target.getPropertyValue(SystemModel.NAME);
					if(columnCount+i == 0) item.setValue0(targetName);
					else if(columnCount+i == 1) item.setValue1(targetName);
					else if(columnCount+i == 2) item.setValue2(targetName);
					else if(columnCount+i == 3) item.setValue3(targetName);
					else if(columnCount+i == 4) item.setValue4(targetName);
					else if(columnCount+i == 5) item.setValue5(targetName);
					else if(columnCount+i == 6) item.setValue6(targetName);
					else if(columnCount+i == 7) item.setValue7(targetName);
					else if(columnCount+i == 8) item.setValue8(targetName);
					else if(columnCount+i == 9) item.setValue9(targetName);
					else if(columnCount+i == 10) item.setValue10(targetName);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			entities.add(item);
		}
		return entities;
	}
	
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
}
