package org.heed.openapps.search.indexing.job;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.DictionaryModel;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.scheduling.ExecutionContext;
import org.heed.openapps.scheduling.JobSupport;
import org.heed.openapps.search.indexing.zoie.IndexingWorkQueue;
import org.heed.openapps.search.SearchService;


public class EntityIndexingJob extends JobSupport {
	private static final long serialVersionUID = -2401605979260351566L;
	private static final Log log = LogFactory.getLog(EntityIndexingJob.class);
	protected NodeService nodeService;
	protected SearchService searchService;
	protected EntityService entityService;
	protected DataDictionaryService dictionaryService;
	protected QName qname;
	protected boolean silent;
	protected IndexingWorkQueue indexingQueue;
	
	public static final QName OPENAPPS_ENTITIES = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entities");
	
	
	public EntityIndexingJob(NodeService nodeService, EntityService entityService, DataDictionaryService dictionaryService, SearchService searchService, QName qname, boolean silent) {
		this.searchService = searchService;
		this.entityService = entityService;
		this.dictionaryService = dictionaryService;
		this.nodeService = nodeService;
		this.qname = qname;
		this.silent = silent;
		setLastMessage("preparing to index entities");
		//indexingQueue = new IndexingWorkQueue(searchService, entityService, dictionaryService, 5, silent);
	}
	
	@Override
	public void execute(ExecutionContext context) {
		super.execute(context);
		setLastMessage("starting entity indexing job");
		try {						
			List<Long> ids = new ArrayList<Long>();
			int count = 0;
			List<Model> models = new ArrayList<Model>();
			EntityQuery query = new EntityQuery(DictionaryModel.DICTIONARY);
			EntityResultSet results = entityService.search(query);
			for(Entity dictionaryEntity : results.getResults()) {
				DataDictionary dictionary = dictionaryService.getDataDictionary(dictionaryEntity.getId());
				List<Model> list = dictionary.getAllModels();
				for(Model model : list) {
					if(qname == null) {
						models.add(model);
					} else if(qname.equals(dictionaryEntity.getQName())) {
						models.addAll(list);
						models.addAll(dictionary.getChildModels(qname));
					}
				}
			}
			if(models != null) {
				for(Model model : models) {
					List<Long> nodes = nodeService.getNodes(model.getQName());
					ids.addAll(nodes);					
				}
				Collections.sort(ids);
				setLastMessage("indexing "+ids.size()+" entities");
				//indexingQueue.add(ids);
				count += ids.size();
				//while(indexingQueue.size() > 0) {
					//Thread.sleep(30000);
				//}
				log.info("indexing "+ids.size()+" entities in "+getFormattedElapsedTime());
			}
			setLastMessage("finished entity indexing job ("+count+")");
			setComplete(true);
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	protected void indexEntity(Entity entity) throws InvalidEntityException {
		if(entity.getQName() == null) return;
		try {
			//indexingQueue.add(entity.getId());			
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	protected Long getEntityNode(QName qname) {
		try {
			Relationship modelsRelation = nodeService.getSingleRelationship(0, OPENAPPS_ENTITIES, Direction.OUTGOING);
			if(modelsRelation != null) {
				for(Relationship relation : nodeService.getRelationships(modelsRelation.getEndNode(), Direction.OUTGOING)) {
					String relQname = nodeService.hasNodeProperty(relation.getEndNode(), "qname") ? (String)nodeService.getNodeProperty(relation.getEndNode(), "qname") : "";
					if(relQname.equals(qname.toString()))
						return relation.getEndNode();				
				}
			}
		} catch(Exception e) {
			log.error("", e);
		}
		return null;
	}	
}
