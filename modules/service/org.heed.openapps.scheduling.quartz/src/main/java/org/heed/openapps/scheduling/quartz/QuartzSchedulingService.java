package org.heed.openapps.scheduling.service;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.heed.openapps.cache.TimedCache;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.scheduling.Period;
import org.heed.openapps.scheduling.SchedulingException;
import org.heed.openapps.scheduling.SchedulingService;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


public class QuartzSchedulingService implements SchedulingService {
	private Scheduler scheduler;
	private QuartzJobListener quartzJobListener = new QuartzJobListener();
	private TimedCache<String,Job> jobCache = new TimedCache<String,Job>(60);
		
	final static public String GROUP_NAME = "openapps";
	

	@SuppressWarnings("unchecked")
	public void initialize() {
		try {
			StdSchedulerFactory factory = new StdSchedulerFactory();
			factory.initialize(new FileInputStream(new File("configuration/scheduler.properties")));
            scheduler = factory.getScheduler();
            scheduler.getListenerManager().addJobListener(quartzJobListener);
            scheduler.start();
            //run(new HeartBeatJob(), 1, Period.MINUTE);
        } catch (Exception se) {
            se.printStackTrace();
        }
	}
	public void shutdown() {
		try {
			scheduler.shutdown();
		} catch (Exception se) {
            se.printStackTrace();
        }
	}
	
	@Override
	public void run(Job job) {
		try {
			JobDataMap data = new JobDataMap();
			data.put("openapps_job", job);
			JobDetail detail = JobBuilder.newJob(QuartzSchedulingJob.class).usingJobData(data).build();			
			SimpleTrigger trigger = (SimpleTrigger)TriggerBuilder.newTrigger().startNow().build();
			scheduler.scheduleJob(detail, trigger);
			jobCache.put(job.getUid(), job);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void runAndWait(Job job) {
		try {
			JobDataMap data = new JobDataMap();
			data.put("openapps_job", job);
			JobDetail detail = JobBuilder.newJob(QuartzSchedulingJob.class).usingJobData(data).build();			
			SimpleTrigger trigger = (SimpleTrigger)TriggerBuilder.newTrigger().startAt(new Date()).build();
			scheduler.scheduleJob(detail, trigger);
			System.out.print("starting job");
			while(!job.isComplete()) {
				System.out.print(".");
			}
			System.out.print("job ended");
			jobCache.put(job.getUid(), job);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void run(Job job, int count, Period period) {
		try {
			JobDataMap data = new JobDataMap();
			data.put("openapps_job", job);
			JobDetail detail = JobBuilder.newJob(QuartzSchedulingJob.class).usingJobData(data).build();			
			SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule();
			if(period.equals(Period.MINUTE)) scheduleBuilder.withIntervalInMinutes(count).repeatForever();
			else if(period.equals(Period.HOUR)) scheduleBuilder.withIntervalInHours(count).repeatForever();
			else if(period.equals(Period.DAY)) scheduleBuilder.withIntervalInHours(count * 24).repeatForever();
			else if(period.equals(Period.WEEK)) scheduleBuilder.withIntervalInHours(count * 24 * 7).repeatForever();
			
			Trigger trigger = TriggerBuilder.newTrigger().withSchedule(scheduleBuilder).build();
			scheduler.scheduleJob(detail, trigger);
			jobCache.put(job.getUid(), job);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public Job getJob(String jobid) {
		Job job = jobCache.get(jobid);
		return job;
	}
	@Override
	public List<Job> getJobs(String node) {
		List<Job> list = new ArrayList<Job>();
		for(String key : jobCache.keySet()) {
			Job job = jobCache.get(key);
			if(job != null && job.getId() != null) {
				if(job.getId().toString().equals(node)) list.add(job);
			}
		}
		return list;
	}
	@Override
	public List<Job> getJobs() {
		List<Job> list = new ArrayList<Job>();
		for(String key : jobCache.keySet()) {
			Job job = jobCache.get(key);
			if(job.getId() != null) {
				list.add(job);
			}
		}
		return list;
	}
	@Override
	public void updateJob(String id, String message) {
		Job job = getJob(id);
		job.setLastMessage(message);
	}
	
	@Override
	public void interruptJob(String id) throws SchedulingException {
		try {
			JobKey key = new JobKey(id, GROUP_NAME);
			scheduler.interrupt(key);
		} catch(Exception e) {
			throw new SchedulingException(e);
		}
	}
	@Override
	public boolean startJob(String id) throws SchedulingException {
		try {
			if (isJobRunning(id)) {
				return false;
			}
			// start the job
			JobKey key = new JobKey(id, GROUP_NAME);
			scheduler.triggerJob(key);

			return true;
		} catch(Exception e) {
			throw new SchedulingException(e);
		}
	}
	@Override
	public void removeJob(String id) throws SchedulingException {
		try {
			JobKey key = new JobKey(id, GROUP_NAME);
			scheduler.deleteJob(key);
		} catch(Exception e) {
			throw new SchedulingException(e);
		}
	}
	/*
	@Override
	public List<JobInfo> getJobs() throws SchedulingException {
		ArrayList list = new ArrayList();
		try {
		List currJobs = scheduler.getCurrentlyExecutingJobs();

		// make an array for faster evaluation
		JobExecutionContext[] jobContexts = new JobExecutionContext[currJobs.size()];
		currJobs.toArray(jobContexts);

		List<String> groupNames = scheduler.getJobGroupNames();
		if (groupNames != null) {
			for (int i = 0; i < groupNames.size(); i++) {
				String groupName = groupNames.get(i);
				for(String group: scheduler.getJobGroupNames()) {
				    for(JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
				        System.out.println("Found job identified by: " + jobKey);
				        String jobName = jobKey.getName();
				        
				        // get info on the job
						JobDetail jobDetail = scheduler.getJobDetail(jobKey);
						JobDataMap dataMap = jobDetail.getJobDataMap();
						//XMLConfig params = new XMLConfig();//(XMLConfig) dataMap.get("jobparams");

						Trigger trigger = getCurrentTrigger(jobName, groupName);
						String schedule = getSchedule(trigger);
						String nextFireTime = getNextFireTime(trigger);
						String lastMessage;
						int errorCount = 0;
						int warningCount = 0;
						boolean isRunning;

						// returns a job instance if executing, else null
						Job pipelineJob = getCurrentJob(jobContexts, jobDetail);
						
						if (pipelineJob == null) {
							// job not executing
							//String jobClassStr = params.getProperty("jobclass");
							//Class jobClass = Class.forName(jobClassStr);
							//pipelineJob = (Connector) jobClass.newInstance();

							// get the status when the job last ended
							lastMessage = dataMap.getString("lastmessage");
							String errs = dataMap.getString("errorcount");
							if (errs != null) {
								errorCount = Integer.parseInt(errs);
							}
							String warnings = dataMap.getString("warningcount");
							if (warnings != null) {
								warningCount = Integer.parseInt(warnings);
							}
							
							isRunning = false;

						} else {
							// job executing
							lastMessage = pipelineJob.getLastMessage();
							errorCount = pipelineJob.getErrorCount();
							warningCount = pipelineJob.getWarningCount();
							isRunning = true;
						}

						//String pageName = pipelineJob.getPageName();
						//String logLink = pipelineJob.getLogLink();

						// populate jobinfo for display
						JobInfo jobInfo = new JobInfo();
						jobInfo.setJobName(jobName);
						jobInfo.setSchedule(schedule);
						jobInfo.setNextFireTime(nextFireTime);
						//jobInfo.setNode(pipelineJob.getNode());
						//jobInfo.setPageName(pageName);
						//jobInfo.setLogLink(logLink);
						jobInfo.setIsRunning(isRunning);
						jobInfo.setLastMessage(lastMessage);
						jobInfo.setErrorCount(errorCount);
						jobInfo.setWarningCount(warningCount);

						list.add(jobInfo);
						
				    }
				}
			}
		}
		} catch(Exception e) {
			throw new SchedulingException(e);
		}
		Collections.sort(list);		
		return list;
	}
	*/
	public boolean isJobRunning(String jobName) throws SchedulerException {
		List list = scheduler.getCurrentlyExecutingJobs();
		for (int i = 0; i < list.size(); i++) {
			JobDetail jobDetail = ((JobExecutionContext) list.get(i)).getJobDetail();
			String name = jobDetail.getKey().getName();
			String group = jobDetail.getKey().getGroup();
			if (name != null && name.equals(jobName) && group != null && group.equals(GROUP_NAME)) {
				return true;
			}
		}
		return false;
	}
	public void addJobListener(JobListener listener) {
		quartzJobListener.addJobListener(listener);
	}
	
	public void removeJobListener(JobListener listener) {
		quartzJobListener.removeJobListener(listener);
	}
	
	/**
	 * Roll through the currently executing jobs and see if any match the specified
	 * jobDetail. If so, return the context. This holds a reference to the
	 * actually job instance.
	 */
	private Job getCurrentJob(JobExecutionContext[] jobContexts, JobDetail jobDetail) {
		String fullName = jobDetail.getKey().getName();
		for (int i = 0; i < jobContexts.length; i++) {
			JobExecutionContext context = jobContexts[i];
			// Trigger trigger = context.getTrigger(); // this is avail if we need it
			JobDetail currDetail = context.getJobDetail();
			String currName = currDetail.getKey().getName();
			if (currName.equals(fullName)) {
				QuartzSchedulingJob wrapper = (QuartzSchedulingJob) context.getJobInstance();
				Job job = wrapper.getJob();
				return job;
			}
		}
		return null;
	}
	/**
	 * Return the trigger that was defined in the config for this job, or if none,
	 * return null.
	 */
	private Trigger getCurrentTrigger(String jobName, String groupName) throws SchedulerException {
		/* if there are no triggers, then the job is on-demand
		 * if there are two, then the first is the one defined in the config,
		 *   and the second is an adhoc trigger.
		 * if there is only one, then it's tricker. If the trigger has no description,
		 * then assume it's an adhoc trigger, else it's the defined one.
		 */
		JobKey key = new JobKey(jobName, groupName);
		List<Trigger> triggers = (List<Trigger>)scheduler.getTriggersOfJob(key);
		if (triggers == null || triggers.size() == 0) {
			return null;

		} else if (triggers.size() == 1) {
			String desc = triggers.get(0).getDescription();
			if (desc == null) {
				return null;
			} else {
				return triggers.get(0);
			}

		} else {
			return triggers.get(0);
		}

	}
	/**
	 * Get the next time the trigger will fire.
	 */
	private String getNextFireTime(Trigger trigger) {

		if (trigger == null) {
			return null;
		}

		Date date = trigger.getNextFireTime();
		if (date == null)
			return null;

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");

		return df.format(date);
	}

	/**
	 * Get a human-readable description of the schedule for display.
	 */
	private String getSchedule(Trigger trigger) {
		if (trigger == null) {
			return "On Demand";
		} else {
			return trigger.getDescription();
		}
	}

}