package org.heed.openapps.scheduling.quartz;

import java.util.Date;

import org.heed.openapps.scheduling.ExecutionContext;
import org.quartz.JobExecutionContext;


public class QuartzExecutionContext implements ExecutionContext {
	private JobExecutionContext ctx;
	
	
	public QuartzExecutionContext(JobExecutionContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public boolean isRecovering() {
		return ctx.isRecovering();
	}
	@Override
	public int getRefireCount() {
		return ctx.getRefireCount();
	}
	@Override
	public Date getFireTime() {
		return ctx.getFireTime();
	}
	@Override
	public Date getScheduledFireTime() {
		return ctx.getScheduledFireTime();
	}
	@Override
	public Date getPreviousFireTime() {
		return ctx.getPreviousFireTime();
	}
	@Override
	public Date getNextFireTime() {
		return ctx.getNextFireTime();
	}
	@Override
	public String getFireInstanceId() {
		return ctx.getFireInstanceId();
	}
	@Override
	public long getJobRunTime() {
		return ctx.getJobRunTime();
	}
	@Override
	public void put(Object key, Object value) {
		ctx.put(key, value);
	}
	@Override
	public Object get(Object key) {
		return ctx.get(key);
	}
	
}
