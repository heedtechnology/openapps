package org.heed.openapps.scheduling.quartz;

import org.heed.openapps.scheduling.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class QuartzSchedulingJob implements org.quartz.Job {
	private Job job;
	
	public QuartzSchedulingJob() {
		
	}
	
	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		job = (Job)ctx.getJobDetail().getJobDataMap().get("openapps_job");
		job.execute(new QuartzExecutionContext(ctx));
	}

	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	
}
