package org.heed.openapps.scheduling.quartz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

class QuartzJobListener implements org.quartz.JobListener {

	private List<JobListener> listeners = Collections.synchronizedList(new ArrayList());
	
	public String getName() {
		return "quartzJobListener";
	}

	public void removeJobListener(JobListener listener) {
		// the remove method below is based on .equals(). The default
		// implementation of equals() in Object, though, is ==.
		// so this works.
		listeners.remove(listener);
	}

	public void addJobListener(JobListener listener) {
		listeners.add(listener);
	}
	
	@SuppressWarnings("unused")
	public void jobExecutionVetoed(JobExecutionContext context) {
	}

	public void jobToBeExecuted(JobExecutionContext context) {
		/*
		Connector job = ((PipelineJobWrapper)context.getJobInstance()).getConnector();
		String jobName = context.getJobDetail().getKey().getName();
		for (JobListener jl: listeners) {
			jl.jobToBeExecuted(jobName, job);
		}
		*/
	}

	@SuppressWarnings("unused")
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException exception) {
		// exception ignored for now
		/*
		Connector job = ((PipelineJobWrapper)context.getJobInstance()).getConnector();
		String jobName = context.getJobDetail().getKey().getName();
		for (JobListener jl: listeners) {
			jl.jobToBeExecuted(jobName, job);
		}
		*/
	}
	
}
